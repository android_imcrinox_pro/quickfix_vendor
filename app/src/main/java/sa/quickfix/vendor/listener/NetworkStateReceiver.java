package sa.quickfix.vendor.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;

public class NetworkStateReceiver extends BroadcastReceiver {

    public static String TAG = "NetworkStateReceiver";

    GlobalVariables globalVariables = new GlobalVariables();
    GlobalFunctions globalFunctions = new GlobalFunctions();
    AppController appController = new AppController();


    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d("app","Network connectivity change");
        if(intent.getExtras() != null)
        {
            NetworkInfo ni = (NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
            if(ni != null && ni.getState() == NetworkInfo.State.CONNECTED)
            {
                globalFunctions.setNetworkConnectionStatus(context,true);
                Log.i("app", "Network " + ni.getTypeName() + " connected");
            }
        }

        if(intent.getExtras().getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE))
        {
            globalFunctions.setNetworkConnectionStatus(context,false);
            Log.d("app", "There's no network connectivity");
        }

    }
}
