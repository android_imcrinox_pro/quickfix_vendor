package sa.quickfix.vendor.view;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;


public class AppTextView_TitleArabic extends AppCompatTextView {
    public AppTextView_TitleArabic(Context context) {
        super(context);
        init();
    }

    public AppTextView_TitleArabic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AppTextView_TitleArabic(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "app_arabic_bold.ttf");
        setTypeface(tf);
    }
}
