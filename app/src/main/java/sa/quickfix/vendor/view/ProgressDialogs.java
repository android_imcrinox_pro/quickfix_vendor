package sa.quickfix.vendor.view;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.widget.ImageView;

import sa.quickfix.vendor.R;

public class ProgressDialogs {

    private static Context context;
    private Dialog dialog;
    private boolean isProgresDialogs;
    private ProgressDialog pDialog;
    private ImageView loading_gif_image;

    public ProgressDialogs() {
    }

    public void show_dialog(Context cont, boolean b,String message) {
        try {
            this.context = cont;
            this.isProgresDialogs = b;
           /* if (isProgresDialogs) {
                dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.progress_bar_layout);
                dialog.setCancelable(true);
                dialog.show();
            }*/

            pDialog = new ProgressDialog(context);
                pDialog.setMessage(message);

//            pDialog.setMessage("loading...");
            pDialog.setCancelable(true);
            pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            pDialog.setIndeterminate(true);
            pDialog.setCanceledOnTouchOutside(false);
            if (isProgresDialogs) {
                pDialog.show();
                pDialog.setContentView(R.layout.progress_bar_layout);
                loading_gif_image = (ImageView) pDialog.findViewById(R.id.loading_gif_image);

//                Glide.with(context).load(R.drawable.progress_bar).diskCacheStrategy(DiskCacheStrategy.RESOURCE).into(loading_gif_image);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void dismiss_dialog() {
        try {
            if (pDialog != null) {
                pDialog.cancel();
                pDialog.dismiss();
                pDialog = null;
            } else {
                pDialog.cancel();
                pDialog.dismiss();
                pDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setCancelable(boolean isCancelable) {
        try {
            if (pDialog != null) {
                pDialog.setCancelable(isCancelable);
                pDialog.setCanceledOnTouchOutside(isCancelable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setMessage(String message) {
        try {
            if (pDialog != null) {
                pDialog.setMessage(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isShowing() {
        boolean status = false;
        if (isProgresDialogs) {
            status = true;
        } else {
            status = false;
        }
        return status;
    }
}
