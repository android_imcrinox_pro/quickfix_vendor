package sa.quickfix.vendor.view;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;


public class AppEditTextViewArabic extends AppCompatEditText {
    public AppEditTextViewArabic(Context context) {
        super(context);
        init();
    }

    public AppEditTextViewArabic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AppEditTextViewArabic(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "app_arabic_regular.ttf");
        setTypeface(tf);
    }


}
