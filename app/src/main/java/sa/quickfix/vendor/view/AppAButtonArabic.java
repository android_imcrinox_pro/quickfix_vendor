package sa.quickfix.vendor.view;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatButton;
import android.util.AttributeSet;


public class AppAButtonArabic extends AppCompatButton {
    public AppAButtonArabic(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public AppAButtonArabic(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public AppAButtonArabic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.get("app_arabic_regular.ttf", context);
        setTypeface(customFont);
    }

}
