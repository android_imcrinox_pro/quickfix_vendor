package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ScientificDocumentsListModel implements Serializable {

    private final String TAG = "ScientificDocListModel";

    private final String RESPONSE = "scientific_documents";

    List<ScientificDocumentModel> scientificDocumentList = new ArrayList<ScientificDocumentModel>();

    public ScientificDocumentsListModel(){}

    public List<ScientificDocumentModel> getScientificDocumentList() {
        return scientificDocumentList;
    }

    public void setScientificDocumentList(List<ScientificDocumentModel> scientificDocumentList) {
        this.scientificDocumentList = scientificDocumentList;
    }

    public List<String> getImages(){
        List<String> list = new ArrayList<String>();
        for(int i =0 ;i<this.scientificDocumentList.size(); i++){
            list.add(scientificDocumentList.get(i).getImage());
        }
        return list;
    }

    public boolean toObject(String jsonObjectString){
        try{
            JSONObject json = new JSONObject(jsonObjectString);
            JSONArray array = json.getJSONArray(RESPONSE);
            List<ScientificDocumentModel> list = new ArrayList<ScientificDocumentModel>();
            for (int i=0;i<array.length();i++){
                JSONObject jsonObject = array.getJSONObject(i);
                ScientificDocumentModel model = new ScientificDocumentModel();
                model.toObject(jsonObject.toString());
                list.add(model);
            }
            this.scientificDocumentList = list;
            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    public boolean toObject(JSONArray jsonArray){
        try{
            List<ScientificDocumentModel> list = new ArrayList<ScientificDocumentModel>();
            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ScientificDocumentModel model = new ScientificDocumentModel();
                model.toObject(jsonObject.toString());
                list.add(model);
            }
            this.scientificDocumentList = list;
            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            List<ScientificDocumentModel> list = this.scientificDocumentList;
            for(int i=0;i<list.size();i++){
                jsonArray.put(new JSONObject(list.get(i).toString()));
            }
            jsonMain.put(RESPONSE, jsonArray);
            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }

    public String toString(boolean isArray){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            List<ScientificDocumentModel> list = this.scientificDocumentList;
            for(int i=0;i<list.size();i++){
                jsonArray.put(new JSONObject(list.get(i).toString()));
            }
            if(!isArray){
                jsonMain.put(RESPONSE, jsonArray);
                returnString = jsonMain.toString();
            }else{
                returnString = jsonArray.toString();
            }

        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
