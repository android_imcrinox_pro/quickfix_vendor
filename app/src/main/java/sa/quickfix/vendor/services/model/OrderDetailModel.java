package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class OrderDetailModel implements Serializable {
    private final String TAG = "OrderDetailModel";
    private final String
            ORDER_DETAIL_ID            = "order_detail_id",
            ICON                       = "icon",
            SERVICE_TITLE              = "service_title",
            CATEGORY_ID                = "category_id",
            QUANTITY                   = "quantity",
            UNIT_PRICE                 = "unit_price",
            VAT                        = "vat",
            DISCOUNT                   = "discount",
            SPARE_PARTS_PRICE          = "spare_part_price",
            SPARE_PARTS_VAT            = "spare_part_vat",
            TOTAL                      = "total",
            IS_INSPECTION              = "is_inspection",
            PRICE_PER_HOUR             = "price_per_hour",
            INSPECTION_TIME            = "inspection_time",
            INSPECTION_PRICE           = "inspection_price",
            STATUS                     = "status",
            STATUS_TITLE               = "status_title",
            SUBSCRIPTION_QUANTITY      = "subscription_quantity";

    private String
            orderDetailId              = null,
            icon                       = null,
            serviceTitle               = null,
            categoryId                 = null,
            quantity                   = null,
            unitPrice                  = null,
            vat                        = null,
            discount                   = null,
            sparePartPrice             = null,
            sparePartVat               = null,
            total                      = null,
            isInspection              = null,
            pricePerHour              = null,
            inspectionTime            = null,
            inspectionPrice           = null,
            status                    = null,
            statusTitle               = null,
            subscriptionQuantity       = null;

    public OrderDetailModel(){}

    public String getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(String orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getSparePartPrice() {
        return sparePartPrice;
    }

    public void setSparePartPrice(String sparePartPrice) {
        this.sparePartPrice = sparePartPrice;
    }

    public String getSparePartVat() {
        return sparePartVat;
    }

    public void setSparePartVat(String sparePartVat) {
        this.sparePartVat = sparePartVat;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSubscriptionQuantity() {
        return subscriptionQuantity;
    }

    public void setSubscriptionQuantity(String subscriptionQuantity) {
        this.subscriptionQuantity = subscriptionQuantity;
    }

    public String getIsInspection() {
        return isInspection;
    }

    public void setIsInspection(String isInspection) {
        this.isInspection = isInspection;
    }

    public String getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(String pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public String getInspectionTime() {
        return inspectionTime;
    }

    public void setInspectionTime(String inspectionTime) {
        this.inspectionTime = inspectionTime;
    }

    public String getInspectionPrice() {
        return inspectionPrice;
    }

    public void setInspectionPrice(String inspectionPrice) {
        this.inspectionPrice = inspectionPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public boolean toObject(String jsonObject){
        try{

            JSONObject json = new JSONObject(jsonObject);

            if(json.has(ORDER_DETAIL_ID)){this.orderDetailId = json.getString(ORDER_DETAIL_ID);}
            if(json.has(ICON)){this.icon = json.getString(ICON);}
            if(json.has(SERVICE_TITLE)){this.serviceTitle = json.getString(SERVICE_TITLE);}
            if(json.has(CATEGORY_ID)){this.categoryId = json.getString(CATEGORY_ID);}
            if(json.has(QUANTITY)){this.quantity = json.getString(QUANTITY);}
            if(json.has(UNIT_PRICE)){this.unitPrice = json.getString(UNIT_PRICE);}
            if(json.has(VAT)){this.vat = json.getString(VAT);}
            if(json.has(DISCOUNT)){this.discount = json.getString(DISCOUNT);}
            if(json.has(SPARE_PARTS_PRICE)){this.sparePartPrice = json.getString(SPARE_PARTS_PRICE);}
            if(json.has(SPARE_PARTS_VAT)){this.sparePartVat = json.getString(SPARE_PARTS_VAT);}
            if(json.has(SUBSCRIPTION_QUANTITY)){this.subscriptionQuantity = json.getString(SUBSCRIPTION_QUANTITY);}
            if(json.has(STATUS)){this.status = json.getString(STATUS);}
            if(json.has(STATUS_TITLE)){this.statusTitle = json.getString(STATUS_TITLE);}
            if(json.has(IS_INSPECTION)){this.isInspection = json.getString(IS_INSPECTION);}
            if(json.has(PRICE_PER_HOUR)){this.pricePerHour = json.getString(PRICE_PER_HOUR);}
            if(json.has(INSPECTION_TIME)){this.inspectionTime = json.getString(INSPECTION_TIME);}
            if(json.has(INSPECTION_PRICE)){this.inspectionPrice = json.getString(INSPECTION_PRICE);}

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;

        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ORDER_DETAIL_ID, orderDetailId);
            jsonMain.put(ICON, icon);
            jsonMain.put(SERVICE_TITLE, serviceTitle);
            jsonMain.put(CATEGORY_ID, categoryId);
            jsonMain.put(QUANTITY, quantity);
            jsonMain.put(UNIT_PRICE, unitPrice);
            jsonMain.put(VAT, vat);
            jsonMain.put(DISCOUNT, discount);
            jsonMain.put(SPARE_PARTS_PRICE, sparePartPrice);
            jsonMain.put(SPARE_PARTS_VAT, sparePartVat);
            jsonMain.put(SUBSCRIPTION_QUANTITY, subscriptionQuantity);
            jsonMain.put(IS_INSPECTION, isInspection);
            jsonMain.put(PRICE_PER_HOUR, pricePerHour);
            jsonMain.put(INSPECTION_TIME, inspectionTime);
            jsonMain.put(INSPECTION_PRICE, inspectionPrice);
            jsonMain.put(STATUS, status);
            jsonMain.put(STATUS_TITLE, statusTitle);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}


