package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class CompanyRegisterModel implements Serializable {
    private final String TAG = "CompanyRegisterModel";

    private final String

            REGISTRATION_NUMBER   = "registration_number",
            LOGO                  = "logo",
            COMPANY_NAME          = "company_name",
            MANAGER_NAME          = "manager_name",
            MANAGER_MOBILE        = "manager_mobile",
            MANAGER_EMAIL         = "manager_email",
            PASSWORD              = "password",
            BANK_NAME             = "bank_name",
            BRANCH                = "branch",
            ACCOUNT_NUMBER        = "account_number",
            SWIFT_CODE            = "swift_code",
            ACCOUNT_HOLDER_NAME   = "account_holder_name",
            IBAN                  = "iban",
            ABOUT_COMPANY         = "about_company",
            LICENCE_IMAGES        = "license_images",
            SCIENTIFIC_DOCUMENTS  = "scientific_documents",
            ADDRESSES             = "addresses",
            UUID                  = "uuid",
            SYSTEM_INFO           = "system_info",
            DEVICE_TYPE           = "device_type";

    String
            password              = null,
            bankName              = null,
            branch                = null,
            accountNumber         = null,
            swiftCode             = null,
            accountHolderName     = null,
            iban                  = null,
            uid                   = null,
            deviceType            = "1",
            systemInfo            = null,
            registrationNumber    = null,
            logo                  = null,
            companyName           = null,
            managerName           = null,
            managerMobile         = null,
            managerEmail          = null,
            aboutCompany          = null;

    ScientificDocumentsListModel
            scientificDocumentsList    = null;

    LicenceImagesListModel
            licenceImagesList    = null;

    RegistrationAddressListModel
            addressList    = null;

    public CompanyRegisterModel(){}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getSystemInfo() {
        return systemInfo;
    }

    public void setSystemInfo(String systemInfo) {
        this.systemInfo = systemInfo;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerMobile() {
        return managerMobile;
    }

    public void setManagerMobile(String managerMobile) {
        this.managerMobile = managerMobile;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getAboutCompany() {
        return aboutCompany;
    }

    public void setAboutCompany(String aboutCompany) {
        this.aboutCompany = aboutCompany;
    }

    public ScientificDocumentsListModel getScientificDocumentsList() {
        return scientificDocumentsList;
    }

    public void setScientificDocumentsList(ScientificDocumentsListModel scientificDocumentsList) {
        this.scientificDocumentsList = scientificDocumentsList;
    }

    public LicenceImagesListModel getLicenceImagesList() {
        return licenceImagesList;
    }

    public void setLicenceImagesList(LicenceImagesListModel licenceImagesList) {
        this.licenceImagesList = licenceImagesList;
    }

    public RegistrationAddressListModel getAddressList() {
        return addressList;
    }

    public void setAddressList(RegistrationAddressListModel addressList) {
        this.addressList = addressList;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            if(json.has(REGISTRATION_NUMBER)){this.registrationNumber = json.getString(REGISTRATION_NUMBER);}
            if(json.has(PASSWORD)){this.password = json.getString(PASSWORD);}
            if(json.has(BANK_NAME)){this.bankName = json.getString(BANK_NAME);}
            if(json.has(BRANCH)){this.branch = json.getString(BRANCH);}
            if(json.has(ACCOUNT_NUMBER)){this.accountNumber = json.getString(ACCOUNT_NUMBER);}
            if(json.has(SWIFT_CODE)){this.swiftCode = json.getString(SWIFT_CODE);}
            if(json.has(ACCOUNT_HOLDER_NAME)){this.accountHolderName = json.getString(ACCOUNT_HOLDER_NAME);}
            if(json.has(IBAN)){this.iban = json.getString(IBAN);}
            if(json.has(UUID)){this.uid = json.getString(UUID);}
            if(json.has(DEVICE_TYPE)){this.deviceType = json.getString(DEVICE_TYPE);}
            if(json.has(SYSTEM_INFO)){this.systemInfo = json.getString(SYSTEM_INFO);}
            if(json.has(LOGO)){this.logo = json.getString(LOGO);}
            if(json.has(COMPANY_NAME)){this.companyName = json.getString(COMPANY_NAME);}
            if(json.has(MANAGER_NAME)){this.managerName = json.getString(MANAGER_NAME);}
            if(json.has(MANAGER_MOBILE)){this.managerMobile = json.getString(MANAGER_MOBILE);}
            if(json.has(MANAGER_EMAIL)){this.managerEmail = json.getString(MANAGER_EMAIL);}
            if(json.has(ABOUT_COMPANY)){this.aboutCompany = json.getString(ABOUT_COMPANY);}

            if(json.has(LICENCE_IMAGES)){
                try{
                    JSONArray jsonArray = json.getJSONArray(LICENCE_IMAGES);
                    LicenceImagesListModel tempModel = new LicenceImagesListModel();
                    if(tempModel.toObject(jsonArray)) this.licenceImagesList = tempModel;
                    else this.licenceImagesList = null;
                }catch (Exception exx){
                    this.licenceImagesList = null;
                }
            }

            if(json.has(SCIENTIFIC_DOCUMENTS)){
                try{
                    JSONArray jsonArray = json.getJSONArray(SCIENTIFIC_DOCUMENTS);
                    ScientificDocumentsListModel tempModel = new ScientificDocumentsListModel();
                    if(tempModel.toObject(jsonArray)) this.scientificDocumentsList = tempModel;
                    else this.scientificDocumentsList = null;
                }catch (Exception exx){
                    this.scientificDocumentsList = null;
                }
            }

            if(json.has(ADDRESSES)){
                try{
                    JSONArray jsonArray = json.getJSONArray(ADDRESSES);
                    RegistrationAddressListModel tempModel = new RegistrationAddressListModel();
                    if(tempModel.toObject(jsonArray)) this.addressList = tempModel;
                    else this.addressList = null;
                }catch (Exception exx){
                    this.addressList = null;
                }
            }

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(REGISTRATION_NUMBER, this.registrationNumber);
            jsonMain.put(PASSWORD, this.password);
            jsonMain.put(BANK_NAME, this.bankName);
            jsonMain.put(BRANCH, this.branch);
            jsonMain.put(ACCOUNT_NUMBER, this.accountNumber);
            jsonMain.put(SWIFT_CODE, this.swiftCode);
            jsonMain.put(ACCOUNT_HOLDER_NAME, this.accountHolderName);
            jsonMain.put(IBAN, this.iban);
            jsonMain.put(UUID, this.uid);
            jsonMain.put(DEVICE_TYPE, this.deviceType);
            jsonMain.put(SYSTEM_INFO, this.systemInfo);
            jsonMain.put(LOGO, this.logo);
            jsonMain.put(COMPANY_NAME, this.companyName);
            jsonMain.put(MANAGER_NAME, this.managerName);
            jsonMain.put(MANAGER_MOBILE, this.managerMobile);
            jsonMain.put(MANAGER_EMAIL, this.managerEmail);
            jsonMain.put(ABOUT_COMPANY, this.aboutCompany);
            jsonMain.put(LICENCE_IMAGES, licenceImagesList != null ? new JSONArray(this.licenceImagesList.toString(true)) : null);
            jsonMain.put(SCIENTIFIC_DOCUMENTS, scientificDocumentsList != null ? new JSONArray(this.scientificDocumentsList.toString(true)) : null);
            jsonMain.put(ADDRESSES, addressList != null ? new JSONArray(this.addressList.toString(true)) : null);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
