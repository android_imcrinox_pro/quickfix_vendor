package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class ProfileModel implements Serializable {
    private final String TAG = "ProfileModel";
    private final String
            ID                    = "id",
            COMPANY_NAME          = "company_name",
            TECHNICIAN_NUMBER     = "technician_number",
            SUPPORT_NUMBER        = "support_number",
            FIRST_NAME            = "first_name",
            LAST_NAME             = "last_name",
            COUNTRY_CODE          = "country_code",
            PHONE                 = "mobile_number",
            EMAIL                 = "email_id",
            GENDER                = "gender",
            EXPERIENCE            = "experience",
            EXPERIENCE_M          = "experience_m",
            SKILLS                = "skills",
            ID_NUMBER             = "id_number",
            ID_IMAGE_STATUS       = "id_img_status",
            SERVICES_APPROVED     = "services_approved",
            STATUS                = "status",
            IS_AVAIL              = "is_available",
            CERTIFICATE_STATUS    = "cer_img_status",
            PROFILE_IMAGE         = "profile_image",
            SPOKEN_LANGUAGES      = "spoken_languages",
            SPOKEN_LANGUAGE_NAMES = "spoken_language_names",
            ADDRESS               = "address",
            LATITUDE              = "latitude",
            LONGITUDE             = "longitude",
            CITY_ID               = "city_id",
            CITY_NAME             = "city_name",
            COUNTRY_ID            = "country_id",
            NATIONALITY           = "nationality",
            BANK_NAME             = "bank_name",
            BRANCH                = "branch",
            ACCOUNT_NUMBER        = "account_number",
            SWIFT_CODE            = "swift_code",
            ACCOUNT_HOLDER_NAME   = "account_holder_name",
            IBAN                  = "iban",
            SCHEDULE              = "schedule",
            WALLET                = "wallet",
            RATING                = "rating",
            CERTIFICATES_LIST     = "cirtificates",
            ID_IMAGES             = "id_images",
            TOKEN                 = "token",
            NOTIFICATION_COUNT    = "notification_count",
            MESSAGE               = "message";

    String
            id                    = null,
            companyName           = null,
            supportNumber         = null,
            technicianNumber      = null,
            firstName             = null,
            lastName              = null,
            countryCode           = null,
            mobileNumber          = null,
            is_available          = null,
            emailId               = null,
            gender                = null,
            experience            = null,
            experienceM           = null,
            skills                = null,
            idNumber              = null,
            idImageStatus         = null,
            servicesApproved      = null,
            status                = null,
            certificateImgStatus  = null,
            profileImage          = null,
            spokenLanguages       = null,
            spokenLanguageNames   = null,
            address               = null,
            latitude              = null,
            longitude             = null,
            cityId                = null,
            cityName              = null,
            countryId             = null,
            nationality           = null,
            bankName              = null,
            branch                = null,
            accountNumber         = null,
            swiftCode             = null,
            accountHolderName     = null,
            iban                  = null,
            schedule              = null,
            wallet                = null,
            rating                = null,
            token                 = null,
            notificationCount     = null,
            message               = null;

    CertificatesListModel
            certificatesList    = null;

    IdImagesListModel
            idImageList         = null;

    public ProfileModel(){}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTechnicianNumber() {
        return technicianNumber;
    }

    public void setTechnicianNumber(String technicianNumber) {
        this.technicianNumber = technicianNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getExperienceM() {
        return experienceM;
    }

    public void setExperienceM(String experienceM) {
        this.experienceM = experienceM;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getIdImageStatus() {
        return idImageStatus;
    }

    public void setIdImageStatus(String idImageStatus) {
        this.idImageStatus = idImageStatus;
    }

    public String getServicesApproved() {
        return servicesApproved;
    }

    public void setServicesApproved(String servicesApproved) {
        this.servicesApproved = servicesApproved;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCertificateImgStatus() {
        return certificateImgStatus;
    }

    public void setCertificateImgStatus(String certificateImgStatus) {
        this.certificateImgStatus = certificateImgStatus;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getSpokenLanguages() {
        return spokenLanguages;
    }

    public void setSpokenLanguages(String spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }

    public String getSpokenLanguageNames() {
        return spokenLanguageNames;
    }

    public void setSpokenLanguageNames(String spokenLanguageNames) {
        this.spokenLanguageNames = spokenLanguageNames;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public CertificatesListModel getCertificatesList() {
        return certificatesList;
    }

    public void setCertificatesList(CertificatesListModel certificatesList) {
        this.certificatesList = certificatesList;
    }

    public IdImagesListModel getIdImageList() {
        return idImageList;
    }

    public void setIdImageList(IdImagesListModel idImageList) {
        this.idImageList = idImageList;
    }

    public String getIs_available() {
        return is_available;
    }

    public void setIs_available(String is_available) {
        this.is_available = is_available;
    }

    public String getSupportNumber() {
        return supportNumber;
    }

    public void setSupportNumber(String supportNumber) {
        this.supportNumber = supportNumber;
    }

    public String getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(String notificationCount) {
        this.notificationCount = notificationCount;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            if(json.has(ID)){id = json.getString(ID);}
            if(json.has(COMPANY_NAME)){companyName = json.getString(COMPANY_NAME);}
            if(json.has(TECHNICIAN_NUMBER)){this.technicianNumber = json.getString(TECHNICIAN_NUMBER);}
            if(json.has(FIRST_NAME)){this.firstName = json.getString(FIRST_NAME);}
            if(json.has(LAST_NAME)){this.lastName = json.getString(LAST_NAME);}
            if(json.has(COUNTRY_CODE)){this.countryCode = json.getString(COUNTRY_CODE);}
            if(json.has(PHONE)){this.mobileNumber = json.getString(PHONE);}
            if(json.has(EMAIL)){this.emailId = json.getString(EMAIL);}
            if(json.has(GENDER)){this.gender = json.getString(GENDER);}
            if(json.has(EXPERIENCE)){this.experience = json.getString(EXPERIENCE);}
            if(json.has(EXPERIENCE_M)){this.experienceM = json.getString(EXPERIENCE_M);}
            if(json.has(SKILLS)){this.skills = json.getString(SKILLS);}
            if(json.has(ID_NUMBER)){this.idNumber = json.getString(ID_NUMBER);}
            if(json.has(ID_IMAGE_STATUS)){this.idImageStatus = json.getString(ID_IMAGE_STATUS);}
            if(json.has(SERVICES_APPROVED)){this.servicesApproved = json.getString(SERVICES_APPROVED);}
            if(json.has(STATUS)){this.status = json.getString(STATUS);}
            if(json.has(CERTIFICATE_STATUS)){this.certificateImgStatus = json.getString(CERTIFICATE_STATUS);}
            if(json.has(PROFILE_IMAGE)){this.profileImage = json.getString(PROFILE_IMAGE);}
            if(json.has(SPOKEN_LANGUAGES)){this.spokenLanguages = json.getString(SPOKEN_LANGUAGES);}
            if(json.has(SPOKEN_LANGUAGE_NAMES)){this.spokenLanguageNames = json.getString(SPOKEN_LANGUAGE_NAMES);}
            if(json.has(ADDRESS)){this.address = json.getString(ADDRESS);}
            if(json.has(LATITUDE)){this.latitude = json.getString(LATITUDE);}
            if(json.has(LONGITUDE)){this.longitude = json.getString(LONGITUDE);}
            if(json.has(CITY_ID)){this.cityId = json.getString(CITY_ID);}
            if(json.has(CITY_NAME)){this.cityName = json.getString(CITY_NAME);}
            if(json.has(COUNTRY_ID)){this.countryId = json.getString(COUNTRY_ID);}
            if(json.has(NATIONALITY)){this.nationality = json.getString(NATIONALITY);}
            if(json.has(BANK_NAME)){this.bankName = json.getString(BANK_NAME);}
            if(json.has(BRANCH)){this.branch = json.getString(BRANCH);}
            if(json.has(ACCOUNT_NUMBER)){this.accountNumber = json.getString(ACCOUNT_NUMBER);}
            if(json.has(SWIFT_CODE)){this.swiftCode = json.getString(SWIFT_CODE);}
            if(json.has(ACCOUNT_HOLDER_NAME)){this.accountHolderName = json.getString(ACCOUNT_HOLDER_NAME);}
            if(json.has(IBAN)){this.iban = json.getString(IBAN);}
            if(json.has(SCHEDULE)){this.schedule = json.getString(SCHEDULE);}
            if(json.has(WALLET)){this.wallet = json.getString(WALLET);}
            if(json.has(RATING)){this.rating = json.getString(RATING);}
            if(json.has(TOKEN)){this.token = json.getString(TOKEN);}
            if(json.has(IS_AVAIL)){this.is_available = json.getString(IS_AVAIL);}
            if(json.has(MESSAGE)){this.message = json.getString(MESSAGE);}
            if(json.has(SUPPORT_NUMBER)){this.supportNumber = json.getString(SUPPORT_NUMBER);}
            if(json.has(NOTIFICATION_COUNT)){this.notificationCount = json.getString(NOTIFICATION_COUNT);}

            if(json.has(CERTIFICATES_LIST)){
                try{
                    JSONArray jsonArray = json.getJSONArray(CERTIFICATES_LIST);
                    CertificatesListModel tempModel = new CertificatesListModel();
                    if(tempModel.toObject(jsonArray)) this.certificatesList = tempModel;
                    else this.certificatesList = null;
                }catch (Exception exx){
                    this.certificatesList = null;
                }
            }

            if(json.has(ID_IMAGES)){
                try{
                    JSONArray jsonArray = json.getJSONArray(ID_IMAGES);
                    IdImagesListModel tempModel = new IdImagesListModel();
                    if(tempModel.toObject(jsonArray)) this.idImageList = tempModel;
                    else this.idImageList = null;
                }catch (Exception exx){
                    this.idImageList = null;
                }
            }

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ID, id);
            jsonMain.put(COMPANY_NAME, companyName);
            jsonMain.put(TECHNICIAN_NUMBER, technicianNumber);
            jsonMain.put(FIRST_NAME, firstName);
            jsonMain.put(LAST_NAME, lastName);
            jsonMain.put(COUNTRY_CODE, countryCode);
            jsonMain.put(PHONE, mobileNumber);
            jsonMain.put(EMAIL, emailId);
            jsonMain.put(GENDER, gender);
            jsonMain.put(EXPERIENCE, experience);
            jsonMain.put(EXPERIENCE_M, experienceM);
            jsonMain.put(SKILLS, skills);
            jsonMain.put(ID_NUMBER, idNumber);
            jsonMain.put(ID_IMAGE_STATUS, idImageStatus);
            jsonMain.put(SERVICES_APPROVED, servicesApproved);
            jsonMain.put(STATUS, status);
            jsonMain.put(CERTIFICATE_STATUS, certificateImgStatus);
            jsonMain.put(PROFILE_IMAGE, profileImage);
            jsonMain.put(SPOKEN_LANGUAGES, spokenLanguages);
            jsonMain.put(SPOKEN_LANGUAGE_NAMES, spokenLanguageNames);
            jsonMain.put(ADDRESS, address);
            jsonMain.put(LATITUDE, latitude);
            jsonMain.put(LONGITUDE, longitude);
            jsonMain.put(CITY_ID, cityId);
            jsonMain.put(CITY_NAME, cityName);
            jsonMain.put(COUNTRY_ID, countryId);
            jsonMain.put(NATIONALITY, nationality);
            jsonMain.put(BANK_NAME, bankName);
            jsonMain.put(BRANCH, branch);
            jsonMain.put(ACCOUNT_NUMBER, accountNumber);
            jsonMain.put(SWIFT_CODE, swiftCode);
            jsonMain.put(ACCOUNT_HOLDER_NAME, accountHolderName);
            jsonMain.put(IBAN, iban);
            jsonMain.put(SCHEDULE, schedule);
            jsonMain.put(WALLET, wallet);
            jsonMain.put(RATING, rating);
            jsonMain.put(TOKEN, token);
            jsonMain.put(IS_AVAIL, is_available);
            jsonMain.put(MESSAGE, message);
            jsonMain.put(SUPPORT_NUMBER, supportNumber);
            jsonMain.put(NOTIFICATION_COUNT, notificationCount);
            jsonMain.put(CERTIFICATES_LIST, certificatesList != null ? new JSONArray(this.certificatesList.toString(true)) : null);
            jsonMain.put(ID_IMAGES, idImageList != null ? new JSONArray(this.idImageList.toString(true)) : null);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
