package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class AddInspectionModel implements Serializable {

    private final String TAG = "AddInspectionModel";
    private final String
            ORDER_DETAIL_ID      = "order_detail_id",
            TIME                 = "inspection_time";

    String
            orderDetailId       = null,
            time                = null;

    public AddInspectionModel(){}

    public String getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(String orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);

            if(json.has(ORDER_DETAIL_ID)){this.orderDetailId = json.getString(ORDER_DETAIL_ID);}
            if(json.has(TIME)){this.time = json.getString(TIME);}

            return true;
        }catch(Exception ex){Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ORDER_DETAIL_ID, orderDetailId);
            jsonMain.put(TIME, time);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}

