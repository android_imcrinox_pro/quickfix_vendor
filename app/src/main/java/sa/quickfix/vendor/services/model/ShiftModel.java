package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class ShiftModel implements Serializable {
    private final String TAG = "ShiftModel";
    private final String
            ID                  = "shift_id",
            START_TIME          = "start_time",
            END_TIME            = "end_time",
            SUNDAY              = "sunday",
            MONDAY              = "monday",
            TUEDAY              = "tuesDay",
            WEDDAY              = "wedDay",
            THUDAY              = "thuDay",
            FRIDAY              = "friDay",
            SATDAY              = "satDay",
            NO_DAYS             = "no_days",
            DAYS                = "days";


    String
            id                    = null,
            startTime             = null,
            endTime               = null,
            sunday                = null,
            monday                = null,
            tuesDay               = null,
            wedDay                = null,
            thuDay                = null,
            friDay                = null,
            satDay                = null,
            days                  = null;

    public ShiftModel(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getTuesDay() {
        return tuesDay;
    }

    public void setTuesDay(String tuesDay) {
        this.tuesDay = tuesDay;
    }

    public String getWedDay() {
        return wedDay;
    }

    public void setWedDay(String wedDay) {
        this.wedDay = wedDay;
    }

    public String getThuDay() {
        return thuDay;
    }

    public void setThuDay(String thuDay) {
        this.thuDay = thuDay;
    }

    public String getFriDay() {
        return friDay;
    }

    public void setFriDay(String friDay) {
        this.friDay = friDay;
    }

    public String getSatDay() {
        return satDay;
    }

    public void setSatDay(String satDay) {
        this.satDay = satDay;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            if(json.has(ID))this.id = json.getString(ID);
            if(json.has(START_TIME))this.startTime = json.getString(START_TIME);
            if(json.has(END_TIME))this.endTime = json.getString(END_TIME);
            if(json.has(DAYS))this.days = json.getString(DAYS);
            if(json.has(SUNDAY))this.sunday = json.getString(SUNDAY);
            if(json.has(MONDAY))this.monday = json.getString(MONDAY);
            if(json.has(TUEDAY))this.tuesDay = json.getString(TUEDAY);
            if(json.has(WEDDAY))this.wedDay = json.getString(WEDDAY);
            if(json.has(THUDAY))this.thuDay = json.getString(THUDAY);
            if(json.has(FRIDAY))this.friDay = json.getString(FRIDAY);
            if(json.has(SATDAY))this.satDay = json.getString(SATDAY);

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);
            return false;}
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ID, id);
            jsonMain.put(START_TIME, startTime);
            jsonMain.put(END_TIME, endTime);
            jsonMain.put(DAYS, days);
            jsonMain.put(SUNDAY, sunday);
            jsonMain.put(MONDAY, monday);
            jsonMain.put(TUEDAY, tuesDay);
            jsonMain.put(WEDDAY, wedDay);
            jsonMain.put(THUDAY, thuDay);
            jsonMain.put(FRIDAY, friDay);
            jsonMain.put(SATDAY, satDay);
            returnString = jsonMain.toString();
        }
        catch (Exception ex){
            Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
