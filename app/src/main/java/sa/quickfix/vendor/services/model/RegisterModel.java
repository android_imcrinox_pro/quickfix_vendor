package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class RegisterModel implements Serializable {
    private final String TAG = "RegisterModel";

    private final String
            COUNTRY_CODE          = "country_code",
            PHONE                 = "mobile_number",
            EMAIL                 = "email_id",
            PASSWORD              = "password",
            FIRST_NAME            = "first_name",
            LAST_NAME             = "last_name",
            UUID                  = "uuid",
            SYSTEM_INFO           = "system_info",
            DEVICE_TYPE           = "device_type",
            LOGIN_TOKEN           = "login_token";
    String
            countryCode           = null,
            mobileNumber          = null,
            emailId               = null,
            password              = null,
            firstName             = null,
            lastName              = null,
            uid                   = null,
            deviceType            = "1",
            systemInfo            = null,
            loginToken            = null;

    public RegisterModel(){}

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getSystemInfo() {
        return systemInfo;
    }

    public void setSystemInfo(String systemInfo) {
        this.systemInfo = systemInfo;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            if(json.has(COUNTRY_CODE)){this.countryCode = json.getString(COUNTRY_CODE);}
            if(json.has(PHONE)){this.mobileNumber = json.getString(PHONE);}
            if(json.has(EMAIL)){this.emailId = json.getString(EMAIL);}
            if(json.has(PASSWORD)){this.password = json.getString(PASSWORD);}
            if(json.has(FIRST_NAME)){this.firstName = json.getString(FIRST_NAME);}
            if(json.has(LAST_NAME)){this.lastName = json.getString(LAST_NAME);}
            if(json.has(UUID)){this.uid = json.getString(UUID);}
            if(json.has(DEVICE_TYPE)){this.deviceType = json.getString(DEVICE_TYPE);}
            if(json.has(SYSTEM_INFO)){this.systemInfo = json.getString(SYSTEM_INFO);}
            if(json.has(LOGIN_TOKEN)){this.loginToken = json.getString(LOGIN_TOKEN);}

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(COUNTRY_CODE, countryCode);
            jsonMain.put(PHONE, mobileNumber);
            jsonMain.put(EMAIL, emailId);
            jsonMain.put(PASSWORD, password);
            jsonMain.put(FIRST_NAME, firstName);
            jsonMain.put(LAST_NAME, lastName);
            jsonMain.put(UUID, uid);
            jsonMain.put(DEVICE_TYPE, deviceType);
            jsonMain.put(SYSTEM_INFO, systemInfo);
            jsonMain.put(LOGIN_TOKEN, loginToken);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
