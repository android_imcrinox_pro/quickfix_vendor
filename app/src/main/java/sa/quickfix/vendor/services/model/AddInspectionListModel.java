package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AddInspectionListModel implements Serializable {

    private final String TAG = "AddInspectionListModel";

    private final String RESPONSE = "inspection";

    List<AddInspectionModel> addInspectionList = new ArrayList<AddInspectionModel>();

    public AddInspectionListModel(){}

    public List<AddInspectionModel> getAddInspectionList() {
        return addInspectionList;
    }

    public void setAddInspectionList(List<AddInspectionModel> addInspectionList) {
        this.addInspectionList = addInspectionList;
    }

    public boolean toObject(String jsonObjectString){
        try{
            JSONObject json = new JSONObject(jsonObjectString);
            JSONArray array = json.getJSONArray(RESPONSE);
            List<AddInspectionModel> list = new ArrayList<AddInspectionModel>();
            for (int i=0;i<array.length();i++){
                JSONObject jsonObject = array.getJSONObject(i);
                AddInspectionModel model = new AddInspectionModel();
                model.toObject(jsonObject.toString());
                list.add(model);
            }
            this.addInspectionList = list;
            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    public boolean toObject(JSONArray jsonArray){
        try{
            List<AddInspectionModel> list = new ArrayList<AddInspectionModel>();
            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                AddInspectionModel model = new AddInspectionModel();
                model.toObject(jsonObject.toString());
                list.add(model);
            }
            this.addInspectionList = list;
            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            List<AddInspectionModel> list = this.addInspectionList;
            for(int i=0;i<list.size();i++){
                jsonArray.put(new JSONObject(list.get(i).toString()));
            }
            jsonMain.put(RESPONSE, jsonArray);
            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }

    public String toString(boolean isArray){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            List<AddInspectionModel> list = this.addInspectionList;
            for(int i=0;i<list.size();i++){
                jsonArray.put(new JSONObject(list.get(i).toString()));
            }
            if(!isArray){
                jsonMain.put(RESPONSE, jsonArray);
                returnString = jsonMain.toString();
            }else{
                returnString = jsonArray.toString();
            }

        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
