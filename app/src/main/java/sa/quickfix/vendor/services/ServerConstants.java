package sa.quickfix.vendor.services;

public class ServerConstants {

     //TEST SERVER...
     //public static final String SERVER_URL = "http://35.154.4.154/quickfix/technician/";

    //LIVE SERVER
//   public static final String SERVER_URL = "https://api.quickfix.sa/technician/";
    public static final String SERVER_URL             = "https://staging-api.quickfix.sa/technician/";

    public static final String SERVER_API_VERSION = "v0_1";
    public static final String BASE_URL = SERVER_URL + SERVER_API_VERSION + "/api/";

    public static final String URL_GuestUserCreation    = BASE_URL + "guest_user_creation.php";
    public static final String URL_SendOTP              = BASE_URL + "send_otp.php";
    public static final String URL_VerifyOTP            = BASE_URL + "verify_otp.php";
    public static final String URL_LoginVerifyOTP            = BASE_URL + "login_verify_otp.php";
    public static final String URL_LoginUser            = BASE_URL + "login.php";
    public static final String URL_LocationCheck        = BASE_URL + "check_city_by_latlong.php";
    public static final String URL_LocationUpdate       = BASE_URL + "update_lat_long.php";
    public static final String URL_ForgotMyPassword     = BASE_URL + "change_password.php";
    public static final String URL_RegisterUser         = BASE_URL + "registration.php";
    public static final String URL_RegisterAsCompany    = BASE_URL + "company_registration.php";
    public static final String URL_RegisterAsIndividual = BASE_URL + "registration.php";
    public static final String URL_UpdateCategory       = BASE_URL + "update_category.php";
    public static final String URL_OrderList            = BASE_URL + "order_list.php";
    public static final String URL_NotificationList     = BASE_URL + "notification_list.php";
    public static final String URL_ClearNotification    = BASE_URL + "clear_notification.php";
    public static final String URL_ClearNotificationAll = BASE_URL + "clear_notification_all.php";
    public static final String URL_GetSeenNotificationAll= BASE_URL+"seen_notification_all.php";
    public static final String URL_OrderDetails         = BASE_URL + "order_detail.php";
    public static final String URL_OrderIsAvail         = BASE_URL + "is_available.php";
    public static final String URL_CreateSchedule       = BASE_URL + "create_schedule.php";
    public static final String URL_CancelSchedule       = BASE_URL + "cancel_schedule.php";
    public static final String URL_GetSchedule          = BASE_URL + "get_schedule.php";
    public static final String URL_WalletHistory        = BASE_URL + "wallet_history.php";
    public static final String URL_FeedbackList         = BASE_URL + "feedback_list.php";
    public static final String URL_SparePartsList       = BASE_URL + "spare_parts_list.php";
    public static final String URL_RemoveSparePart      = BASE_URL + "remove_spare_parts.php";
    public static final String URL_SubmitSpareParts     = BASE_URL + "submit_spare_parts.php";
    public static final String URL_UpdateOrderStatus    = BASE_URL + "update_order_status.php";
    public static final String URL_RequestPayment       = BASE_URL + "request_payment.php";
    public static final String URL_AddSparePart         = BASE_URL + "add_spare_parts.php";
    public static final String URL_AddComment           = BASE_URL + "add_comment.php";
    public static final String URL_AddInspection        = BASE_URL + "add_inspection.php";
    public static final String URL_OnlineWalletCredit   = BASE_URL + "online_wallet_credit.php";
    public static final String URL_WalletPaymentCheck   = BASE_URL + "wallet_payment_check.php";
    public static final String URL_ContactUs            = BASE_URL + "contact_us.php";
    public static final String URL_UpdateBankDetails    = BASE_URL + "update_bank_details.php";
    public static final String URL_InsertFeedback       = BASE_URL + "insert_feedback.php";
    public static final String URL_DeleteGalleryImage   = BASE_URL + "gallery_delet.php";
    public static final String URL_CheckMobileNumber    = BASE_URL + "check_mobile.php";
    public static final String URL_LogoutUser           = BASE_URL + "logout.php";
    public static final String URL_GetMenu              = BASE_URL + "home.php";
    public static final String URL_GetSubCatList        = BASE_URL + "sub_category_list.php";
    public static final String URL_GetCategoryList      = BASE_URL + "category_list.php";
    public static final String URL_UpdateServices       = BASE_URL + "update_vendor_service.php";
    public static final String URL_GetProfile           = BASE_URL + "get_profile.php";
    public static final String URL_GetUtility           = BASE_URL + "utility.php";
    public static final String URL_UpdateRequestStatus  = BASE_URL + "update_request_status.php";
    public static final String URL_PushNotification     = BASE_URL + "push.php";
    public static final String URL_ChangePassword       = BASE_URL + "change_password.php";
    public static final String URL_CountryList          = BASE_URL + "get_country_list.php";
    public static final String URL_GetCityList          = BASE_URL + "city_list.php";
    public static final String URL_GetSpokenLanguageList = BASE_URL + "spoken_languages_list.php";
    public static final String URL_UpdateCity           = BASE_URL + "update_city.php";
    public static final String URL_ForgotPassword       = BASE_URL + "forget_password_driver";
    public static final String URL_ResetPassword        = BASE_URL + "forget_password_update_driver";

//    public static final String URL_TERMS_CONDITIONS     = BASE_URL +"pages.php?page=tnc_service_reg";
    public static final String URL_TERMS_CONDITIONS     = "https://staging-api.quickfix.sa/app/v0_1/api/pages.php";
    public static final String URL_ABOUT_US             = "http://sportsdot.in/aboutus.html";
    public static final String URL_PRIVACY_POLICY       = "http://sportsdot.in/terms.html";

}
