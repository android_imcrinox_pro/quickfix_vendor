package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class WalletAmountCreditModel implements Serializable {
    private final String TAG = "WalletCreditModel";

    private final String
            TYPE                  = "online_type",
            AMOUNT                = "amount";

    String
            type                  = null,
            amount                = null;

    public WalletAmountCreditModel(){}

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            if(json.has(AMOUNT)){this.amount = json.getString(AMOUNT);}
            if(json.has(TYPE))this.type = json.getString(TYPE);

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);
            return false;}

    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(AMOUNT, this.amount);
            jsonMain.put(TYPE, type);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }

}
