package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class ForgotPasswordModel implements Serializable {

    private final String TAG = "ForgotPasswordModel";
    private final String
            PHONE           = "mobile_number",
            OLD_PASSWORD    = "old_password",
            PASSWORD        = "password";


    String
            oldPassword     = null,
            password        = null,
            mobile          = null;

    public ForgotPasswordModel(){}

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            oldPassword = json.getString(OLD_PASSWORD);
            password = json.getString(PASSWORD);
            mobile = json.getString(PHONE);
            return true;
        }catch(Exception ex){Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(OLD_PASSWORD, oldPassword);
            jsonMain.put(PASSWORD, password);
            jsonMain.put(PHONE, mobile);
            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
