package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class OrderListMainModel implements Serializable {
    private final String TAG = "OrderListMainModel";

    private final String
            ORDER_LIST         = "response",
            FEEDBACK           = "feedback",
            USER_NAME          = "user_name",
            USER_IMAGE         = "user_image",
            ACIVE              = "active",
            ANDROID_UPDATE        = "android_update",
            FORCE_ANDROID_UPDATE  = "force_android_update";

    OrderListModel
            orderList            = null;

    String
            feedback             = null,
            userName             = null,
            userImage            = null,
            active               = null,
            androidUpdate        = null,
            forceAndroidUpdate   = null;

    public OrderListMainModel(){}

    public OrderListModel getOrderList() {
        return orderList;
    }

    public void setOrderList(OrderListModel orderList) {
        this.orderList = orderList;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getAndroidUpdate() {
        return androidUpdate;
    }

    public void setAndroidUpdate(String androidUpdate) {
        this.androidUpdate = androidUpdate;
    }

    public String getForceAndroidUpdate() {
        return forceAndroidUpdate;
    }

    public void setForceAndroidUpdate(String forceAndroidUpdate) {
        this.forceAndroidUpdate = forceAndroidUpdate;
    }

    public boolean toObject(String jsonObjectString){
        try{
            JSONObject json = new JSONObject(jsonObjectString);

            if(json.has(FEEDBACK)){this.feedback = json.getString(FEEDBACK);}
            if(json.has(USER_NAME)){this.userName = json.getString(USER_NAME);}
            if(json.has(USER_IMAGE)){this.userImage = json.getString(USER_IMAGE);}
            if(json.has(ACIVE)){this.active = json.getString(ACIVE);}
            if (json.has(ANDROID_UPDATE)) androidUpdate = json.getString(ANDROID_UPDATE);
            if (json.has(FORCE_ANDROID_UPDATE)) forceAndroidUpdate = json.getString(FORCE_ANDROID_UPDATE);

            if(json.has(ORDER_LIST)) {
                JSONArray array = json.getJSONArray(ORDER_LIST);
                OrderListModel listModelLocal = new OrderListModel();
                if(listModelLocal.toObject(array)){this.orderList = listModelLocal;}
                else{this.orderList = null;}
            }

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(FEEDBACK, this.feedback);
            jsonMain.put(USER_NAME, this.userName);
            jsonMain.put(USER_IMAGE, this.userImage);
            jsonMain.put(ACIVE, this.active);
            jsonMain.put(ANDROID_UPDATE, androidUpdate);
            jsonMain.put(FORCE_ANDROID_UPDATE, forceAndroidUpdate);
            jsonMain.put(ORDER_LIST, orderList!=null?new JSONArray(orderList.toString(true)):null);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){
            Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }

}


