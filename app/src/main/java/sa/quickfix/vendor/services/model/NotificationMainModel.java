package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class NotificationMainModel implements Serializable {
    private final String TAG = "NotificationMainModel";

    private final String
            NOTIFICATION       = "notification",
            COUNT              = "count";

    private MyNotificationListModel
            notificationListModel = null;

    String count               = null;

    public NotificationMainModel(){}

    public MyNotificationListModel getNotificationListModel() {
        return notificationListModel;
    }

    public void setNotificationListModel(MyNotificationListModel notificationListModel) {
        this.notificationListModel = notificationListModel;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public boolean toObject(String jsonObjectString){
        try{
            JSONObject json = new JSONObject(jsonObjectString);

            if(json.has(NOTIFICATION)) {
                JSONArray array = json.getJSONArray(NOTIFICATION);
                MyNotificationListModel listModelLocal = new MyNotificationListModel();
                if(listModelLocal.toObject(array)){this.notificationListModel = listModelLocal;}
                else{this.notificationListModel = null;}
            }

            if(json.has(COUNT)){this.count = json.getString(COUNT);}

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(NOTIFICATION, notificationListModel!=null?new JSONArray(notificationListModel.toString(true)):null);
            jsonMain.put(COUNT, this.count);
            returnString = jsonMain.toString();
        }
        catch (Exception ex){
            Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }

}


