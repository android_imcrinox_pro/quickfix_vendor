package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class IndexModel implements Serializable {
    private final String TAG = "IndexModel";
    private final String
            TRIP            = "request",
            TIMESTAMP       = "timestamp",
            SYSTEM_INFO     = "system_info",
            ONLINE_STUATS   = "online_status",
            PROFILE_STATUS  = "profile_status",
            REQUEST_TOTAL   = "pending_request",
            TODAY_EARNING   = "today_earnings",
            PROFILE         = "driver",
            UUID            = "uuid";

    String
            systemInfo      = null,
            uuid            = null;

    int
            requestTotal    = 0;
    double
            todayEaringn    = 0.0;
    long
            timestamp       = 0;

    boolean
            profileStatus = true;

    ProfileModel
            profile = null;

    public IndexModel(){}

    public String getSystemInfo() {
        return systemInfo;
    }

    public void setSystemInfo(String systemInfo) {
        this.systemInfo = systemInfo;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getRequestTotal() {
        return requestTotal;
    }

    public void setRequestTotal(int requestTotal) {
        this.requestTotal = requestTotal;
    }

    public double getTodayEaringn() {
        return todayEaringn;
    }

    public void setTodayEaringn(double todayEaringn) {
        this.todayEaringn = todayEaringn;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(boolean profileStatus) {
        this.profileStatus = profileStatus;
    }

    public ProfileModel getProfile() {
        return profile;
    }

    public void setProfile(ProfileModel profile) {
        this.profile = profile;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);

            if(json.has(SYSTEM_INFO))this.systemInfo = json.getString(SYSTEM_INFO);
            if(json.has(UUID))this.uuid = json.getString(UUID);

            if(json.has(TIMESTAMP)){timestamp = json.getLong(TIMESTAMP);}
            else{timestamp = 0;}

            if(json.has(TODAY_EARNING)){todayEaringn = json.getDouble(TODAY_EARNING);}
            else{todayEaringn = 0.0;}

            if(json.has(REQUEST_TOTAL)){requestTotal = json.getInt(REQUEST_TOTAL);}
            else{requestTotal = 0;}

            if(json.has(PROFILE_STATUS)){profileStatus = json.getBoolean(PROFILE_STATUS);}
            else{profileStatus = true;}

            /*get Cities*/
            /*KeyValueListModel cityKeyValueModel = new KeyValueListModel();
            JSONArray cityJson = new JSONArray();
            if(json.has(CITIES)){cityJson = json.getJSONArray(CITIES);}else{cityJson=null;}
            if(cityJson!=null){cityKeyValueModel.toObject(cityJson);}
            cities = cityKeyValueModel;*/

            /*get Categories*/
            if(json.has(PROFILE)) {
                ProfileModel tripModel = new ProfileModel();
                JSONObject tripJSON = json.getJSONObject(PROFILE);
                if(tripModel.toObject(tripJSON.toString())) {
                    profile = tripModel;
                }
            }

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(UUID, this.uuid);
            jsonMain.put(TIMESTAMP, timestamp);
            jsonMain.put(TODAY_EARNING, todayEaringn);
            jsonMain.put(REQUEST_TOTAL, requestTotal);
            jsonMain.put(SYSTEM_INFO, this.systemInfo);
            jsonMain.put(PROFILE_STATUS, profileStatus);
            jsonMain.put(PROFILE, profile!=null ? new JSONObject(profile.toString()) : new JSONObject());
            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
