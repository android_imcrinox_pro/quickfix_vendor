package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class SparePartsPostModel implements Serializable {

    private final String TAG = "SparePartsPostModel";
    private final String
            INDEX              = "index",
            SIZE               = "size",
            CATEGORY_ID        = "category_id",
            SUB_CATEGORY_ID    = "sub_category_id",
            SEARCH             = "search";

    String
            index              = null,
            size               = null,
            categoryId         = null,
            subCategoryId      = null,
            search             = null;

    public SparePartsPostModel(){}

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);

            if(json.has(INDEX)){this.index = json.getString(INDEX);}
            if(json.has(SIZE)){this.size = json.getString(SIZE);}
            if(json.has(CATEGORY_ID)){this.categoryId = json.getString(CATEGORY_ID);}
            if(json.has(SUB_CATEGORY_ID)){this.subCategoryId = json.getString(SUB_CATEGORY_ID);}
            if(json.has(SEARCH)){this.search = json.getString(SEARCH);}

            return true;
        }catch(Exception ex){Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(INDEX, index);
            jsonMain.put(SIZE, size);
            jsonMain.put(CATEGORY_ID, categoryId);
            jsonMain.put(SUB_CATEGORY_ID, subCategoryId);
            jsonMain.put(SEARCH, search);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}

