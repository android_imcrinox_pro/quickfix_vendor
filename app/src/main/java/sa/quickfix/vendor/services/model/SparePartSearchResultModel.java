package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class SparePartSearchResultModel implements Serializable {
    private final String TAG = "PartSearchResultModel";
    private final String
            ID                = "id",
            IMAGE             = "image",
            PRICE             = "price",
            PRODUCT_CODE      = "product_code",
            TITLE             = "title",
            QUANTITY          = "quantity",
            SELECTED_QUANTITY = "selected_quantity";

    private String
            id               = null,
            image             = null,
            price             = null,
            productCode       = null,
            title             = null,
            quantity          = null,
            selectedQuantity  = null;

    public SparePartSearchResultModel(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getSelectedQuantity() {
        return selectedQuantity;
    }

    public void setSelectedQuantity(String selectedQuantity) {
        this.selectedQuantity = selectedQuantity;
    }

    public boolean toObject(String jsonObject){
        try{

            JSONObject json = new JSONObject(jsonObject);

            if(json.has(ID)){this.id = json.getString(ID);}
            if(json.has(IMAGE)){this.image = json.getString(IMAGE);}
            if(json.has(PRICE)){this.price = json.getString(PRICE);}
            if(json.has(PRODUCT_CODE)){this.productCode = json.getString(PRODUCT_CODE);}
            if(json.has(TITLE)){this.title = json.getString(TITLE);}
            if(json.has(QUANTITY)){this.quantity = json.getString(QUANTITY);}
            if(json.has(SELECTED_QUANTITY)){this.selectedQuantity = json.getString(SELECTED_QUANTITY);}

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;

        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ID, id);
            jsonMain.put(IMAGE, image);
            jsonMain.put(PRICE, price);
            jsonMain.put(PRODUCT_CODE, productCode);
            jsonMain.put(TITLE, title);
            jsonMain.put(QUANTITY, quantity);
            jsonMain.put(SELECTED_QUANTITY, selectedQuantity);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}


