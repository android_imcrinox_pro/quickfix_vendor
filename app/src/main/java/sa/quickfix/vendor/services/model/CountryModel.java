package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;


public class CountryModel implements Serializable {
    private final String TAG = "CountryModel";

    private final String
            ID                  = "id",
            NAME                = "title",
            CURRENCY            = "currency_title",
            COUNTRY_CODE        = "country_code",
//            COUNTRY_LENGTH      = "country_length",
           CITY_LIST           = "city";

    String
            id                  = null,
            name                = null,
            currency            = null,
            countryCode         = null,
            countryLength       = null;

    MyCityListModel
            cityList         = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getContactLength() {
        return countryLength;
    }

    public void setContactLength(String countryLength) {
        this.countryLength = countryLength;
    }

    public MyCityListModel getCityList() {
        return cityList;
    }

    public void setCityList(MyCityListModel cityList) {
        this.cityList = cityList;
    }


    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            if(json.has(ID)){this.id = json.getString(ID);}
            if(json.has(NAME)){this.name = json.getString(NAME);}
            if(json.has(CURRENCY)){this.currency = json.getString(CURRENCY);}
            if(json.has(COUNTRY_CODE)){this.countryCode = json.getString(COUNTRY_CODE);}
//            if(json.has(COUNTRY_LENGTH)){this.countryLength = json.getString(COUNTRY_LENGTH);}

            if(json.has(CITY_LIST)) {
                JSONArray timeSlotArray = json.getJSONArray(CITY_LIST);
                MyCityListModel cityListModel = new MyCityListModel();
                if (cityListModel.toObject(timeSlotArray)) {
                    this.cityList = cityListModel;
                } else {
                    this.cityList = null;
                }
            }

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);
            return false;}

    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ID, this.id);
            jsonMain.put(NAME, this.name);
            jsonMain.put(CURRENCY, this.currency);
            jsonMain.put(COUNTRY_CODE, this.countryCode);
//            jsonMain.put(COUNTRY_LENGTH, this.countryLength);
            jsonMain.put(CITY_LIST, cityList!=null ? new JSONArray(cityList.toString(true)) : new JSONArray());

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }

}
