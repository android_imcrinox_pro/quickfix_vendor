package sa.quickfix.vendor.services.model;
import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class AboutUsMainModel implements Serializable {
    private final String TAG = "AboutUsModel";
    private final String
            RESPONSE               = "response";


    private AboutUsModel response=null;

    public AboutUsModel getResponse() {
        return response;
    }

    public void setResponse(AboutUsModel response) {
        this.response = response;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);

           if(json.has(RESPONSE)) {
                JSONObject responseJson = new JSONObject();
                responseJson = json.getJSONObject(RESPONSE);
               AboutUsModel aboutUsModel = new AboutUsModel();
                if (aboutUsModel != null) {
                    aboutUsModel.toObject(responseJson.toString());
                }
                this.response = aboutUsModel;
            }

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);
            return false;}
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(RESPONSE, this.response!=null?new JSONObject(this.response.toString()):"{}");
            returnString = jsonMain.toString();
        }
        catch (Exception ex){
            Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
