package sa.quickfix.vendor.services;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.util.Log;


import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.BuildConfig;
import sa.quickfix.vendor.services.model.AboutUsMainModel;
import sa.quickfix.vendor.services.model.AddInspectionListModel;
import sa.quickfix.vendor.services.model.AddPartPostModel;
import sa.quickfix.vendor.services.model.AddScheduleModel;
import sa.quickfix.vendor.services.model.AddedPartsListModel;
import sa.quickfix.vendor.services.model.AddressListModel;
import sa.quickfix.vendor.services.model.AddressModel;
import sa.quickfix.vendor.services.model.BankDetailsModel;
import sa.quickfix.vendor.services.model.CategoryListModel;
import sa.quickfix.vendor.services.model.ChangePasswordModel;
import sa.quickfix.vendor.services.model.CompanyRegisterModel;
import sa.quickfix.vendor.services.model.ContactPostModel;
import sa.quickfix.vendor.services.model.CountryListModel;
import sa.quickfix.vendor.services.model.FeedbackMainModel;
import sa.quickfix.vendor.services.model.FeedbackPostModel;
import sa.quickfix.vendor.services.model.FixingTimePostModel;
import sa.quickfix.vendor.services.model.ForgotPasswordModel;
import sa.quickfix.vendor.services.model.HomeIndexModel;
import sa.quickfix.vendor.services.model.HomePageModel;
import sa.quickfix.vendor.services.model.HomeSubCategoryListModel;
import sa.quickfix.vendor.services.model.IndexModel;
import sa.quickfix.vendor.services.model.IndividualRegisterModel;
import sa.quickfix.vendor.services.model.LanguageListModel;
import sa.quickfix.vendor.services.model.LocationApiModel;
import sa.quickfix.vendor.services.model.LocationCheckPostModel;
import sa.quickfix.vendor.services.model.LocationCheckResponseModel;
import sa.quickfix.vendor.services.model.LocationUpdateModel;
import sa.quickfix.vendor.services.model.LoginModel;
import sa.quickfix.vendor.services.model.MyCityListModel;
import sa.quickfix.vendor.services.model.NotificationListModel;
import sa.quickfix.vendor.services.model.NotificationMainModel;
import sa.quickfix.vendor.services.model.OrderDetailMainModel;
import sa.quickfix.vendor.services.model.OrderListMainModel;
import sa.quickfix.vendor.services.model.OrderPostModel;
import sa.quickfix.vendor.services.model.OrderStatusUpdateModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.PushNotificationModel;
import sa.quickfix.vendor.services.model.RegisterModel;
import sa.quickfix.vendor.services.model.ScheduleModel;
import sa.quickfix.vendor.services.model.SparePartsPostModel;
import sa.quickfix.vendor.services.model.SparePartsSearchResultListModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.services.model.UpdateLanguageModel;
import sa.quickfix.vendor.services.model.UtilityModel;
import sa.quickfix.vendor.services.model.WalletAmountCreditModel;
import sa.quickfix.vendor.services.model.WalletMainModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.services.model.KeyValueListModel;

import org.json.JSONObject;


public class ServicesMethodsManager {

    public static final String TAG = "ServicesMethodsMgr";
    private ServerResponseInterface mUiCallBack;

    GlobalFunctions globalFunctions;
    GlobalVariables globalVariables;

    public ServicesMethodsManager() {
        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();
    }


    private void setCallbacks(ServerResponseInterface mCallBack) {
        mUiCallBack = mCallBack;
    }

    private void postData(final Context context, final Object obj, String URL, String query, String TAG, final boolean isCookieSave) {
        /*if(obj instanceof LoginModel){
            String token =  GlobalFunctions.getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_TOKEN);
            if(token!=null){URL += "?"+"token="+ token;}
        }*/

        final VolleyServices request = new VolleyServices(context);
        request.setCallbacks(new VolleyServices.ResposeCallBack() {
            @Override
            public void OnSuccess(JSONObject arg0) {
                if (isCookieSave) {
                    GlobalFunctions.setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_COOKIE, request.getCookie());
                }
                parseResponse(context, obj, arg0);
            }

            @Override
            public void OnFailure(int cause) {
                mUiCallBack.OnFailureFromServer(context.getString(cause));
            }

            @Override
            public void OnFailure(String cause) {
                mUiCallBack.OnFailureFromServer(cause);
            }
        });
        request.setBody(obj.toString());
        request.makeJsonPostRequest(context, URL, query, TAG);
    }

    private void postData(final Context context, final Object obj, String URL, String query, String TAG) {
        /*if(obj instanceof LoginModel){
            String token =  GlobalFunctions.getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_TOKEN);
            if(token!=null){URL += "?"+"token="+ token;}
        }*/

        final VolleyServices request = new VolleyServices(context);
        request.setCallbacks(new VolleyServices.ResposeCallBack() {
            @Override
            public void OnSuccess(JSONObject arg0) {
                if (obj instanceof HomeIndexModel) {
                    GlobalFunctions.setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_COOKIE, request.getCookie());
                }
                parseResponse(context, obj, arg0);
            }

            @Override
            public void OnFailure(int cause) {
                mUiCallBack.OnFailureFromServer(context.getString(cause));
            }

            @Override
            public void OnFailure(String cause) {
                mUiCallBack.OnFailureFromServer(cause);
            }
        });
        request.setBody(obj.toString());
        request.makeJsonPostRequest(context, URL, query, TAG);
    }

    private void postData(final Context context, final Object obj, String URL, String TAG) {
        final VolleyServices request = new VolleyServices(context);
        request.setCallbacks(new VolleyServices.ResposeCallBack() {
            @Override
            public void OnSuccess(JSONObject arg0) {
                if (obj instanceof HomeIndexModel) {
                    GlobalFunctions.setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_COOKIE, request.getCookie());
                }
                parseResponse(context, obj, arg0);
            }

            @Override
            public void OnFailure(int cause) {
                mUiCallBack.OnFailureFromServer(context.getString(cause));
            }

            @Override
            public void OnFailure(String cause) {
                mUiCallBack.OnFailureFromServer(cause);
            }
        });
        request.setBody(obj.toString());
        request.makeJsonPostRequest(context, URL, null, TAG);
    }

    private void getData(@NonNull final Context context, @NonNull final Object obj, @NonNull String URL, @Nullable String query, @Nullable String TAG) {
        //if(query!=null){if(!query.equalsIgnoreCase("")){URL += "?"+query;}}
        VolleyServices request = new VolleyServices(context);
        request.setCallbacks(new VolleyServices.ResposeCallBack() {
            @Override
            public void OnSuccess(JSONObject arg0) {
                Log.d("Response", arg0.toString());
                parseResponse(context, obj, arg0);
            }

            @Override
            public void OnFailure(int cause) {
                mUiCallBack.OnFailureFromServer(context.getString(cause));
            }

            @Override
            public void OnFailure(String cause) {
                mUiCallBack.OnFailureFromServer(cause);
            }
        });
        request.setBody(obj.toString());
        request.makeJsonGETRequest(context, URL, query, TAG);
    }

    private void parseResponse(Context context, Object obj, JSONObject resp) {
        Log.d(TAG, resp.toString());
        if (obj instanceof IndexModel) {
            IndexModel model = new IndexModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                ProfileModel profileModel = new ProfileModel();
                if (profileModel.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(profileModel);
                } else {
                    mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                }
            }

        } else if (obj instanceof NotificationListModel) {
            NotificationListModel model = new NotificationListModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof KeyValueListModel) {
            KeyValueListModel model = new KeyValueListModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof RegisterModel || obj instanceof LoginModel || obj instanceof CompanyRegisterModel || obj instanceof IndividualRegisterModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                ProfileModel profileModel = new ProfileModel();
                if (profileModel.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(profileModel);
                } else {
                    mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                }
            }
        } else if (obj instanceof LocationCheckPostModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                LocationCheckResponseModel responseModel = new LocationCheckResponseModel();
                if (responseModel.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(responseModel);
                } else {
                    mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                }
            }
        } else if (obj instanceof MyCityListModel) {
            MyCityListModel model = new MyCityListModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof UpdateLanguageModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof LanguageListModel) {
            LanguageListModel model = new LanguageListModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof CategoryListModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                CategoryListModel categoryListModel = new CategoryListModel();
                if (categoryListModel.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(categoryListModel);
                } else {
                    mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                }
            }
        } else if (obj instanceof HomePageModel) {
            HomePageModel model = new HomePageModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof HomeSubCategoryListModel) {
            HomeSubCategoryListModel model = new HomeSubCategoryListModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof UtilityModel) {
            UtilityModel model = new UtilityModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        }else if (obj instanceof AddScheduleModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof ScheduleModel) {
            ScheduleModel scheduleModel = new ScheduleModel();
            if (scheduleModel.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(scheduleModel);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof ProfileModel) {
            ProfileModel profileModel = new ProfileModel();
            if (profileModel.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(profileModel);
            } else {
                StatusModel statusModel = new StatusModel();
                if (statusModel.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(statusModel);
                } else {
                    mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                }
            }
        } else if (obj instanceof StatusModel || obj instanceof ChangePasswordModel || obj instanceof PushNotificationModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                ProfileModel profileMode = new ProfileModel();
                if (profileMode.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(profileMode);
                } else {
                    mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                }
            }
        } else if (obj instanceof OrderDetailMainModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                OrderDetailMainModel orderDetailMainModel = new OrderDetailMainModel();
                if (orderDetailMainModel.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(orderDetailMainModel);
                } else {
                    mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                }
            }
        } else if (obj instanceof CountryListModel) {
            CountryListModel model = new CountryListModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof OrderStatusUpdateModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof FixingTimePostModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof AddPartPostModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                AddedPartsListModel addedPartsListModel = new AddedPartsListModel();
                if (addedPartsListModel.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(addedPartsListModel);
                } else {
                    mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                }
            }
        } else if (obj instanceof AddInspectionListModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof AddedPartsListModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                AddedPartsListModel addedPartsListModel = new AddedPartsListModel();
                if (addedPartsListModel.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(addedPartsListModel);
                } else {
                    mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                }
            }
        } else if (obj instanceof SparePartsPostModel) {
            SparePartsSearchResultListModel model = new SparePartsSearchResultListModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof ForgotPasswordModel || obj instanceof WalletAmountCreditModel || obj instanceof ContactPostModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof LocationUpdateModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof AboutUsMainModel) {
            AboutUsMainModel model = new AboutUsMainModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof OrderListMainModel) {
            OrderListMainModel model = new OrderListMainModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof NotificationMainModel) {
            NotificationMainModel model = new NotificationMainModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof WalletMainModel) {
            WalletMainModel model = new WalletMainModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof FeedbackMainModel) {
            FeedbackMainModel model = new FeedbackMainModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof BankDetailsModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof FeedbackPostModel) {
            StatusModel model = new StatusModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof AddressModel) {
            AddressListModel listModel = new AddressListModel();
            if (listModel.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(listModel);
            } else {
                AddressModel model = new AddressModel();
                if (model.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(model);
                } else {
                    StatusModel statusModel = new StatusModel();
                    if (statusModel.toObject(resp.toString())) {
                        mUiCallBack.OnSuccessFromServer(statusModel);
                    } else {
                        mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                    }
                }

            }
        } else if (obj instanceof AddressListModel) {
            AddressListModel listModel = new AddressListModel();
            if (listModel.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(listModel);
            } else {
                mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
            }
        } else if (obj instanceof LocationApiModel) {
            LocationApiModel model = new LocationApiModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                StatusModel statusModel = new StatusModel();
                if (statusModel.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(statusModel);
                } else {
                    mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                }
            }
        } else if (obj instanceof IndexModel) {
            IndexModel model = new IndexModel();
            if (model.toObject(resp.toString())) {
                mUiCallBack.OnSuccessFromServer(model);
            } else {
                StatusModel statusModel = new StatusModel();
                if (statusModel.toObject(resp.toString())) {
                    mUiCallBack.OnSuccessFromServer(statusModel);
                } else {
                    mUiCallBack.OnError(context.getString(R.string.ErrorResponseData));
                }
            }
        }
    }

    public void getGuestUserCreation(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        HomeIndexModel model = new HomeIndexModel();
        model.setUid(GlobalFunctions.getUniqueID(context));
        model.setSystem_info(GlobalFunctions.getDevice());
        postData(context, model, ServerConstants.URL_GuestUserCreation, null, TAG, true);
    }

    public void resendOtp(Context context, LoginModel loginModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
//        query = query!=null? query+"&phone="+mobileNumber : "phone="+mobileNumber;
        String URL = ServerConstants.URL_SendOTP;
        postData(context, loginModel, URL, query, TAG, false);
    }

    public void getwallet(@NonNull int type, @NonNull int index, @NonNull int size, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
       /* query = query!=null? query+"type="+type : "type="+type;
        query = query!=null? query+"&index="+index : "&index="+index;
        query = query!=null? query+"&size="+size : "size="+size;*/
        // getData(new WalletListModel(), null, ServerConstants.URL_WalletHistory, query, TAG);
    }

    public void sendPushNotificationID(Context context, PushNotificationModel pushNotificationModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        pushNotificationModel.setUid(GlobalFunctions.getUniqueID(context));
        pushNotificationModel.setSystemInfo(GlobalFunctions.getDevice());
        postData(context, pushNotificationModel, ServerConstants.URL_PushNotification, query, TAG);
    }

    public void logout(Context context, UpdateLanguageModel model, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, model, ServerConstants.URL_LogoutUser, null, TAG);
    }

    public void logout(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        getData(context, new StatusModel(), ServerConstants.URL_LogoutUser, null, TAG);
    }

    public void getMenu(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        getData(context, new HomePageModel(), ServerConstants.URL_GetMenu, query, TAG);
    }

    public void getSubCatList(Context context, String catId, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "&id=" + catId : "id=" + catId;
        getData(context, new HomeSubCategoryListModel(), ServerConstants.URL_GetSubCatList, query, TAG);
    }

    public void getHomeData(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        setCallbacks(mCallInterface);
//        getData(context, new HomeModel(), ServerConstants.URL_GetMenu,query, TAG);
    }

    public void sendOtp(Context context, LoginModel loginModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
//        query = query!=null? query+"&phone="+mobileNumber : "phone="+mobileNumber;
        String URL = ServerConstants.URL_SendOTP;
        postData(context, loginModel, URL, query, TAG, false);
    }

    public void verifyOTP(Context context, LoginModel loginModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
//        query = query!=null? query+"&phone="+mobileNumber : "phone="+mobileNumber;
        String URL = ServerConstants.URL_VerifyOTP;
        postData(context, loginModel, URL, query, TAG, false);
    }

    public void verifyLoginOTP(Context context, LoginModel loginModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
//        query = query!=null? query+"&phone="+mobileNumber : "phone="+mobileNumber;
        String URL = ServerConstants.URL_LoginVerifyOTP;
        postData(context, loginModel, URL, query, TAG, false);
    }

    public void loginUser(Context context, RegisterModel registerModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        registerModel.setLoginToken(globalFunctions.md5(registerModel.getMobileNumber() + "w8s7dghrteys"));
        registerModel.setUid(GlobalFunctions.getUniqueID(context));
        registerModel.setSystemInfo(GlobalFunctions.getDevice());
        postData(context, registerModel, ServerConstants.URL_LoginUser, TAG);
    }

    public void loginUser(Context context, LoginModel loginModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        loginModel.setLoginToken(globalFunctions.md5(loginModel.getMobile() + "w8s7dghrteys"));
        loginModel.setUid(GlobalFunctions.getUniqueID(context));
        loginModel.setSystemInfo(GlobalFunctions.getDevice());
        postData(context, loginModel, ServerConstants.URL_LoginUser, TAG);
    }

    public void checkLocation(Context context, LocationCheckPostModel locationCheckPostModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, locationCheckPostModel, ServerConstants.URL_LocationCheck, TAG);
    }

    public void registerUser(Context context, RegisterModel registerModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, registerModel, ServerConstants.URL_RegisterUser, TAG);
    }

    public void registerAsCompany(Context context, CompanyRegisterModel companyRegisterModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, companyRegisterModel, ServerConstants.URL_RegisterAsCompany, TAG);
    }

    public void registerAsIndividual(Context context, IndividualRegisterModel individualRegisterModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, individualRegisterModel, ServerConstants.URL_RegisterAsIndividual, TAG);
    }

    public void getCityList(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        getData(context, new MyCityListModel(), ServerConstants.URL_GetCityList, null, TAG);
    }

    public void getSpokenLanguagesList(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        getData(context, new LanguageListModel(), ServerConstants.URL_GetSpokenLanguageList, null, TAG);
    }

    public void getCategoryList(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        getData(context, new CategoryListModel(), ServerConstants.URL_GetCategoryList, query, TAG);
    }

    public void updateServices(Context context, CategoryListModel categoryListModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, categoryListModel, ServerConstants.URL_UpdateCategory, TAG);
    }

    public void updateLocation(Context context, LocationUpdateModel locationUpdateModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, locationUpdateModel, ServerConstants.URL_LocationUpdate, TAG);
    }

    public void getOrderList(Context context, OrderPostModel orderPostModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        String versionName = null;
        if (orderPostModel != null) {
            query = query != null ? query + "&index=" + orderPostModel.getIndex() : "index=" + orderPostModel.getIndex();
            query = query != null ? query + "&size=" + orderPostModel.getSize() : "size=" + orderPostModel.getSize();
            query = query != null ? query + "&status=" + orderPostModel.getStatus() : "status=" + orderPostModel.getStatus();
        }
        try {
            versionName = BuildConfig.VERSION_NAME;
        } catch (Exception exccc) {
            versionName = null;
        }
        if (versionName != null) {
            query = query != null ? query + "&android_version=" + versionName : "android_version=" + versionName;
        }
        getData(context, new OrderListMainModel(), ServerConstants.URL_OrderList, query, TAG);
    }

    public void getNotificationList(Context context, OrderPostModel orderPostModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        if (orderPostModel != null) {
            query = query != null ? query + "&index=" + orderPostModel.getIndex() : "index=" + orderPostModel.getIndex();
            query = query != null ? query + "&size=" + orderPostModel.getSize() : "size=" + orderPostModel.getSize();
        }
        getData(context, new NotificationMainModel(), ServerConstants.URL_NotificationList, query, TAG);
    }

    public void clearNotification(Context context, String notificationId, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "&id=" + notificationId : "id=" + notificationId;
        getData(context, new StatusModel(), ServerConstants.URL_ClearNotification, query, TAG);
    }

    public void clearAllNotifications(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        getData(context, new StatusModel(), ServerConstants.URL_ClearNotificationAll, query, TAG);
    }

    public void seenNotificationAllData(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        getData(context, new StatusModel(), ServerConstants.URL_GetSeenNotificationAll, query, TAG);
    }

    public void getWalletHistoryList(Context context, OrderPostModel orderPostModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        if (orderPostModel != null) {
            query = query != null ? query + "&index=" + orderPostModel.getIndex() : "index=" + orderPostModel.getIndex();
            query = query != null ? query + "&size=" + orderPostModel.getSize() : "size=" + orderPostModel.getSize();
        }
        getData(context, new WalletMainModel(), ServerConstants.URL_WalletHistory, query, TAG);
    }

    public void getReviewsList(Context context, OrderPostModel orderPostModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        if (orderPostModel != null) {
            query = query != null ? query + "&index=" + orderPostModel.getIndex() : "index=" + orderPostModel.getIndex();
            query = query != null ? query + "&size=" + orderPostModel.getSize() : "size=" + orderPostModel.getSize();
        }
        getData(context, new FeedbackMainModel(), ServerConstants.URL_FeedbackList, query, TAG);
    }

    public void getOrderDetails(Context context, String orderId, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "&order_id=" + orderId : "order_id=" + orderId;
        getData(context, new OrderDetailMainModel(), ServerConstants.URL_OrderDetails, query, TAG);
    }

    public void getIsAvailable(Context context, String status, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "&status=" + status : "status=" + status;
        getData(context, new StatusModel(), ServerConstants.URL_OrderIsAvail, query, TAG);
    }

    public void updateCustomerNotPaid(Context context, OrderStatusUpdateModel orderStatusUpdateModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        if (orderStatusUpdateModel != null) {
            query = query != null ? query + "&order_id=" + orderStatusUpdateModel.getOrderId() : "order_id=" + orderStatusUpdateModel.getOrderId();
        }
        getData(context, new OrderStatusUpdateModel(), ServerConstants.URL_RequestPayment, query, TAG);
    }

    public void updateOrderStatus(Context context, OrderStatusUpdateModel orderStatusUpdateModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, orderStatusUpdateModel, ServerConstants.URL_UpdateOrderStatus, TAG);
    }

    public void addComment(Context context, OrderStatusUpdateModel orderStatusUpdateModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, orderStatusUpdateModel, ServerConstants.URL_AddComment, TAG);
    }

    public void addSparePart(Context context, AddPartPostModel addPartPostModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, addPartPostModel, ServerConstants.URL_AddSparePart, TAG);
    }

    public void addInspection(Context context, AddInspectionListModel addInspectionListModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, addInspectionListModel, ServerConstants.URL_AddInspection, TAG);
    }

    public void updateFixingTime(Context context, FixingTimePostModel fixingTimePostModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, fixingTimePostModel, ServerConstants.URL_AddSparePart, TAG);
    }

    public void removeSparePart(Context context, String id, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "&id=" + id : "id=" + id;
        getData(context, new AddedPartsListModel(), ServerConstants.URL_RemoveSparePart, query, TAG);
    }

    public void submitSpareParts(Context context, String orderId, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "&order_id=" + orderId : "order_id=" + orderId;
        getData(context, new StatusModel(), ServerConstants.URL_SubmitSpareParts, query, TAG);
    }

    public void getSparePartsList(Context context, SparePartsPostModel sparePartsPostModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, sparePartsPostModel, ServerConstants.URL_SparePartsList, TAG);
    }

    public void onlineWalletCredit(Context context, WalletAmountCreditModel walletAmountCreditModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, walletAmountCreditModel, ServerConstants.URL_OnlineWalletCredit, TAG);
    }

    public void checkOnlineWalletCredit(Context context, String resourcePath, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "resourcePath=" + resourcePath : "resourcePath=" + resourcePath;
        getData(context, new StatusModel(), ServerConstants.URL_WalletPaymentCheck, query, TAG);
    }

    public void contactUs(Context context, ContactPostModel contactPostModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, contactPostModel, ServerConstants.URL_ContactUs, TAG);
    }

    public void createSchedule(Context context, AddScheduleModel addScheduleModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, addScheduleModel, ServerConstants.URL_CreateSchedule, TAG);
    }

    public void cancelSchedule(Context context,String shift_id, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "shift_id=" + shift_id : "shift_id=" + shift_id;
        getData(context, new StatusModel(), ServerConstants.URL_CancelSchedule, query, TAG);
    }

    public void updateBankDetails(Context context, BankDetailsModel bankDetailsModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, bankDetailsModel, ServerConstants.URL_UpdateBankDetails, TAG);
    }

    public void feedbackPost(Context context, FeedbackPostModel feedbackPostModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, feedbackPostModel, ServerConstants.URL_InsertFeedback, TAG);
    }

    public void deleteGalleryImage(Context context, String imageId, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "id=" + imageId : "id=" + imageId;
        getData(context, new StatusModel(), ServerConstants.URL_DeleteGalleryImage, query, TAG);
    }

    public void checkMobileNumber(Context context, LoginModel loginModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        String url = ServerConstants.URL_CheckMobileNumber;
        postData(context, loginModel, url, query, TAG);
    }

    public void getTerms(Context context,String page, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "&page=" + page : "page=" + page;
        String url = ServerConstants.URL_TERMS_CONDITIONS;
        getData(context, new AboutUsMainModel(), url, query, TAG);
    }

    public void forgotMyPassword(Context context, ForgotPasswordModel forgotPasswordModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, forgotPasswordModel, ServerConstants.URL_ForgotMyPassword, TAG);
    }

    public void getUserRequest(Context context, String id, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "&id=" + id : "id=" + id;
//        postData(context, new UserRequestModel(), ServerConstants.URL_GetUserRequest, query, TAG);
    }

    public void getCountryList(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
//        getData(context, new CountryListModel(), ServerConstants.URL_CountryList, query, TAG);
    }

    public void getBankList(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
//        getData(context, new BankListModel(), ServerConstants.URL_BankList, query, TAG);
    }
/*
    public void profileImageUpdate(Context context, DocumentUploadModel documentUploadModel, ServerResponseInterface mCallInterface, String TAG){
        setCallbacks(mCallInterface);
        postData(context, documentUploadModel, ServerConstants.URL_ProfilePhoto, TAG);
    }*/

    public void updateUser(Context context, ProfileModel profileModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String url = ServerConstants.URL_GetProfile;
        postData(context, profileModel, url, TAG);
    }

    public void getProfile(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        String url = ServerConstants.URL_GetProfile;
        getData(context, new ProfileModel(), url, query, TAG);
    }

    public void getUtility(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        String url = ServerConstants.URL_GetUtility;
        getData(context, new UtilityModel(), url, query, TAG);
    }

    public void getSchedule(Context context, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        String url = ServerConstants.URL_GetSchedule;
        getData(context, new ScheduleModel(), url, query, TAG);
    }

    public void updateRequestStatus(Context context, String orderId, String requestAcceptStatus, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "&order_id=" + orderId : "order_id=" + orderId;
        query = query != null ? query + "&status=" + requestAcceptStatus : "status=" + requestAcceptStatus;
        String url = ServerConstants.URL_UpdateRequestStatus;
        getData(context, new StatusModel(), url, query, TAG);
    }

    public void updateProfile(Context context, ProfileModel profileModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        //query = query!=null? query+"&user_type="+userType.toInt() : "user_type="+userType.toInt();
        String url = ServerConstants.URL_GetProfile;
        getData(context, profileModel, url, query, TAG);
    }

    public void changePassword(Context context, ChangePasswordModel changePasswordModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        postData(context, changePasswordModel, ServerConstants.URL_ChangePassword, TAG);
    }

    public void changePassword(Context context, ChangePasswordModel changePasswordModel, StatusModel model, GlobalVariables.USER_TYPE userType, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "&id=" + model.getExtra() : "id=" + model.getExtra();
        query = query != null ? query + "&user_type=" + userType.toInt() : "user_type=" + userType.toInt();
        postData(context, changePasswordModel, ServerConstants.URL_ChangePassword, query, TAG);
    }

    public void forgotPassword(Context context, LoginModel loginModel, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        //query = query!=null? query+"&user_type="+loginModel.getUserType() : "user_type="+loginModel.getUserType();
        GlobalFunctions.removeSharedPreferenceKey(context, GlobalVariables.SHARED_PREFERENCE_COOKIE);
        String URL = ServerConstants.URL_ForgotPassword;
        postData(context, loginModel, URL, query, TAG);
    }

    public void updateCity(Context context, String country_id, String city_id, ServerResponseInterface mCallInterface, String TAG) {
        setCallbacks(mCallInterface);
        String query = null;
        query = query != null ? query + "&country_id=" + country_id : "country_id=" + country_id;
        if (!city_id.isEmpty()) {
//            query = query != null ? query + "&city_id=" + city_id : "city_id=" + city_id;
            query = query != null ? query + "&id=" + city_id : "id=" + city_id;
        }
        getData(context, new StatusModel(), ServerConstants.URL_UpdateCity, query, TAG);
    }

    private String getQuery(String query, String value) {
        if (value == null) {
            return query;
        }
        if (query == null) {
            return value;
        }
        return query + "/" + value;
    }


}
