package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class ServicesModel implements Serializable {
    private final String TAG = "ServicesModel";
    private final String
            ID                  = "id",
            CAT_TITLE           = "category_title",
            SUB_CAT_TITLE       = "sub_category_title",
            SUB_CAT_IMAGE       = "sub_category_image",
            CHILD_SUB_CAT_TITLE = "child_sub_category_title",
            PRICE               = "price";


    String
            id                    = null,
            catTitle              = null,
            subCatTitle           = null,
            subCatImages          = null,
            childSubCatTitle      = null,
            price                 = null;

    public ServicesModel(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCatTitle() {
        return catTitle;
    }

    public void setCatTitle(String catTitle) {
        this.catTitle = catTitle;
    }

    public String getSubCatTitle() {
        return subCatTitle;
    }

    public void setSubCatTitle(String subCatTitle) {
        this.subCatTitle = subCatTitle;
    }

    public String getChildSubCatTitle() {
        return childSubCatTitle;
    }

    public void setChildSubCatTitle(String childSubCatTitle) {
        this.childSubCatTitle = childSubCatTitle;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSubCatImages() {
        return subCatImages;
    }

    public void setSubCatImages(String subCatImages) {
        this.subCatImages = subCatImages;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            this.id = json.getString(ID);
            if(json.has(CAT_TITLE))this.catTitle = json.getString(CAT_TITLE);
            if(json.has(SUB_CAT_TITLE))this.subCatTitle = json.getString(SUB_CAT_TITLE);
            if(json.has(CHILD_SUB_CAT_TITLE))this.childSubCatTitle = json.getString(CHILD_SUB_CAT_TITLE);
            if(json.has(PRICE))this.price = json.getString(PRICE);
            if(json.has(SUB_CAT_IMAGE))this.subCatImages = json.getString(SUB_CAT_IMAGE);

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);
            return false;}
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ID, id);
            jsonMain.put(CAT_TITLE, catTitle);
            jsonMain.put(SUB_CAT_TITLE, subCatTitle);
            jsonMain.put(CHILD_SUB_CAT_TITLE, childSubCatTitle);
            jsonMain.put(PRICE, price);
            jsonMain.put(SUB_CAT_IMAGE, subCatImages);
            returnString = jsonMain.toString();
        }
        catch (Exception ex){
            Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
