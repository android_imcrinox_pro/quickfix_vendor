package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class AddPartPostModel implements Serializable {

    private final String TAG = "AddPartPostModel";
    private final String
            ORDER_ID             = "order_id",
            ORDER_DETAIL_ID      = "order_detail_id",
            SPARE_PART_ID        = "spare_part_id",
            CATEGORY_ID          = "category_id",
            SUB_CATEGORY_ID      = "sub_category_id",
            TITLE                = "title",
            PRICE                = "price",
            TIME                 = "time",
            QUANTITY             = "quantity";

    String
            orderId             = null,
            orderDetailId       = null,
            sparePartId         = null,
            categoryId          = null,
            subCategoryId       = null,
            title               = null,
            price               = null,
            time                = null,
            quantity            = null;

    public AddPartPostModel(){}

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(String orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getSparePartId() {
        return sparePartId;
    }

    public void setSparePartId(String sparePartId) {
        this.sparePartId = sparePartId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);

            if(json.has(ORDER_ID)){this.orderId = json.getString(ORDER_ID);}
            if(json.has(ORDER_DETAIL_ID)){this.orderDetailId = json.getString(ORDER_DETAIL_ID);}
            if(json.has(SPARE_PART_ID)){this.sparePartId = json.getString(SPARE_PART_ID);}
            if(json.has(CATEGORY_ID)){this.categoryId = json.getString(CATEGORY_ID);}
            if(json.has(SUB_CATEGORY_ID)){this.subCategoryId = json.getString(SUB_CATEGORY_ID);}
            if(json.has(TITLE)){this.title = json.getString(TITLE);}
            if(json.has(PRICE)){this.price = json.getString(PRICE);}
            if(json.has(TIME)){this.time = json.getString(TIME);}
            if(json.has(QUANTITY)){this.quantity = json.getString(QUANTITY);}

            return true;
        }catch(Exception ex){Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ORDER_ID, orderId);
            jsonMain.put(ORDER_DETAIL_ID, orderDetailId);
            jsonMain.put(SPARE_PART_ID, sparePartId);
            jsonMain.put(CATEGORY_ID, categoryId);
            jsonMain.put(SUB_CATEGORY_ID, subCategoryId);
            jsonMain.put(TITLE, title);
            jsonMain.put(PRICE, price);
            jsonMain.put(TIME, time);
            jsonMain.put(QUANTITY, quantity);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}

