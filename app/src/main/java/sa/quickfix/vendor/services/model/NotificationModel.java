package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class NotificationModel implements Serializable {
    private final String TAG = "NotificationModel";
    private final String
            MESSAGE = "body",
            TITLE = "title",
            TYPE = "type",
            OID = "oid",
            IMAGE = "image",
            PUSH_ID = "push_id";

    String
            message = null,
            title = null,
            type = null,
            oID = null,
            image = null,
            pushId = null;

    public NotificationModel(){}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    /*public JSONObject getResponse() {
        JSONObject resp = new JSONObject();
        try{resp = new JSONObject(response);}
        catch (Exception ee){resp = new JSONObject();}
        return resp;
    }*/

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getoID() {
        return oID;
    }

    public void setoID(String oID) {
        this.oID = oID;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            if(json.has(MESSAGE))this.message = json.getString(MESSAGE);
            if(json.has(TITLE))this.title = json.getString(TITLE);
            if(json.has(TYPE))this.type = json.getString(TYPE);
            if(json.has(OID))this.oID = json.getString(OID);
            if(json.has(PUSH_ID))this.pushId = json.getString(PUSH_ID);
            if(json.has(IMAGE))this.image = json.getString(IMAGE);
//            if(json.has(RESPONSE))this.response = json.getJSONObject(RESPONSE).toString();
            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);
            return false;}

    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(MESSAGE, this.message);
            jsonMain.put(TITLE, this.message);
            jsonMain.put(TYPE, this.type);
            jsonMain.put(OID, this.oID);
            jsonMain.put(PUSH_ID, this.pushId);
            jsonMain.put(IMAGE, this.image);
//            jsonMain.put(RESPONSE, this.response != null ? new JSONObject(response) : new JSONObject());
            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
