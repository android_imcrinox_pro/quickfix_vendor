package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class OrderStatusUpdateModel implements Serializable {
    private final String TAG = "OrderStatusUpdateModel";

    private final String
            ORDER_ID                  = "order_id",
            STATUS                    = "status",
            REASON                    = "reason",
            COMMENT                   = "comment";

    String
            orderId                = null,
            status                 = null,
            reason                 = null,
            comment                = null;

    public OrderStatusUpdateModel(){}

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            if(json.has(ORDER_ID)){this.orderId = json.getString(ORDER_ID);}
            if(json.has(STATUS)){this.status = json.getString(STATUS);}
            if(json.has(REASON)){this.reason = json.getString(REASON);}
            if(json.has(COMMENT)){this.comment = json.getString(COMMENT);}

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);
            return false;}

    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ORDER_ID, this.orderId);
            jsonMain.put(STATUS, this.status);
            jsonMain.put(REASON, this.reason);
            jsonMain.put(COMMENT, this.comment);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }

}
