package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class CheckLocationModel implements Serializable {
    private final String TAG = "CheckLocationModel";
    private final String
            ID                     = "id",
            LATITUDE               = "lat",
            LONGITUDE              = "long";


    String
            id                   = null,
            longitude            = null,
            latitude             = null;

    public CheckLocationModel(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }


    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            if(json.has(ID))this.id = json.getString(ID);
            if(json.has(LATITUDE))this.latitude = json.getString(LATITUDE);
            if(json.has(LONGITUDE))this.longitude = json.getString(LONGITUDE);

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);
            return false;}

    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ID, id);
            jsonMain.put(LATITUDE, latitude);
            jsonMain.put(LONGITUDE, longitude);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
