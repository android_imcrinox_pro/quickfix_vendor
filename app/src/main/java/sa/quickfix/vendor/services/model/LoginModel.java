package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class LoginModel implements Serializable {

    private final String TAG = "LoginModel";
    private final String
            PHONE           = "mobile_number",
            PASSWORD        = "password",
            COUNTRY_CODE    = "country_code",
            UUID            = "uuid",
            FLAG            = "flag",
            SYSTEM_INFO     = "system_info",
            DEVICE_TYPE     = "device_type",
            OTP             = "otp",
            LOGIN_TOKEN     = "login_token";


    String
            password        = null,
            mobile          = null,
            countryCode     = null,
            uid             = null,
            deviceType      = "1",
            flag            = null,
            otp             = null,
            systemInfo      = null,
            loginToken      = null;

    public LoginModel(){}

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getSystemInfo() {
        return systemInfo;
    }

    public void setSystemInfo(String systemInfo) {
        this.systemInfo = systemInfo;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            password = json.getString(PASSWORD);
            mobile = json.getString(PHONE);
            countryCode = json.getString(COUNTRY_CODE);
            if(json.has(UUID)){this.uid = json.getString(UUID);}
            if(json.has(DEVICE_TYPE)){this.deviceType = json.getString(DEVICE_TYPE);}
            if(json.has(SYSTEM_INFO)){this.systemInfo = json.getString(SYSTEM_INFO);}
            if(json.has(LOGIN_TOKEN)){this.loginToken = json.getString(LOGIN_TOKEN);}
            if(json.has(FLAG))flag = json.getString(FLAG);
            if(json.has(OTP))otp = json.getString(OTP);
            return true;
        }catch(Exception ex){Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(PASSWORD, password);
            jsonMain.put(PHONE, mobile);
            jsonMain.put(COUNTRY_CODE, countryCode);
            jsonMain.put(UUID, uid);
            jsonMain.put(DEVICE_TYPE, deviceType);
            jsonMain.put(SYSTEM_INFO, systemInfo);
            jsonMain.put(LOGIN_TOKEN, loginToken);
            jsonMain.put(FLAG, this.flag);
            jsonMain.put(OTP, this.otp);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}

