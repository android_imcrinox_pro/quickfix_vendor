package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class UtilityModel implements Serializable {

    private final String TAG = "UtilityModel";

    private final String RESPONSE = "response";

    private final String
            CONTACT_NUMBER        = "contact_number",
            EMAIL_ID              = "email_id";

    String
            contactNumber    = null,
            emailId          = null;

    public UtilityModel(){}

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject jsonTemp = new JSONObject(jsonObject);
            JSONObject json = jsonTemp.getJSONObject(RESPONSE);

            if(json.has(CONTACT_NUMBER)){this.contactNumber = json.getString(CONTACT_NUMBER);}
            if(json.has(EMAIL_ID)){this.emailId = json.getString(EMAIL_ID);}

            return true;
        }catch(Exception ex){Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(CONTACT_NUMBER, contactNumber);
            jsonMain.put(EMAIL_ID, emailId);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}

