package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class CommentModel implements Serializable {
    private final String TAG = "CommentModel";
    private final String
            COMMENTS              = "comments",
            CREATED_ON            = "created_on",
            FROM                  = "from";

    private String
            comments           = null,
            createdOn          = null,
            from               = null;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public CommentModel(){}

    public boolean toObject(String jsonObject){
        try{

            JSONObject json = new JSONObject(jsonObject);

            if(json.has(COMMENTS)){this.comments = json.getString(COMMENTS);}
            if(json.has(CREATED_ON)){this.createdOn = json.getString(CREATED_ON);}
            if(json.has(FROM)){this.from = json.getString(FROM);}

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;

        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(COMMENTS, comments);
            jsonMain.put(CREATED_ON, createdOn);
            jsonMain.put(FROM, from);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}


