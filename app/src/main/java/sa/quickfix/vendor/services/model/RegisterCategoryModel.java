package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class RegisterCategoryModel implements Serializable {

    private final String TAG = "RegisterCategoryModel";

    private final String
            ID               = "id",
            TITLE            = "title",
            IMAGE            = "image",
            SELECTED         = "selected",
            SUB_CATEGORY     = "sub_category";

    String
            id               = null,
            title            = null,
            image            = null,
            selected         = null;

    SubCategoryListModel
            subCategoryListModel = null;

    public RegisterCategoryModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public SubCategoryListModel getSubCategoryListModel() {
        return subCategoryListModel;
    }

    public void setSubCategoryListModel(SubCategoryListModel subCategoryListModel) {
        this.subCategoryListModel = subCategoryListModel;
    }

    public boolean toObject(String jsonObject) {
        try {
            JSONObject json = new JSONObject(jsonObject);
            id = json.getString(ID);
            if (json.has(TITLE)) title = json.getString(TITLE);
            if (json.has(IMAGE)) image = json.getString(IMAGE);
            if (json.has(SELECTED)) selected = json.getString(SELECTED);

            if (json.has(SUB_CATEGORY)) {
                try {
                    SubCategoryListModel servicesListModel = new SubCategoryListModel();
                    if (servicesListModel.toObject(json.getJSONArray(SUB_CATEGORY))) {
                        this.subCategoryListModel = servicesListModel;
                    }
                } catch (Exception exx) {
                    this.subCategoryListModel = null;
                }
            }

            return true;
        } catch (Exception ex) {
            Log.d(TAG, "Json Exception : " + ex);
        }
        return false;
    }

    @Override
    public String toString() {
        String returnString = null;
        try {
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ID, id);
            jsonMain.put(TITLE, title);
            jsonMain.put(IMAGE, image);
            jsonMain.put(SELECTED, selected);
            jsonMain.put(SUB_CATEGORY, subCategoryListModel != null ? new JSONArray(subCategoryListModel.toString(true)) : new JSONArray());

            returnString = jsonMain.toString();
        } catch (Exception ex) {
            Log.d(TAG, " To String Exception : " + ex);
        }
        return returnString;
    }
}
