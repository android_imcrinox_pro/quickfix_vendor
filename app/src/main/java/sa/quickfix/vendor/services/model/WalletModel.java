package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class WalletModel implements Serializable {
    private final String TAG = "WalletModel";
    private final String
            REFERENCE_ID          = "reference_id",
            USAGED                = "usaged",
            BALANCE               = "balance",
            DATE                  = "date",
            TYPE                  = "type",
            FLAG                  = "flag",
            CATEGORY_TITLE        = "category_title",
            CATEGORY_ICON         = "category_icon";

    private String
            referenceId          = null,
            usaged               = null,
            balance              = null,
            date                 = null,
            type                 = null,
            flag                 = null,
            categoryTitle        = null,
            categoryIcon         = null;

    public WalletModel(){}

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getUsaged() {
        return usaged;
    }

    public void setUsaged(String usaged) {
        this.usaged = usaged;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(String categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public boolean toObject(String jsonObject){
        try{

            JSONObject json = new JSONObject(jsonObject);

            if(json.has(REFERENCE_ID)){this.referenceId = json.getString(REFERENCE_ID);}
            if(json.has(USAGED)){this.usaged = json.getString(USAGED);}
            if(json.has(BALANCE)){this.balance = json.getString(BALANCE);}
            if(json.has(DATE)){this.date = json.getString(DATE);}
            if(json.has(TYPE)){this.type = json.getString(TYPE);}
            if(json.has(FLAG)){this.flag = json.getString(FLAG);}
            if(json.has(CATEGORY_TITLE)){this.categoryTitle = json.getString(CATEGORY_TITLE);}
            if(json.has(CATEGORY_ICON)){this.categoryIcon = json.getString(CATEGORY_ICON);}

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;

        try{
            JSONObject jsonMain = new JSONObject();

            jsonMain.put(REFERENCE_ID, referenceId);
            jsonMain.put(USAGED, usaged);
            jsonMain.put(BALANCE, balance);
            jsonMain.put(DATE, date);
            jsonMain.put(TYPE, type);
            jsonMain.put(FLAG, flag);
            jsonMain.put(CATEGORY_TITLE, categoryTitle);
            jsonMain.put(CATEGORY_ICON, categoryIcon);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}


