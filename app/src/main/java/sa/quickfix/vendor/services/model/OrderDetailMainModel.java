package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class OrderDetailMainModel implements Serializable {
    private final String TAG = "OrderDetailMainModel";

    private final String
            WORK_ACTIVE                = "work_active",
            ORDER_NUMBER               = "order_number",
            ORDER_ID                   = "order_id",
            SUB_TOTAL                  = "sub_total",
            VAT                        = "vat",
            DISCOUNT_TOTAL             = "discount_total",
            WALLET_AMOUNT              = "wallet_amount",
            GRAND_TOTAL                = "grand_total",
            COMMISSION                 = "commission",
            DATE                       = "date",
            TIME                       = "time",
            ADDRESS                    = "address",
            LATITUDE                   = "latitude",
            LONGITUDE                  = "longitude",
            PHONE_NUMBER               = "phone_number",
            FULL_NAME                  = "full_name",
            LANDMARK                   = "landmark",
            PAYMENT_TYPE               = "payment_type",
            STATUS                     = "status",
            STATUS_TITLE               = "status_title",
            CATEGORY_ICON              = "category_icon",
            CATEGORY_TITLE             = "category_title",
            CATEGORY_ID                = "category_id",
            SERVICES                   = "services",
            USER_MOBILE_NUMBER         = "user_mobile_number",
            COMMUNICATION_NUMBER       = "communication_number",
            USER_NAME                  = "user_name",
            USER_IMAGE                 = "user_image",
            USER_RATING                = "user_rating",
            COUNTS                     = "counts",
            SUBSCRIPTION_DISCOUNT      = "subscription_discount",
            SPARE_PARTS_PRICE          = "spare_part_price",
            SPARE_PARTS_VAT            = "spare_part_vat",
            PAYMENT_STATUS             = "payment_status",
            COMMENTS                   = "comments",
            VENDOR_COMMENTS            = "vendor_comments",
            STATUS_LIST                = "status_list",
            ORDER_DETAIL               = "order_detail",
            ORDER_IMAGE                = "order_image",
            SPARE_PART                 = "spare_part",
            PAYABLE_AMOUNT             = "payable_amount";

    OrderImageListModel
            orderImageList                 = null;

    CommentsListModel
            commentsListModel = null;

    StatusListModel
            statusList                 = null;

    OrderDetailListModel
            orderDetailList            = null;

    SparePartsListModel
            sparePartsList             = null;

    private String
            workActive                = null,
            orderNumber               = null,
            orderId                   = null,
            subTotal                  = null,
            vat                       = null,
            discountTotal             = null,
            walletAmount              = null,
            grandTotal                = null,
            commission                = null,
            date                      = null,
            time                      = null,
            address                   = null,
            latitude                  = null,
            longitude                 = null,
            phoneNumber               = null,
            fullName                  = null,
            landmark                  = null,
            paymentType               = null,
            status                    = null,
            statusTitle               = null,
            categoryIcon              = null,
            categoryTitle             = null,
            categoryId                = null,
            services                  = null,
            userMobileNumber          = null,
            userName                  = null,
            userImage                 = null,
            userRating                = null,
            counts                    = null,
            subscriptionDiscount      = null,
            sparePartPrice            = null,
            comments                  = null,
            sparePartVat              = null,
            paymentStatus             = null,
            payableAmount             = null,
            communicationNumber       = null;

    public OrderDetailMainModel(){}

    public StatusListModel getStatusList() {
        return statusList;
    }

    public void setStatusList(StatusListModel statusList) {
        this.statusList = statusList;
    }

    public OrderDetailListModel getOrderDetailList() {
        return orderDetailList;
    }

    public void setOrderDetailList(OrderDetailListModel orderDetailList) {
        this.orderDetailList = orderDetailList;
    }

    public SparePartsListModel getSparePartsList() {
        return sparePartsList;
    }

    public void setSparePartsList(SparePartsListModel sparePartsList) {
        this.sparePartsList = sparePartsList;
    }

    public String getWorkActive() {
        return workActive;
    }

    public void setWorkActive(String workActive) {
        this.workActive = workActive;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getDiscountTotal() {
        return discountTotal;
    }

    public void setDiscountTotal(String discountTotal) {
        this.discountTotal = discountTotal;
    }

    public String getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(String walletAmount) {
        this.walletAmount = walletAmount;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public String getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(String categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getUserMobileNumber() {
        return userMobileNumber;
    }

    public void setUserMobileNumber(String userMobileNumber) {
        this.userMobileNumber = userMobileNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserRating() {
        return userRating;
    }

    public void setUserRating(String userRating) {
        this.userRating = userRating;
    }

    public String getCounts() {
        return counts;
    }

    public void setCounts(String counts) {
        this.counts = counts;
    }

    public String getSubscriptionDiscount() {
        return subscriptionDiscount;
    }

    public void setSubscriptionDiscount(String subscriptionDiscount) {
        this.subscriptionDiscount = subscriptionDiscount;
    }

    public String getSparePartPrice() {
        return sparePartPrice;
    }

    public void setSparePartPrice(String sparePartPrice) {
        this.sparePartPrice = sparePartPrice;
    }

    public String getSparePartVat() {
        return sparePartVat;
    }

    public void setSparePartVat(String sparePartVat) {
        this.sparePartVat = sparePartVat;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(String payableAmount) {
        this.payableAmount = payableAmount;
    }

    public String getCommunicationNumber() {
        return communicationNumber;
    }

    public void setCommunicationNumber(String communicationNumber) {
        this.communicationNumber = communicationNumber;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public OrderImageListModel getOrderImageList() {
        return orderImageList;
    }

    public void setOrderImageList(OrderImageListModel orderImageList) {
        this.orderImageList = orderImageList;
    }

    public CommentsListModel getCommentsListModel() {
        return commentsListModel;
    }

    public void setCommentsListModel(CommentsListModel commentsListModel) {
        this.commentsListModel = commentsListModel;
    }

    public boolean toObject(String jsonObjectString){
        try{
            JSONObject json = new JSONObject(jsonObjectString);

            if(json.has(WORK_ACTIVE)){this.workActive = json.getString(WORK_ACTIVE);}
            if(json.has(ORDER_NUMBER)){this.orderNumber = json.getString(ORDER_NUMBER);}
            if(json.has(ORDER_ID)){this.orderId = json.getString(ORDER_ID);}
            if(json.has(SUB_TOTAL)){this.subTotal = json.getString(SUB_TOTAL);}
            if(json.has(VAT)){this.vat = json.getString(VAT);}
            if(json.has(DISCOUNT_TOTAL)){this.discountTotal = json.getString(DISCOUNT_TOTAL);}
            if(json.has(WALLET_AMOUNT)){this.walletAmount = json.getString(WALLET_AMOUNT);}
            if(json.has(GRAND_TOTAL)){this.grandTotal = json.getString(GRAND_TOTAL);}
            if(json.has(COMMISSION)){this.commission = json.getString(COMMISSION);}
            if(json.has(DATE)){this.date = json.getString(DATE);}
            if(json.has(TIME)){this.time = json.getString(TIME);}
            if(json.has(ADDRESS)){this.address = json.getString(ADDRESS);}
            if(json.has(LATITUDE)){this.latitude = json.getString(LATITUDE);}
            if(json.has(LONGITUDE)){this.longitude = json.getString(LONGITUDE);}
            if(json.has(PHONE_NUMBER)){this.phoneNumber = json.getString(PHONE_NUMBER);}
            if(json.has(FULL_NAME)){this.fullName = json.getString(FULL_NAME);}
            if(json.has(LANDMARK)){this.landmark = json.getString(LANDMARK);}
            if(json.has(PAYMENT_TYPE)){this.paymentType = json.getString(PAYMENT_TYPE);}
            if(json.has(STATUS)){this.status = json.getString(STATUS);}
            if(json.has(STATUS_TITLE)){this.statusTitle = json.getString(STATUS_TITLE);}
            if(json.has(CATEGORY_ICON)){this.categoryIcon = json.getString(CATEGORY_ICON);}
            if(json.has(CATEGORY_TITLE)){this.categoryTitle = json.getString(CATEGORY_TITLE);}
            if(json.has(CATEGORY_ID)){this.categoryId = json.getString(CATEGORY_ID);}
            if(json.has(SERVICES)){this.services = json.getString(SERVICES);}
            if(json.has(USER_MOBILE_NUMBER)){this.userMobileNumber = json.getString(USER_MOBILE_NUMBER);}
            if(json.has(USER_NAME)){this.userName = json.getString(USER_NAME);}
            if(json.has(USER_IMAGE)){this.userImage = json.getString(USER_IMAGE);}
            if(json.has(USER_RATING)){this.userRating = json.getString(USER_RATING);}
            if(json.has(COUNTS)){this.counts = json.getString(COUNTS);}
            if(json.has(SUBSCRIPTION_DISCOUNT)){this.subscriptionDiscount = json.getString(SUBSCRIPTION_DISCOUNT);}
            if(json.has(SPARE_PARTS_PRICE)){this.sparePartPrice = json.getString(SPARE_PARTS_PRICE);}
            if(json.has(SPARE_PARTS_VAT)){this.sparePartVat = json.getString(SPARE_PARTS_VAT);}
            if(json.has(PAYMENT_STATUS)){this.paymentStatus = json.getString(PAYMENT_STATUS);}
            if(json.has(COMMENTS)){this.comments = json.getString(COMMENTS);}
            if(json.has(PAYABLE_AMOUNT)){this.payableAmount = json.getString(PAYABLE_AMOUNT);}
            if(json.has(COMMUNICATION_NUMBER)){this.communicationNumber = json.getString(COMMUNICATION_NUMBER);}

            if(json.has(VENDOR_COMMENTS)) {
                JSONArray array = json.getJSONArray(VENDOR_COMMENTS);
                CommentsListModel ListModel = new CommentsListModel();
                if(ListModel.toObject(array)){this.commentsListModel = ListModel;}
                else{this.commentsListModel = null;}
            }

            if(json.has(ORDER_IMAGE)) {
                JSONArray array = json.getJSONArray(ORDER_IMAGE);
                OrderImageListModel orderImageListModel = new OrderImageListModel();
                if(orderImageListModel.toObject(array)){this.orderImageList = orderImageListModel;}
                else{this.orderImageList = null;}
            }

            if(json.has(STATUS_LIST)) {
                JSONArray array = json.getJSONArray(STATUS_LIST);
                StatusListModel listModelLocal = new StatusListModel();
                if(listModelLocal.toObject(array)){this.statusList = listModelLocal;}
                else{this.statusList = null;}
            }

            if(json.has(ORDER_DETAIL)) {
                JSONArray array = json.getJSONArray(ORDER_DETAIL);
                OrderDetailListModel listModelLocal = new OrderDetailListModel();
                if(listModelLocal.toObject(array)){this.orderDetailList = listModelLocal;}
                else{this.orderDetailList = null;}
            }

            if(json.has(SPARE_PART)) {
                JSONArray array = json.getJSONArray(SPARE_PART);
                SparePartsListModel listModelLocal = new SparePartsListModel();
                if(listModelLocal.toObject(array)){this.sparePartsList = listModelLocal;}
                else{this.sparePartsList = null;}
            }

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(WORK_ACTIVE, workActive);
            jsonMain.put(ORDER_NUMBER, orderNumber);
            jsonMain.put(ORDER_ID, orderId);
            jsonMain.put(SUB_TOTAL, subTotal);
            jsonMain.put(VAT, vat);
            jsonMain.put(DISCOUNT_TOTAL, discountTotal);
            jsonMain.put(WALLET_AMOUNT, walletAmount);
            jsonMain.put(GRAND_TOTAL, grandTotal);
            jsonMain.put(COMMISSION, commission);
            jsonMain.put(DATE, date);
            jsonMain.put(TIME, time);
            jsonMain.put(ADDRESS, address);
            jsonMain.put(LATITUDE, latitude);
            jsonMain.put(LONGITUDE, longitude);
            jsonMain.put(PHONE_NUMBER, phoneNumber);
            jsonMain.put(FULL_NAME, fullName);
            jsonMain.put(LANDMARK, landmark);
            jsonMain.put(PAYMENT_TYPE, paymentType);
            jsonMain.put(STATUS, status);
            jsonMain.put(STATUS_TITLE, statusTitle);
            jsonMain.put(CATEGORY_ICON, categoryIcon);
            jsonMain.put(CATEGORY_TITLE, categoryTitle);
            jsonMain.put(CATEGORY_ID, categoryId);
            jsonMain.put(SERVICES, services);
            jsonMain.put(USER_MOBILE_NUMBER, userMobileNumber);
            jsonMain.put(USER_NAME, userName);
            jsonMain.put(USER_IMAGE, userImage);
            jsonMain.put(USER_RATING, userRating);
            jsonMain.put(COUNTS, counts);
            jsonMain.put(SUBSCRIPTION_DISCOUNT, subscriptionDiscount);
            jsonMain.put(SPARE_PARTS_PRICE, sparePartPrice);
            jsonMain.put(SPARE_PARTS_VAT, sparePartVat);
            jsonMain.put(PAYMENT_STATUS, paymentStatus);
            jsonMain.put(COMMENTS, comments);
            jsonMain.put(PAYABLE_AMOUNT, payableAmount);
            jsonMain.put(COMMUNICATION_NUMBER, communicationNumber);
            jsonMain.put(ORDER_IMAGE, orderImageList!=null?new JSONArray(orderImageList.toString(true)):null);
            jsonMain.put(STATUS_LIST, statusList!=null?new JSONArray(statusList.toString(true)):null);
            jsonMain.put(VENDOR_COMMENTS, commentsListModel!=null?new JSONArray(commentsListModel.toString(true)):null);
            jsonMain.put(ORDER_DETAIL, orderDetailList!=null?new JSONArray(orderDetailList.toString(true)):null);
            jsonMain.put(SPARE_PART, sparePartsList!=null?new JSONArray(sparePartsList.toString(true)):null);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){
            Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }

}


