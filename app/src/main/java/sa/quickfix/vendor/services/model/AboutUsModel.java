package sa.quickfix.vendor.services.model;
import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class AboutUsModel implements Serializable {
    private final String TAG = "AboutUsModel";
    private final String
            ID                     = "id",
            TITLE                  = "title",
            DESC                   = "description";


    String
            id                   = null,
            title                = null,
            description          = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            if(json.has(ID))this.id = json.getString(ID);
            if(json.has(TITLE))this.title = json.getString(TITLE);
            if(json.has(DESC))this.description = json.getString(DESC);

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);
            return false;}
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ID, id);
            jsonMain.put(TITLE, title);
            jsonMain.put(DESC, description);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){
            Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
