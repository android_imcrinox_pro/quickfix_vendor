package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OrderImageListModel implements Serializable {

    private final String TAG = "OrderImageListModel";

    private final String RESPONSE = "order_image";

    List<OrderImageModel> orderImageList = new ArrayList<OrderImageModel>();

    public OrderImageListModel(){}

    public List<OrderImageModel> getOrderImageList() {
        return orderImageList;
    }

    public void setOrderImageList(List<OrderImageModel> orderImageList) {
        this.orderImageList = orderImageList;
    }

    public ArrayList<String> getImages(){
        ArrayList<String> list = new ArrayList<String>();
        for(int i =0 ;i<this.orderImageList.size(); i++){
            list.add(orderImageList.get(i).getImage());
        }
        return list;
    }

    public boolean toObject(String jsonObjectString){
        try{
            JSONObject json = new JSONObject(jsonObjectString);
            JSONArray array = json.getJSONArray(RESPONSE);
            List<OrderImageModel> list = new ArrayList<OrderImageModel>();
            for (int i=0;i<array.length();i++){
                JSONObject jsonObject = array.getJSONObject(i);
                OrderImageModel model = new OrderImageModel();
                model.toObject(jsonObject.toString());
                list.add(model);
            }
            this.orderImageList = list;
            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    public boolean toObject(JSONArray jsonArray){
        try{
            List<OrderImageModel> list = new ArrayList<OrderImageModel>();
            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                OrderImageModel model = new OrderImageModel();
                model.toObject(jsonObject.toString());
                list.add(model);
            }
            this.orderImageList = list;
            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            List<OrderImageModel> list = this.orderImageList;
            for(int i=0;i<list.size();i++){
                jsonArray.put(new JSONObject(list.get(i).toString()));
            }
            jsonMain.put(RESPONSE, jsonArray);
            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }

    public String toString(boolean isArray){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            List<OrderImageModel> list = this.orderImageList;
            for(int i=0;i<list.size();i++){
                jsonArray.put(new JSONObject(list.get(i).toString()));
            }
            if(!isArray){
                jsonMain.put(RESPONSE, jsonArray);
                returnString = jsonMain.toString();
            }else{
                returnString = jsonArray.toString();
            }

        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
