package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class SparePartModel implements Serializable {
    private final String TAG = "SparePartModel";
    private final String
            ID               = "id",
            TITLE            = "title",
            IMAGE            = "image",
            UNIT_PRICE       = "unit_price",
            QUANTITY         = "quantity",
            FIXING_PRICE     = "fixing_price",
            TIME             = "time",
            PRICE            = "price",
            STATUS           = "status",
            STATUS_TITLE     = "status_title",
            HOUR             = "hour",
            MINUTE           = "minute";

    private String
            id              = null,
            title           = null,
            image           = null,
            unitPrice       = null,
            quantity        = null,
            fixingPrice     = null,
            time            = null,
            price           = null,
            status          = null,
            statusTitle     = null,
            hour            = null,
            minute          = null;

    public SparePartModel(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getFixingPrice() {
        return fixingPrice;
    }

    public void setFixingPrice(String fixingPrice) {
        this.fixingPrice = fixingPrice;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public boolean toObject(String jsonObject){
        try{

            JSONObject json = new JSONObject(jsonObject);

            if(json.has(ID)){this.id = json.getString(ID);}
            if(json.has(TITLE)){this.title = json.getString(TITLE);}
            if(json.has(IMAGE)){this.image = json.getString(IMAGE);}
            if(json.has(UNIT_PRICE)){this.unitPrice = json.getString(UNIT_PRICE);}
            if(json.has(QUANTITY)){this.quantity = json.getString(QUANTITY);}
            if(json.has(FIXING_PRICE)){this.fixingPrice = json.getString(FIXING_PRICE);}
            if(json.has(TIME)){this.time = json.getString(TIME);}
            if(json.has(PRICE)){this.price = json.getString(PRICE);}
            if(json.has(STATUS)){this.status = json.getString(STATUS);}
            if(json.has(STATUS_TITLE)){this.statusTitle = json.getString(STATUS_TITLE);}
            if(json.has(HOUR)){this.hour = json.getString(HOUR);}
            if(json.has(MINUTE)){this.minute = json.getString(MINUTE);}

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;

        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ID, id);
            jsonMain.put(TITLE, title);
            jsonMain.put(IMAGE, image);
            jsonMain.put(UNIT_PRICE, unitPrice);
            jsonMain.put(QUANTITY, quantity);
            jsonMain.put(FIXING_PRICE, fixingPrice);
            jsonMain.put(TIME, time);
            jsonMain.put(PRICE, price);
            jsonMain.put(STATUS, status);
            jsonMain.put(STATUS_TITLE, statusTitle);
            jsonMain.put(HOUR, hour);
            jsonMain.put(MINUTE, minute);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}


