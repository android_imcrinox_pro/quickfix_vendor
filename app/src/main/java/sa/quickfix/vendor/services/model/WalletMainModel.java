package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class WalletMainModel implements Serializable {
    private final String TAG = "WalletMainModel";

    private final String
            CURRENT_WALLET_BALANCE     = "current_wallet_balance",
            MIN_ADD_AMOUNT             = "min_rechargable_amount",
            RESPONSE                   = "response";

    private String
            currentWalletBalance       = null,
            minAddAmount               = null;

    WalletListModel
            walletList             = null;

    public WalletMainModel(){}

    public String getCurrentWalletBalance() {
        return currentWalletBalance;
    }

    public void setCurrentWalletBalance(String currentWalletBalance) {
        this.currentWalletBalance = currentWalletBalance;
    }

    public WalletListModel getWalletList() {
        return walletList;
    }

    public void setWalletList(WalletListModel walletList) {
        this.walletList = walletList;
    }

    public String getMinAddAmount() {
        return minAddAmount;
    }

    public void setMinAddAmount(String minAddAmount) {
        this.minAddAmount = minAddAmount;
    }

    public boolean toObject(String jsonObjectString){
        try{
            JSONObject json = new JSONObject(jsonObjectString);

            if(json.has(CURRENT_WALLET_BALANCE)){this.currentWalletBalance = json.getString(CURRENT_WALLET_BALANCE);}
            if(json.has(MIN_ADD_AMOUNT)){this.minAddAmount = json.getString(MIN_ADD_AMOUNT);}

            if(json.has(RESPONSE)) {
                JSONArray array = json.getJSONArray(RESPONSE);
                WalletListModel listModelLocal = new WalletListModel();
                if(listModelLocal.toObject(array)){this.walletList = listModelLocal;}
                else{this.walletList = null;}
            }

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(CURRENT_WALLET_BALANCE, currentWalletBalance);
            jsonMain.put(MIN_ADD_AMOUNT, minAddAmount);
            jsonMain.put(RESPONSE, walletList!=null?new JSONArray(walletList.toString(true)):null);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){
            Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }

}


