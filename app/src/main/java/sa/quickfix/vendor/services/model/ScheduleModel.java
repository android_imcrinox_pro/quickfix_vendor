package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class ScheduleModel implements Serializable {

    private final String TAG = "ScheduleModel";


    private final String
            ID                    = "id",
            START_DATE            = "start_date",
            END_DATE              = "end_date",
            SHIFTS                = "shifts";

    String
            id               = null,
            startDate        = null,
            endDate          = null;

    ShiftsListModel shiftsListModel=null;

    public ScheduleModel(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public ShiftsListModel getShiftsListModel() {
        return shiftsListModel;
    }

    public void setShiftsListModel(ShiftsListModel shiftsListModel) {
        this.shiftsListModel = shiftsListModel;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject jsonTemp = new JSONObject(jsonObject);

            if(jsonTemp.has(ID)){this.id = jsonTemp.getString(ID);}
            if(jsonTemp.has(START_DATE)){this.startDate = jsonTemp.getString(START_DATE);}
            if(jsonTemp.has(END_DATE)){this.endDate = jsonTemp.getString(END_DATE);}

            if(jsonTemp.has(SHIFTS)) {
                JSONArray array = jsonTemp.getJSONArray(SHIFTS);
                ShiftsListModel listModelLocal = new ShiftsListModel();
                if(listModelLocal.toObject(array)){this.shiftsListModel = listModelLocal;}
                else{this.shiftsListModel = null;}
            }

            return true;
        }catch(Exception ex){Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ID, id);
            jsonMain.put(START_DATE, startDate);
            jsonMain.put(END_DATE, endDate);
            jsonMain.put(SHIFTS, shiftsListModel!=null?new JSONArray(shiftsListModel.toString(true)):null);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}

