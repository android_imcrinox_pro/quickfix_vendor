package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

public class HomePageModel {
    private final String TAG = "TestimonialListModel";

    private final String
            BANNERS                 = "slider",
            HOME_CATEGORY           = "category",
            FEEDBACK                = "feedback",
            SELECTED_COUNTRY        = "country";


    private CountryListModel
            selectedCountry = null;

    BannerListModel
            bannerList              = null;

    HomeCategoryListModel
            homeCategoryList        = null;

    String
            feedback               = null;


    public HomePageModel(){}

    public BannerListModel getBannerList() {
        return bannerList;
    }

    public void setBannerList(BannerListModel bannerList) {
        this.bannerList = bannerList;
    }

    public CountryListModel getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(CountryListModel selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public HomeCategoryListModel getHomeCategoryList() {
        return homeCategoryList;
    }

    public void setHomeCategoryList(HomeCategoryListModel homeCategoryList) {
        this.homeCategoryList = homeCategoryList;
    }
    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public boolean toObject(String jsonObjectString){
        try{
            JSONObject json = new JSONObject(jsonObjectString);

            if(json.has(BANNERS)) {
                JSONArray array = json.getJSONArray(BANNERS);
                BannerListModel listModelLocal = new BannerListModel();
                if(listModelLocal.toObject(array)){this.bannerList = listModelLocal;}
                else{this.bannerList = null;}
            }

            if(json.has(HOME_CATEGORY)) {
                JSONArray array = json.getJSONArray(HOME_CATEGORY);
                HomeCategoryListModel listModelLocal = new HomeCategoryListModel();
                if(listModelLocal.toObject(array)){this.homeCategoryList = listModelLocal;}
                else{this.homeCategoryList = null;}
            }


            if(json.has(SELECTED_COUNTRY)){
                JSONArray array = json.getJSONArray(SELECTED_COUNTRY);
                CountryListModel listModelLocal = new CountryListModel();
                if(listModelLocal.toObject(array)){this.selectedCountry = listModelLocal;}
                else{this.selectedCountry = null;}
            }

            if(json.has(FEEDBACK)){this.feedback = json.getString(FEEDBACK);}

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(BANNERS, bannerList!=null?new JSONArray(bannerList.toString(true)):null);
            jsonMain.put(HOME_CATEGORY, homeCategoryList!=null?new JSONArray(homeCategoryList.toString(true)):null);
            jsonMain.put(SELECTED_COUNTRY, selectedCountry!=null?new JSONArray(selectedCountry.toString(true)):null);
            jsonMain.put(FEEDBACK, this.feedback);
            returnString = jsonMain.toString();
        }
        catch (Exception ex){
            Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }

}


