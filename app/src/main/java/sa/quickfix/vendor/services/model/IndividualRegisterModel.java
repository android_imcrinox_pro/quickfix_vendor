package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class IndividualRegisterModel implements Serializable {
    private final String TAG = "IndividualRegisterModel";

    private final String
            COMPANY_ID            = "company_id",
            COUNTRY_CODE          = "country_code",
            PHONE                 = "mobile_number",
            EMAIL                 = "email_id",
            PASSWORD              = "password",
            CONFIRM_PASSWORD      = "confirm_password",
            PROFILE_IMAGE         = "profile_image",
            GENDER                = "gender",
            ID_NUMBER             = "id_number",
            ID_IMAGE              = "id_image",
            CERTIFICATE           = "cirtificate",
            FIRST_NAME            = "first_name",
            LAST_NAME             = "last_name",
            SPOKEN_LANGUAGES      = "spoken_languages",
            LANGUAGES_NAMES       = "languages_names",
            ADDRESS               = "address",
            LATITUDE              = "latitude",
            LONGITUDE             = "longitude",
            CITY_ID               = "city_id",
            CITY_NAME             = "city_name",
            COUNTRY_ID            = "country_id",
            NATIONALITY           = "nationality",
            EXPERIENCE_M          = "experience_m",
            EXPERIENCE            = "experience",
            SKILLS                = "skills",
            BANK_NAME             = "bank_name",
            BRANCH                = "branch",
            ACCOUNT_NUMBER        = "account_number",
            SWIFT_CODE            = "swift_code",
            ACCOUNT_HOLDER_NAME   = "account_holder_name",
            IBAN                  = "iban",
            CERTIFICATES_LIST     = "cirtificates",
            ID_IMAGES             = "id_images",
            CATEGORY              = "category",
            UUID                  = "uuid",
            SYSTEM_INFO           = "system_info",
            DEVICE_TYPE           = "device_type";

    String
            companyId             = null,
            countryCode           = null,
            mobileNumber          = null,
            emailId               = null,
            password              = null,
            profileImage          = null,
            gender                = null,
            idNumber              = null,
            idImage               = null,
            cirtificate           = null,
            firstName             = null,
            lastName              = null,
            spokenLanguages       = null,
            languagesNames        = null,
            address               = null,
            latitude              = null,
            longitude             = null,
            cityId                = null,
            cityName              = null,
            countryId             = null,
            nationality           = null,
            experienceM           = null,
            experience            = null,
            skills                = null,
            bankName              = null,
            branch                = null,
            accountNumber         = null,
            swiftCode             = null,
            accountHolderName     = null,
            iban                  = null,
            uid                   = null,
            deviceType            = "1",
            systemInfo            = null,
            confirmPassword       = null;

    CertificatesListModel
            certificatesList    = null;

    IdImagesListModel
            idImageList         = null;

    CategoryListModel
            categoryList        = null;

    public IndividualRegisterModel(){}

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getIdImage() {
        return idImage;
    }

    public void setIdImage(String idImage) {
        this.idImage = idImage;
    }

    public String getCirtificate() {
        return cirtificate;
    }

    public void setCirtificate(String cirtificate) {
        this.cirtificate = cirtificate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpokenLanguages() {
        return spokenLanguages;
    }

    public void setSpokenLanguages(String spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }

    public String getLanguagesNames() {
        return languagesNames;
    }

    public void setLanguagesNames(String languagesNames) {
        this.languagesNames = languagesNames;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getExperienceM() {
        return experienceM;
    }

    public void setExperienceM(String experienceM) {
        this.experienceM = experienceM;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getSystemInfo() {
        return systemInfo;
    }

    public void setSystemInfo(String systemInfo) {
        this.systemInfo = systemInfo;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public CertificatesListModel getCertificatesList() {
        return certificatesList;
    }

    public void setCertificatesList(CertificatesListModel certificatesList) {
        this.certificatesList = certificatesList;
    }

    public IdImagesListModel getIdImageList() {
        return idImageList;
    }

    public void setIdImageList(IdImagesListModel idImageList) {
        this.idImageList = idImageList;
    }

    public CategoryListModel getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(CategoryListModel categoryList) {
        this.categoryList = categoryList;
    }

    public boolean toObject(String jsonObject){
        try{
            JSONObject json = new JSONObject(jsonObject);
            if(json.has(COMPANY_ID)){this.companyId = json.getString(COMPANY_ID);}
            if(json.has(COUNTRY_CODE)){this.countryCode = json.getString(COUNTRY_CODE);}
            if(json.has(PHONE)){this.mobileNumber = json.getString(PHONE);}
            if(json.has(EMAIL)){this.emailId = json.getString(EMAIL);}
            if(json.has(PASSWORD)){this.password = json.getString(PASSWORD);}
            if(json.has(PROFILE_IMAGE)){this.profileImage = json.getString(PROFILE_IMAGE);}
            if(json.has(GENDER)){this.gender = json.getString(GENDER);}
            if(json.has(ID_NUMBER)){this.idNumber = json.getString(ID_NUMBER);}
            if(json.has(ID_IMAGE)){this.idImage = json.getString(ID_IMAGE);}
            if(json.has(CERTIFICATE)){this.cirtificate = json.getString(CERTIFICATE);}
            if(json.has(FIRST_NAME)){this.firstName = json.getString(FIRST_NAME);}
            if(json.has(LAST_NAME)){this.lastName = json.getString(LAST_NAME);}
            if(json.has(SPOKEN_LANGUAGES)){this.spokenLanguages = json.getString(SPOKEN_LANGUAGES);}
            if(json.has(LANGUAGES_NAMES)){this.languagesNames = json.getString(LANGUAGES_NAMES);}
            if(json.has(ADDRESS)){this.address = json.getString(ADDRESS);}
            if(json.has(LATITUDE)){this.latitude = json.getString(LATITUDE);}
            if(json.has(LONGITUDE)){this.longitude = json.getString(LONGITUDE);}
            if(json.has(CITY_ID)){this.cityId = json.getString(CITY_ID);}
            if(json.has(CITY_NAME)){this.cityName = json.getString(CITY_NAME);}
            if(json.has(COUNTRY_ID)){this.countryId = json.getString(COUNTRY_ID);}
            if(json.has(NATIONALITY)){this.nationality = json.getString(NATIONALITY);}
            if(json.has(EXPERIENCE_M)){this.experienceM = json.getString(EXPERIENCE_M);}
            if(json.has(EXPERIENCE)){this.experience = json.getString(EXPERIENCE);}
            if(json.has(SKILLS)){this.skills = json.getString(SKILLS);}
            if(json.has(BANK_NAME)){this.bankName = json.getString(BANK_NAME);}
            if(json.has(BRANCH)){this.branch = json.getString(BRANCH);}
            if(json.has(ACCOUNT_NUMBER)){this.accountNumber = json.getString(ACCOUNT_NUMBER);}
            if(json.has(SWIFT_CODE)){this.swiftCode = json.getString(SWIFT_CODE);}
            if(json.has(ACCOUNT_HOLDER_NAME)){this.accountHolderName = json.getString(ACCOUNT_HOLDER_NAME);}
            if(json.has(IBAN)){this.iban = json.getString(IBAN);}
            if(json.has(UUID)){this.uid = json.getString(UUID);}
            if(json.has(DEVICE_TYPE)){this.deviceType = json.getString(DEVICE_TYPE);}
            if(json.has(SYSTEM_INFO)){this.systemInfo = json.getString(SYSTEM_INFO);}
            if(json.has(CONFIRM_PASSWORD)){this.confirmPassword = json.getString(CONFIRM_PASSWORD);}

            if(json.has(CERTIFICATES_LIST)){
                try{
                    JSONArray jsonArray = json.getJSONArray(CERTIFICATES_LIST);
                    CertificatesListModel tempModel = new CertificatesListModel();
                    if(tempModel.toObject(jsonArray)) this.certificatesList = tempModel;
                    else this.certificatesList = null;
                }catch (Exception exx){
                    this.certificatesList = null;
                }
            }

            if(json.has(ID_IMAGES)){
                try{
                    JSONArray jsonArray = json.getJSONArray(ID_IMAGES);
                    IdImagesListModel tempModel = new IdImagesListModel();
                    if(tempModel.toObject(jsonArray)) this.idImageList = tempModel;
                    else this.idImageList = null;
                }catch (Exception exx){
                    this.idImageList = null;
                }
            }

            if(json.has(CATEGORY)){
                try{
                    JSONArray jsonArray = json.getJSONArray(CATEGORY);
                    CategoryListModel tempModel = new CategoryListModel();
                    if(tempModel.toObject(jsonArray)) this.categoryList = tempModel;
                    else this.categoryList = null;
                }catch (Exception exx){
                    this.categoryList = null;
                }
            }

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;
        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(COMPANY_ID, companyId);
            jsonMain.put(COUNTRY_CODE, countryCode);
            jsonMain.put(PHONE, mobileNumber);
            jsonMain.put(EMAIL, emailId);
            jsonMain.put(PASSWORD, password);
            jsonMain.put(PROFILE_IMAGE, profileImage);
            jsonMain.put(GENDER, gender);
            jsonMain.put(ID_NUMBER, idNumber);
            jsonMain.put(ID_IMAGE, idImage);
            jsonMain.put(CERTIFICATE, cirtificate);
            jsonMain.put(FIRST_NAME, firstName);
            jsonMain.put(LAST_NAME, lastName);
            jsonMain.put(SPOKEN_LANGUAGES, spokenLanguages);
            jsonMain.put(LANGUAGES_NAMES, languagesNames);
            jsonMain.put(ADDRESS, address);
            jsonMain.put(LATITUDE, latitude);
            jsonMain.put(LONGITUDE, longitude);
            jsonMain.put(CITY_ID, cityId);
            jsonMain.put(CITY_NAME, cityName);
            jsonMain.put(COUNTRY_ID, countryId);
            jsonMain.put(NATIONALITY, nationality);
            jsonMain.put(EXPERIENCE_M, experienceM);
            jsonMain.put(EXPERIENCE, experience);
            jsonMain.put(SKILLS, skills);
            jsonMain.put(BANK_NAME, bankName);
            jsonMain.put(BRANCH, branch);
            jsonMain.put(ACCOUNT_NUMBER, accountNumber);
            jsonMain.put(SWIFT_CODE, swiftCode);
            jsonMain.put(ACCOUNT_HOLDER_NAME, accountHolderName);
            jsonMain.put(IBAN, iban);
            jsonMain.put(UUID, uid);
            jsonMain.put(DEVICE_TYPE, deviceType);
            jsonMain.put(SYSTEM_INFO, systemInfo);
            jsonMain.put(CONFIRM_PASSWORD, confirmPassword);
            jsonMain.put(CERTIFICATES_LIST, certificatesList != null ? new JSONArray(this.certificatesList.toString(true)) : null);
            jsonMain.put(ID_IMAGES, idImageList != null ? new JSONArray(this.idImageList.toString(true)) : null);
            jsonMain.put(CATEGORY, categoryList != null ? new JSONArray(this.categoryList.toString(true)) : null);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}
