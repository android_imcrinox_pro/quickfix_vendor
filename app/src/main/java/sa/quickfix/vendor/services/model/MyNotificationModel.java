package sa.quickfix.vendor.services.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class MyNotificationModel implements Serializable {
    private final String TAG = "MyNotificationModel";
    private final String
            ID                = "id",
            TITLE             = "title",
            MESSAGE           = "message",
            ORDER_ID          = "order_id",
            TYPE              = "type",
            IMAGE             = "image",
            CREATED_ON        = "created_on";

    private String
            id             = null,
            title          = null,
            message        = null,
            orderId        = null,
            type           = null,
            image          = null,
            createdOn      = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public MyNotificationModel(){}

    public boolean toObject(String jsonObject){
        try{

            JSONObject json = new JSONObject(jsonObject);

            if(json.has(ID)){this.id = json.getString(ID);}
            if(json.has(TITLE)){this.title = json.getString(TITLE);}
            if(json.has(MESSAGE)){this.message = json.getString(MESSAGE);}
            if(json.has(ORDER_ID)){this.orderId = json.getString(ORDER_ID);}
            if(json.has(TYPE)){this.type = json.getString(TYPE);}
            if(json.has(IMAGE)){this.image = json.getString(IMAGE);}
            if(json.has(CREATED_ON)){this.createdOn = json.getString(CREATED_ON);}

            return true;
        }catch(Exception ex){
            Log.d(TAG, "Json Exception : " + ex);}
        return false;
    }

    @Override
    public String toString(){
        String returnString = null;

        try{
            JSONObject jsonMain = new JSONObject();
            jsonMain.put(ID, id);
            jsonMain.put(TITLE, title);
            jsonMain.put(MESSAGE, message);
            jsonMain.put(ORDER_ID, orderId);
            jsonMain.put(TYPE, type);
            jsonMain.put(IMAGE, image);
            jsonMain.put(CREATED_ON, createdOn);

            returnString = jsonMain.toString();
        }
        catch (Exception ex){Log.d(TAG," To String Exception : "+ex);}
        return returnString;
    }
}


