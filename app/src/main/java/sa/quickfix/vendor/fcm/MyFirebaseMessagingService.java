package sa.quickfix.vendor.fcm;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import sa.quickfix.vendor.SplashScreen;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.fcm.util.NotificationUtils;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.services.model.NotificationModel;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private NotificationUtils notificationUtils;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.e(TAG, "##########onMessageReceived From: #########" + remoteMessage.getFrom());
//        Log.e(TAG, "##########onMessageReceived From: #########" + remoteMessage.getNotification().getBody());
//        Log.e(TAG, "##########onMessageReceived From: #########" + remoteMessage.getNotification().getTitle());
//        Log.e(TAG, "##########onMessageReceived From: #########" + remoteMessage.getNotification().getIcon());
//        Log.e(TAG, "##########onMessageReceived From: #########" + remoteMessage.getData());
//        Log.e(TAG, "##########onMessageReceived From: #########" + remoteMessage.getMessageType());

        if (remoteMessage == null)
            return;
        // Check if message contains a data payload.

        if (!remoteMessage.getData().isEmpty()) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                if (!remoteMessage.getData().isEmpty()) {
                    Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

                    try {
                        JSONObject json = new JSONObject(remoteMessage.getData());
                        handleDataMessage(json, remoteMessage);
                    } catch (Exception e) {
                        Log.e(TAG, "Exception: " + e.getMessage());
                    }
                    // Check if message contains a notification payload.
                } else {
                    handleDataMessage(remoteMessage);
                }
              /*  if (remoteMessage.getData() != null) {
                    String title = (String) remoteMessage.getData().get("title");
                    String type = (String) remoteMessage.getData().get("type");
                    String massage = (String) remoteMessage.getData().get("body");

                    NotificationModel notificationModel = new NotificationModel();
                    notificationModel.setTitle(title);
                    notificationModel.setMessage(massage);
                    notificationModel.setType(type);
                    handleNotification(notificationModel, remoteMessage);
                }*/
            } catch (Exception excc) {
                Log.e(TAG, "Exception: " + excc.getMessage());
            }
        }
    }

    private void handleDataMessage(RemoteMessage remoteMessage) {
        NotificationModel notificationModel=new NotificationModel();
        notificationModel.setMessage(remoteMessage.getNotification().getBody());
        notificationModel.setTitle(remoteMessage.getNotification().getTitle());
        notificationModel.setImage(remoteMessage.getNotification().getIcon());
        handleNotification(notificationModel, remoteMessage);
    }

    private void handleNotification(NotificationModel notificationModel, RemoteMessage remoteMessage) {
        sendMyNotification(notificationModel);
      /*  int i = 0;

        *//*if(!TextUtils.isEmpty(notificationModel.getAlbum_id()))
            i= Integer.parseInt(notificationModel.getAlbum_id());
*//*
        Uri soundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.incoming_request);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        //icon appears in device notification bar and right hand corner of notification
        builder.setSmallIcon(R.drawable.ic_logo);

        Intent intent = getIntentToOpen(notificationModel);

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), i, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Set the intent that will fire when the user taps the notification.
        builder.setContentIntent(pendingIntent);
        // Large icon appears on the left of the notification
        builder.setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ic_logo));

        // Content title, which appears in large type at the top of the notification
        //  builder.setContentTitle(remoteMessage.getNotification().getTitle());
        builder.setContentTitle(notificationModel.getTitle());
        builder.setContentText(notificationModel.getMessage());
        builder.setWhen(System.currentTimeMillis());
        builder.setAutoCancel(true);
        builder.setSound(soundUri);
        builder.setShowWhen(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = builder.build();

        notification.sound = soundUri;
        notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.when = System.currentTimeMillis();
        notificationManager.notify(i, notification);
        GlobalFunctions.setNotification(this, true);*/

    }

    private void sendMyNotification(NotificationModel notificationModel) {

        Intent intent = SplashScreen.newInstance(getBaseContext(), notificationModel);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri soundUri = Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.notification);
        @SuppressLint("ResourceAsColor") NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "CH_ID")
                .setSmallIcon(R.drawable.ic_logo)
                .setColor(R.color.ColorAccent)
                .setContentTitle(notificationModel.getTitle())
                .setContentText(notificationModel.getMessage())
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            if (soundUri != null) {
                // Changing Default mode of notification
                notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
                // Creating an Audio Attribute
                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .build();

                // Creating Channel
                NotificationChannel notificationChannel = new NotificationChannel("CH_ID", "Testing_Audio", NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setSound(soundUri, audioAttributes);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }
        }
        mNotificationManager.notify(0, notificationBuilder.build());
    }

    private void handleDataMessage(JSONObject json, RemoteMessage remoteMessage) {
        Log.e(TAG, "push json: " + json.toString());
        Context context = getApplicationContext();
        NotificationModel notificationModel = new NotificationModel();
        if (notificationModel.toObject(json.toString())) {
            notificationModel.setMessage(remoteMessage.getNotification().getBody());
            handleNotification(notificationModel, remoteMessage);
            sendMessage(notificationModel);
        }
    }

    private Intent getIntentToOpen(NotificationModel notificationModel) {
        Intent intent = SplashScreen.newInstance(getApplicationContext(), notificationModel);
       // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            /*if(notificationModel.getTitle().equalsIgnoreCase(GlobalVariables.NOTIFICATION_TYPE.REQUEST.toString())){
            }*/
        return intent;
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    private void sendMessage(NotificationModel notificationModel) {
        Log.d("sender", "Broadcasting message");
        Intent intent = new Intent(GlobalVariables.NOTIFICATION_LOCAL_BROADCAST_KEY);
        // You can also include some extra data.
        intent.putExtra(GlobalVariables.NOTIFICATION_LOCAL_BROADCAST_NOTIFICATION_MODEL, notificationModel);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
