package sa.quickfix.vendor.full_image_view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Sivasabharish Chinnaswamy on 2/26/2016.
 */
public class ImageDownloader extends AsyncTask<Void, Void, Bitmap> {

    private static final String TAG = "GetBitmapImage";

    String url = null;
    Context context = null;
    LruBitmapCache cache = null;
    ImageLoaderInterface listener;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    public ImageDownloader(Context context, String url, LruBitmapCache cache, ImageLoaderInterface listener){
        this.context = context;
        this.url = url;
        this.cache = cache;
        this.listener = listener;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        Bitmap bitmap = getBitmapFromURL(url);
        if(cache!=null){cache.putBitmap(url,bitmap);}
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if(bitmap!=null){
            listener.onSuccess(cache, url, bitmap);
        }else{
            listener.onError();
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            return null;
        }
    }
}
