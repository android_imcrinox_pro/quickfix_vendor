package sa.quickfix.vendor.full_image_view;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

import java.util.ArrayList;


/**
 * Created by Sivasabharish Chinnaswamy on 2/26/2016.
 */
    public class CustomThumbnailsAdapter extends ArrayAdapter<String> implements ImageLoaderInterface{

    Activity context;
    ArrayList<String> imgID;
    LruBitmapCache cache;
    Integer placeholder = null;
    static int selected = -1;

    public CustomThumbnailsAdapter(Activity context, LruBitmapCache cache, ArrayList<String> imgID, Integer placeholder) {

        super(context, R.layout.custom_thumbnails_item,imgID);
        this.context = context;
        this.cache = cache;
        this.imgID = imgID;
        this.placeholder = placeholder;

    }

    static class ViewHolder{
        protected ImageView imageView;
        protected LinearLayout mainLayout;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;

        if(convertView == null){
            view = context.getLayoutInflater().inflate(R.layout.custom_thumbnails_item, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.mainLayout = (LinearLayout) view.findViewById(R.id.custom_thumbnails_item_layout);
            viewHolder.imageView = (ImageView) view.findViewById(R.id.custom_thumbnails_item_imageview);
           // viewHolder.progressBar = (ProgressBar) view.findViewById(R.id.custom_full_image_adapter_progress);
            view.setTag(viewHolder);
        }else{view = convertView;}

        final ViewHolder holder = (ViewHolder) view.getTag();

        String imageURL = imgID.get(position);

        Bitmap bitmap = null;

        if(cache!=null){bitmap = cache.getBitmap(imageURL);}

        if(bitmap == null) {
            if (placeholder != null) {
                holder.imageView.setImageResource(placeholder);
            } else {
                holder.imageView.setImageResource(R.mipmap.ic_launcher);
            }
        }
        else{holder.imageView.setImageBitmap(bitmap);}

        if(selected==position){holder.mainLayout.setBackgroundColor(getContext().getResources().getColor(R.color.ColorPrimary));}
        else{holder.mainLayout.setBackgroundColor(getContext().getResources().getColor(R.color.white));}

        if (GlobalFunctions.isNotNullValue(imageURL)) {
            SeparateProgress.loadImage(holder.imageView,imageURL, SeparateProgress.setProgress(context));
        }

        return view;

    }


    public static void setSelected(int position){
        selected = position;
    }

    public static int getSelected(){
        return selected;
    }

    @Override
    public void onSuccess(LruBitmapCache cache, String url, Bitmap bitmap) {
        if(cache!=null){this.notifyDataSetChanged();}
    }

    @Override
    public void onError() {

    }
}
