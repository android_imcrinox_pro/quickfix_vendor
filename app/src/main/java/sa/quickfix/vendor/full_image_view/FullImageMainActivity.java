package sa.quickfix.vendor.full_image_view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import sa.quickfix.vendor.R;

import java.util.ArrayList;

public class FullImageMainActivity extends AppCompatActivity {

    public static final String FULL_IMAGE_ACTIVITY_TITLE = "FullImageActivityTitle";
    public static final String FULL_IMAGE_ACTIVITY_ARRAY_URL = "FullImageActivityArrayURL";
    public static final String FULL_IMAGE_ACTIVITY_SELECTED_IMAGE = "FullImageActivitySelectedImage";
    public static LruBitmapCache cache = new LruBitmapCache();
    private ArrayList<String> list = new ArrayList<String>();
    int position=0;
    String title=null;
    static Activity activity;
    static Window window;

    TextView toolbar_title;
    ImageView toolbar_icon;

    private static FrameLayout thumbnailFragment;

    public static Intent newInstatnce(Context context, String title, ArrayList<String> stringArrayList, int position){
        Intent intent = new Intent(context, FullImageMainActivity.class);
        intent.putExtra(FULL_IMAGE_ACTIVITY_TITLE, title);
        intent.putStringArrayListExtra(FULL_IMAGE_ACTIVITY_ARRAY_URL, stringArrayList);
        intent.putExtra(FULL_IMAGE_ACTIVITY_SELECTED_IMAGE, position);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_main_activity);

        activity = this;
        window= getWindow();
        // cache = new LruBitmapCache();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_icon = (ImageView) toolbar.findViewById(R.id.toolbar_icon);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }


/*

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            //enable translucent statusbar via flags
            GlobalFunctions.setTranslucentStatusFlag(window, true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //we don't need the translucent flag this is handled by the theme
            GlobalFunctions.setTranslucentStatusFlag(window, true);
            //set the statusbarcolor transparent to remove the black shadow
            this.getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
*/

        thumbnailFragment = (FrameLayout) findViewById(R.id.thumbnail_container);

        list.clear();
        list.addAll(getIntent().getStringArrayListExtra(FULL_IMAGE_ACTIVITY_ARRAY_URL));

        this.title = getIntent().getExtras().getString(FULL_IMAGE_ACTIVITY_TITLE);
        this.position = getIntent().getExtras().getInt(FULL_IMAGE_ACTIVITY_SELECTED_IMAGE);

        setTitle(this.title);

        FullImageFragment fullImageFragment = FullImageFragment.newInstance(list);
        fullImageFragment.setImagePosition(position);
        getSupportFragmentManager().beginTransaction().replace(R.id.image_container, fullImageFragment).commit();

        ThumbnailFragment thumbnailsFragment = ThumbnailFragment.newInstance(list, R.mipmap.ic_launcher);
        thumbnailsFragment.setImagePosition(position);
        getSupportFragmentManager().beginTransaction().replace(R.id.thumbnail_container, thumbnailsFragment).commit();

        toolbar_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setTitle(String title){
        if(toolbar_title!=null){toolbar_title.setText(title!=null?title:getString(R.string.gallery));}
    }

    public static void setThumbnailVisibility(final int visibility){
        if(thumbnailFragment!=null){
            int duration = 300;
            float alpha = 0.0f;
            int height = thumbnailFragment.getHeight();
            if(visibility== View.VISIBLE){
                alpha = 1.0f;
                height = 0;
                thumbnailFragment.setVisibility(visibility);
            }
            thumbnailFragment.animate()
                    .translationY(height)
                    .alpha(alpha)
                    .setDuration(duration).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    thumbnailFragment.setVisibility(visibility);
                }
            });
        }
    }

    public static void toggleThumbnailVisibility(){
        if(thumbnailFragment!=null){
            if(thumbnailFragment.getVisibility()== View.VISIBLE){
                setThumbnailVisibility(View.GONE);
            }else{
                setThumbnailVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    protected void onDestroy() {
        cache = null;
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        closeThisActivity();
        super.onBackPressed();
    }

    public static void closeThisActivity(){
        if(activity!=null){
            activity.finish();
            //activity.overridePendingTransition(R.anim.zoom_in,R.anim.zoom_out);
        }
    }
}
