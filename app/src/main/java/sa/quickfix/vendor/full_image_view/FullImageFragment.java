package sa.quickfix.vendor.full_image_view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sa.quickfix.vendor.R;

import java.util.ArrayList;


/**
 * Created by Sivasabharish Chinnaswamy on 2/26/2016.
 */
public class FullImageFragment extends Fragment {

    private static final String FULL_IMAGE_FRAGMENT_IMAGE_URLS = "FullImageFrgamentImageURLS";
    //CustomThumbnailsAdapter customFullImageAdapter;

    ViewPagerAdapter adapter;

    ArrayList<String> list = new ArrayList<String>();
    static ViewPager viewPager;
    int imagePosition=0;
   // TwoWayView twoWayView;

    public static FullImageFragment newInstance(ArrayList<String> url){
        FullImageFragment fragment = new FullImageFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(FULL_IMAGE_FRAGMENT_IMAGE_URLS, url);
        fragment.setArguments(bundle);
        return fragment;
    }

    public int getImagePosition() {
        return imagePosition;
    }

    public void setImagePosition(int imagePosition) {
        this.imagePosition = imagePosition;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.full_image_layout,container,false);

       /* Clearing and adding items to list*/
        list.clear();
        list.addAll(getArguments().getStringArrayList(FULL_IMAGE_FRAGMENT_IMAGE_URLS));

      // customFullImageAdapter = new CustomThumbnailsAdapter(getActivity(),MainActivity.cache,list);

        adapter = new ViewPagerAdapter(getChildFragmentManager(), FullImageMainActivity.cache,list);
        viewPager = (ViewPager) view.findViewById(R.id.custom_full_image_adapter_ViewPager);
        viewPager.setAdapter(adapter);

        setViewPagerPosition(getImagePosition());

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //Toast.makeText(getActivity(), "" + position, Toast.LENGTH_SHORT).show();
                ThumbnailFragment.imageSet(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return view;
    }

    public static void setViewPagerPosition(int position){
        if(viewPager!=null){viewPager.setCurrentItem(position, true);}
    }
}
