package sa.quickfix.vendor.full_image_view;

import android.graphics.Bitmap;

/**
 * Created by Sivasabharish Chinnaswamy on 2/26/2016.
 */
public interface ImageLoaderInterface {
    void onSuccess(LruBitmapCache cache, String url, Bitmap bitmap);
    void onError();
}
