package sa.quickfix.vendor.full_image_view;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by Sivasabharish Chinnaswamy on 2/26/2016.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<String> images;
    LruBitmapCache cache;

    public ViewPagerAdapter(FragmentManager fm, LruBitmapCache cache, List<String> imagesList) {
        super(fm);
        this.cache = cache;
        this.images = imagesList;
    }

    @Override
    public Fragment getItem(int position) {
        return ViewPagerFragmentItems.getInstance(images.get(position),cache);

    }

    @Override
    public int getCount() {
        return images.size();
    }
}
