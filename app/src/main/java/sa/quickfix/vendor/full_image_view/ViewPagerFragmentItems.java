package sa.quickfix.vendor.full_image_view;

import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import sa.quickfix.vendor.R;


/**
 * Created by Sivasabharish Chinnaswamy on 2/26/2016.
 */
public class ViewPagerFragmentItems extends Fragment implements ImageLoaderInterface{

    private static final String VIEW_PAGER_FRAGMENT_ITEM_IMAGE_URL = "ViewpagerFragmentItemImageURL";
    private static final String VIEW_PAGER_FRAGMENT_ITEM_IMAGE_CACHE = "ViewpagerFragmentItemCACHE";
    private String url;
    private Bitmap bitmap;
    private LruBitmapCache cache;
    protected SubsamplingScaleImageView imageView;
    protected ProgressBar progressBar;

    public static ViewPagerFragmentItems getInstance(String url, LruBitmapCache cache) {
        ViewPagerFragmentItems f = new ViewPagerFragmentItems();
        Bundle args = new Bundle();
        args.putString(VIEW_PAGER_FRAGMENT_ITEM_IMAGE_URL, url);
        args.putSerializable(VIEW_PAGER_FRAGMENT_ITEM_IMAGE_CACHE, cache);
        f.setArguments(args);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString(VIEW_PAGER_FRAGMENT_ITEM_IMAGE_URL);
        cache = (LruBitmapCache)getArguments().getSerializable(VIEW_PAGER_FRAGMENT_ITEM_IMAGE_CACHE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.view_pager_items, container, false);

        imageView = (SubsamplingScaleImageView)view.findViewById(R.id.view_pager_items_imageView);
        progressBar = (ProgressBar)view.findViewById(R.id.view_pager_items_progress);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bitmap bitmap = null;
        if(cache!=null){
            bitmap = cache.getBitmap(url);
        }
        if(bitmap == null){
            progressBar.setVisibility(View.VISIBLE);
            ImageDownloader imageDownloader = new ImageDownloader(getActivity(), url, cache, this);
            imageDownloader.execute();
        }else{
            imageView.recycle();
            imageView.setImage(ImageSource.bitmap(bitmap));
            progressBar.setVisibility(View.GONE);
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FullImageMainActivity.toggleThumbnailVisibility();
            }
        });
    }

    private void setImageView(Bitmap bitmap){
        if(bitmap!=null){
            if(imageView!=null && progressBar!=null){
                imageView.recycle();
                imageView.setImage(ImageSource.bitmap(bitmap));
                progressBar.setVisibility(View.GONE);
            }
        }else{
            if(imageView!=null && progressBar!=null){
                imageView.recycle();
                //imageView.setImage(ImageSource.bitmap(bitmap));
                progressBar.setVisibility(View.GONE);
            }
        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSuccess(LruBitmapCache cache, String url, Bitmap bitmap) {
        if(cache!=null){
            setImageView(cache.getBitmap(url));
            ThumbnailFragment.imageSet();
        }else{
            setImageView(bitmap);
        }
    }

    @Override
    public void onError() {

    }
}
