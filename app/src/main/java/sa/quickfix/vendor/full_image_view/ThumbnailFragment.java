package sa.quickfix.vendor.full_image_view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import sa.quickfix.vendor.R;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;

/**
 * Created by Sivasabharish Chinnaswamy on 2/27/2016.
 */

public class ThumbnailFragment extends Fragment {

    private static final String THUMBNAIL_FRAGMENT_IMAGE_URLS = "ThumbnailFrgamentImageURLS";
    private static final String THUMBNAIL_FRAGMENT_PLACE_HOLDER = "ThumbnailFrgamentPlaceHolder";

    ArrayList<String> list = new ArrayList<String>();
    Integer placeholder = null;
    static TwoWayView twoWayView;
    static CustomThumbnailsAdapter adapter;
    int imagePosition=0;

    public static ThumbnailFragment newInstance(ArrayList<String> url, @Nullable Integer placeholderResourceID){
        ThumbnailFragment fragment = new ThumbnailFragment();
        Bundle bundle = new Bundle();
        if(placeholderResourceID==null){placeholderResourceID=0;}
        bundle.putStringArrayList(THUMBNAIL_FRAGMENT_IMAGE_URLS, url);
        bundle.putInt(THUMBNAIL_FRAGMENT_PLACE_HOLDER, placeholderResourceID);
        fragment.setArguments(bundle);
        return fragment;
    }

    public int getImagePosition() {
        return imagePosition;
    }

    public void setImagePosition(int imagePosition) {
        this.imagePosition = imagePosition;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.thumbnail_fragment,container,false);

               /* Clearing and adding items to list*/
        list.clear();
        list.addAll(getArguments().getStringArrayList(THUMBNAIL_FRAGMENT_IMAGE_URLS));

        placeholder = getArguments().getInt(THUMBNAIL_FRAGMENT_PLACE_HOLDER);

        if(placeholder==0){placeholder=null;}

        adapter = new CustomThumbnailsAdapter(getActivity(), FullImageMainActivity.cache, list, placeholder);
        twoWayView = (TwoWayView) view.findViewById(R.id.thumbnails_fragment_twowayview);

        twoWayView.setAdapter(adapter);

        imageSet(getImagePosition());

        twoWayView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FullImageFragment.setViewPagerPosition(position);
            }
        });

        CustomThumbnailsAdapter.setSelected(getImagePosition());
        return view;
    }

    public static void imageSet(){
        if(adapter!=null){
            synchronized (adapter){adapter.notifyDataSetChanged();}
            if(CustomThumbnailsAdapter.getSelected()>=0){twoWayView.setSelectionFromOffset(CustomThumbnailsAdapter.getSelected(),1);}
        }
    }

    public static void imageSet(int position){
        if(adapter!=null && position>=0){
            CustomThumbnailsAdapter.setSelected(position);
            synchronized (adapter){adapter.notifyDataSetChanged();}
            twoWayView.setSelectionFromOffset(CustomThumbnailsAdapter.getSelected(),1);
        }
    }

}
