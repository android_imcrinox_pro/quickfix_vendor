package sa.quickfix.vendor.flows.bank_details.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import sa.quickfix.vendor.flows.about_us.AboutUsActivity;
import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.BankDetailsModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;

import com.facebook.FacebookSdk;


public class EditBankDetailsActivity extends AppCompatActivity {

    public static final String TAG = "EditBankDetailsActivity";

    Context context;
    private static Activity activity;
    View mainView;

    BankDetailsModel bankDetailsModel = null;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    private CardView save_cardview;
    private ImageView back_iv;
    private AppCompatCheckBox terms_checkbox;
    private TextView terms_and_conditions_tv,privacy_policy_tv;
    private EditText account_number_etv, iban_etv, holder_name_etv, bank_branch_address_etv, bank_name_etv, swift_code_etv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.edit_bank_details_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);
        terms_checkbox = (AppCompatCheckBox) findViewById(R.id.terms_checkbox);

        terms_and_conditions_tv = (TextView) findViewById(R.id.terms_and_conditions_tv);
        privacy_policy_tv = (TextView) findViewById(R.id.privacy_policy_tv);

        account_number_etv = (EditText) findViewById(R.id.account_number_etv);
        iban_etv = (EditText) findViewById(R.id.iban_etv);
        holder_name_etv = (EditText) findViewById(R.id.holder_name_etv);
        bank_branch_address_etv = (EditText) findViewById(R.id.bank_branch_address_etv);
        bank_name_etv = (EditText) findViewById(R.id.bank_name_etv);
        swift_code_etv = (EditText) findViewById(R.id.swift_code_etv);

        save_cardview = (CardView) findViewById(R.id.save_cardview);

        mainView = save_cardview;

        setThisPage();

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        terms_and_conditions_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = AboutUsActivity.newInstance(activity, GlobalVariables.PAGE_LOGIN_TNC);
                startActivity(intent);
//                GlobalFunctions.openBrowser(context,GlobalVariables.HTTP_URL_TERMS_AND_CONDITION);
            }
        });
        privacy_policy_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = AboutUsActivity.newInstance(activity, GlobalVariables.PAGE_PP);
                startActivity(intent);
//                GlobalFunctions.openBrowser(context,GlobalVariables.HTTP_URL_TERMS_AND_CONDITION);
            }
        });

    }

    private void validateInput() {
        if (context != null) {
            String
                    accountNumber = account_number_etv.getText().toString().trim(),
                    iban = iban_etv.getText().toString().trim();
//                    holderName = holder_name_etv.getText().toString().trim(),
//                    bankBranchAddress = bank_branch_address_etv.getText().toString().trim(),
//                    bankName = bank_name_etv.getText().toString().trim(),
//                    swiftCode = swift_code_etv.getText().toString().trim();

            if (accountNumber.isEmpty()) {
                account_number_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                account_number_etv.setFocusableInTouchMode(true);
                account_number_etv.requestFocus();
            } else if (iban.isEmpty()) {
                iban_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                iban_etv.setFocusableInTouchMode(true);
                iban_etv.requestFocus();
            } /*else if (holderName.isEmpty()) {
                holder_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                holder_name_etv.setFocusableInTouchMode(true);
                holder_name_etv.requestFocus();
            } else if (bankBranchAddress.isEmpty()) {
                bank_branch_address_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                bank_branch_address_etv.setFocusableInTouchMode(true);
                bank_branch_address_etv.requestFocus();
            } else if (bankName.isEmpty()) {
                bank_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                bank_name_etv.setFocusableInTouchMode(true);
                bank_name_etv.requestFocus();
            } else if (swiftCode.isEmpty()) {
                swift_code_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                swift_code_etv.setFocusableInTouchMode(true);
                swift_code_etv.requestFocus();
            }*/ else if (!terms_checkbox.isChecked()) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_select_terms_and_conditions));
            } else {

                if (bankDetailsModel == null) {
                    bankDetailsModel = new BankDetailsModel();
                }
//                bankDetailsModel.setBankName(bankName);
//                bankDetailsModel.setAccountHolderName(holderName);
//                bankDetailsModel.setBranch(bankBranchAddress);
                bankDetailsModel.setIban(iban);
                bankDetailsModel.setAccountNumber(accountNumber);
//                bankDetailsModel.setSwiftCode(swiftCode);
                updateBankDetails(context, bankDetailsModel);
            }

        }
    }

    private void updateBankDetails(final Context context, final BankDetailsModel model) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.updateBankDetails(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "UpdateBankDetails");
    }

    private void validateOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            if (!statusModel.isStatus()) {
                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            } else {
                showAlertMessage(statusModel.getMessage());
            }
        }
    }

    private void showAlertMessage(String message) {

        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(EditBankDetailsActivity.this, MainActivity.class);
                startActivity(intent);
                closeThisActivity();
            }
        });

        alertDialog.show();
    }

    private void setThisPage() {

        ProfileModel profileModel = globalFunctions.getProfile(context);
        if (profileModel != null) {

            if (profileModel.getAccountNumber() != null) {
                account_number_etv.setText(profileModel.getAccountNumber());
            }

            if (profileModel.getIban() != null) {
                iban_etv.setText(profileModel.getIban());
            }

            /*if (profileModel.getAccountHolderName() != null) {
                holder_name_etv.setText(profileModel.getAccountHolderName());
            }

            if (profileModel.getBranch() != null) {
                bank_branch_address_etv.setText(profileModel.getBranch());
            }

            if (profileModel.getBankName() != null) {
                bank_name_etv.setText(profileModel.getBankName());
            }

            if (profileModel.getSwiftCode() != null) {
                swift_code_etv.setText(profileModel.getSwiftCode());
            }*/

            save_cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    validateInput();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}