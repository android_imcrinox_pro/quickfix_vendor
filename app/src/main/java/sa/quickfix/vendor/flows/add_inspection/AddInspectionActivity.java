package sa.quickfix.vendor.flows.add_inspection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.MyOptionsPickerView;
import com.facebook.FacebookSdk;

import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.AddInspectionListModel;
import sa.quickfix.vendor.services.model.AddInspectionModel;
import sa.quickfix.vendor.services.model.OrderDetailMainModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;


public class AddInspectionActivity extends AppCompatActivity {

    public static final String TAG = "AddInspectionActivity";

    public static final String BUNDLE_ORDER_DETAIL_MAIN_MODEL = "BundleOrderDetailMainModel";
    public static final String BUNDLE_SELECTED_HOUR = "BundleSelectedHour";
    public static final String BUNDLE_SELECTED_MINUTE = "BundleSelectedMinute";

    Context context;
    private static Activity activity;
    View mainView;

    String orderDetailId = null;
    String serviceTitle = null;
    String inspectionAMount = null,hours = "",mins = "";

    OrderDetailMainModel orderDetailMainModel = null;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    private CardView send_for_approval_cardview;
    private ImageView back_iv;
    private TextView service_title_tv, inspection_amount_tv, hours_etv, minutes_etv;

    boolean
            isHourSelected = false,
            isMinuteSelected = false;

    MyOptionsPickerView twoPicker;

    ArrayList<String> hourlist = new ArrayList<>();
    ArrayList<String> timelist = new ArrayList<>();

    String index = "0";
    String size = "50";

    public static Intent newInstance(Activity activity, OrderDetailMainModel orderDetailMainModel) {
        Intent intent = new Intent(activity, AddInspectionActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_ORDER_DETAIL_MAIN_MODEL, orderDetailMainModel);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.add_inspection_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);

        service_title_tv = (TextView) findViewById(R.id.service_title_tv);
        inspection_amount_tv = (TextView) findViewById(R.id.inspection_amount_tv);


        service_title_tv = (TextView) findViewById(R.id.service_title_tv);
        hours_etv = (TextView) findViewById(R.id.hours_etv);
        minutes_etv = (TextView) findViewById(R.id.minutes_etv);

        send_for_approval_cardview = (CardView) findViewById(R.id.send_for_approval_cardview);

        mainView = send_for_approval_cardview;

        if (getIntent().hasExtra(BUNDLE_ORDER_DETAIL_MAIN_MODEL)) {
            orderDetailMainModel = (OrderDetailMainModel) getIntent().getSerializableExtra(BUNDLE_ORDER_DETAIL_MAIN_MODEL);
        } else {
            orderDetailMainModel = null;
        }

        if (orderDetailMainModel != null) {

            if (orderDetailMainModel.getOrderDetailList() != null && orderDetailMainModel.getOrderDetailList().getOrderDetailList().size() >= 0) {
                orderDetailId = orderDetailMainModel.getOrderDetailList().getOrderDetailList().get(0).getOrderDetailId();
                serviceTitle = orderDetailMainModel.getOrderDetailList().getOrderDetailList().get(0).getServiceTitle();
//                inspectionAMount = orderDetailMainModel.getOrderDetailList().getOrderDetailList().get(0).getInspectionPrice();

                if (GlobalFunctions.isNotNullValue( orderDetailMainModel.getOrderDetailList().getOrderDetailList().get(0).getInspectionTime())){
                    try {
                        String tmpvalue= orderDetailMainModel.getOrderDetailList().getOrderDetailList().get(0).getInspectionTime();
                        String[] tmpSplitText= tmpvalue.split(":");

                        if (tmpSplitText.length>1){
                            hours=tmpSplitText[0];
                            mins=tmpSplitText[1];
                        }

                        hours_etv.setText(hours);
                        minutes_etv.setText(mins);

                    }catch (Exception e){

                    }
                }

                service_title_tv.setText(serviceTitle);
//                inspection_amount_tv.setText(activity.getString(R.string.sar)+""+ inspectionAMount);
            }
        }

        hours_etv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openTwoPickerView();
//                Intent intent = TimeListActivity.newInstance(activity, globalVariables.FROM_HOUR);
//                startActivityForResult(intent, globalVariables.REQUEST_CODE_FOR_HOUR);
            }
        });

        minutes_etv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openTwoPickerView();
//                Intent intent = TimeListActivity.newInstance(activity, globalVariables.FROM_MINUTE);
//                startActivityForResult(intent, globalVariables.REQUEST_CODE_FOR_MINUTE);
            }
        });

        send_for_approval_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        hourlist.clear();
        timelist.clear();
        setUpListForHour();
        setUpListForMinute();

        twoPicker = new MyOptionsPickerView(activity);

        twoPicker.setPicker(hourlist, timelist, false);
        twoPicker.setTitle("");
        twoPicker.setCyclic(false, false, false);
        twoPicker.setSelectOptions(1, 1);
        twoPicker.setSubmitButtonText(activity.getString(R.string.submit));
        twoPicker.setCancelButtonText(activity.getString(R.string.cancel));

        twoPicker.setOnoptionsSelectListener(new MyOptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {

                if (hourlist.get(options1) != null) {
                    isHourSelected = true;
                    hours_etv.setText(hourlist.get(options1).replaceAll(" hours",""));
                }

                if (timelist.get(option2) != null) {
                    isMinuteSelected = true;
                    minutes_etv.setText(timelist.get(option2).replaceAll(" min",""));
                }

//                Toast.makeText(activity, "" + hourlist.get(options1) + " " + timelist.get(option2), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void openTwoPickerView() {
        if (GlobalFunctions.isKeyboardOpen(activity)) {
            GlobalFunctions.closeKeyboard(activity);
        }
        twoPicker.show();
    }

    private void setUpListForMinute() {
        String min = " min";
        timelist.clear();
        for (int i = 0; i < 60; i++) {
            if (i == 0) {
                timelist.add("00"+min);
            } else if (i == 1) {
                timelist.add("01"+min);
            } else if (i == 2) {
                timelist.add("02"+min);
            } else if (i == 3) {
                timelist.add("03"+min);
            } else if (i == 4) {
                timelist.add("04"+min);
            } else if (i == 5) {
                timelist.add("05"+min);
            } else if (i == 6) {
                timelist.add("06"+min);
            } else if (i == 7) {
                timelist.add("07"+min);
            } else if (i == 8) {
                timelist.add("08"+min);
            } else if (i == 9) {
                timelist.add("09"+min);
            } else {
                timelist.add(i +min);
            }
        }

    }

    private void setUpListForHour() {
        String hours = " hours";
        hourlist.clear();
        for (int i = 0; i < 100; i++) {
            if (i == 0) {
                hourlist.add("00"+hours);
            } else if (i == 1) {
                hourlist.add("01"+hours);
            } else if (i == 2) {
                hourlist.add("02"+hours);
            } else if (i == 3) {
                hourlist.add("03"+hours);
            } else if (i == 4) {
                hourlist.add("04"+hours);
            } else if (i == 5) {
                hourlist.add("05"+hours);
            } else if (i == 6) {
                hourlist.add("06"+hours);
            } else if (i == 7) {
                hourlist.add("07"+hours);
            } else if (i == 8) {
                hourlist.add("08"+hours);
            } else if (i == 9) {
                hourlist.add("09"+hours);
            } else {
                hourlist.add(i + hours);
            }
        }
    }

    private void validateInput() {
        if (context != null) {
            String
                    hours = hours_etv.getText().toString().trim(),
                    minutes = minutes_etv.getText().toString().trim();

            if (!isHourSelected) {
                globalFunctions.displayMessaage(context, mainView, getString(R.string.select_time));
            } else if (hours.equalsIgnoreCase("00") && minutes.equalsIgnoreCase("00")) {
                globalFunctions.displayMessaage(context, mainView, getString(R.string.select_valid_time));
            }else {
                AddInspectionListModel addInspectionListModel = new AddInspectionListModel();
                List<AddInspectionModel> addInspectionList = new ArrayList<AddInspectionModel>();

                AddInspectionModel addInspectionModel = new AddInspectionModel();
                addInspectionModel.setOrderDetailId(orderDetailId);
                addInspectionModel.setTime(hours + ":" + minutes + ":" + "00");

                addInspectionList.add(addInspectionModel);
                addInspectionListModel.setAddInspectionList(addInspectionList);

                addInspection(context, addInspectionListModel);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        GlobalFunctions.hideProgress();
        if (resultCode == activity.RESULT_OK && requestCode == globalVariables.REQUEST_CODE_FOR_HOUR) {

            String selectedHour = (String) data.getExtras().getSerializable(BUNDLE_SELECTED_HOUR);
            if (selectedHour != null) {
                isHourSelected = true;
                hours_etv.setText(selectedHour);
                minutes_etv.setText("00");
            }

        } else if (resultCode == activity.RESULT_OK && requestCode == globalVariables.REQUEST_CODE_FOR_MINUTE) {

            String selectedMinute = (String) data.getExtras().getSerializable(BUNDLE_SELECTED_MINUTE);
            if (selectedMinute != null) {
                isMinuteSelected = true;
                minutes_etv.setText(selectedMinute);
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    private void addInspection(final Context context, final AddInspectionListModel addInspectionListModel) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.addInspection(context, addInspectionListModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateAddInspectionOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "AddSparePart");
    }

    private void validateAddInspectionOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            showAlertMessage(statusModel);
        }
    }

    private void showAlertMessage(final StatusModel statusModel ) {

        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(statusModel.getMessage());
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (statusModel.isStatus()) {
                    //go to active page..
                    Intent intent = MainActivity.newInstance(activity, globalVariables.TO_PAGE_ACTIVE_REQUESTS);
                    activity.startActivity(intent);
                    closeThisActivity();
                }
            }
        });

        alertDialog.show();
    }

}