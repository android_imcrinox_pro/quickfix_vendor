package sa.quickfix.vendor.flows.reset_password.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.LoginModel;
import sa.quickfix.vendor.services.model.RegisterModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.services.model.StatusModel;

import com.facebook.FacebookSdk;


public class ForgotPasswordActivity extends AppCompatActivity {

    public static final String TAG = "ForgotPasswordActivity";

    public static final String BUNDLE_REGISTER_MODEL = "BundleRegisterModel";

    Context context;
    private static Activity activity;
    View mainView;

    String selected_country_code = "";

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    private TextView send_request_tv;
    private EditText mobile_number_etv;
    private ImageView back_iv;

    RegisterModel registerModel = null;

    public static Intent newInstance(Context context, RegisterModel registerModel) {
        Intent intent = new Intent(context, ForgotPasswordActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_REGISTER_MODEL, registerModel);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.forgot_password_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);

        send_request_tv = (TextView) findViewById(R.id.send_request_tv);
        mobile_number_etv = (EditText) findViewById(R.id.mobile_number_etv);

        mainView = send_request_tv;

        if (getIntent().hasExtra(BUNDLE_REGISTER_MODEL)) {
            registerModel = (RegisterModel) getIntent().getSerializableExtra(BUNDLE_REGISTER_MODEL);
        } else {
            registerModel = null;
        }

        if (registerModel != null) {
            if (registerModel.getCountryCode() != null && registerModel.getMobileNumber() != null) {
                selected_country_code = registerModel.getCountryCode();
                mobile_number_etv.setText( registerModel.getCountryCode()+""+registerModel.getMobileNumber());
//                mobile_number_etv.requestFocus(mobile_number_etv.getText().toString().length());
            }

        }

        mobile_number_etv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String digits = mobile_number_etv.getText().toString().trim();
                if (digits.length() >= 10) {
                    globalFunctions.closeKeyboard(activity);
                }
            }
        });

        send_request_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
              /*  Intent intent = new Intent(ForgotPasswordActivity.this, ResetOtpActivity.class);
                startActivity(intent);*/
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void validateInput() {
        if (mobile_number_etv != null) {
            String
                    mobileNumber = mobile_number_etv.getText().toString().trim();

            if (selected_country_code.isEmpty()) {
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.countryCodeNONotValid));
            } else if (mobileNumber.isEmpty()) {
                mobile_number_etv.setError(getString(R.string.pleaseEnterMobileNumber));
                mobile_number_etv.setFocusableInTouchMode(true);
                mobile_number_etv.requestFocus();
            } else {
                if (registerModel == null) {
                    registerModel = new RegisterModel();
                }
                registerModel.setCountryCode(selected_country_code);
                registerModel.setMobileNumber(registerModel.getMobileNumber());

                LoginModel model = new LoginModel();
                model.setMobile(registerModel.getMobileNumber());
                checkMobileNumber(context, model);
            }
        }
    }

    private void checkMobileNumber(final Context context, final LoginModel loginModel  ) {
        GlobalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.checkMobileNumber(context, loginModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                GlobalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof StatusModel) {
                    validateOutputAfterCheckingMobileNumber((StatusModel) arg0);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "CheckMobileNumber");
    }

    private void validateOutputAfterCheckingMobileNumber(StatusModel statusModel ) {
        if (statusModel.isStatus()) {
            sendOtpToMyMobileNumber(registerModel);
        } else {
            //registered user..
            globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
        }

    }

    private void sendOtpToMyMobileNumber(RegisterModel model) {
        String phoneNumber = selected_country_code + registerModel.getMobileNumber();
        Intent intent = ResetOtpActivity.newInstance(context, registerModel, phoneNumber);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}