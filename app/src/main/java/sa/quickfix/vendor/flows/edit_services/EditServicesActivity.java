package sa.quickfix.vendor.flows.edit_services;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookSdk;

import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.flows.my_services.activities.MyServicesActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.EditServicesListAdapter;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.CategoryListModel;
import sa.quickfix.vendor.services.model.CategoryModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;


public class EditServicesActivity extends AppCompatActivity {

    public static final String TAG = "EditServicesActivity";

    public static final String BUNDLE_CATEGORY_LIST_MODEL = "BundleCategoryListModel";
    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";

    Context context;
    private static Activity activity;
    View mainView;

    private CardView update_cardview;
    private ImageView back_iv;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    CategoryListModel categoryListModel = new CategoryListModel();
    List<CategoryModel> list = new ArrayList<>();

    LinearLayoutManager categoryLayoutManager;
    RecyclerView category_recycler_view;
    EditServicesListAdapter editServicesListAdapter;

    public static Intent newInstance(Context context, CategoryListModel model) {
        Intent intent = new Intent(context, EditServicesActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_CATEGORY_LIST_MODEL, model);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.edit_services_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        if (getIntent().hasExtra(BUNDLE_CATEGORY_LIST_MODEL)) {
            categoryListModel = (CategoryListModel) getIntent().getSerializableExtra(BUNDLE_CATEGORY_LIST_MODEL);
        } else {
            categoryListModel = null;
        }

        categoryLayoutManager = new LinearLayoutManager(context);

        back_iv = (ImageView) findViewById(R.id.back_iv);
        update_cardview = (CardView) findViewById(R.id.update_cardview);
        category_recycler_view = (RecyclerView) findViewById(R.id.category_recycler_view);

        mainView = update_cardview;

        //category recycler view....

        initCategoryList();

        if (categoryListModel != null) {
            setUpList(categoryListModel);
        } else {
            loadList(context);
        }

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        update_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

    }

    private void validateInput() {
        if (context != null) {

            if (!isAnyServiceSelected()) {
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.select_atleast_one_service));
            } else {
                if (categoryListModel == null) {
                    categoryListModel = new CategoryListModel();
                }
                categoryListModel.setRESPONSE("category");
                updateServices(context, categoryListModel);
            }

        }
    }

    private void updateServices(final Context context, final CategoryListModel model) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.updateServices(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "UpdateServices");
    }

    private void validateOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            if (!statusModel.isStatus()) {
                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            } else {
                showAlertMessage(statusModel.getMessage());
            }
        }
    }

    private void showAlertMessage(String message) {

        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                MyServicesActivity.closeThisActivity();
                Intent intent = new Intent(EditServicesActivity.this, MyServicesActivity.class);
                startActivity(intent);
                closeThisActivity();
            }
        });

        alertDialog.show();
    }

    private boolean isAnyServiceSelected() {
        boolean
                isAnyServiceSelected = false;

        if (categoryListModel != null) {

            int listSizeMain = categoryListModel.getCategoryList().size();

            for (int i = 0; i < listSizeMain; i++) {
                boolean isAdded = false;
                if(categoryListModel.getCategoryList().get(i).getSubCategoryListModel() != null){
                    int listSizeSub = categoryListModel.getCategoryList().get(i).getSubCategoryListModel().getSubCategoryListModels().size();
                    if (listSizeSub > 0) {
                        for (int j = 0; j < listSizeSub; j++) {

                            if (!isAdded && categoryListModel.getCategoryList().get(i).getSubCategoryListModel().getSubCategoryListModels().get(j).getSelected().equalsIgnoreCase("1")) {
                                isAnyServiceSelected = true;
                                isAdded = true;
                                return isAnyServiceSelected;
                            }
                        }
                    } else if (categoryListModel.getCategoryList().get(i).getSelected().equalsIgnoreCase("1")) {
                        isAnyServiceSelected = true;
                        return isAnyServiceSelected;
                    }
                } else if (categoryListModel.getCategoryList().get(i).getSelected().equalsIgnoreCase("1")) {
                    isAnyServiceSelected = true;
                    return isAnyServiceSelected;
                }
            }

        }
        return isAnyServiceSelected;

    }

    private void initCategoryList() {
        editServicesListAdapter = new EditServicesListAdapter(activity, list);
        category_recycler_view.setLayoutManager(categoryLayoutManager);
        category_recycler_view.setAdapter(editServicesListAdapter);

        synchronized (editServicesListAdapter) {
            editServicesListAdapter.notifyDataSetChanged();
        }
    }

    private void loadList(final Context context) {
        globalFunctions.showProgress(context, context.getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getCategoryList(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                CategoryListModel model = (CategoryListModel) arg0;
                setUpList(model);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                Log.d(TAG, "Failure : " + msg);
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                Log.d(TAG, "Error : " + msg);
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
            }
        }, "GetCategoryList");
    }

    private void setUpList(CategoryListModel categoryListModel1) {
        if (categoryListModel1 != null && list != null) {
            list.clear();
            if (categoryListModel == null) {
                categoryListModel = new CategoryListModel();
            }
            categoryListModel.setCategoryList(categoryListModel1.getCategoryList());
            list = categoryListModel1.getCategoryList();
            initCategoryList();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}