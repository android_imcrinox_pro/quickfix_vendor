package sa.quickfix.vendor.flows.add_parts.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.vlonjatg.progressactivity.ProgressRelativeLayout;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnYearItemClickInvoke;
import sa.quickfix.vendor.adapters.TimeListAdapter;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.R;

public class TimeListActivity extends AppCompatActivity implements OnYearItemClickInvoke {

    public static final String TAG = "TimeListActivity";
    public static final String BUNDLE_SELECTED_HOUR = "BundleSelectedHour";
    public static final String BUNDLE_SELECTED_MINUTE = "BundleSelectedMinute";
    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";

    Context context = null;
    static Activity activity = null;

    RecyclerView recyclerView;
    public View mainView;

    TimeListAdapter adapter;

    ProgressRelativeLayout progressActivity;
    DilatingDotsProgressBar progressBar;

    List<String> list = new ArrayList<>();

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;
    Window window = null;

    Toolbar toolbar;
    ActionBar actionBar;
    String mTitle;
    int mResourceID, titleResourseID;
    TextView toolbar_title;
    ImageView toolbar_icon;
    AppBarLayout appBarLayout;
    Menu menu;

    String fromPage = null;

    LinearLayoutManager linearLayoutManager;

    public static Intent newInstance(Context context, String fromPage) {
        Intent intent = new Intent(context, TimeListActivity.class);
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_FROM_PAGE, fromPage);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.time_list_activity);

        context = this;
        activity = this;
        window = getWindow();

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        progressActivity = findViewById(R.id.details_progressActivity);
        progressBar = (DilatingDotsProgressBar) findViewById(R.id.extraProgressBar);
        recyclerView = (RecyclerView) findViewById(R.id.time_list_recyclerView);


        linearLayoutManager = new LinearLayoutManager(activity);

        mainView = recyclerView;

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        //toolbar.setPadding(0, GlobalFunctions.getStatusBarHeight(context), 0, 0);
        //toolbar.setNavigationIcon(R.drawable.ic_back_draw);
        //toolbar.setContentInsetsAbsolute(0,0);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_icon = (ImageView) toolbar.findViewById(R.id.toolbar_icon);
        toolbar_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

//        initRecyclerView();

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_PAGE)) {
            fromPage = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_PAGE);
        } else {
            fromPage = null;
        }

        if (fromPage != null) {
            if (fromPage.equalsIgnoreCase(globalVariables.FROM_HOUR)) {
                setUpListForHour();
            } else if (fromPage.equalsIgnoreCase(globalVariables.FROM_MINUTE)) {
                setUpListForMinute();
            }
        }

    }

    private void setUpListForMinute() {
        list.clear();
        for (int i = 0; i < 60; i++) {
            if (i == 0) {
                list.add("00");
            } else if (i == 1) {
                list.add("01");
            } else if (i == 2) {
                list.add("02");
            } else if (i == 3) {
                list.add("03");
            } else if (i == 4) {
                list.add("04");
            } else if (i == 5) {
                list.add("05");
            } else if (i == 6) {
                list.add("06");
            } else if (i == 7) {
                list.add("07");
            } else if (i == 8) {
                list.add("08");
            } else if (i == 9) {
                list.add("09");
            } else {
                list.add(i + "");
            }
        }

        if (adapter != null) {
            synchronized (adapter) {
                adapter.notifyDataSetChanged();
            }
        }
        if (list.size() <= 0) {
        } else {
            initRecyclerView();
        }
    }

    private void setUpListForHour() {

        list.clear();
        for (int i = 0; i < 100; i++) {
            if (i == 0) {
                list.add("00");
            } else if (i == 1) {
                list.add("01");
            } else if (i == 2) {
                list.add("02");
            } else if (i == 3) {
                list.add("03");
            } else if (i == 4) {
                list.add("04");
            } else if (i == 5) {
                list.add("05");
            } else if (i == 6) {
                list.add("06");
            } else if (i == 7) {
                list.add("07");
            } else if (i == 8) {
                list.add("08");
            } else if (i == 9) {
                list.add("09");
            } else {
                list.add(i + "");
            }
        }

        if (adapter != null) {
            synchronized (adapter) {
                adapter.notifyDataSetChanged();
            }
        }
        if (list.size() <= 0) {
        } else {
            initRecyclerView();
        }
    }


    public void initRecyclerView() {
        adapter = new TimeListAdapter(activity, list, this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        //list.clear();
        super.onStart();
    }

    @Override
    protected void onResume() {
        if (fromPage != null) {
            if (fromPage.equalsIgnoreCase(globalVariables.FROM_HOUR)) {
                setTitle(getString(R.string.select_hour), 0, 0);
            } else if (fromPage.equalsIgnoreCase(globalVariables.FROM_MINUTE)) {
                setTitle(getString(R.string.select_minute), 0, 0);
            }
        }
        super.onResume();
    }

    public void setTitle(String title, int titleImageID, int backgroundResourceID) {
        mTitle = title;
        if (backgroundResourceID != 0) {
            mResourceID = backgroundResourceID;
        } else {
            mResourceID = 0;
        }
        if (titleImageID != 0) {
            titleResourseID = titleImageID;
        } else {
            titleResourseID = 0;
        }
        restoreToolbar();
    }

    private void restoreToolbar() {
        //toolbar = (Toolbar) findViewById(R.id.tool_bar);
        Log.d(TAG, "Restore Tool Bar");
        if (actionBar != null) {
            Log.d(TAG, "Restore Action Bar not Null");
            Log.d(TAG, "Title : " + mTitle);
            if (titleResourseID != 0) {
                toolbar_title.setVisibility(View.GONE);
            } else {
                toolbar_title.setVisibility(View.VISIBLE);
                toolbar_title.setText(mTitle);
            }

            if (mResourceID != 0) toolbar.setBackgroundResource(mResourceID);
            //actionBar.setTitle("");
            //actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (appBarLayout != null) {
            appBarLayout.bringToFront();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_nothing, menu);
        this.menu = menu;

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_nothing, menu);
        this.menu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return false;
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
            //activity.overridePendingTransition(R.anim.zoom_in,R.anim.zoom_out);
        }
    }

    @Override
    public void onBackPressed() {
        closeThisActivity();
    }

    @Override
    public void OnClickInvoke(int position, String selectedValue) {
        if (selectedValue != null) {
            if (fromPage != null) {
                if (fromPage.equalsIgnoreCase(globalVariables.FROM_HOUR)) {
                    setResultForHour(true, selectedValue);
                } else if (fromPage.equalsIgnoreCase(globalVariables.FROM_MINUTE)) {
                    setResultForMinute(true, selectedValue);
                }
            }
        }
    }

    private void setResultForMinute(boolean isSuccess, String minute) {
        Intent intent = new Intent();
        intent.putExtra(BUNDLE_SELECTED_MINUTE, minute);
        if (isSuccess) setResult(RESULT_OK, intent);
        else setResult(RESULT_CANCELED, intent);
        closeThisActivity();
    }

    private void setResultForHour(boolean isSuccess, String hour) {
        Intent intent = new Intent();
        intent.putExtra(BUNDLE_SELECTED_HOUR, hour);
        if (isSuccess) setResult(RESULT_OK, intent);
        else setResult(RESULT_CANCELED, intent);
        closeThisActivity();
    }
}
