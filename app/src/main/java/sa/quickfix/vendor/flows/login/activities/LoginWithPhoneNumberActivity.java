package sa.quickfix.vendor.flows.login.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.SplashScreen;
import sa.quickfix.vendor.flows.about_us.AboutUsActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerConstants;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.LoginModel;
import sa.quickfix.vendor.services.model.RegisterModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;

import com.facebook.FacebookSdk;
import com.hbb20.CountryCodePicker;


public class LoginWithPhoneNumberActivity extends AppCompatActivity {

    public static final String TAG = "LoginWithPhnNumActivity";

    public static final String BUNDLE_REGISTER_MODEL = "BundleRegisterModel";
    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";

    Context context;
    private static Activity activity;
    View mainView;

    String selected_country_code = "";

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    String pageType = globalVariables.PAGE_FROM_LOGIN;

    RegisterModel registerModel = null;
    RegisterModel myRegisterModel = null;

    private CountryCodePicker country_code_picker;
    private EditText phone_number_etv;

    private TextView next_tv, login_with_password_tv, terms_and_conditions_tv, privacy_policy_tv;
    private LinearLayout register_ll;

    private static TextView arabic_language_tv, english_language_tv, urdu_language_tv;

    private static View arabic_language_iv, english_language_iv, urdu_language_iv;

    public static Intent newInstance(Activity activity, String pageType, RegisterModel registerModel) {
        Intent intent = new Intent(activity, LoginWithPhoneNumberActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_REGISTER_MODEL, registerModel);
        args.putString(BUNDLE_KEY_FROM_PAGE, pageType);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login_with_phone_number_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        country_code_picker = (CountryCodePicker) findViewById(R.id.country_code_picker);
        phone_number_etv = (EditText) findViewById(R.id.phone_number_etv);

        next_tv = (TextView) findViewById(R.id.next_tv);
        login_with_password_tv = (TextView) findViewById(R.id.login_with_password_tv);
        terms_and_conditions_tv = (TextView) findViewById(R.id.terms_and_conditions_tv);
        privacy_policy_tv = (TextView) findViewById(R.id.privacy_policy_tv);

        register_ll = (LinearLayout) findViewById(R.id.register_ll);

        urdu_language_iv = (View) findViewById(R.id.urdu_language_iv);
        arabic_language_iv = (View) findViewById(R.id.arabic_language_iv);
        english_language_iv = (View) findViewById(R.id.english_language_iv);

        english_language_tv = (TextView) findViewById(R.id.english_language_tv);
        arabic_language_tv = (TextView) findViewById(R.id.arabic_language_tv);
        urdu_language_tv = (TextView) findViewById(R.id.urdu_language_tv);

        mainView = next_tv;

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_PAGE)) {
            pageType = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_PAGE);
        } else {
            pageType = null;
        }

        if (getIntent().hasExtra(BUNDLE_REGISTER_MODEL)) {
            myRegisterModel = (RegisterModel) getIntent().getSerializableExtra(BUNDLE_REGISTER_MODEL);
        } else {
            myRegisterModel = null;
        }

        //first time.....
        country_code_picker.setCountryForPhoneCode(+966);

        if (pageType != null) {
            if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_EDIT_NUMBER)) {
                //set phone number and country code....
                if (myRegisterModel != null) {
                    selected_country_code = myRegisterModel.getCountryCode();
                    country_code_picker.setCountryForPhoneCode(Integer.parseInt(myRegisterModel.getCountryCode()));
                    phone_number_etv.setText(myRegisterModel.getMobileNumber());
                }
            }
        }

        phone_number_etv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               /* if (s.toString().startsWith("0")) {
                    s.clear();
                }*/
                String digits = phone_number_etv.getText().toString().trim();
                if (digits.length() >= getResources().getInteger(R.integer.mobile_max_length)) {
                    globalFunctions.closeKeyboard(activity);
                }
            }
        });

        country_code_picker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                selected_country_code = country_code_picker.getSelectedCountryCodeWithPlus();
                phone_number_etv.setText("");
            }
        });

        country_code_picker.registerCarrierNumberEditText(phone_number_etv);

        country_code_picker.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
            @Override
            public void onValidityChanged(boolean isValidNumber) {
            }
        });

        selected_country_code = country_code_picker.getSelectedCountryCodeWithPlus();

        next_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput(globalVariables.PAGE_FROM_LOGIN);
            }
        });

        login_with_password_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput(globalVariables.PAGE_FROM_LOGIN_WITH_PASSWORD);
            }
        });

        register_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput(globalVariables.PAGE_FROM_REGISTRATION);
            }
        });

        terms_and_conditions_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = AboutUsActivity.newInstance(activity, GlobalVariables.PAGE_LOGIN_TNC);
                startActivity(intent);
//                GlobalFunctions.openBrowser(context, ServerConstants.URL_TERMS_CONDITIONS);
            }
        });

        privacy_policy_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = AboutUsActivity.newInstance(activity, GlobalVariables.PAGE_PP);
                startActivity(intent);
//                GlobalFunctions.openBrowser(context, ServerConstants.URL_TERMS_CONDITIONS);
            }
        });

        if (globalFunctions.getLanguage(context) == GlobalVariables.LANGUAGE.ENGLISH) {
            english_language_iv.setVisibility(View.VISIBLE);
            arabic_language_iv.setVisibility(View.GONE);
            urdu_language_iv.setVisibility(View.GONE);
        } else if (globalFunctions.getLanguage(context) == GlobalVariables.LANGUAGE.URDU) {
            english_language_iv.setVisibility(View.GONE);
            arabic_language_iv.setVisibility(View.GONE);
            urdu_language_iv.setVisibility(View.VISIBLE);
        } else {
            urdu_language_iv.setVisibility(View.GONE);
            english_language_iv.setVisibility(View.GONE);
            arabic_language_iv.setVisibility(View.VISIBLE);
        }

        arabic_language_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (globalFunctions.getLanguage(context) != GlobalVariables.LANGUAGE.ARABIC) {
                    //LanguageChange(context);
                    RestartEntireApp(context, true, "ar");
                } else {
                    ///nothing...
                }
            }
        });

        english_language_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (globalFunctions.getLanguage(context) != GlobalVariables.LANGUAGE.ENGLISH) {
                    //LanguageChange(context);
                    RestartEntireApp(context, true, "en");
                } else {
                    //nothing

                }
            }
        });

        urdu_language_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (globalFunctions.getLanguage(context) != GlobalVariables.LANGUAGE.URDU) {
                    //LanguageChange(context);
                    RestartEntireApp(context, true, "ur");
                } else {
                    //nothing
                }
            }
        });

    }

    public void LanguageChange(Context context) {
        ShowPopUpLanguage(context);
    }

    public void ShowPopUpLanguage(final Context context) {
        final AlertDialog alertDialog = new AlertDialog(activity);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.app_icon);
        alertDialog.setTitle(activity.getString(R.string.app_name));
        alertDialog.setMessage(activity.getString(R.string.lang_change));
        alertDialog.setPositiveButton(activity.getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
//                RestartEntireApp(context, true);
            }
        });

        alertDialog.setNegativeButton(activity.getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }


    public void RestartEntireApp(Context context, boolean isLanguageChange, String language) {
        if (isLanguageChange) {
            SharedPreferences shared_preference = PreferenceManager.getDefaultSharedPreferences(this
                    .getApplicationContext());

//            String mCustomerLanguage = shared_preference.getString(
//                    globalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE, "null");
            String mCurrentlanguage;
            if ((language.equalsIgnoreCase("en"))) {
                globalFunctions.setLanguage(context, GlobalVariables.LANGUAGE.ENGLISH);

                mCurrentlanguage = "en";
            } else if ((language.equalsIgnoreCase("ur"))) {
                globalFunctions.setLanguage(context, GlobalVariables.LANGUAGE.URDU);

                mCurrentlanguage = "ur";
            } else {
                mCurrentlanguage = "ar";
                globalFunctions.setLanguage(context, GlobalVariables.LANGUAGE.ARABIC);

            }
            SharedPreferences.Editor editor = shared_preference.edit();
            editor.putString(globalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE, mCurrentlanguage);
            editor.commit();
        }
        globalFunctions.closeAllActivities();
        Intent i = new Intent(this, SplashScreen.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        System.exit(0);
    }


    private void validateInput(String pageType) {
        if (phone_number_etv != null) {
            String
                    mobileNumber = phone_number_etv.getText().toString().trim();

            if (mobileNumber.isEmpty()) {
                phone_number_etv.setError(getString(R.string.pleaseEnterMobileNumber));
                phone_number_etv.setFocusableInTouchMode(true);
                phone_number_etv.requestFocus();
            } else if (selected_country_code.isEmpty()) {
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.countryCodeNONotValid));
            } else if (!country_code_picker.isValidFullNumber()) {
//                mobile_number_etv.setText("");
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.pleaseEnterValidMobileNumber));
                phone_number_etv.setSelection(phone_number_etv.getText().length());
                phone_number_etv.setFocusableInTouchMode(true);
                phone_number_etv.requestFocus();
            } else {
                if (registerModel == null) {
                    registerModel = new RegisterModel();
                }
                registerModel.setCountryCode(selected_country_code);
                registerModel.setMobileNumber(mobileNumber);

                LoginModel model = new LoginModel();
                model.setMobile(registerModel.getMobileNumber());
                checkMobileNumber(context, model, registerModel, pageType);
               /* if (pageType != null) {
                    if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_LOGIN_WITH_PASSWORD)) {
                        //go to login with password page...
                        if (registerModel == null) {
                            registerModel = new RegisterModel();
                        }
                        registerModel.setCountryCode(selected_country_code);
                        registerModel.setMobileNumber(mobileNumber);
                        Intent intent = LoginWithPasswordActivity.newInstance(activity, globalVariables.PAGE_FROM_LOGIN_WITH_PASSWORD, registerModel);
                        startActivity(intent);
                    } else {
                        //go to otp page....
                        if (registerModel == null) {
                            registerModel = new RegisterModel();
                        }
                        registerModel.setCountryCode(selected_country_code);
                        registerModel.setMobileNumber(mobileNumber);

                        LoginModel model = new LoginModel();
                        model.setMobile(registerModel.getMobileNumber());
                        checkMobileNumber(context, model, registerModel, pageType);
                    }
                }*/
            }
        }
    }

    private void checkMobileNumber(final Context context, final LoginModel loginModel, final RegisterModel registerModel, final String pageType) {
        GlobalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.checkMobileNumber(context, loginModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                GlobalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof StatusModel) {
                    validateOutputAfterCheckingMobileNumber((StatusModel) arg0, registerModel, pageType);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "CheckMobileNumber");
    }

    private void validateOutputAfterCheckingMobileNumber(StatusModel statusModel, RegisterModel registerModel, String pageType) {

        /*if (statusModel != null) {
            if (statusModel.getExtra().equalsIgnoreCase("3") || statusModel.getExtra().equalsIgnoreCase("4") ||
                    statusModel.getExtra().equalsIgnoreCase("5") || statusModel.getExtra().equalsIgnoreCase("1")) {
                if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_REGISTRATION) && !statusModel.getExtra().equalsIgnoreCase("1")) {
                    globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
                    return;
                } else {
                    if (!pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_REGISTRATION) && !statusModel.getExtra().equalsIgnoreCase("1")) {
                        globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
                        return;
                    }else if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_REGISTRATION)) {
                        sendOtpToMyMobileNumber(registerModel, pageType);
                        return;
                    }else if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_LOGIN_WITH_PASSWORD && statusModel.getExtra().equalsIgnoreCase("1"))
                    showAlertDialog(statusModel.getMessage(), registerModel, pageType);
                    return;
                }
            } else if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_LOGIN)) {
                sendOtpToMyMobileNumber(registerModel, pageType);
            } else if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_REGISTRATION)) {
                if (statusModel.isStatus()) {
                    globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
                    return;
                }
                sendOtpToMyMobileNumber(registerModel, pageType);
            } else {
                if (!statusModel.isStatus()) {
                    globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
                    return;
                }
                //go to loginWith password...
                Intent intent = LoginWithPasswordActivity.newInstance(activity, globalVariables.PAGE_FROM_LOGIN_WITH_PASSWORD, registerModel);
                startActivity(intent);
            }
        }*/

        if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_REGISTRATION)) {
            if (!statusModel.isStatus() && statusModel.getExtra().equalsIgnoreCase("1")) {
                //new user....go for registration..
                sendOtpToMyMobileNumber(registerModel, pageType);
//                Intent intent = RegisterActivity.newInstance(context, registerModel);
//                startActivity(intent);
            } else if (!statusModel.isStatus() && statusModel.getExtra().equalsIgnoreCase("3")) {
                //blocked...show popup msg and contact number to call or cancel option...
                showAlertDialogForSupport(statusModel.getMessage());
            } else {
                //registered user..
//                showAlertMessage(statusModel.getMessage());
                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            }
        } else if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_LOGIN)) {
            if (statusModel.isStatus()) {
                sendOtpToMyMobileNumber(registerModel, pageType);
            } else if (!statusModel.isStatus() && statusModel.getExtra().equalsIgnoreCase("1")) {
                sendOtpToMyMobileNumber(registerModel, globalVariables.PAGE_FROM_REGISTRATION);
//                showAlertMessage(statusModel.getMessage());
                //registered user..
//                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            } else if (!statusModel.isStatus() && statusModel.getExtra().equalsIgnoreCase("3")) {
                //blocked...show popup msg and contact number to call or cancel option...
                showAlertDialogForSupport(statusModel.getMessage());
            } else {
                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            }
        } else if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_LOGIN_WITH_PASSWORD)) {
            if (statusModel.isStatus()) {
                Intent intent = LoginWithPasswordActivity.newInstance(activity, globalVariables.PAGE_FROM_LOGIN_WITH_PASSWORD, registerModel);
                startActivity(intent);
            } else if (!statusModel.isStatus() && statusModel.getExtra().equalsIgnoreCase("3")) {
                //blocked...show popup msg and contact number to call or cancel option...
                showAlertDialogForSupport(statusModel.getMessage());
            } else {
//                showAlertMessage(statusModel.getMessage());
                //registered user..
//                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
                sendOtpToMyMobileNumber(registerModel, globalVariables.PAGE_FROM_REGISTRATION);
            }
        }

    }

    private void showAlertDialogForSupport(String message) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finishAffinity();
            }
        });

        alertDialog.show();
    }

    private void showAlertDialog(String message, final RegisterModel registerModel, final String pageType) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getString(R.string.register), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                sendOtpToMyMobileNumber(registerModel, pageType);
            }
        });
        alertDialog.setNegativeButton(getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }


    private void sendOtpToMyMobileNumber(RegisterModel model, String pageType) {
        String phoneNumber = selected_country_code + model.getMobileNumber();
        Intent intent = OtpActivity.newInstance(context, model, phoneNumber, pageType);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}