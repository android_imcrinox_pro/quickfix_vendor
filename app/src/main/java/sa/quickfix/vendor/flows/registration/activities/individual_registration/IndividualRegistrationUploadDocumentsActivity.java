package sa.quickfix.vendor.flows.registration.activities.individual_registration;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.flows.image_picker.ImagePickerActivity;
import sa.quickfix.vendor.flows.registration.activities.SelectServicesActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.PhotosListAdapter;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnPhotoDeleteClickInvoke;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.model.IndividualRegisterModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class IndividualRegistrationUploadDocumentsActivity extends AppCompatActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate, OnPhotoDeleteClickInvoke {

    public static final String TAG = "IndividualUpload";

    public static final int REQUEST_IMAGE = 100;

    public static final String BUNDLE_INDIVIDUAL_REGISTER_MODEL = "BundleIndividualRegisterModel";
    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";
    public static final String BUNDLE_SELECTED_YEAR = "BundleSelectedYear";
    public static final String BUNDLE_SELECTED_MONTH = "BundleSelectedMonth";

    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int PICK_FILE_PDF = 200;

    Context context;
    private static Activity activity;
    View mainView;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    List<String> profileImageList;

    List<Uri>
            uriProfileImageList,
            uriIdImageList,
            uriCertificatesImageList;

    List<Bitmap>
            bitmapProfileImageList,
            bitmapIdImageList,
            btmapCertificateImageList;

    String imagePath = "";

    boolean isProfileImageClicked = false;
    boolean isCertificateImageClicked = false;
    boolean isIdImageClicked = false;

    String fromPage = null;
    IndividualRegisterModel individualRegisterModel = null;

    boolean
            isYearsSelected = false,
            isMonthsSelected = false;

    private CardView continue_cardview;
    private CircleImageView vendor_iv;
    private TextView vendor_name_tv, years_tv, months_tv;
    private LinearLayout upload_profile_pic_ll;
    private ImageView id_iv_one, id_one_delete_iv, id_iv_two, id_two_delete_iv, id_iv_three,
            id_three_delete_iv, certificate_iv_one, certificate_one_delete_iv, certificate_iv_two, certificate_two_delete_iv, certificate_iv_three, certificate_three_delete_iv,
            add_national_id_iv, add_certificate_id_iv;

    private RecyclerView add_national_id_rv, add_certificate_id_rv;
    LinearLayoutManager linearLayoutManager, certificate_linearLayoutManager;
    PhotosListAdapter photosListAdapter;
    LayoutInflater layoutInflater;
    Intent intent;
    int MULTI_SELECT_NATIONAL_MAX_VALUE = 3;
    int MULTI_SELECT_CERTIFICATE_MAX_VALUE = 3;
    private static final int INTENT_REQUEST_GET_IMAGES_FROM_CAMERA = 10;

    public static Intent newInstance(Context context, IndividualRegisterModel model) {
        Intent intent = new Intent(context, IndividualRegistrationUploadDocumentsActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_INDIVIDUAL_REGISTER_MODEL, model);
        intent.putExtras(args);
        return intent;
    }

    public static Intent newInstance(Context context, IndividualRegisterModel model, String fromPage) {
        Intent intent = new Intent(context, IndividualRegistrationUploadDocumentsActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_INDIVIDUAL_REGISTER_MODEL, model);
        args.putString(BUNDLE_KEY_FROM_PAGE, fromPage);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.individual_registration_upload_documents_activity);
        ButterKnife.bind(this);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        layoutInflater = activity.getLayoutInflater();
        linearLayoutManager = new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false);
        certificate_linearLayoutManager = new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false);

        profileImageList = new ArrayList<>();
        uriProfileImageList = new ArrayList<>();
        bitmapProfileImageList = new ArrayList<>();

        uriIdImageList = new ArrayList<>();
        bitmapIdImageList = new ArrayList<>();

        uriCertificatesImageList = new ArrayList<>();
        btmapCertificateImageList = new ArrayList<>();

        uriProfileImageList.clear();
        uriIdImageList.clear();
        uriCertificatesImageList.clear();

        if (getIntent().hasExtra(BUNDLE_INDIVIDUAL_REGISTER_MODEL)) {
            individualRegisterModel = (IndividualRegisterModel) getIntent().getSerializableExtra(BUNDLE_INDIVIDUAL_REGISTER_MODEL);
        } else {
            individualRegisterModel = null;
        }

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_PAGE)) {
            fromPage = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_PAGE);
        } else {
            fromPage = null;
        }

        continue_cardview = (CardView) findViewById(R.id.continue_cardview);
        vendor_name_tv = (TextView) findViewById(R.id.vendor_name_tv);
        years_tv = (TextView) findViewById(R.id.years_tv);
        months_tv = (TextView) findViewById(R.id.months_tv);
        vendor_iv = (CircleImageView) findViewById(R.id.vendor_iv);
        upload_profile_pic_ll = (LinearLayout) findViewById(R.id.upload_profile_pic_ll);

        id_iv_one = (ImageView) findViewById(R.id.id_iv_one);
        id_one_delete_iv = (ImageView) findViewById(R.id.id_one_delete_iv);
        id_iv_two = (ImageView) findViewById(R.id.id_iv_two);
        id_two_delete_iv = (ImageView) findViewById(R.id.id_two_delete_iv);
        id_iv_three = (ImageView) findViewById(R.id.id_iv_three);
        id_three_delete_iv = (ImageView) findViewById(R.id.id_three_delete_iv);
        certificate_iv_one = (ImageView) findViewById(R.id.certificate_iv_one);
        certificate_one_delete_iv = (ImageView) findViewById(R.id.certificate_one_delete_iv);
        certificate_iv_two = (ImageView) findViewById(R.id.certificate_iv_two);
        certificate_two_delete_iv = (ImageView) findViewById(R.id.certificate_two_delete_iv);
        certificate_iv_three = (ImageView) findViewById(R.id.certificate_iv_three);
        certificate_three_delete_iv = (ImageView) findViewById(R.id.certificate_three_delete_iv);

        add_national_id_iv = (ImageView) findViewById(R.id.add_national_id_iv);
        add_certificate_id_iv = (ImageView) findViewById(R.id.add_certificate_id_iv);
        add_national_id_rv = (RecyclerView) findViewById(R.id.add_national_id_rv);
        add_certificate_id_rv = (RecyclerView) findViewById(R.id.add_certificate_id_rv);

        mainView = continue_cardview;

        if (individualRegisterModel != null) {
            vendor_name_tv.setText(individualRegisterModel.getFirstName() + " " + individualRegisterModel.getLastName());
        }

        if (fromPage != null) {
            if (fromPage.equalsIgnoreCase(globalVariables.FROM_PAGE_REVIEW_INDIVIDUAL_REGISTRATION)) {
                //from edit....set all the selected images....
                Intent i = getIntent();
                uriProfileImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE);
                uriIdImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_LIST_CODE);
                uriCertificatesImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_CERTIFICATES_LIST_CODE);
                setSelectedImages();
            }
        }

        years_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = YearListActivity.newInstance(activity, globalVariables.FROM_YEAR);
                startActivityForResult(intent, globalVariables.REQUEST_CODE_FOR_YEAR);
            }
        });

        months_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = YearListActivity.newInstance(activity, globalVariables.FROM_MONTH);
                startActivityForResult(intent, globalVariables.REQUEST_CODE_FOR_MONTH);
            }
        });

        vendor_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (profileImageList.size() == 0) {
                    isProfileImageClicked = true;
                    isIdImageClicked = false;
                    isCertificateImageClicked = false;
                    openCropFunctionImage();
                } else {
                    openCancelAlertDialog(context);
                }
            }
        });

        upload_profile_pic_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (profileImageList.size() == 0) {
                    isProfileImageClicked = true;
                    isIdImageClicked = false;
                    isCertificateImageClicked = false;
                    openCropFunctionImage();
                } else {
                    openCancelAlertDialog(context);
                }
            }
        });

        //id doc.....
        add_national_id_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (profileImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_profile_photo_first));
                } else if (uriIdImageList.size() >= 3) {
//                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_profile_photo_first));
                } else {
//                    openCropFunctionImage();
                    openFileAlertDialog();
                    isIdImageClicked = true;
                    isProfileImageClicked = false;
                    isCertificateImageClicked = false;
                }
            }
        });

        add_certificate_id_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (profileImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_profile_photo_first));
                } else if (uriIdImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_id_image_first));
                } else if (uriCertificatesImageList.size() >= 3) {
//                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_id_image_first));
                } else {
//                    openCropFunctionImage();
                    openFileAlertDialog();
                    isIdImageClicked = false;
                    isProfileImageClicked = false;
                    isCertificateImageClicked = true;
                }
            }
        });

        /*id_iv_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriIdImageList.size() == 0 && profileImageList.size() == 1) {
                    isIdImageClicked = true;
                    openCropFuctionalImage();
                } else if (profileImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_profile_photo_first));
                }
            }
        });

        id_iv_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriIdImageList.size() == 1 && profileImageList.size() == 1) {
                    isIdImageClicked = true;
                    openCropFuctionalImage();
                }
            }
        });

        id_iv_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriIdImageList.size() == 2 && profileImageList.size() == 1) {
                    isIdImageClicked = true;
                    openCropFuctionalImage();
                }

            }
        });

        id_one_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 0;
               *//* if (imageServerList.size() > 0) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeIdImage(position);
                //}
            }
        });

        id_two_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 1;
              *//*  if (imageServerList.size() > 1) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeIdImage(position);
                //}
            }
        });

        id_three_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 2;
               *//* if (imageServerList.size() > 2) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeIdImage(position);
                //  }
            }
        });*/

        ////certificate images.....

        /*certificate_iv_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriCertificatesImageList.size() == 0 && profileImageList.size() == 1 && uriIdImageList.size() >= 1) {
                    openCropFuctionalImage();
                } else if (profileImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_profile_photo_first));
                } else if (uriIdImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_id_image_first));
                }
            }
        });

        certificate_iv_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriCertificatesImageList.size() == 1 && profileImageList.size() == 1 && uriIdImageList.size() >= 1) {
                    openCropFuctionalImage();
                }
            }
        });

        certificate_iv_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriCertificatesImageList.size() == 2 && profileImageList.size() == 1 && uriIdImageList.size() >= 1) {
                    openCropFuctionalImage();
                }

            }
        });*/

        /*certificate_one_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 0;
               *//* if (imageServerList.size() > 0) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeCertificateImage(position);
                //}
            }
        });

        certificate_two_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 1;
              *//*  if (imageServerList.size() > 1) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeCertificateImage(position);
                //}
            }
        });

        certificate_three_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 2;
               *//* if (imageServerList.size() > 2) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeCertificateImage(position);
                //  }
            }
        });*/

        continue_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

    }

    private void openFileAlertDialog() {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.add_custom_file_dialog, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        ImageView image_iv,pdf_iv;

        image_iv = (ImageView) alertView.findViewById(R.id.image_iv);
        pdf_iv = (ImageView) alertView.findViewById(R.id.pdf_iv);

        image_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openCropFunctionImage();
            }
        });

        pdf_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openPdfSelectFile();

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void openPdfSelectFile() {

        Intent intentPDF = new Intent(Intent.ACTION_GET_CONTENT);
        intentPDF.setType("application/pdf");
        intentPDF.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intentPDF, PICK_FILE_PDF);
    }

    private void openCropFunctionImage() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(IndividualRegistrationUploadDocumentsActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(IndividualRegistrationUploadDocumentsActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(IndividualRegistrationUploadDocumentsActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }


    private void openMultiFunctionImage() {
        openDialogForPhotos();
    }

    private void openDialogForPhotos() {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        final View alertView = layoutInflater.inflate(R.layout.dialog_get_photoes, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(false);
        final View dialogView = alertView;
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();
        TextView camera_tv = alertView.findViewById(R.id.photos_dialog_camera_tv);
        TextView gallery_tv = alertView.findViewById(R.id.photos_dialog_gallery_tv);
        TextView close_tv = alertView.findViewById(R.id.photos_dialog_close_tv);

        close_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        camera_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openCameraImage();
            }
        });

        gallery_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (isCertificateImageClicked) {
                    openMultiFunctionCertificateImage();
                } else {
                    openMultiSelectionImage();
                }
            }
        });
        dialog.show();
    }

    private void openCameraImage() {
        intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES_FROM_CAMERA);
    }

    private void openMultiSelectionImage() {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("quickfix.sa.vendor.fileprovider")
                .setMaximumDisplayingImages(Integer.MAX_VALUE)
                .isMultiSelect()
                .setMinimumMultiSelectCount(1)
                .setMaximumMultiSelectCount(MULTI_SELECT_NATIONAL_MAX_VALUE)
                .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                .setMultiSelectDoneTextColor(R.color.primary_text) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                .disableOverSelectionMessage()
                .build();
        pickerDialog.show(getSupportFragmentManager(), "picker");
    }

    private void openMultiFunctionCertificateImage() {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("quickfix.sa.vendor.fileprovider")
                .setMaximumDisplayingImages(Integer.MAX_VALUE)
                .isMultiSelect()
                .setMinimumMultiSelectCount(1)
                .setMaximumMultiSelectCount(MULTI_SELECT_CERTIFICATE_MAX_VALUE)
                .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                .setMultiSelectDoneTextColor(R.color.primary_text) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                .disableOverSelectionMessage()
                .build();
        pickerDialog.show(getSupportFragmentManager(), "picker");
    }


    private void validateInput() {
        if (context != null) {
            String
                    years = years_tv.getText().toString().trim(),
                    months = months_tv.getText().toString().trim();

            if (profileImageList.size() == 0) {
                GlobalFunctions.displayMessageTop(context, mainView, getString(R.string.select_profile_photo));
            } else if (!isYearsSelected) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_select_year));
            } else if (!isMonthsSelected) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_select_month));
            } else if (uriIdImageList.size() == 0) {
                GlobalFunctions.displayMessageTop(context, mainView, getString(R.string.select_atleast_one_id));
            } else if (uriCertificatesImageList.size() == 0) {
                GlobalFunctions.displayMessageTop(context, mainView, getString(R.string.select_atleast_one_certificate));
            } else {
                if (individualRegisterModel == null) {
                    individualRegisterModel = new IndividualRegisterModel();
                }
                individualRegisterModel.setExperienceM(months);
                individualRegisterModel.setExperience(years);
                Intent intent = SelectServicesActivity.newInstance(activity, individualRegisterModel, fromPage);
                intent.putExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE, (Serializable) uriProfileImageList);
                intent.putExtra(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_LIST_CODE, (Serializable) uriIdImageList);
                intent.putExtra(GlobalVariables.UPLOAD_CERTIFICATES_LIST_CODE, (Serializable) uriCertificatesImageList);
                activity.startActivity(intent);
            }

        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //  Bitmap bitmap = null;
        //Uri selectedImageUri = null;
        GlobalFunctions.hideProgress();

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_IMAGE) {
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImageUri = null;
                    Uri uri = data.getParcelableExtra("path");
                    try {
                        // You can update this bitmap to your server
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                        selectedImageUri = uri;
                        /// setImagesView(selectedImageUri, bitmap);
                        if (isIdImageClicked) {
                            //doc image is clicked...
                            //this.imagePath = selectedImagePath;
                            uriIdImageList.add(selectedImageUri);
                            initNationalIdRecyclerView();
//                        setIdImageSelectedAlignMent(selectedImageUri);
                            MULTI_SELECT_NATIONAL_MAX_VALUE = MULTI_SELECT_NATIONAL_MAX_VALUE - 1;
                        } else if (isCertificateImageClicked) {
                            uriCertificatesImageList.add(selectedImageUri);
                            initCertificateIdRecyclerView();
                            MULTI_SELECT_CERTIFICATE_MAX_VALUE = MULTI_SELECT_CERTIFICATE_MAX_VALUE - 1;
                        } else if (isProfileImageClicked) {
                            // this.imagePath = selectedImagePath;
                            // selectUri = selectedImageUri;
                            setImagesToModel(bitmap, selectedImageUri);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == globalVariables.REQUEST_CODE_FOR_YEAR) {

                String selectedYear = (String) data.getExtras().getSerializable(BUNDLE_SELECTED_YEAR);
                if (selectedYear != null) {
                    isYearsSelected = true;
                    years_tv.setText(selectedYear);
                }

            } else if (requestCode == globalVariables.REQUEST_CODE_FOR_MONTH) {

                String selectedMonth = (String) data.getExtras().getSerializable(BUNDLE_SELECTED_MONTH);
                if (selectedMonth != null) {
                    isMonthsSelected = true;
                    months_tv.setText(selectedMonth);
                }

            }else if (requestCode == PICK_FILE_PDF) {
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImageUri = null;
                    selectedImageUri = data.getData();
                    if (isIdImageClicked) {
                        //doc image is clicked...
                        //this.imagePath = selectedImagePath;
                        uriIdImageList.add(selectedImageUri);
                        initNationalIdRecyclerView();
//                        setIdImageSelectedAlignMent(selectedImageUri);
                        MULTI_SELECT_NATIONAL_MAX_VALUE = MULTI_SELECT_NATIONAL_MAX_VALUE - 1;
                    } else if (isCertificateImageClicked) {
                        uriCertificatesImageList.add(selectedImageUri);
                        initCertificateIdRecyclerView();
                        MULTI_SELECT_CERTIFICATE_MAX_VALUE = MULTI_SELECT_CERTIFICATE_MAX_VALUE - 1;
                    }
                }
            }
        }
    }

    private void setImagesToModel(Bitmap bitmap, Uri selectUri) {
        if (profileImageList.size() == 0) {
            Glide.with(this).load(selectUri).into(vendor_iv);
            //vendor_iv.setImageBitmap(bitmap);
            profileImageList.add("1.jpg");
            uriProfileImageList.add(selectUri);
            //  bitmapProfileImageList.add(bitmap);
        }
    }

    private void removeIdImage(int position) {
        uriIdImageList.remove(position);
        initNationalIdRecyclerView();
    }

    private void removeCertificateImage(int position) {
        uriCertificatesImageList.remove(position);
        initCertificateIdRecyclerView();
    }

    private void openCancelAlertDialog(final Context context) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(activity.getString(R.string.appDeleteText));
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileImageList.clear();
                uriProfileImageList.clear();
                //  bitmapProfileImageList.clear();
                vendor_iv.setImageResource(R.drawable.ic_default_user);
                GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.deleted_successfully));
                alertDialog.dismiss();
            }
        });
        alertDialog.setNegativeButton(getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    private void openCropFuctionalImage() {
        CropImage.activity()
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(2, 2)
                .start(activity);

    }

    private void setSelectedImages() {

        //set month and year..
        if (individualRegisterModel != null) {
            if (individualRegisterModel.getExperienceM() != null) {
                months_tv.setText(individualRegisterModel.getExperienceM());
                isMonthsSelected = true;
            }

            if (individualRegisterModel.getExperience() != null) {
                years_tv.setText(individualRegisterModel.getExperience());
                isYearsSelected = true;

            }
        }

        //profile image....
        if (uriProfileImageList != null) {
            if (uriProfileImageList.size() > 0) {
                Glide.with(this).load(uriProfileImageList.get(0)).into(vendor_iv);
                profileImageList.add("1.jpg");
            }
        }

        //IdImage image....

        initNationalIdRecyclerView();

        /*if (uriIdImageList != null) {
            if (uriIdImageList.size() > 0) {

                if (uriIdImageList.size() == 1) {

                    Glide.with(this).load(uriIdImageList.get(0)).into(id_iv_one);
                    id_one_delete_iv.setVisibility(View.VISIBLE);
                } else if (uriIdImageList.size() == 2) {

                    Glide.with(this).load(uriIdImageList.get(0)).into(id_iv_one);
                    id_one_delete_iv.setVisibility(View.VISIBLE);

                    Glide.with(this).load(uriIdImageList.get(1)).into(id_iv_two);
                    id_two_delete_iv.setVisibility(View.VISIBLE);
                } else if (uriIdImageList.size() == 3) {

                    Glide.with(this).load(uriIdImageList.get(0)).into(id_iv_one);
                    id_one_delete_iv.setVisibility(View.VISIBLE);

                    Glide.with(this).load(uriIdImageList.get(1)).into(id_iv_two);
                    id_two_delete_iv.setVisibility(View.VISIBLE);

                    Glide.with(this).load(uriIdImageList.get(2)).into(id_iv_three);
                    id_three_delete_iv.setVisibility(View.VISIBLE);
                }
            }
        }*/

        //CertificateImage ....
        initCertificateIdRecyclerView();

        /*if (uriCertificatesImageList != null) {
            if (uriCertificatesImageList.size() > 0) {

                if (uriCertificatesImageList.size() == 1) {

                    Glide.with(this).load(uriCertificatesImageList.get(0)).into(certificate_iv_one);
                    certificate_one_delete_iv.setVisibility(View.VISIBLE);
                    certificateImageList.add("image_" + 1 + "_iv");
                } else if (uriCertificatesImageList.size() == 2) {

                    Glide.with(this).load(uriCertificatesImageList.get(0)).into(certificate_iv_one);
                    certificate_one_delete_iv.setVisibility(View.VISIBLE);
                    certificateImageList.add("image_" + 1 + "_iv");

                    Glide.with(this).load(uriCertificatesImageList.get(1)).into(certificate_iv_two);
                    certificate_two_delete_iv.setVisibility(View.VISIBLE);
                    certificateImageList.add("image_" + 2 + "_iv");
                } else if (uriCertificatesImageList.size() == 3) {

                    Glide.with(this).load(uriCertificatesImageList.get(0)).into(certificate_iv_one);
                    certificate_one_delete_iv.setVisibility(View.VISIBLE);
                    certificateImageList.add("image_" + 1 + "_iv");

                    Glide.with(this).load(uriCertificatesImageList.get(1)).into(certificate_iv_two);
                    certificate_two_delete_iv.setVisibility(View.VISIBLE);
                    certificateImageList.add("image_" + 2 + "_iv");

                    Glide.with(this).load(uriCertificatesImageList.get(2)).into(certificate_iv_three);
                    certificate_three_delete_iv.setVisibility(View.VISIBLE);
                    certificateImageList.add("image_" + 3 + "_iv");
                }
            }
        }*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void loadImage(File imageFile, ImageView ivImage) {
        Glide.with(activity).load(imageFile).into(ivImage);
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {
        if (isCertificateImageClicked) {
            MULTI_SELECT_CERTIFICATE_MAX_VALUE = MULTI_SELECT_CERTIFICATE_MAX_VALUE - uriList.size();
            for (int i = 1; i <= uriList.size(); i++) {
                uriCertificatesImageList.add(uriList.get(i - 1));
                initCertificateIdRecyclerView();
            }
        } else {
            MULTI_SELECT_NATIONAL_MAX_VALUE = MULTI_SELECT_NATIONAL_MAX_VALUE - uriList.size();
            for (int i = 1; i <= uriList.size(); i++) {
                uriIdImageList.add(uriList.get(i - 1));
                initNationalIdRecyclerView();
            }
        }

    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        //Do something with your Uri
    }

    public void initNationalIdRecyclerView() {
        if (uriIdImageList.size() > 0) {
            add_national_id_rv.setVisibility(View.VISIBLE);
            add_national_id_rv.setLayoutManager(linearLayoutManager);
            add_national_id_rv.setHasFixedSize(true);
            photosListAdapter = new PhotosListAdapter(activity, uriIdImageList, true, this);
            add_national_id_rv.setAdapter(photosListAdapter);
        } else {
            add_national_id_rv.setVisibility(View.GONE);
        }
    }

    public void initCertificateIdRecyclerView() {
        if (uriIdImageList.size() > 0) {
            add_certificate_id_rv.setVisibility(View.VISIBLE);
            add_certificate_id_rv.setLayoutManager(certificate_linearLayoutManager);
            add_certificate_id_rv.setHasFixedSize(true);
            photosListAdapter = new PhotosListAdapter(activity, uriCertificatesImageList, false, this);
            add_certificate_id_rv.setAdapter(photosListAdapter);
        } else {
            add_certificate_id_rv.setVisibility(View.GONE);
        }
    }

    @Override
    public void OnClickInvoke(int position, boolean isNationalId, Uri item) {
        if (isNationalId) {
            MULTI_SELECT_NATIONAL_MAX_VALUE = MULTI_SELECT_NATIONAL_MAX_VALUE + 1;
            removeIdImage(position);
        } else {
            MULTI_SELECT_CERTIFICATE_MAX_VALUE = MULTI_SELECT_CERTIFICATE_MAX_VALUE + 1;
            removeCertificateImage(position);
        }

    }
}