package sa.quickfix.vendor.flows.schedule_date;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.facebook.FacebookSdk;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.adapters.ScheduleDateListAdapter;
import sa.quickfix.vendor.adapters.adapter_interfaces.ShiftItemClickInvoke;
import sa.quickfix.vendor.addon.SelectDateFragment;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.AddScheduleModel;
import sa.quickfix.vendor.services.model.ScheduleModel;
import sa.quickfix.vendor.services.model.ShiftModel;
import sa.quickfix.vendor.services.model.ShiftsListModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AppTextView;


public class ScheduleDateActivity extends AppCompatActivity implements SelectDateFragment.DatePickerInterface, ShiftItemClickInvoke {

    public static final String TAG = "ScheduleDateActivity";

    Context context;
    private static Activity activity;
    View mainView;

    GlobalFunctions globalFunctions;
    GlobalVariables globalVariables;

    private CardView send_card_view;
    private ImageView back_iv, current_schedule_date_delete_iv;
    private AppTextView start_date_tv, end_date_etv;
    private TextView current_schedule_start_date_tv, current_schedule_end_date_tv;
    private LinearLayout current_schedule_ll;
    private RecyclerView shift_rv;
    ScheduleDateListAdapter adapter;
    private LinearLayoutManager linearLayoutManager;

    Calendar
            dateCalender;
    int
            hour = 0,
            minute = 0;

    String selectedStartDate, selectedEndDate = null;
    String selectedStartTime = "", selectedEndTime = "";
    boolean isEndDateClicked = false;

    List<ShiftModel> shiftList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.schedule_date_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);
        current_schedule_date_delete_iv = (ImageView) findViewById(R.id.current_schedule_date_delete_iv);

        start_date_tv = (AppTextView) findViewById(R.id.start_date_tv);
        end_date_etv = (AppTextView) findViewById(R.id.end_date_etv);

        current_schedule_end_date_tv = (TextView) findViewById(R.id.current_schedule_end_date_tv);
        current_schedule_start_date_tv = (TextView) findViewById(R.id.current_schedule_start_date_tv);
        current_schedule_ll = (LinearLayout) findViewById(R.id.current_schedule_ll);
        shift_rv = (RecyclerView) findViewById(R.id.shift_rv);

        linearLayoutManager = new LinearLayoutManager(activity);

        send_card_view = (CardView) findViewById(R.id.send_card_view);

        mainView = back_iv;

        getSchedule();

        dateCalender = Calendar.getInstance();
        dateCalender.setTimeZone(TimeZone.getDefault());
//        setDate(dateCalender);

        send_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        current_schedule_date_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                cancelSchedule();

            }
        });

        start_date_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEndDateClicked = false;
                SelectDate();
            }
        });

        end_date_etv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEndDateClicked = true;
                SelectDate();
            }
        });

        initRecyclerView();

    }

    public void initRecyclerView() {
        shift_rv.setLayoutManager(linearLayoutManager);
        adapter = new ScheduleDateListAdapter(activity, shiftList, this);
        shift_rv.setAdapter(adapter);
    }

    private void cancelSchedule(String shiftId) {
        globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.cancelSchedule(context, shiftId, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                Log.d(TAG, "Response : " + arg0.toString());
                globalFunctions.hideProgress();
                validateOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "GetSchedule");
    }

    private void SelectDate() {
        SelectDateFragment newFragment = new SelectDateFragment();
        newFragment.setListener(this);
        newFragment.setDisablePreviousDate(true);
        newFragment.setDefaultDate(dateCalender);
        newFragment.show(getSupportFragmentManager(), "Journey_Date_DatePicker");
    }

    private void getSchedule() {
        globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getSchedule(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                Log.d(TAG, "Response : " + arg0.toString());
                globalFunctions.hideProgress();
                if (arg0 instanceof ScheduleModel) {
                    ScheduleModel scheduleModel = (ScheduleModel) arg0;
                    setThisPage(scheduleModel);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "GetSchedule");
    }

    private void setThisPage(ScheduleModel scheduleModel) {

        if (scheduleModel != null && GlobalFunctions.isNotNullValue(scheduleModel.getStartDate())) {
            current_schedule_ll.setVisibility(View.GONE);

            if ((scheduleModel.getStartDate() != null)) {
//                current_schedule_start_date_tv.setText(GlobalFunctions.getSelectedFormatDateNew(scheduleModel.getStartDate()));
                start_date_tv.setText(GlobalFunctions.getSelectedFormatDateNew(scheduleModel.getStartDate()));
            }

           /* if ((scheduleModel.getEndDate() != null)) {
                current_schedule_end_date_tv.setText(GlobalFunctions.getSelectedFormatDateNew(scheduleModel.getEndDate()));
            }*/

//            start_date_etv.setText("");
//            start_date_etv.setHint(activity.getString(R.string.start_date));
//            end_date_etv.setText("");
//            end_date_etv.setHint(activity.getString(R.string.end_date));
        } else {
            current_schedule_ll.setVisibility(View.GONE);
        }

        setUpList(scheduleModel);

    }

    private void setUpList(ScheduleModel scheduleModel) {

        if (scheduleModel != null) {
            if (scheduleModel.getShiftsListModel() != null) {
                shiftList.clear();
                shiftList.addAll(scheduleModel.getShiftsListModel().getShiftList());

                if (adapter != null) {
                    synchronized (adapter) {
                        adapter.notifyDataSetChanged();
                    }
                }
                if (shiftList.size() <= 0) {
//                    showEmptyPage(getString(R.string.emptyNewRequestListMessage));
                    setUpFirstList();
                } else {
                    initRecyclerView();
//                    showContent();
                }
            } else {
                setUpFirstList();
            }
        }
    }

    private void setUpFirstList() {
        shiftList.clear();
        ShiftModel shiftModel = new ShiftModel();
        shiftModel.setId("0");
        shiftList.add(shiftModel);
        initRecyclerView();
    }

    private void validateInput() {
        String
                startDate = start_date_tv.getText().toString().trim();
//                endDate = end_date_etv.getText().toString().trim();

        if (startDate.isEmpty()) {
            globalFunctions.displayMessageTop(activity, mainView, activity.getString(R.string.select_start_date));
        } /*else if (endDate.isEmpty()) {
            globalFunctions.displayMessageTop(activity, mainView, activity.getString(R.string.select_end_date));
        }*/ else if (shiftList.size() == 0) {
            globalFunctions.displayMessageTop(activity, mainView, activity.getString(R.string.add_one_shift));
        } else if (!GlobalFunctions.isValidShiftList(shiftList)) {
            globalFunctions.displayMessageTop(activity, mainView, activity.getString(R.string.add_one_shift));
        } else {

            AddScheduleModel addScheduleModel = new AddScheduleModel();
            String finalSelectedStartDate = GlobalFunctions.getSelectedFormatDate(startDate);
//            String finalSelectedEndDate = GlobalFunctions.getSelectedFormatDate(endDate);
            addScheduleModel.setStartDate(finalSelectedStartDate);
            ShiftsListModel shiftsListModel = new ShiftsListModel();
            shiftsListModel.setShiftList(shiftList);
            addScheduleModel.setShiftsListModel(shiftsListModel);
//            scheduleModel.setEndDate(finalSelectedEndDate);
            //scheduleModel.setStartDate(startDate);
            //scheduleModel.setEndDate(endDate);
            createSchedule(context, addScheduleModel);

        }
    }

    private void createSchedule(final Context context, final AddScheduleModel scheduleModel) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.createSchedule(context, scheduleModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "ContactUs");
    }

    private void validateOutput(Object model) {
        if (model instanceof StatusModel) {
            final StatusModel statusModel = (StatusModel) model;
            globalFunctions.displayMessaage(context, mainView, statusModel.getMessage());
            if (statusModel.isStatus()) {
                getSchedule();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void onDateSet(Calendar date) {

        dateCalender.set(Calendar.YEAR, date.get(Calendar.YEAR));
        dateCalender.set(Calendar.MONTH, date.get(Calendar.MONTH));
        dateCalender.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH));

        setDate(dateCalender);
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    private void selectTime() {
        Calendar mcurrentTime = Calendar.getInstance();
        if (dateCalender != null) {
            mcurrentTime.setTimeInMillis(dateCalender.getTimeInMillis());
        } else {
            mcurrentTime = Calendar.getInstance();
        }

        final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        final int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if (dateCalender != null) {
                    setHour(selectedHour);
                    setMinute(selectedMinute);
                    dateCalender.set(Calendar.HOUR_OF_DAY, selectedHour);
                    dateCalender.set(Calendar.MINUTE, selectedMinute);
                    setTime(dateCalender);
                }
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void setTime(Calendar time) {
        if (isEndDateClicked) {
            selectedEndTime = (globalFunctions.getTimefromTimestamp((time.getTimeInMillis() / 1000), GlobalVariables.TIME_FORMAT, Locale.ENGLISH));
            end_date_etv.setText(selectedEndDate + " " + selectedEndTime);
        } else {
            selectedStartTime = (globalFunctions.getTimefromTimestamp((time.getTimeInMillis() / 1000), GlobalVariables.TIME_FORMAT, Locale.ENGLISH));
            start_date_tv.setText(selectedStartDate + " " + selectedStartTime);
        }
    }

    private void setDate(Calendar dateCalender) {
        if (isEndDateClicked) {
            selectedEndDate = (GlobalFunctions.getDatefromTimestamp((dateCalender.getTimeInMillis() / 1000), GlobalVariables.DATE_FORMAT, Locale.ENGLISH));
            end_date_etv.setText(selectedEndDate);
        } else {
            selectedStartDate = (GlobalFunctions.getDatefromTimestamp((dateCalender.getTimeInMillis() / 1000), GlobalVariables.DATE_FORMAT, Locale.ENGLISH));
            start_date_tv.setText(selectedStartDate);
        }
//        selectTime();
    }

    @Override
    public void OnClickInvoke(int position, ShiftModel shiftModel) {
        if (!shiftModel.getId().equalsIgnoreCase("0")) {
            cancelSchedule(shiftModel.getId());
        } else {
            shiftList.remove(shiftModel);
            if (shiftList.size()==0){
                setUpFirstList();
            }else {
                initRecyclerView();
            }
        }
    }

    @Override
    public void OnAddInvoke(int position, ShiftModel shiftModel) {
        shiftList.add(shiftModel);
        initRecyclerView();
    }
}