package sa.quickfix.vendor.flows.registration.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import sa.quickfix.vendor.flows.registration.activities.company_registration.CompanyRegistrationActivity;
import sa.quickfix.vendor.flows.registration.activities.individual_registration.IndividualRegistrationActivity;
import sa.quickfix.vendor.services.model.RegisterModel;
import sa.quickfix.vendor.R;

import com.facebook.FacebookSdk;


public class RegisterActivity extends AppCompatActivity {

    public static final String TAG = "RegisterActivity";

    public static final String BUNDLE_REGISTER_ACTIVITY_REGISTER_MODEL = "BundleRegisterActivityRegisterModel";

    Context context;
    private static Activity activity;
    View mainView;

    private CardView individual_card_view, company_card_view;

    RegisterModel myRegisterModel = null;

    public static Intent newInstance(Context context, RegisterModel model) {
        Intent intent = new Intent(context, RegisterActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_REGISTER_ACTIVITY_REGISTER_MODEL, model);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.register_activity);

        context = this;
        activity = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        individual_card_view = (CardView) findViewById(R.id.individual_card_view);
        company_card_view = (CardView) findViewById(R.id.company_card_view);

        mainView = individual_card_view;

        if (getIntent().hasExtra(BUNDLE_REGISTER_ACTIVITY_REGISTER_MODEL)) {
            myRegisterModel = (RegisterModel) getIntent().getSerializableExtra(BUNDLE_REGISTER_ACTIVITY_REGISTER_MODEL);
        }

        individual_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = IndividualRegistrationActivity.newInstance(context, myRegisterModel);
                startActivity(intent);
            }
        });

        company_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = CompanyRegistrationActivity.newInstance(context, myRegisterModel);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}