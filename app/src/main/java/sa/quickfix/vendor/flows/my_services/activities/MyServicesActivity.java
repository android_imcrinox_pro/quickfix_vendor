package sa.quickfix.vendor.flows.my_services.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.MyServicesListAdapter;
import sa.quickfix.vendor.flows.edit_services.EditServicesActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.CategoryListModel;
import sa.quickfix.vendor.services.model.CategoryModel;
import sa.quickfix.vendor.R;

import com.facebook.FacebookSdk;

import java.util.ArrayList;
import java.util.List;


public class MyServicesActivity extends AppCompatActivity {

    public static final String TAG = "MyServicesActivity";

    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";

    Context context;
    private static Activity activity;
    View mainView;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    private LayoutInflater layoutInflater;

   // private LinearLayout add_more_services_ll;

    CategoryListModel categoryListModel = new CategoryListModel();
    List<CategoryModel> list = new ArrayList<>();

    LinearLayoutManager categoryLayoutManager;
    RecyclerView category_recycler_view;
    MyServicesListAdapter myServicesListAdapter;

    private ImageView back_iv, edit_iv;

    String pageType = null;

    public static Intent newInstance(Activity activity, String pageType) {
        Intent intent = new Intent(activity, MyServicesActivity.class);
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_FROM_PAGE, pageType);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.my_services_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        this.layoutInflater = activity.getLayoutInflater();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        categoryLayoutManager = new LinearLayoutManager(context);

        back_iv = (ImageView) findViewById(R.id.back_iv);
        edit_iv = (ImageView) findViewById(R.id.edit_iv);
        category_recycler_view = (RecyclerView) findViewById(R.id.category_recycler_view);
      //  add_more_services_ll = (LinearLayout) findViewById(R.id.add_more_services_ll);

        mainView = back_iv;

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_PAGE)) {
            pageType = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_PAGE);
        } else {
            pageType = null;
        }

        initCategoryList();
        loadList(context);

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

      /*  add_more_services_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddMoreServiceDialog(context);
            }
        });*/
    }

    private void loadList(final Context context) {
        globalFunctions.showProgress(context, context.getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getCategoryList(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                CategoryListModel model = (CategoryListModel) arg0;
                setUpList(model);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                Log.d(TAG, "Failure : " + msg);
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                Log.d(TAG, "Error : " + msg);
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
            }
        }, "GetCategoryList");
    }

    private void setUpList(CategoryListModel categoryListModel1) {
        if (categoryListModel1 != null && list != null) {
            list.clear();
            if (categoryListModel == null) {
                categoryListModel = new CategoryListModel();
            }
            categoryListModel.setCategoryList(categoryListModel1.getCategoryList());

            if (categoryListModel.getCategoryList() != null) {
                //already selected list data..
                // from this list filter only selected data....
                list.clear();
                int listSizeMain = categoryListModel.getCategoryList().size();
                for (int i = 0; i < listSizeMain; i++) {
                    boolean isAdded = false;
                    if(categoryListModel.getCategoryList().get(i).getSubCategoryListModel() != null){
                        int listSizeSub = categoryListModel.getCategoryList().get(i).getSubCategoryListModel().getSubCategoryListModels().size();
                        if (listSizeSub > 0) {
                            for (int j = 0; j < listSizeSub; j++) {

                                if (!isAdded && categoryListModel.getCategoryList().get(i).getSubCategoryListModel().getSubCategoryListModels().get(j).getSelected().equalsIgnoreCase("1")) {
//                        if (list.size() == 0) {
                                    list.add(categoryListModel.getCategoryList().get(i));
//                        }
                                    isAdded = true;
                                }
                            }
                        } else if (categoryListModel.getCategoryList().get(i).getSelected().equalsIgnoreCase("1")) {
                            list.add(categoryListModel.getCategoryList().get(i));
                        }
                    }else if (categoryListModel.getCategoryList().get(i).getSelected().equalsIgnoreCase("1")) {
                        list.add(categoryListModel.getCategoryList().get(i));
                    }
                }
                if (list.size() > 0) {
                    initCategoryList();
                }

            }

            edit_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = EditServicesActivity.newInstance(activity, categoryListModel);
                    activity.startActivity(intent);
                }
            });

        }
    }

    private void initCategoryList() {
        myServicesListAdapter = new MyServicesListAdapter(activity, list);
        category_recycler_view.setLayoutManager(categoryLayoutManager);
        category_recycler_view.setAdapter(myServicesListAdapter);

        synchronized (myServicesListAdapter) {
            myServicesListAdapter.notifyDataSetChanged();
        }
    }

    private void openAddMoreServiceDialog(final Context context) {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.add_custom_service, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        TextView submit_request_tv;

        submit_request_tv = (TextView) alertView.findViewById(R.id.submit_request_tv);

        submit_request_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
             /*   Intent intent = PaymentMethodActivity.newInstance(activity, "3");
                startActivity(intent);*/
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}