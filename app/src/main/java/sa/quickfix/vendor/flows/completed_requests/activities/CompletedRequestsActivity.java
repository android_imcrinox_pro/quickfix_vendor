package sa.quickfix.vendor.flows.completed_requests.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.CompletedRequestListAdapter;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.DateModel;
import sa.quickfix.vendor.services.model.OrderListMainModel;
import sa.quickfix.vendor.services.model.OrderListModel;
import sa.quickfix.vendor.services.model.OrderModel;
import sa.quickfix.vendor.services.model.OrderPostModel;
import sa.quickfix.vendor.R;

import com.facebook.FacebookSdk;
import com.vlonjatg.progressactivity.ProgressRelativeLayout;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.List;

public class CompletedRequestsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "CompletedListActivity";

    Context context;
    private static Activity activity;

    SwipeRefreshLayout swipe_container;
    ProgressRelativeLayout progressActivity;
    DilatingDotsProgressBar progressBar;

    RecyclerView recyclerView;
    CompletedRequestListAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    OrderListModel detail = null;
    List<OrderModel> list = new ArrayList<>();

    List<DateModel> dateList = new ArrayList<>();

    OrderPostModel orderPostModel = null;

    int index = 0;
    int preLast = 0;
    boolean dataEnd = false;
    boolean loadingList = false;

    int visibleItemCount = 0, totalItemCount = 0, pastVisiblesItems = 0;

    GlobalFunctions globalFunctions = null;
    GlobalVariables globalVariables = null;

    private ImageView back_iv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.completed_requests_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);
        swipe_container = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(activity);
        progressActivity = (ProgressRelativeLayout) findViewById(R.id.details_progressActivity);
        progressBar = (DilatingDotsProgressBar) findViewById(R.id.extraProgressBar);

        if (detail == null) {
            index = 0;
            preLast = 0;
            dataEnd = false;
            detail = null;
        }

        initRecyclerView();

        swipe_container.setOnRefreshListener(this);
        swipe_container.setColorSchemeResources(R.color.fab_color_normal,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });

        /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (!isLoadingList()) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            //Log.v("...", "Last Item Wow !");
                            final int lastItem = pastVisiblesItems + visibleItemCount;
                            if (lastItem == totalItemCount) {
                                if (preLast != lastItem) { //to avoid multiple calls for last item
                                    preLast = lastItem;
                                    orderPostModel.setIndex(String.valueOf(getIndex()));
                                    orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
                                    orderPostModel.setStatus(globalVariables.HISTORY_REQUEST);
                                    completedRequestList(context);
                                }
                            }
                        }

                    }
                }
            }
        });*/

        //   swipe_container.add

          if (orderPostModel == null) {
            orderPostModel = new OrderPostModel();
        }
        orderPostModel.setIndex("0");
        orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
        orderPostModel.setStatus(globalVariables.HISTORY_REQUEST);
        list.clear();
        completedRequestList(context);


        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onResume() {
       /* if (orderPostModel == null) {
            orderPostModel = new OrderPostModel();
        }
        orderPostModel.setIndex("0");
        orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
        orderPostModel.setStatus(globalVariables.HISTORY_REQUEST);
        list.clear();
        completedRequestList(context);*/
        super.onResume();
    }

    private void completedRequestList(final Context context) {
        if (!isLoading()) {
            if (index == 0) {
                showLoading();
            } else {
                showExtraLoading();
            }
            //globalFunctions.showProgress(activity, getString(R.string.loading));
            ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
            servicesMethodsManager.getOrderList(context, orderPostModel, new ServerResponseInterface() {
                @Override
                public void OnSuccessFromServer(Object arg0) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        Log.d(TAG, "Response: " + arg0.toString());
                        if (index == 0) {
                            showContent();
                        } else {
                            hideExtraLoading();
                        }
                        setLoadingList(false);
                        setIndex(getIndex() + 1);
                        OrderListMainModel model = (OrderListMainModel) arg0;
                        setUpList(model);
                    }
                }

                @Override
                public void OnFailureFromServer(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        if (index == 0) {
                            showErrorPage();
                        } else {
                            hideExtraLoading();
                        }
                        Log.d(TAG, "Failure : " + msg);
                    }
                    // globalFunctions.hideProgress();
                    //globalFunctions.displayMessaage(activity, mainView, msg);
                    //Log.d(TAG, "Failure : " + msg);
                }

                @Override
                public void OnError(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        if (index == 0) {
                            showErrorPage();
                        } else {
                            hideExtraLoading();
                        }
                        Log.d(TAG, "Error : " + msg);
                    }
                    //globalFunctions.hideProgress();
                    // globalFunctions.displayMessaage(activity, mainView, msg);
                    //Log.d(TAG, "Error : " + msg);
                }
            }, "CompletedRequestList");
        }
    }

    private void setUpList(OrderListMainModel orderListMainModel) {
        if (orderListMainModel != null) {
            if (orderListMainModel.getOrderList() != null) {
                detail = orderListMainModel.getOrderList();
                list.addAll(orderListMainModel.getOrderList().getOrderList());
                if (adapter != null) {
                    synchronized (adapter) {
                        adapter.notifyDataSetChanged();
                    }
                }
                if (list.size() <= 0) {
                    showEmptyPage();
                } else {
                    setUpDateWiseList();
                    initRecyclerView();
                    showContent();
                }
            }
        }
    }

    private void setUpDateWiseList() {
        ArrayList<String> updatedList = new ArrayList<String>();

        updatedList.clear();

        for (int i = 0; i < list.size(); i++) {
            if (!updatedList.contains(list.get(i).getDate())) {
                updatedList.add(list.get(i).getDate());
                list.get(i).setAddedDate(true);
            }
        }
    }

    public void initRecyclerView() {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager.onSaveInstanceState();
        Parcelable state = linearLayoutManager.onSaveInstanceState();
        adapter = new CompletedRequestListAdapter(activity, list);
        recyclerView.setAdapter(adapter);
        linearLayoutManager.onRestoreInstanceState(state);
    }

    private void showExtraLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.showNow();
        }
    }

    private void hideExtraLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            progressBar.hideNow();
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isDataEnd() {
        return dataEnd;
    }

    public void setDataEnd(boolean dataEnd) {
        this.dataEnd = dataEnd;
    }

    public boolean isLoadingList() {
        return loadingList;
    }

    public void setLoadingList(boolean loadingList) {
        this.loadingList = loadingList;
    }

    private void showLoading() {
        if (progressActivity != null) {
            progressActivity.showLoading();
        }
    }

    private boolean isLoading() {
        if (progressActivity != null) {
            return progressActivity.isLoadingCurrentState();
        }
        return false;
    }

    private void showContent() {
        if (progressActivity != null) {
            progressActivity.showContent();
        }
    }

    private void showEmptyPage() {
        if (progressActivity != null) {
            progressActivity.showEmpty(getResources().getDrawable(R.drawable.ic_logo), getString(R.string.emptyRequestListMessage),
                    "");
        }

    }

    private void showErrorPage() {
        if (progressActivity != null) {
            progressActivity.showError(getResources().getDrawable(R.drawable.app_icon), getString(R.string.noConnection),
                    getString(R.string.noConnectionErrorMessage),
                    getString(R.string.tryAgain), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            refreshList();
                        }
                    });
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(CompletedRequestsActivity.this, MainActivity.class);
        startActivity(intent);
        closeThisActivity();
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void refreshList() {
        index = 0;
        preLast = 0;
        dataEnd = false;
        detail = null;
        list.clear();
        if (orderPostModel == null) {
            orderPostModel = new OrderPostModel();
        }
        orderPostModel.setIndex("0");
        orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
        orderPostModel.setStatus(globalVariables.HISTORY_REQUEST);
        completedRequestList(context);
    }
}