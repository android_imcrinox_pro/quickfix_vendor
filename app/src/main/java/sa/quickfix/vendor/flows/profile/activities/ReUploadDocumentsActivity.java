package sa.quickfix.vendor.flows.profile.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.flows.image_picker.ImagePickerActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.CertificatesListModel;
import sa.quickfix.vendor.services.model.CertificatesModel;
import sa.quickfix.vendor.services.model.IdImageModel;
import sa.quickfix.vendor.services.model.IdImagesListModel;
import sa.quickfix.vendor.services.model.IndividualRegisterModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.upload.UploadImage;
import sa.quickfix.vendor.upload.UploadListener;
import sa.quickfix.vendor.view.AlertDialog;


public class ReUploadDocumentsActivity extends AppCompatActivity implements UploadListener {

    public static final String TAG = "ReUploadActivity";

    public static final String BUNDLE_INDIVIDUAL_REGISTER_MODEL = "BundleIndividualRegisterModel";
    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";
    public static final String BUNDLE_SELECTED_YEAR = "BundleSelectedYear";
    public static final String BUNDLE_SELECTED_MONTH = "BundleSelectedMonth";

    private static final int PERMISSION_REQUEST_CODE = 200;

    public static final int REQUEST_IMAGE = 100;
    private static final int PICK_FILE_PDF = 200;

    Context context;
    private static Activity activity;
    LayoutInflater layoutInflater;
    View mainView;

    private boolean
            isIdImageBlocked = false,
            isCertificateImageBlocked = false;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    ProfileModel profileModel = null;

    List<String> idImageList, certificateImageList;

    IdImagesListModel idImageListModel = new IdImagesListModel();
    List<IdImageModel> myIdImageList = new ArrayList<>();

    CertificatesListModel certificateListModel = new CertificatesListModel();
    List<CertificatesModel> certificateList = new ArrayList<>();

    List<Uri>
            uriIdImageList,
            uriCertificatesImageList;

    List<Bitmap>
            bitmapProfileImageList,
            bitmapIdImageList,
            btmapCertificateImageList;

    String imagePath = "";

    String typeOfDocumentClicked = "";
    boolean isIdImageClicked = false;

    private ImageView back_iv;
    private LinearLayout id_images_ll, certificates_ll;

    String fromPage = null;
    IndividualRegisterModel individualRegisterModel = null;

    private CardView continue_cardview;
    private ImageView id_iv_one, id_one_delete_iv, id_iv_two, id_two_delete_iv, id_iv_three,
            id_three_delete_iv, certificate_iv_one, certificate_one_delete_iv, certificate_iv_two, certificate_two_delete_iv, certificate_iv_three, certificate_three_delete_iv;

    public static Intent newInstance(Context context, IndividualRegisterModel model) {
        Intent intent = new Intent(context, ReUploadDocumentsActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_INDIVIDUAL_REGISTER_MODEL, model);
        intent.putExtras(args);
        return intent;
    }

    public static Intent newInstance(Context context, IndividualRegisterModel model, String fromPage) {
        Intent intent = new Intent(context, ReUploadDocumentsActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_INDIVIDUAL_REGISTER_MODEL, model);
        args.putString(BUNDLE_KEY_FROM_PAGE, fromPage);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.re_upload_documents_activity);

        context = this;
        activity = this;

        layoutInflater = activity.getLayoutInflater();

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        idImageList = new ArrayList<>();
        uriIdImageList = new ArrayList<>();
        bitmapIdImageList = new ArrayList<>();

        certificateImageList = new ArrayList<>();
        uriCertificatesImageList = new ArrayList<>();
        btmapCertificateImageList = new ArrayList<>();

        uriIdImageList.clear();
        uriCertificatesImageList.clear();

        if (getIntent().hasExtra(BUNDLE_INDIVIDUAL_REGISTER_MODEL)) {
            individualRegisterModel = (IndividualRegisterModel) getIntent().getSerializableExtra(BUNDLE_INDIVIDUAL_REGISTER_MODEL);
        } else {
            individualRegisterModel = null;
        }

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_PAGE)) {
            fromPage = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_PAGE);
        } else {
            fromPage = null;
        }

        continue_cardview = (CardView) findViewById(R.id.continue_cardview);

        back_iv = (ImageView) findViewById(R.id.back_iv);

        id_images_ll = (LinearLayout) findViewById(R.id.id_images_ll);
        certificates_ll = (LinearLayout) findViewById(R.id.certificates_ll);

        id_iv_one = (ImageView) findViewById(R.id.id_iv_one);
        id_one_delete_iv = (ImageView) findViewById(R.id.id_one_delete_iv);
        id_iv_two = (ImageView) findViewById(R.id.id_iv_two);
        id_two_delete_iv = (ImageView) findViewById(R.id.id_two_delete_iv);
        id_iv_three = (ImageView) findViewById(R.id.id_iv_three);
        id_three_delete_iv = (ImageView) findViewById(R.id.id_three_delete_iv);
        certificate_iv_one = (ImageView) findViewById(R.id.certificate_iv_one);
        certificate_one_delete_iv = (ImageView) findViewById(R.id.certificate_one_delete_iv);
        certificate_iv_two = (ImageView) findViewById(R.id.certificate_iv_two);
        certificate_two_delete_iv = (ImageView) findViewById(R.id.certificate_two_delete_iv);
        certificate_iv_three = (ImageView) findViewById(R.id.certificate_iv_three);
        certificate_three_delete_iv = (ImageView) findViewById(R.id.certificate_three_delete_iv);

        mainView = continue_cardview;

        id_images_ll.setVisibility(View.GONE);
        certificates_ll.setVisibility(View.GONE);

        setThisPage();

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //id doc.....
        id_iv_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriIdImageList.size() == 0) {
                    typeOfDocumentClicked = globalVariables.ID_IMAGE_CLICKED;
                    isIdImageClicked = true;
                    openFileAlertDialog();
//                    openCropFuctionalImage();
                }
            }
        });

        id_iv_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriIdImageList.size() == 1) {
                    typeOfDocumentClicked = globalVariables.ID_IMAGE_CLICKED;
                    isIdImageClicked = true;
                    openFileAlertDialog();
//                    openCropFuctionalImage();
                }
            }
        });

        id_iv_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriIdImageList.size() == 2) {
                    typeOfDocumentClicked = globalVariables.ID_IMAGE_CLICKED;
                    isIdImageClicked = true;
                    openFileAlertDialog();
//                    openCropFuctionalImage();
                }
            }
        });

        id_one_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 0;
               /* if (imageServerList.size() > 0) {
                    confirmDeleteAlertDialog(position);
                } else {*/
                removeIdImage(position);
                //}
            }
        });

        id_two_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 1;
              /*  if (imageServerList.size() > 1) {
                    confirmDeleteAlertDialog(position);
                } else {*/
                removeIdImage(position);
                //}
            }
        });

        id_three_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 2;
               /* if (imageServerList.size() > 2) {
                    confirmDeleteAlertDialog(position);
                } else {*/
                removeIdImage(position);
                //  }
            }
        });

        ////certificate images.....

        certificate_iv_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriCertificatesImageList.size() == 0) {
//                    openCropFuctionalImage();
                    openFileAlertDialog();
                    typeOfDocumentClicked = globalVariables.CERTIFICATE_IMAGE_CLICKED;
                }
            }
        });

        certificate_iv_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriCertificatesImageList.size() == 1) {
                    typeOfDocumentClicked = globalVariables.CERTIFICATE_IMAGE_CLICKED;
//                    openCropFuctionalImage();
                    openFileAlertDialog();
                }
            }
        });

        certificate_iv_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriCertificatesImageList.size() == 2) {
                    typeOfDocumentClicked = globalVariables.CERTIFICATE_IMAGE_CLICKED;
//                    openCropFuctionalImage();
                    openFileAlertDialog();
                }
            }
        });

        certificate_one_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 0;
               /* if (imageServerList.size() > 0) {
                    confirmDeleteAlertDialog(position);
                } else {*/
                removeCertificateImage(position);
                //}
            }
        });

        certificate_two_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 1;
              /*  if (imageServerList.size() > 1) {
                    confirmDeleteAlertDialog(position);
                } else {*/
                removeCertificateImage(position);
                //}
            }
        });

        certificate_three_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 2;
               /* if (imageServerList.size() > 2) {
                    confirmDeleteAlertDialog(position);
                } else {*/
                removeCertificateImage(position);
                //  }
            }
        });

    }

    private void openFileAlertDialog() {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.add_custom_file_dialog, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        ImageView image_iv, pdf_iv;

        image_iv = (ImageView) alertView.findViewById(R.id.image_iv);
        pdf_iv = (ImageView) alertView.findViewById(R.id.pdf_iv);

        image_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openCropFuctionalImage();
            }
        });

        pdf_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openPdfSelectFile();

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void openPdfSelectFile() {

        Intent intentPDF = new Intent(Intent.ACTION_GET_CONTENT);
        intentPDF.setType("application/pdf");
        intentPDF.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intentPDF, PICK_FILE_PDF);
    }

    private void setThisPage() {
        ProfileModel mProfileModel = globalFunctions.getProfile(context);
        if (mProfileModel != null) {

            if (mProfileModel.getIdImageStatus() != null) {
                if (mProfileModel.getIdImageStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_BLOCKED)) {
                    isIdImageBlocked = true;
                }
            }

            if (mProfileModel.getCertificateImgStatus() != null) {
                if (mProfileModel.getCertificateImgStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_BLOCKED)) {
                    isCertificateImageBlocked = true;
                }
            }

            if (isIdImageBlocked) {
                id_images_ll.setVisibility(View.VISIBLE);
            } else {
                id_images_ll.setVisibility(View.GONE);
            }

            if (isCertificateImageBlocked) {
                certificates_ll.setVisibility(View.VISIBLE);
            } else {
                certificates_ll.setVisibility(View.GONE);
            }

            continue_cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    validateInput();
                }
            });
        }
    }

    private void validateInput() {
        if (context != null) {

            if (isIdImageBlocked && idImageList.size() == 0) {
                globalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_id_image));
            } else if (isCertificateImageBlocked && certificateImageList.size() == 0) {
                globalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_certificate_image));
            } else {
                if (profileModel == null) {
                    profileModel = new ProfileModel();
                }
                profileModel = globalFunctions.getProfile(context);
                CertificatesListModel certificatesListModel = new CertificatesListModel();
                IdImagesListModel idImagesListModel = new IdImagesListModel();
                profileModel.setCertificatesList(certificatesListModel);
                profileModel.setIdImageList(idImagesListModel);

                if (uriIdImageList.size() != 0) {
                    uploadImage(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_PATH);
                } else if (uriCertificatesImageList.size() != 0) {
                    uploadImage(GlobalVariables.UPLOAD_CERTIFICATE_PHOTO_PATH);
                }
            }
        }
    }

    private void uploadImage(String type) {
        if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_PATH)) {
            if (uriIdImageList.size() != 0) {
                globalFunctions.showProgress(activity, context.getString(R.string.uploading_images));
                for (int j = 0; j < uriIdImageList.size(); j++) {
                    Uri uri = (uriIdImageList.get(j));
                    UploadImage uploadImage = new UploadImage(context);
                    uploadImage.startUploading(uri, type, "image", this);
                }
            }
        } else if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_CERTIFICATE_PHOTO_PATH)) {
            if (uriCertificatesImageList.size() != 0) {
                globalFunctions.showProgress(activity, context.getString(R.string.uploading_images));
                for (int j = 0; j < uriCertificatesImageList.size(); j++) {
                    Uri uri = (uriCertificatesImageList.get(j));
                    UploadImage uploadImage = new UploadImage(context);
                    uploadImage.startUploading(uri, type, "image", this);
                }
            }
        } else {
            globalFunctions.hideProgress();
            updateProfile(context, profileModel);
        }
    }

    private void updateProfile(final Context context, ProfileModel profileModel) {
        GlobalFunctions.hideProgress();
        globalFunctions.showProgress(activity, getString(R.string.updatingProfile));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.updateUser(context, profileModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                checkUpdateAfterUpdate(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "UpdateUser");
    }

    private void checkUpdateAfterUpdate(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
        } else {
            /*Profile Model */
            ProfileModel profileModel = (ProfileModel) model;
            globalFunctions.setProfile(context, profileModel);
            showAlertMessage(context.getString(R.string.updated_successfully));
        }
    }

    private void showAlertMessage(String message) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                MainActivity.setProfileImage();
                ProfileActivity.closeThisActivity();
                Intent intent = new Intent(ReUploadDocumentsActivity.this, ProfileActivity.class);
                startActivity(intent);
                closeThisActivity();
            }
        });
        alertDialog.show();
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        GlobalFunctions.hideProgress();
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectUri = null;
                Uri uri = data.getParcelableExtra("path");
                selectUri = uri;
                setImagesToModel(selectUri);
            }
        } else if (requestCode == PICK_FILE_PDF) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectUri = null;
                selectUri = data.getData();
                setImagesToModel(selectUri);
            }
        }
    }

    private void setImagesToModel(Uri selectUri) {
        // check here which document is selected and set .....
        if (typeOfDocumentClicked != null) {
            if (typeOfDocumentClicked.equalsIgnoreCase(globalVariables.ID_IMAGE_CLICKED)) {
                setIdImageSelectedAlignMent(0, selectUri);
            } else if (typeOfDocumentClicked.equalsIgnoreCase(globalVariables.CERTIFICATE_IMAGE_CLICKED)) {
                setCertificateImageSelectedAlignMent(0, selectUri);
            }
        }
    }

    private void setCertificateImageSelectedAlignMent(int position, Uri uriList) {

        if (certificateImageList.size() == 0) {
            String file_sting= String.valueOf(uriList);
            if (!file_sting.contains(".jpg") && !file_sting.contains(".jpeg") && !file_sting.contains(".png")){
                certificate_iv_one.setImageDrawable(activity.getResources().getDrawable(R.drawable.pdf_icon));
            }else {
                Glide.with(this).load(uriList).into(certificate_iv_one);
            }
            certificate_one_delete_iv.setVisibility(View.VISIBLE);
            certificateImageList.add("image_" + 1 + "_iv");
            uriCertificatesImageList.add(uriList);

        } else if (certificateImageList.size() > 0) {

            if (certificateImageList.size() == 1) {
                String file_sting= String.valueOf(uriList);
                if (!file_sting.contains(".jpg") && !file_sting.contains(".jpeg") && !file_sting.contains(".png")){
                    certificate_iv_two.setImageDrawable(activity.getResources().getDrawable(R.drawable.pdf_icon));
                }else {
                    Glide.with(this).load(uriList).into(certificate_iv_two);
                }
//                Glide.with(this).load(uriList).into(certificate_iv_two);
                certificate_two_delete_iv.setVisibility(View.VISIBLE);
                certificateImageList.add("image_" + 2 + "_iv");
                uriCertificatesImageList.add(uriList);

            } else if (certificateImageList.size() == 2) {
                String file_sting= String.valueOf(uriList);
                if (!file_sting.contains(".jpg") && !file_sting.contains(".jpeg") && !file_sting.contains(".png")){
                    certificate_iv_three.setImageDrawable(activity.getResources().getDrawable(R.drawable.pdf_icon));
                }else {
                    Glide.with(this).load(uriList).into(certificate_iv_three);
                }
//                Glide.with(this).load(uriList).into(certificate_iv_three);
                certificate_three_delete_iv.setVisibility(View.VISIBLE);
                certificateImageList.add("image_" + 3 + "_iv");
                uriCertificatesImageList.add(uriList);

            }
        }
    }

    private void setIdImageSelectedAlignMent(int position, Uri uriList) {

        if (idImageList.size() == 0) {
            String file_sting= String.valueOf(uriList);
            if (!file_sting.contains(".jpg") && !file_sting.contains(".jpeg") && !file_sting.contains(".png")){
                id_iv_one.setImageDrawable(activity.getResources().getDrawable(R.drawable.pdf_icon));
            }else {
                Glide.with(this).load(uriList).into(id_iv_one);
            }
//            Glide.with(this).load(uriList).into(id_iv_one);
            id_one_delete_iv.setVisibility(View.VISIBLE);
            idImageList.add("image_" + 1 + "_iv");
            uriIdImageList.add(uriList);

        } else if (idImageList.size() > 0) {

            if (idImageList.size() == 1) {
                String file_sting= String.valueOf(uriList);
                if (!file_sting.contains(".jpg") && !file_sting.contains(".jpeg") && !file_sting.contains(".png")){
                    id_iv_two.setImageDrawable(activity.getResources().getDrawable(R.drawable.pdf_icon));
                }else {
                    Glide.with(this).load(uriList).into(id_iv_two);
                }
//                Glide.with(this).load(uriList).into(id_iv_two);
                id_two_delete_iv.setVisibility(View.VISIBLE);
                idImageList.add("image_" + 2 + "_iv");
                uriIdImageList.add(uriList);

            } else if (idImageList.size() == 2) {
                String file_sting= String.valueOf(uriList);
                if (!file_sting.contains(".jpg") && !file_sting.contains(".jpeg") && !file_sting.contains(".png")){
                    id_iv_three.setImageDrawable(activity.getResources().getDrawable(R.drawable.pdf_icon));
                }else {
                    Glide.with(this).load(uriList).into(id_iv_three);
                }
//                Glide.with(this).load(uriList).into(id_iv_three);
                id_three_delete_iv.setVisibility(View.VISIBLE);
                idImageList.add("image_" + 3 + "_iv");
                uriIdImageList.add(uriList);

            }
        }
    }

    private void removeIdImage(int position) {
       /* if (updateProfileModel != null) {
            //from update....
            galleryImageList.remove(position);
            uriGalleryImageList.remove(position);
            setServerImagesAlignment();

        } else {*/
        idImageList.remove(position);
        uriIdImageList.remove(position);
        setIdmageAlignment();
        // }
    }

    private void setIdmageAlignment() {
        try {

            if (idImageList.size() == 0) {
                id_iv_one.setImageResource(R.drawable.ic_upload_cover_image_default);
                id_one_delete_iv.setVisibility(View.GONE);
            } else if (idImageList.size() > 0) {
                if (idImageList.size() == 1) {
                    idImageList.clear();
                    id_one_delete_iv.setVisibility(View.VISIBLE);
                    idImageList.add("image_" + 1 + "_iv");
                    Glide.with(this).load(uriIdImageList.get(0)).into(id_iv_one);

                    id_iv_two.setImageResource(R.drawable.ic_upload_cover_image_default);
                    id_two_delete_iv.setVisibility(View.GONE);

                } else if (idImageList.size() == 2) {
                    idImageList.clear();
                    id_one_delete_iv.setVisibility(View.VISIBLE);
                    idImageList.add("image_" + 1 + "_iv");
                    Glide.with(this).load(uriIdImageList.get(0)).into(id_iv_one);

                    idImageList.add("image_" + 2 + "_iv");
                    Glide.with(this).load(uriIdImageList.get(1)).into(id_iv_two);
                    id_two_delete_iv.setVisibility(View.VISIBLE);

                    id_iv_three.setImageResource(R.drawable.ic_upload_cover_image_default);
                    id_three_delete_iv.setVisibility(View.GONE);
                } else if (idImageList.size() == 3) {
                    idImageList.clear();

                    idImageList.add("image_" + 1 + "_iv");
                    Glide.with(this).load(uriIdImageList.get(0)).into(id_iv_one);
                    id_one_delete_iv.setVisibility(View.VISIBLE);

                    idImageList.add("image_" + 2 + "_iv");
                    Glide.with(this).load(uriIdImageList.get(1)).into(id_iv_two);
                    id_two_delete_iv.setVisibility(View.VISIBLE);

                    idImageList.add("image_" + 3 + "_iv");
                    Glide.with(this).load(uriIdImageList.get(2)).into(id_iv_three);
                    id_three_delete_iv.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            GlobalFunctions.displayMessaage(activity, mainView, context.getString(R.string.something_went_wrong_message));
        }
    }

    private void setCertificateImageAlignment() {
        try {

            if (certificateImageList.size() == 0) {
                certificate_iv_one.setImageResource(R.drawable.ic_upload_cover_image_default);
                certificate_one_delete_iv.setVisibility(View.GONE);
            } else if (certificateImageList.size() > 0) {
                if (certificateImageList.size() == 1) {
                    certificateImageList.clear();
                    certificate_one_delete_iv.setVisibility(View.VISIBLE);
                    certificateImageList.add("image_" + 1 + "_iv");
                    Glide.with(this).load(uriCertificatesImageList.get(0)).into(certificate_iv_one);

                    certificate_iv_two.setImageResource(R.drawable.ic_upload_cover_image_default);
                    certificate_two_delete_iv.setVisibility(View.GONE);

                } else if (certificateImageList.size() == 2) {
                    certificateImageList.clear();
                    certificate_one_delete_iv.setVisibility(View.VISIBLE);
                    certificateImageList.add("image_" + 1 + "_iv");
                    Glide.with(this).load(uriCertificatesImageList.get(0)).into(certificate_iv_one);

                    certificateImageList.add("image_" + 2 + "_iv");
                    Glide.with(this).load(uriCertificatesImageList.get(1)).into(certificate_iv_two);
                    certificate_two_delete_iv.setVisibility(View.VISIBLE);

                    certificate_iv_three.setImageResource(R.drawable.ic_upload_cover_image_default);
                    certificate_three_delete_iv.setVisibility(View.GONE);
                } else if (certificateImageList.size() == 3) {
                    certificateImageList.clear();

                    certificateImageList.add("image_" + 1 + "_iv");
                    Glide.with(this).load(uriCertificatesImageList.get(0)).into(certificate_iv_one);
                    certificate_one_delete_iv.setVisibility(View.VISIBLE);

                    certificateImageList.add("image_" + 2 + "_iv");
                    Glide.with(this).load(uriCertificatesImageList.get(1)).into(certificate_iv_two);
                    certificate_two_delete_iv.setVisibility(View.VISIBLE);

                    certificateImageList.add("image_" + 3 + "_iv");
                    Glide.with(this).load(uriCertificatesImageList.get(2)).into(certificate_iv_three);
                    certificate_three_delete_iv.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            GlobalFunctions.displayMessaage(activity, mainView, context.getString(R.string.something_went_wrong_message));
        }
    }

    private void removeCertificateImage(int position) {
       /* if (updateProfileModel != null) {
            //from update....
            galleryImageList.remove(position);
            uriGalleryImageList.remove(position);
            setServerImagesAlignment();

        } else {*/
        certificateImageList.remove(position);
        uriCertificatesImageList.remove(position);
        setCertificateImageAlignment();
        // }
    }

    private void openCropFuctionalImage() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    /*    CropImage.activity()
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(2, 2)
                .start(activity);*/

    }

    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(ReUploadDocumentsActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(ReUploadDocumentsActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(ReUploadDocumentsActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void OnSuccess(final String imgUrl, final String type, final String uploadingFile) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                globalFunctions.displayMessaage(activity, mainView, "Uploaded");
                //GlobalFunctions.hideProgress();
                if (uploadingFile.equalsIgnoreCase("image")) {
                    setUploadedImagesToModel(type, imgUrl);
                }
            }
        });

    }

    private void setUploadedImagesToModel(String type, String imagePath) {
        if (type != null) {
            if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_PATH)) {
                setIdImagesList(imagePath);
            } else if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_CERTIFICATE_PHOTO_PATH)) {
                //globalFunctions.hideProgress();
                //  globalFunctions.showProgress(activity, getString(R.string.loading));
                setCertificatesImagesList(imagePath);
            }
        }
    }

    private void setCertificatesImagesList(String imagePath) {
        CertificatesModel certificateImageModel = new CertificatesModel();
        certificateImageModel.setImage(imagePath);
        certificateList.add(certificateImageModel);
        if (uriCertificatesImageList.size() == certificateList.size()) {
            certificateListModel.setCertificateList(certificateList);
            profileModel.setCertificatesList(certificateListModel);
            //GlobalFunctions.hideProgress();
            updateProfile(context, profileModel);
        } else {
            GlobalFunctions.hideProgress();
        }
    }

    private void setIdImagesList(String imagePath) {
        IdImageModel idImageModel = new IdImageModel();
        idImageModel.setImage(imagePath);
        myIdImageList.add(idImageModel);
        if (uriIdImageList.size() == myIdImageList.size()) {
            idImageListModel.setIdImageList(myIdImageList);
            profileModel.setIdImageList(idImageListModel);
            //GlobalFunctions.hideProgress();

            if (uriCertificatesImageList.size() != 0) {
                uploadImage(GlobalVariables.UPLOAD_CERTIFICATE_PHOTO_PATH);
            } else {
                updateProfile(context, profileModel);
            }
        } else {
            GlobalFunctions.hideProgress();
        }
    }

    @Override
    public void OnFailure() {
        GlobalFunctions.hideProgress();
        globalFunctions.displayErrorDialog(activity, context.getString(R.string.failed_upload));
    }
}