package sa.quickfix.vendor.flows.profile.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.facebook.FacebookSdk;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnLanguageRemoveInvoke;
import sa.quickfix.vendor.flows.image_picker.ImagePickerActivity;
import sa.quickfix.vendor.flows.registration.activities.individual_registration.CityListActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.location.LocationListener;
import sa.quickfix.vendor.location.LocationService;
import sa.quickfix.vendor.map.SearchPlaceOnMapActivity;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.AddressModel;
import sa.quickfix.vendor.services.model.CertificatesListModel;
import sa.quickfix.vendor.services.model.IdImagesListModel;
import sa.quickfix.vendor.services.model.IndividualRegisterModel;
import sa.quickfix.vendor.services.model.LanguageListModel;
import sa.quickfix.vendor.services.model.LanguageModel;
import sa.quickfix.vendor.services.model.MyCityModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.upload.UploadImage;
import sa.quickfix.vendor.upload.UploadListener;
import sa.quickfix.vendor.util.PermissionUtils;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;


public class EditProfileActivity extends AppCompatActivity implements LocationListener, OnLanguageRemoveInvoke, UploadListener {

    public static final String TAG = "EditProfileActivity";

    public static final String BUNDLE_PROFILE_MODEL = "BundleProfileModel";
    public static final String BUNDLE_INDIVIDUAL_REGISTER_ACTIVITY_INDIVIDUAL_REGISTER_MODEL = "BundleIndividualRegisterActivityIndividualRegisterModel";
    public static final String BUNDLE_MY_CITY_MODE = "BundleMyCityModel";

    private static final int PERMISSION_REQUEST_CODE = 200;

    public static final int REQUEST_IMAGE = 100;

    private static final int
            LOCATION_PERMISSION_REQUEST_CODE = 132,
            PERMISSION_ACCESS_COARSE_LOCATION = 156;

    static Intent locationintent;
    LocationService locationService;

    IndividualRegisterModel individualRegisterModel = null;
    IndividualRegisterModel myIndividualRegisterModel = null;

    Context context;
    private static Activity activity;
    View mainView;

    FirebaseStorage storage;
    StorageReference storageReference;

    List<String> imageList;
    List<Uri> uriImageList;
    List<Bitmap> bitmapImageList;
    List<String> downloadImageList;

    ProfileModel profileModel = null;
    AddressModel addressModel = new AddressModel();

    private boolean
            isIdImageBlocked = false,
            isCertificateImageBlocked = false;

    String imagePath = "";

    MyCityModel myCityModel = null;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    private LayoutInflater layoutInflater;

    ArrayList<Integer> selectedListOfLanguageIdsInt = new ArrayList<>();
    ArrayList<String> selectedListOfLanguageIdsString = new ArrayList<>();
    List<String> selectedListOfLanguageName = new ArrayList<>();

    MultiSelectDialog multiSelectDialog = new MultiSelectDialog();

    LanguageListModel languageListModel = null;
    List<LanguageModel> seletedLanguagesList = new ArrayList<>();

    String
            selectedGender = "0";

    String selectedLanguagesIds = null;
    String selectedCityId = null;

    private CardView continue_cardview;
    private ImageView back_iv;
    private CircleImageView vendor_iv;
    private LinearLayout nationality_ll, update_profile_photo_ll;
    private RelativeLayout city_rl, select_gender_rl, select_language_rl;
    private TextView city_tv, selected_language_tv, select_gender_tv, address_tv, nationality_tv, upload_certificates_tv;
    private CountryPicker nationality_picker;
    private EditText first_name_etv, last_name_etv, email_etv, national_id_no_etv;

    String selectedCountryName = "";

    public static Intent newInstance(Context context, ProfileModel model) {
        Intent intent = new Intent(context, EditProfileActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_PROFILE_MODEL, model);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.edit_profile_activity);

        context = this;
        activity = this;

        this.layoutInflater = activity.getLayoutInflater();

        downloadImageList = new ArrayList<>();
        uriImageList = new ArrayList<>();
        imageList = new ArrayList<>();
        bitmapImageList = new ArrayList<>();

        imageList.clear();
        uriImageList.clear();
        downloadImageList.clear();
        bitmapImageList.clear();

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        }

        locationintent = new Intent(LocationService.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            locationintent.setPackage(activity.getPackageName());
        }
        if (locationService != null) {
//            GlobalFunctions.showProgress(activity, activity.getString(R.string.fetching_current_location));
            startLocationService();
        }

        locationService = new LocationService();
        locationService.setListener(this);

        if (getIntent().hasExtra(BUNDLE_PROFILE_MODEL)) {
            profileModel = (ProfileModel) getIntent().getSerializableExtra(BUNDLE_PROFILE_MODEL);
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);
        vendor_iv = (CircleImageView) findViewById(R.id.vendor_iv);
        continue_cardview = (CardView) findViewById(R.id.continue_cardview);
        nationality_ll = (LinearLayout) findViewById(R.id.nationality_ll);
        update_profile_photo_ll = (LinearLayout) findViewById(R.id.update_profile_photo_ll);
        city_rl = (RelativeLayout) findViewById(R.id.city_rl);
        select_gender_rl = (RelativeLayout) findViewById(R.id.select_gender_rl);
        select_language_rl = (RelativeLayout) findViewById(R.id.select_language_rl);

        city_tv = (TextView) findViewById(R.id.city_tv);
        selected_language_tv = (TextView) findViewById(R.id.selected_language_tv);
        select_gender_tv = (TextView) findViewById(R.id.select_gender_tv);
        address_tv = (TextView) findViewById(R.id.address_tv);
        nationality_tv = (TextView) findViewById(R.id.nationality_tv);
        upload_certificates_tv = (TextView) findViewById(R.id.upload_certificates_tv);

        nationality_picker = CountryPicker.newInstance(getString(R.string.select_nationality));  // dialog title

        first_name_etv = (EditText) findViewById(R.id.first_name_etv);
        last_name_etv = (EditText) findViewById(R.id.last_name_etv);
        email_etv = (EditText) findViewById(R.id.email_etv);
        national_id_no_etv = (EditText) findViewById(R.id.national_id_no_etv);

        mainView = continue_cardview;

        upload_certificates_tv.setVisibility(View.GONE);

        getSpokenLanguagesList(context);

        if (profileModel != null) {
            setEnteredData(profileModel);
        }

        national_id_no_etv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith("1")) {
                    s.clear();
                }
                String digits = national_id_no_etv.getText().toString().trim();
                if (digits.length() >= getResources().getInteger(R.integer.national_id_max_length)) {
                    globalFunctions.closeKeyboard(activity);
                }
            }
        });

        nationality_picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                // Implement your code here
                nationality_tv.setText(name);
                nationality_picker.dismiss();
            }
        });

        nationality_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nationality_picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        nationality_ll.setClickable(false);
        nationality_ll.setEnabled(false);

        address_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    return;
                } else if (!sLocationEnabled(activity)) {
                    showSettingsAlert();
                } else {
                    globalFunctions.showProgress(activity, getString(R.string.fetching_location));
                    Intent mIntent = SearchPlaceOnMapActivity.newInstance(context, addressModel, GlobalVariables.LOCATION_TYPE.NONE);
                    startActivityForResult(mIntent, GlobalVariables.REQUEST_INDIVIDUAL_ADDRESS_LOCATION_CODE);
                }
            }
        });

        city_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = CityListActivity.newInstance(activity, myCityModel);
                startActivityForResult(intent, globalVariables.REQUEST_SELECT_CITY_CODE);
            }
        });

        select_language_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
            }
        });

        select_gender_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGenderDialog(activity);
            }
        });

        continue_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public boolean sLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);

// Setting Dialog Title
        alertDialog.setTitle("GPS settings");

// Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

// Setting Icon to Dialog
//alertDialog.setIcon(R.drawable.delete);

// On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivity(intent);
            }
        });

// on pressing cancel button
        alertDialog.setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

// Showing Alert Message
        alertDialog.show();
    }

    private void setEnteredData(ProfileModel mProfileModel) {

        if (mProfileModel != null) {

            if (mProfileModel.getIdImageStatus() != null) {
                if (mProfileModel.getIdImageStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_BLOCKED)) {
                    isIdImageBlocked = true;
                }
            }

            if (mProfileModel.getCertificateImgStatus() != null) {
                if (mProfileModel.getCertificateImgStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_BLOCKED)) {
                    isCertificateImageBlocked = true;
                }
            }

            if (isIdImageBlocked || isCertificateImageBlocked) {
                upload_certificates_tv.setVisibility(View.VISIBLE);
            } else {
                upload_certificates_tv.setVisibility(View.GONE);
            }

            if (GlobalFunctions.isNotNullValue(mProfileModel.getProfileImage())) {
                SeparateProgress.loadImage(vendor_iv, mProfileModel.getProfileImage(), SeparateProgress.setProgress(activity));
            }

            if (mProfileModel.getFirstName() != null) {
                first_name_etv.setText(mProfileModel.getFirstName());
            }

            if (mProfileModel.getLastName() != null) {
                last_name_etv.setText(mProfileModel.getLastName());
            }

            if (mProfileModel.getEmailId() != null) {
                email_etv.setText(mProfileModel.getEmailId());
            }

            if (mProfileModel.getIdNumber() != null) {
                national_id_no_etv.setText(mProfileModel.getIdNumber());
            }

            if (addressModel == null) {
                addressModel = new AddressModel();
            }
            addressModel.setAddress(mProfileModel.getAddress());
            address_tv.setText(mProfileModel.getAddress());
            addressModel.setLatitude(Double.parseDouble(mProfileModel.getLatitude()));
            addressModel.setLongitude(Double.parseDouble(mProfileModel.getLongitude()));

            if (profileModel == null) {
                profileModel = new ProfileModel();
            }
            profileModel.setAddress(mProfileModel.getAddress());
            profileModel.setLatitude(mProfileModel.getLatitude() + "");
            profileModel.setLongitude(mProfileModel.getLongitude() + "");

            selectedCityId = mProfileModel.getCityId();
            city_tv.setText(mProfileModel.getCityName());
            profileModel.setCityName(mProfileModel.getCityName());
            profileModel.setCityId(mProfileModel.getCityId());

            if (mProfileModel.getNationality() != null) {
                nationality_tv.setText(mProfileModel.getNationality());
            }

            if (mProfileModel.getSpokenLanguages() != null) {
                selectedListOfLanguageIdsInt = globalFunctions.getListFromInteger(mProfileModel.getSpokenLanguages());
                selectedLanguagesIds = mProfileModel.getSpokenLanguages();
                profileModel.setSpokenLanguages(mProfileModel.getSpokenLanguages());
            }

            if (mProfileModel.getSpokenLanguageNames() != null) {
                selected_language_tv.setText(mProfileModel.getSpokenLanguageNames());
            }

            if (mProfileModel.getGender() != null) {
                selectedGender = mProfileModel.getGender();
                if (mProfileModel.getGender().equalsIgnoreCase(globalVariables.GENDER_MALE)) {
                    select_gender_tv.setText(getString(R.string.male));
                } else if (mProfileModel.getGender().equalsIgnoreCase(globalVariables.GENDER_FEMALE)) {
                    select_gender_tv.setText(getString(R.string.female));
                }
            }

            vendor_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openCropFunctionImage();
                }
            });

            update_profile_photo_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openCropFunctionImage();
                }
            });

            upload_certificates_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(EditProfileActivity.this, ReUploadDocumentsActivity.class);
                    startActivity(intent);
                }
            });

        }

        first_name_etv.setSelection(first_name_etv.getText().toString().trim().length());
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSION_REQUEST_CODE);
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();
                    // main logic
                    openCropFunctionImage();
                } else {
                    Toast.makeText(activity.getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void openCropFunctionImage() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
     /*   CropImage.activity()
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(2, 2)
                .start(activity);*/

    }

    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(EditProfileActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(EditProfileActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(EditProfileActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void validateInput() {
        if (context != null) {
            String
                    firstName = first_name_etv.getText().toString().trim(),
                    lastName = last_name_etv.getText().toString().trim(),
                    emailId = email_etv.getText().toString().trim(),
                    nationalIdNo = national_id_no_etv.getText().toString().trim(),
                    address = address_tv.getText().toString().trim(),
                    nationality = nationality_tv.getText().toString().trim();

            if (firstName.isEmpty()) {
                first_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                first_name_etv.setFocusableInTouchMode(true);
                first_name_etv.requestFocus();
            } else if (lastName.isEmpty()) {
                last_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                last_name_etv.setFocusableInTouchMode(true);
                last_name_etv.requestFocus();
            } else if (emailId.isEmpty()) {
                email_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                email_etv.setFocusableInTouchMode(true);
                email_etv.requestFocus();
            } else if (nationalIdNo.isEmpty()) {
                national_id_no_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                national_id_no_etv.setFocusableInTouchMode(true);
                national_id_no_etv.requestFocus();
            } else if (address.isEmpty()) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_add_address));
            } else if (selectedCityId == null) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_select_your_city));
            } else if (nationality.isEmpty()) {
                nationality_tv.setError(getString(R.string.pleaseFillMandatoryDetails));
                nationality_tv.setFocusableInTouchMode(true);
                nationality_tv.requestFocus();
            } else if (selectedLanguagesIds == null) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_select_your_language));
            } else if (selectedGender.equalsIgnoreCase("0")) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_select_your_gender));
            } else if (nationalIdNo.length() != 10) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.invalid_national_id_number));
                national_id_no_etv.setFocusableInTouchMode(true);
                national_id_no_etv.requestFocus();
            } else {

                if (profileModel == null) {
                    profileModel = new ProfileModel();
                }
                CertificatesListModel certificatesListModel = new CertificatesListModel();
                IdImagesListModel idImagesListModel = new IdImagesListModel();
                profileModel.setCertificatesList(certificatesListModel);
                profileModel.setIdImageList(idImagesListModel);
                profileModel.setFirstName(firstName);
                profileModel.setLastName(lastName);
                profileModel.setEmailId(emailId);
                profileModel.setGender(selectedGender);
                profileModel.setIdNumber(nationalIdNo);
                profileModel.setSpokenLanguages(selectedLanguagesIds);
                profileModel.setNationality(nationality);

                if (imageList.size() > 0) {
                    uploadImage();
                } else {
                    updateProfile(context, profileModel);
                }
            }

        }
    }

    private void uploadImage() {
        if (imageList.size() != 0) {
            globalFunctions.showProgress(activity, getString(R.string.uploading_image));
            // globalFunctions.showProgress(activity, getString(R.string.uploading_image));
            for (int j = 0; j < imageList.size(); j++) {
                Uri uri = (uriImageList.get(j));
                UploadImage uploadImage = new UploadImage(context);
                uploadImage.startUploading(uri, GlobalVariables.UPLOAD_PROFILE_PHOTO_PATH, "image", this);
            }
        }
    }

    private void getSpokenLanguagesList(final Context context) {
        // globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getSpokenLanguagesList(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                //globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof LanguageListModel) {
                    LanguageListModel languageListModel1 = (LanguageListModel) arg0;
                    languageListModel = languageListModel1;
                    setLanguageList();
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                // globalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                // globalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "GetSpokenLanguagesList");
    }

    private void setLanguageList() {
        ArrayList<MultiSelectModel> listOfLanguages = new ArrayList<>();

        for (int i = 0; i < languageListModel.getLanguageList().size(); i++) {
            String languageName = languageListModel.getLanguageList().get(i).getTitle();
            String languageId = languageListModel.getLanguageList().get(i).getId();
            int id = 0;
            try {

                id = Integer.parseInt(languageId);
            } catch (Exception e) {
                id = 0;
            }
            listOfLanguages.add(new MultiSelectModel(id, languageName));

        }
        setMultiSelectLanguageList(listOfLanguages);
    }

    private void setMultiSelectLanguageList(ArrayList<MultiSelectModel> listOfLanguages) {
        multiSelectDialog
                .title(context.getString(R.string.select_languages)) //setting title for dialog
                .titleSize(25)
                .positiveText(context.getString(R.string.select))
                .negativeText(context.getString(R.string.cancel))
                .setMinSelectionLimit(1) //you can set minimum checkbox selection limit (Optional)
                .setMaxSelectionLimit(languageListModel.getLanguageList().size()) //you can set maximum checkbox selection limit (Optional)
                .preSelectIDsList(selectedListOfLanguageIdsInt) //List of ids that you need to be selected
                .multiSelectList(listOfLanguages) // the multi select model list with ids and name
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                    @Override
                    public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                        selectedListOfLanguageIdsInt.clear();
                        selectedListOfLanguageIdsString.clear();
                        selectedListOfLanguageName.clear();
                        selectedListOfLanguageName.addAll(selectedNames);

                        for (int i = 0; i < selectedNames.size(); i++) {
                            LanguageModel languageModel = null;
                            Object localObjectKeyValue = getObjectFromSelectedItemPosition(selectedNames.get(i), languageListModel);
                            if (localObjectKeyValue != null) {
                                languageModel = (LanguageModel) localObjectKeyValue;
                                if (languageModel != null && languageModel.getId() != null) {
                                    int id = 0;
                                    try {

                                        id = Integer.parseInt(languageModel.getId());
                                    } catch (Exception e) {
                                        id = 0;
                                    }
                                    selectedListOfLanguageIdsInt.add(id);
                                    selectedListOfLanguageIdsString.add(languageModel.getId());
                                }
                            }
                        }
                        selected_language_tv.setText(globalFunctions.getStringFromList(selectedListOfLanguageName));
                        // select_gender_tv.setText(globalFunctions.getIntegerFromList(selectedListOfLanguageIdsInt));
                        selectedLanguagesIds = globalFunctions.getIntegerFromList(selectedListOfLanguageIdsInt);   //.......comma separated ids...
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "Dialog cancelled");
                    }


                });


    }

    private Object getObjectFromSelectedItemPosition(String title, Object object) {
        if (object instanceof LanguageListModel) {
            for (LanguageModel model : ((LanguageListModel) object).getLanguageList()) {
                if (model.getTitle().equalsIgnoreCase(title))
                    return model;
            }
        }
        return null;
    }

    private void openGenderDialog(final Context context) {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.gender_custom_dialog, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        TextView back_tv, male_tv, female_tv;

        back_tv = (TextView) alertView.findViewById(R.id.back_tv);
        male_tv = (TextView) alertView.findViewById(R.id.male_tv);
        female_tv = (TextView) alertView.findViewById(R.id.female_tv);

        back_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        male_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = globalVariables.GENDER_MALE;
                select_gender_tv.setText(getString(R.string.male));
                dialog.dismiss();
            }
        });

        female_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = globalVariables.GENDER_FEMALE;
                select_gender_tv.setText(getString(R.string.female));
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public static void startLocationService() {
        activity.startService(locationintent);
    }

    public static void stopLocationService() {
        activity.stopService(locationintent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        GlobalFunctions.hideProgress();

        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectUri = null;
                Uri uri = data.getParcelableExtra("path");
                try {
                    // You can update this bitmap to your server
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    selectUri = uri;
                    setImage(bitmap, selectUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (resultCode == activity.RESULT_OK && requestCode == globalVariables.REQUEST_INDIVIDUAL_ADDRESS_LOCATION_CODE) {
            if (addressModel == null) {
                addressModel = new AddressModel();
            }
            addressModel = (AddressModel) data.getExtras().getSerializable(SearchPlaceOnMapActivity.BUNDLE_SEARCH_PLACE_ON_MAP_ACTIVITY_ADDRESS_MODEL);
            setSelectedAddress(addressModel);
        } else if (resultCode == activity.RESULT_OK && requestCode == globalVariables.REQUEST_SELECT_CITY_CODE) {
            MyCityModel myCityModel1 = (MyCityModel) data.getExtras().getSerializable(BUNDLE_MY_CITY_MODE);
            if (myCityModel1 != null) {
                if (profileModel == null) {
                    profileModel = new ProfileModel();
                }
                selectedCityId = myCityModel1.getId();
                city_tv.setText(myCityModel1.getTitle());
                profileModel.setCityName(myCityModel1.getTitle());
                profileModel.setCityId(myCityModel1.getId());
            }
        }

    }

    private void setImage(Bitmap bitmap, Uri selectUri) {
        vendor_iv.setImageBitmap(bitmap);
        imageList.clear();
        uriImageList.clear();
        bitmapImageList.clear();
        imageList.add("image_one_iv");
        uriImageList.add(selectUri);
        bitmapImageList.add(bitmap);
    }

    private void setSelectedAddress(AddressModel mAddressModel) {

        if (mAddressModel != null) {

            if (profileModel == null) {
                profileModel = new ProfileModel();
            }
            address_tv.setText(mAddressModel.getAddress());
            profileModel.setAddress(mAddressModel.getAddress());
            profileModel.setLatitude(mAddressModel.getLatitude() + "");
            profileModel.setLongitude(mAddressModel.getLongitude() + "");
        }
    }

    @Override
    public void OnLocationFetch(Location location) {
        globalFunctions.hideProgress();
        setLocation();
    }

    private void setLocation() {
        Address address = globalFunctions.getAddressfromLatLng(activity, locationService.getLatitude(), locationService.getLongitude());

        //  pickUpAddressModel = globalFunctions.getAddressModelFromAddress(address);

      /*  String completeAddress = null;
        try {
            if (pickUpAddressModel.getAddress() != null) {
                completeAddress = pickUpAddressModel.getAddress();
            } else {
                completeAddress = globalFunctions.getAddressTextFromModelExcludingNameAndAddresswithComma(context, pickUpAddressModel);
            }
            // head_office_address_tv.setText(completeAddress);
        } catch (Exception e) {
            globalFunctions.displayMessaage(activity, mainView, getString(R.string.location_error));
        }*/
        stopLocationService();
    }

    @Override
    public void OnProviderDisabled(String provider) {
        globalFunctions.hideProgress();
    }

    @Override
    public void OnProviderEnabled(String provider) {

    }

    @Override
    public void OnLocationFailure(String msg) {
        globalFunctions.hideProgress();
    }

    @Override
    public void OnSelectedLanguageRemoveInvoke(int position, LanguageModel languageModel) {
/*

        if (languageModel != null) {
            seletedLanguagesList.remove(position);
           // setLanguageList();
            initLanguageList();
        }
*/

    }

    @Override
    public void OnSuccess(final String imgUrl, final String type, final String uploadingFile) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                downloadImageList.add(imgUrl);
                setImagesToModel(imgUrl);
            }
        });

    }

    private void setImagesToModel(String imagePath) {
        if (downloadImageList.size() == imageList.size()) {
            if (profileModel == null) {
                profileModel = new ProfileModel();
                CertificatesListModel certificatesListModel = new CertificatesListModel();
                IdImagesListModel idImagesListModel = new IdImagesListModel();
                profileModel.setCertificatesList(certificatesListModel);
                profileModel.setIdImageList(idImagesListModel);
            }
            profileModel.setProfileImage(imagePath);
            // globalFunctions.hideProgress();
            updateProfile(context, profileModel);
        } else {
            globalFunctions.hideProgress();
        }
    }

    private void updateProfile(final Context context, ProfileModel profileModel) {
        globalFunctions.showProgress(activity, getString(R.string.updatingProfile));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.updateUser(context, profileModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                checkUpdateAfter(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "UpdateUser");
    }

    private void checkUpdateAfter(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
        } else {
            /*Profile Model */
            ProfileModel profileModel = (ProfileModel) model;
            globalFunctions.setProfile(context, profileModel);
            showAlertMessage(context.getString(R.string.updated_successfully));
        }
    }

    private void showAlertMessage(String message) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                MainActivity.setProfileImage();
                ProfileActivity.closeThisActivity();
                Intent intent = new Intent(EditProfileActivity.this, ProfileActivity.class);
                startActivity(intent);
                closeThisActivity();
            }
        });
        alertDialog.show();
    }

    @Override
    public void OnFailure() {
        globalFunctions.hideProgress();
        globalFunctions.displayMessaage(activity, mainView, context.getString(R.string.failed_upload_image));
    }
}