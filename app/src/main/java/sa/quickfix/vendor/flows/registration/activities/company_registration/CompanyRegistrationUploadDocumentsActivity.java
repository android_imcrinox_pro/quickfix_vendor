package sa.quickfix.vendor.flows.registration.activities.company_registration;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.flows.image_picker.ImagePickerActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.PhotosListAdapter;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnPhotoDeleteClickInvoke;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.model.CompanyRegisterModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CompanyRegistrationUploadDocumentsActivity extends AppCompatActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate, OnPhotoDeleteClickInvoke {

    public static final String TAG = "CompanyUploadActivity";

    public static final int REQUEST_IMAGE = 100;

    public static final String BUNDLE_COMPANY_REGISTER_MODEL = "BundleCompanyRegisterModel";
    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";

    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int PICK_FILE_PDF = 200;

    Context context;
    private static Activity activity;
    View mainView;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    List<String> profileImageList;

    List<Uri>
            uriProfileImageList,
            uriScientificDocImageList,
            uriLicenceImageList;

    List<Bitmap>
            bitmapProfileImageList,
            bitmapScientificDocImageList,
            btmapLicenceImageList;

    String imagePath = "";

    boolean isProfileImageClicked = false;
    boolean isScientificDocImageClicked = false;
    boolean isLicenseImageClicked = false;

    CompanyRegisterModel companyRegisterModel = null;
    String fromPage = null;

    private CardView continue_cardview;
    private TextView company_name_tv;
    private EditText about_etv;
    private LinearLayout upload_profile_pic_ll;
    private CircleImageView vendor_iv;
    private ImageView scientific_doc_iv_one, scientific_doc_one_delete_iv, scientific_doc_iv_two, scientific_doc_two_delete_iv, scientific_doc_iv_three,
            scientific_doc_three_delete_iv, licence_iv_one, licence_one_delete_iv, licence_iv_two, licence_two_delete_iv, licence_iv_three, licence_three_delete_iv,
            upload_scientific_documents_iv, professional_licence_iv;

    private RecyclerView upload_scientific_documents_rv, professional_licence_rv;
    LinearLayoutManager scientific_documents_linearLayoutManager, professional_licence_linearLayoutManager;
    PhotosListAdapter photosListAdapter;
    LayoutInflater layoutInflater;
    Intent intent;
    int MULTI_SELECT_SCIENTIFIC_DOC_MAX_VALUE = 3;
    int MULTI_SELECT_LICENSE_MAX_VALUE = 3;
    private static final int INTENT_REQUEST_GET_IMAGES_FROM_CAMERA = 10;

    public static Intent newInstance(Context context, CompanyRegisterModel model) {
        Intent intent = new Intent(context, CompanyRegistrationUploadDocumentsActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_COMPANY_REGISTER_MODEL, model);
        intent.putExtras(args);
        return intent;
    }

    public static Intent newInstance(Context context, CompanyRegisterModel model, String fromPage) {
        Intent intent = new Intent(context, CompanyRegistrationUploadDocumentsActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_COMPANY_REGISTER_MODEL, model);
        args.putString(BUNDLE_KEY_FROM_PAGE, fromPage);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.company_registration_upload_documents_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        layoutInflater = activity.getLayoutInflater();
        scientific_documents_linearLayoutManager = new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false);
        professional_licence_linearLayoutManager = new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false);

        profileImageList = new ArrayList<>();
        uriProfileImageList = new ArrayList<>();
        bitmapProfileImageList = new ArrayList<>();

        uriScientificDocImageList = new ArrayList<>();
        bitmapScientificDocImageList = new ArrayList<>();

        uriLicenceImageList = new ArrayList<>();
        btmapLicenceImageList = new ArrayList<>();

        uriProfileImageList.clear();
        uriScientificDocImageList.clear();
        uriLicenceImageList.clear();

        if (getIntent().hasExtra(BUNDLE_COMPANY_REGISTER_MODEL)) {
            companyRegisterModel = (CompanyRegisterModel) getIntent().getSerializableExtra(BUNDLE_COMPANY_REGISTER_MODEL);
        } else {
            companyRegisterModel = null;
        }

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_PAGE)) {
            fromPage = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_PAGE);
        } else {
            fromPage = null;
        }

        continue_cardview = (CardView) findViewById(R.id.continue_cardview);
        company_name_tv = (TextView) findViewById(R.id.company_name_tv);
        about_etv = (EditText) findViewById(R.id.about_etv);
        vendor_iv = (CircleImageView) findViewById(R.id.vendor_iv);
        upload_profile_pic_ll = (LinearLayout) findViewById(R.id.upload_profile_pic_ll);

        scientific_doc_iv_one = (ImageView) findViewById(R.id.scientific_doc_iv_one);
        scientific_doc_one_delete_iv = (ImageView) findViewById(R.id.scientific_doc_one_delete_iv);
        scientific_doc_iv_two = (ImageView) findViewById(R.id.scientific_doc_iv_two);
        scientific_doc_two_delete_iv = (ImageView) findViewById(R.id.scientific_doc_two_delete_iv);
        scientific_doc_iv_three = (ImageView) findViewById(R.id.scientific_doc_iv_three);
        scientific_doc_three_delete_iv = (ImageView) findViewById(R.id.scientific_doc_three_delete_iv);
        licence_iv_one = (ImageView) findViewById(R.id.licence_iv_one);
        licence_one_delete_iv = (ImageView) findViewById(R.id.licence_one_delete_iv);
        licence_iv_two = (ImageView) findViewById(R.id.licence_iv_two);
        licence_two_delete_iv = (ImageView) findViewById(R.id.licence_two_delete_iv);
        licence_iv_three = (ImageView) findViewById(R.id.licence_iv_three);
        licence_three_delete_iv = (ImageView) findViewById(R.id.licence_three_delete_iv);

        upload_scientific_documents_iv = (ImageView) findViewById(R.id.upload_scientific_documents_iv);
        professional_licence_iv = (ImageView) findViewById(R.id.professional_licence_iv);
        professional_licence_rv = (RecyclerView) findViewById(R.id.professional_licence_rv);
        upload_scientific_documents_rv = (RecyclerView) findViewById(R.id.upload_scientific_documents_rv);

        mainView = continue_cardview;

        if (companyRegisterModel != null) {
            if (companyRegisterModel.getCompanyName() != null) {
                company_name_tv.setText(companyRegisterModel.getCompanyName());
            }
        }

        if (fromPage != null) {
            if (fromPage.equalsIgnoreCase(globalVariables.FROM_PAGE_REVIEW_COMPANY_REGISTRATION)) {
                //from edit....set all the selected images....
                Intent i = getIntent();
                uriProfileImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE);
                uriScientificDocImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_SCIENTIFIC_DOC_LIST_CODE);
                uriLicenceImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_LICENSE_PHOTO_LIST_CODE);
                setSelectedImages();
            }
        }

        vendor_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //main logic or main code
                //write your main code to execute, It will execute if the permission is already given.
                if (profileImageList.size() == 0) {
                    isProfileImageClicked = true;
                    isLicenseImageClicked = false;
                    isScientificDocImageClicked = false;
                    openCropFunctionImage();
                } else {
                    openCancelAlertDialog(context);
                }
            }
        });

        upload_profile_pic_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //main logic or main code
                //write your main code to execute, It will execute if the permission is already given.
                if (profileImageList.size() == 0) {
                    isProfileImageClicked = true;
                    isLicenseImageClicked = false;
                    isScientificDocImageClicked = false;
                    openCropFunctionImage();
                } else {
                    openCancelAlertDialog(context);
                }
            }
        });

        //scientific doc.....

        upload_scientific_documents_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (profileImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_profile_photo_first));
                } else if (uriScientificDocImageList.size() >= 3) {
//                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_profile_photo_first));
                } else {
                    openFileAlertDialog();
                    isScientificDocImageClicked = true;
                    isProfileImageClicked = false;
                    isLicenseImageClicked = false;
                }
            }
        });

       /* scientific_doc_iv_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriScientificDocImageList.size() == 0 && profileImageList.size() == 1) {
                    isScientificDocImageClicked = true;
                    openCropFuctionalImage();
                } else if (profileImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_profile_photo_first));
                }
            }
        });

        scientific_doc_iv_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriScientificDocImageList.size() == 1 && profileImageList.size() == 1) {
                    isScientificDocImageClicked = true;
                    openCropFuctionalImage();
                }
            }
        });

        scientific_doc_iv_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriScientificDocImageList.size() == 2 && profileImageList.size() == 1) {
                    isScientificDocImageClicked = true;
                    openCropFuctionalImage();
                }

            }
        });

        scientific_doc_one_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 0;
               *//* if (imageServerList.size() > 0) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeScientificDocImage(position);
                //}
            }
        });

        scientific_doc_two_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 1;
              *//*  if (imageServerList.size() > 1) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeScientificDocImage(position);
                //}
            }
        });

        scientific_doc_three_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 2;
               *//* if (imageServerList.size() > 2) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeScientificDocImage(position);
                //  }
            }
        });*/

        ////licence images.....

        professional_licence_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (profileImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_profile_photo_first));
                } else if (uriScientificDocImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_id_image_first));
                } else if (uriLicenceImageList.size() >= 3) {
//                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_id_image_first));
                } else {
                    openFileAlertDialog();
//                    openCropFunctionImage();
                    isLicenseImageClicked = true;
                    isProfileImageClicked = false;
                    isScientificDocImageClicked = false;
                }
            }
        });

        /*licence_iv_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriLicenceImageList.size() == 0 && profileImageList.size() == 1 && uriScientificDocImageList.size() >= 1) {
                    openCropFuctionalImage();
                } else if (profileImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_profile_photo_first));
                } else if (uriScientificDocImageList.size() == 0) {
                    GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.select_scientific_doc_first));
                }
            }
        });

        licence_iv_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriLicenceImageList.size() == 1 && profileImageList.size() == 1 && uriScientificDocImageList.size() >= 1) {
                    openCropFuctionalImage();
                }
            }
        });

        licence_iv_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriLicenceImageList.size() == 2 && profileImageList.size() == 1 && uriScientificDocImageList.size() >= 1) {
                    openCropFuctionalImage();
                }

            }
        });

        licence_one_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 0;
               *//* if (imageServerList.size() > 0) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeLicenceImage(position);
                //}
            }
        });

        licence_two_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 1;
              *//*  if (imageServerList.size() > 1) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeLicenceImage(position);
                //}
            }
        });

        licence_three_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = 2;
               *//* if (imageServerList.size() > 2) {
                    confirmDeleteAlertDialog(position);
                } else {*//*
                removeLicenceImage(position);
                //  }
            }
        });*/

        continue_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

    }

    private void openFileAlertDialog() {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.add_custom_file_dialog, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        ImageView image_iv,pdf_iv;

        image_iv = (ImageView) alertView.findViewById(R.id.image_iv);
        pdf_iv = (ImageView) alertView.findViewById(R.id.pdf_iv);

        image_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openCropFunctionImage();
            }
        });

        pdf_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openPdfSelectFile();

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void openPdfSelectFile() {

        Intent intentPDF = new Intent(Intent.ACTION_GET_CONTENT);
        intentPDF.setType("application/pdf");
        intentPDF.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intentPDF, PICK_FILE_PDF);
    }

    private void openCropFunctionImage() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(CompanyRegistrationUploadDocumentsActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(CompanyRegistrationUploadDocumentsActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(CompanyRegistrationUploadDocumentsActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }


    private void openMultiFunctionImage() {
        openDialogForPhotos();
    }

    private void openDialogForPhotos() {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        final View alertView = layoutInflater.inflate(R.layout.dialog_get_photoes, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(false);
        final View dialogView = alertView;
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();
        TextView camera_tv = alertView.findViewById(R.id.photos_dialog_camera_tv);
        TextView gallery_tv = alertView.findViewById(R.id.photos_dialog_gallery_tv);
        TextView close_tv = alertView.findViewById(R.id.photos_dialog_close_tv);

        close_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        camera_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openCameraImage();
            }
        });

        gallery_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (isScientificDocImageClicked) {
                    openMultiFunctionCertificateImage();
                } else {
                    openMultiSelectionImage();
                }
            }
        });
        dialog.show();
    }

    private void openCameraImage() {
        intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES_FROM_CAMERA);
    }

    private void openMultiSelectionImage() {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("quickfix.sa.vendor.fileprovider")
                .setMaximumDisplayingImages(Integer.MAX_VALUE)
                .isMultiSelect()
                .setMinimumMultiSelectCount(1)
                .setMaximumMultiSelectCount(MULTI_SELECT_LICENSE_MAX_VALUE)
                .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                .setMultiSelectDoneTextColor(R.color.primary_text) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                .disableOverSelectionMessage()
                .build();
        pickerDialog.show(getSupportFragmentManager(), "picker");
    }

    private void openMultiFunctionCertificateImage() {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("quickfix.sa.vendor.fileprovider")
                .setMaximumDisplayingImages(Integer.MAX_VALUE)
                .isMultiSelect()
                .setMinimumMultiSelectCount(1)
                .setMaximumMultiSelectCount(MULTI_SELECT_SCIENTIFIC_DOC_MAX_VALUE)
                .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                .setMultiSelectDoneTextColor(R.color.primary_text) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                .disableOverSelectionMessage()
                .build();
        pickerDialog.show(getSupportFragmentManager(), "picker");
    }

    private void setSelectedImages() {

        if (companyRegisterModel != null) {
            if (companyRegisterModel.getAboutCompany() != null) {
                about_etv.setText(companyRegisterModel.getAboutCompany());
            }
        }

        //profile image....
        if (uriProfileImageList != null) {
            if (uriProfileImageList.size() > 0) {
                Glide.with(this).load(uriProfileImageList.get(0)).into(vendor_iv);
                profileImageList.add("1.jpg");
            }
        }

        //ScientificDocImage image....

        initSeientificDocRecyclerView();

        /*if(uriScientificDocImageList != null){
            if (uriScientificDocImageList.size() > 0) {

                if (uriScientificDocImageList.size() == 1) {

                    Glide.with(this).load(uriScientificDocImageList.get(0)).into(scientific_doc_iv_one);
                    scientific_doc_one_delete_iv.setVisibility(View.VISIBLE);
                    scientificDocImageList.add("image_" + 1 + "_iv");
                }else if (uriScientificDocImageList.size() == 2) {

                    Glide.with(this).load(uriScientificDocImageList.get(0)).into(scientific_doc_iv_one);
                    scientific_doc_one_delete_iv.setVisibility(View.VISIBLE);
                    scientificDocImageList.add("image_" + 1 + "_iv");

                    Glide.with(this).load(uriScientificDocImageList.get(1)).into(scientific_doc_iv_two);
                    scientific_doc_two_delete_iv.setVisibility(View.VISIBLE);
                    scientificDocImageList.add("image_" + 2 + "_iv");
                }else if (uriScientificDocImageList.size() == 3) {

                    Glide.with(this).load(uriScientificDocImageList.get(0)).into(scientific_doc_iv_one);
                    scientific_doc_one_delete_iv.setVisibility(View.VISIBLE);
                    scientificDocImageList.add("image_" + 1 + "_iv");

                    Glide.with(this).load(uriScientificDocImageList.get(1)).into(scientific_doc_iv_two);
                    scientific_doc_two_delete_iv.setVisibility(View.VISIBLE);
                    scientificDocImageList.add("image_" + 2 + "_iv");

                    Glide.with(this).load(uriScientificDocImageList.get(2)).into(scientific_doc_iv_three);
                    scientific_doc_three_delete_iv.setVisibility(View.VISIBLE);
                    scientificDocImageList.add("image_" + 3 + "_iv");
                }
            }
        }*/

        //licenceImage image....

        initLicenseRecyclerView();

        /*if(uriLicenceImageList != null){
            if (uriLicenceImageList.size() > 0) {

                if (uriLicenceImageList.size() == 1) {

                    Glide.with(this).load(uriLicenceImageList.get(0)).into(licence_iv_one);
                    licence_one_delete_iv.setVisibility(View.VISIBLE);
                    licenceImageList.add("image_" + 1 + "_iv");
                }else if (uriLicenceImageList.size() == 2) {

                    Glide.with(this).load(uriLicenceImageList.get(0)).into(licence_iv_one);
                    licence_one_delete_iv.setVisibility(View.VISIBLE);
                    licenceImageList.add("image_" + 1 + "_iv");

                    Glide.with(this).load(uriLicenceImageList.get(1)).into(licence_iv_two);
                    licence_two_delete_iv.setVisibility(View.VISIBLE);
                    licenceImageList.add("image_" + 2 + "_iv");
                }else if (uriLicenceImageList.size() == 3) {

                    Glide.with(this).load(uriLicenceImageList.get(0)).into(licence_iv_one);
                    licence_one_delete_iv.setVisibility(View.VISIBLE);
                    licenceImageList.add("image_" + 1 + "_iv");

                    Glide.with(this).load(uriLicenceImageList.get(1)).into(licence_iv_two);
                    licence_two_delete_iv.setVisibility(View.VISIBLE);
                    licenceImageList.add("image_" + 2 + "_iv");

                    Glide.with(this).load(uriLicenceImageList.get(2)).into(licence_iv_three);
                    licence_three_delete_iv.setVisibility(View.VISIBLE);
                    licenceImageList.add("image_" + 3 + "_iv");
                }
            }
        }*/
    }

    private void initSeientificDocRecyclerView() {

        if (uriScientificDocImageList.size() > 0) {
            upload_scientific_documents_rv.setVisibility(View.VISIBLE);
            upload_scientific_documents_rv.setLayoutManager(scientific_documents_linearLayoutManager);
            upload_scientific_documents_rv.setHasFixedSize(true);
            photosListAdapter = new PhotosListAdapter(activity, uriScientificDocImageList, true, this);
            upload_scientific_documents_rv.setAdapter(photosListAdapter);
        } else {
            upload_scientific_documents_rv.setVisibility(View.GONE);
        }
    }


    private void initLicenseRecyclerView() {

        if (uriLicenceImageList.size() > 0) {
            professional_licence_rv.setVisibility(View.VISIBLE);
            professional_licence_rv.setLayoutManager(professional_licence_linearLayoutManager);
            professional_licence_rv.setHasFixedSize(true);
            photosListAdapter = new PhotosListAdapter(activity, uriLicenceImageList, false, this);
            professional_licence_rv.setAdapter(photosListAdapter);
        } else {
            professional_licence_rv.setVisibility(View.GONE);
        }
    }

    private void validateInput() {
        if (context != null) {
            String
                    about = about_etv.getText().toString().trim();

            if (profileImageList.size() == 0) {
                GlobalFunctions.displayMessageTop(context, mainView, getString(R.string.select_profile_photo));
            } else if (uriScientificDocImageList.size() == 0) {
                GlobalFunctions.displayMessageTop(context, mainView, getString(R.string.select_atleast_one_scientific_doc));
            } else if (uriLicenceImageList.size() == 0) {
                GlobalFunctions.displayMessageTop(context, mainView, getString(R.string.select_atleast_one_licence_image));
            } else if (about.isEmpty()) {
                about_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                about_etv.setFocusableInTouchMode(true);
                about_etv.requestFocus();
            } else {
                if (companyRegisterModel == null) {
                    companyRegisterModel = new CompanyRegisterModel();
                }
                companyRegisterModel.setAboutCompany(about);
                Intent intent = CompanyReviewApplicationActivity.newInstance(activity, companyRegisterModel);
                intent.putExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE, (Serializable) uriProfileImageList);
                intent.putExtra(GlobalVariables.UPLOAD_SCIENTIFIC_DOC_LIST_CODE, (Serializable) uriScientificDocImageList);
                intent.putExtra(GlobalVariables.UPLOAD_LICENSE_PHOTO_LIST_CODE, (Serializable) uriLicenceImageList);
                activity.startActivity(intent);
            }

        }
    }

    private void removeScientificDocImage(int position) {
        uriScientificDocImageList.remove(position);
        initSeientificDocRecyclerView();
    }

    private void removeLicenceImage(int position) {
        uriLicenceImageList.remove(position);
        initLicenseRecyclerView();
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        GlobalFunctions.hideProgress();

        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedImageUri = null;
                Uri uri = data.getParcelableExtra("path");
                try {
                    // You can update this bitmap to your server
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    selectedImageUri = uri;
                    if (isScientificDocImageClicked) {
                        //doc image is clicked...
                        uriScientificDocImageList.add(selectedImageUri);
                        initSeientificDocRecyclerView();
                        MULTI_SELECT_SCIENTIFIC_DOC_MAX_VALUE = MULTI_SELECT_SCIENTIFIC_DOC_MAX_VALUE - 1;
                    } else if (isLicenseImageClicked) {
                        uriLicenceImageList.add(selectedImageUri);
                        initLicenseRecyclerView();
                        MULTI_SELECT_LICENSE_MAX_VALUE = MULTI_SELECT_LICENSE_MAX_VALUE - 1;
                    } else if (isProfileImageClicked) {
                        setImagesToModel(bitmap, selectedImageUri);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else if (requestCode == PICK_FILE_PDF) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedImageUri = null;
                 selectedImageUri = data.getData();
                try {
                    // You can update this bitmap to your server
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    if (isScientificDocImageClicked) {
                        //doc image is clicked...
                        uriScientificDocImageList.add(selectedImageUri);
                        initSeientificDocRecyclerView();
                        MULTI_SELECT_SCIENTIFIC_DOC_MAX_VALUE = MULTI_SELECT_SCIENTIFIC_DOC_MAX_VALUE - 1;
                    } else if (isLicenseImageClicked) {
                        uriLicenceImageList.add(selectedImageUri);
                        initLicenseRecyclerView();
                        MULTI_SELECT_LICENSE_MAX_VALUE = MULTI_SELECT_LICENSE_MAX_VALUE - 1;
                    } else if (isProfileImageClicked) {
                        setImagesToModel(bitmap, selectedImageUri);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setImagesToModel(Bitmap bitmap, Uri selectUri) {
        if (profileImageList.size() == 0) {
            Glide.with(this).load(selectUri).into(vendor_iv);
            //vendor_iv.setImageBitmap(bitmap);
            profileImageList.add("1.jpg");
            uriProfileImageList.add(selectUri);
            //  bitmapProfileImageList.add(bitmap);
        }
    }

    private void openCancelAlertDialog(final Context context) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(activity.getString(R.string.appDeleteText));
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileImageList.clear();
                uriProfileImageList.clear();
                //  bitmapProfileImageList.clear();
                vendor_iv.setImageResource(R.drawable.ic_default_user);
                GlobalFunctions.displayMessaage(context, mainView, activity.getString(R.string.deleted_successfully));
                alertDialog.dismiss();
            }
        });
        alertDialog.setNegativeButton(getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    private void openCropFuctionalImage() {
        CropImage.activity()
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(2, 2)
                .start(activity);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void loadImage(File imageFile, ImageView ivImage) {
        Glide.with(activity).load(imageFile).into(ivImage);
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {
        if (isScientificDocImageClicked) {
            MULTI_SELECT_SCIENTIFIC_DOC_MAX_VALUE = MULTI_SELECT_SCIENTIFIC_DOC_MAX_VALUE - uriList.size();
            for (int i = 1; i <= uriList.size(); i++) {
                uriScientificDocImageList.add(uriList.get(i - 1));
                initSeientificDocRecyclerView();
            }
        } else {
            MULTI_SELECT_LICENSE_MAX_VALUE = MULTI_SELECT_LICENSE_MAX_VALUE - uriList.size();
            for (int i = 1; i <= uriList.size(); i++) {
                uriLicenceImageList.add(uriList.get(i - 1));
                initLicenseRecyclerView();
            }
        }

    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        //Do something with your Uri
    }

    @Override
    public void OnClickInvoke(int position, boolean isScientificDoc, Uri item) {
        if (isScientificDoc) {
            MULTI_SELECT_SCIENTIFIC_DOC_MAX_VALUE = MULTI_SELECT_SCIENTIFIC_DOC_MAX_VALUE + 1;
            removeScientificDocImage(position);
        } else {
            MULTI_SELECT_LICENSE_MAX_VALUE = MULTI_SELECT_LICENSE_MAX_VALUE + 1;
            removeLicenceImage(position);
        }

    }

}