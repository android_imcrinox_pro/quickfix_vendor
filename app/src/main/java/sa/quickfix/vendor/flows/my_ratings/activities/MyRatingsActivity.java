package sa.quickfix.vendor.flows.my_ratings.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.MyRatingsListAdapter;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.FeedbackListModel;
import sa.quickfix.vendor.services.model.FeedbackMainModel;
import sa.quickfix.vendor.services.model.FeedbackModel;
import sa.quickfix.vendor.services.model.OrderPostModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

import com.facebook.FacebookSdk;
import com.vlonjatg.progressactivity.ProgressRelativeLayout;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.List;


public class MyRatingsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "MyRatingsActivity";

    Context context;
    private static Activity activity;

    GlobalFunctions globalFunctions;
    GlobalVariables globalVariables;

    View mainView;

    int index = 0;
    int preLast = 0;
    boolean dataEnd = false;
    boolean loadingList = false;

    int visibleItemCount = 0, totalItemCount = 0, pastVisiblesItems = 0;

    SwipeRefreshLayout swipe_container;
    ProgressRelativeLayout progressActivity;
    DilatingDotsProgressBar progressBar;

    RecyclerView recyclerView;
    MyRatingsListAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    FeedbackListModel detail = null;
    List<FeedbackModel> list = new ArrayList<>();

    OrderPostModel orderPostModel = null;

    private ImageView back_iv;
    private CircleImageView vendor_iv;
    private TextView vendor_name_tv, ratings_count_tv, no_of_comments_tv;
    private RatingBar ratingBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.my_ratings_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);
        vendor_iv = (CircleImageView) findViewById(R.id.vendor_iv);

        vendor_name_tv = (TextView) findViewById(R.id.vendor_name_tv);
        ratings_count_tv = (TextView) findViewById(R.id.ratings_count_tv);
        no_of_comments_tv = (TextView) findViewById(R.id.no_of_comments_tv);

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(activity);

        swipe_container = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        progressActivity = (ProgressRelativeLayout) findViewById(R.id.details_progressActivity);
        progressBar = (DilatingDotsProgressBar) findViewById(R.id.extraProgressBar);

        mainView = recyclerView;

        if (detail == null) {
            index = 0;
            preLast = 0;
            dataEnd = false;
            detail = null;
        }

        initRecyclerView();
        refreshList();

        swipe_container.setOnRefreshListener(this);
        swipe_container.setColorSchemeResources(R.color.fab_color_normal,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (!isLoadingList()) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            //Log.v("...", "Last Item Wow !");
                            final int lastItem = pastVisiblesItems + visibleItemCount;
                            if (lastItem == totalItemCount) {
                                if (preLast != lastItem) { //to avoid multiple calls for last item
                                    preLast = lastItem;
                                    orderPostModel.setIndex(String.valueOf(getIndex()));
                                    orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
                                    getReviewsList(context);
                                }
                            }
                        }

                    }
                }
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getReviewsList(final Context context) {
        if (!isLoading()) {
            if (index == 0) {
                showLoading();
            } else {
                showExtraLoading();
            }
            //globalFunctions.showProgress(activity, getString(R.string.loading));
            ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
            servicesMethodsManager.getReviewsList(context, orderPostModel, new ServerResponseInterface() {
                @Override
                public void OnSuccessFromServer(Object arg0) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        Log.d(TAG, "Response: " + arg0.toString());
                        if (index == 0) {
                            showContent();
                        } else {
                            hideExtraLoading();
                        }
                        setLoadingList(false);
                        setIndex(getIndex() + 1);
                        FeedbackMainModel model = (FeedbackMainModel) arg0;
                        if (model != null) {
                            setUpList(model.getFeedbackList());
                            setFeedbackHeader(model);
                        }
                    }
                }

                @Override
                public void OnFailureFromServer(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        if (index == 0) {
                            showErrorPage();
                        } else {
                            hideExtraLoading();
                        }
                        Log.d(TAG, "Failure : " + msg);
                    }
                }

                @Override
                public void OnError(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        if (index == 0) {
                            showErrorPage();
                        } else {
                            hideExtraLoading();
                        }
                        Log.d(TAG, "Error : " + msg);
                    }
                }
            }, "GetReviewsList");
        }
    }

    private void setFeedbackHeader(FeedbackMainModel model) {

        if (model != null) {

            ProfileModel profileModel = globalFunctions.getProfile(context);
            if (profileModel != null && context != null) {
                try {
                    String
                            firstName = null,
                            lastName = null,
                            fullName = null;

                    firstName = profileModel.getFirstName();
                    lastName = profileModel.getLastName();
                    fullName = firstName + " " + lastName;
                    vendor_name_tv.setText(fullName);

                    if (GlobalFunctions.isNotNullValue(profileModel.getProfileImage())) {
                        SeparateProgress.loadImage(vendor_iv,profileModel.getProfileImage(), SeparateProgress.setProgress(activity));
                    }

                } catch (Exception exxx) {
                    Log.e(TAG, exxx.getMessage());
                }
            }

            if (model.getRating() != null) {
                float value;
                value = Float.parseFloat(model.getRating());
                ratingBar.setVisibility(View.VISIBLE);
                ratingBar.setRating(value);
            } else {
                ratingBar.setVisibility(View.GONE);
            }

            if (model.getRatingCount() != null) {
                ratings_count_tv.setText(model.getRatingCount() + " " + getString(R.string.ratings));
            }

            if (model.getCommentCount() != null) {
                no_of_comments_tv.setText(model.getCommentCount() + " " + getString(R.string.comments));
            }
        }
    }

    private void setUpList(FeedbackListModel feedbackListModel) {
        if (feedbackListModel != null) {
            detail = feedbackListModel;
            list.addAll(feedbackListModel.getFeedbackList());
            if (adapter != null) {
                synchronized (adapter) {
                    adapter.notifyDataSetChanged();
                }
            }
            if (list.size() <= 0) {
                showEmptyPage();
            } else {
                initRecyclerView();
                showContent();
            }
        }
    }

    public void initRecyclerView() {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager.onSaveInstanceState();
        Parcelable state = linearLayoutManager.onSaveInstanceState();
        adapter = new MyRatingsListAdapter(activity, list);
        recyclerView.setAdapter(adapter);
        linearLayoutManager.onRestoreInstanceState(state);
    }

    private void showExtraLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.showNow();
        }
    }

    private void hideExtraLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            progressBar.hideNow();
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isDataEnd() {
        return dataEnd;
    }

    public void setDataEnd(boolean dataEnd) {
        this.dataEnd = dataEnd;
    }

    public boolean isLoadingList() {
        return loadingList;
    }

    public void setLoadingList(boolean loadingList) {
        this.loadingList = loadingList;
    }

    private void showLoading() {
        if (progressActivity != null) {
            progressActivity.showLoading();
        }
    }

    private boolean isLoading() {
        if (progressActivity != null) {
            return progressActivity.isLoadingCurrentState();
        }
        return false;
    }

    private void showContent() {
        if (progressActivity != null) {
            progressActivity.showContent();
        }
    }

    private void showEmptyPage() {
        if (progressActivity != null) {
            progressActivity.showEmpty(getResources().getDrawable(R.drawable.ic_logo), getString(R.string.emptyRatingListMessage),
                    "");
        }

    }

    private void showErrorPage() {
        if (progressActivity != null) {
            progressActivity.showError(getResources().getDrawable(R.drawable.app_icon), getString(R.string.noConnection),
                    getString(R.string.noConnectionErrorMessage),
                    getString(R.string.tryAgain), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            refreshList();
                        }
                    });
        }
    }

    private void refreshList() {
        index = 0;
        preLast = 0;
        dataEnd = false;
        detail = null;
        list.clear();
        if (orderPostModel == null) {
            orderPostModel = new OrderPostModel();
        }
        orderPostModel.setIndex("0");
        orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
        getReviewsList(context);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
}