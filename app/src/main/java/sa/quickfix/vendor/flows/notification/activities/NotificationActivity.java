package sa.quickfix.vendor.flows.notification.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.FacebookSdk;
import com.vlonjatg.progressactivity.ProgressRelativeLayout;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.flows.wallet.activities.MyWalletActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.adapters.NotificationListAdapter;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnNotificationItemClickInvoke;
import sa.quickfix.vendor.flows.completed_requests.activities.CompletedRequestsActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.MyNotificationModel;
import sa.quickfix.vendor.services.model.NotificationMainModel;
import sa.quickfix.vendor.services.model.OrderPostModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;

public class NotificationActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, OnNotificationItemClickInvoke {

    public static final String TAG = "NotificationActivity";
    public static final String BUNDLE_NOTIFICATION_MAIN_MODEL = "BundleNotificationMainModel";

    Context context;
    private static Activity activity;

    SwipeRefreshLayout swipe_container;
    ProgressRelativeLayout progressActivity;
    DilatingDotsProgressBar progressBar;

    RecyclerView recyclerView;
    NotificationListAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    List<MyNotificationModel> list = new ArrayList<>();

    OrderPostModel orderPostModel = null;

    GlobalFunctions globalFunctions = null;
    GlobalVariables globalVariables = null;
    View mainView;

    private TextView clear_all_tv;

    private ImageView back_iv;
    NotificationMainModel mNotificationMainModel = null;

    public static Intent newInstance(Activity activity, NotificationMainModel notificationMainModel) {
        Intent intent = new Intent(activity, NotificationActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_NOTIFICATION_MAIN_MODEL, notificationMainModel);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.notification_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        clear_all_tv = (TextView) findViewById(R.id.clear_all_tv);

        back_iv = (ImageView) findViewById(R.id.back_iv);
        swipe_container = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(activity);
        progressActivity = (ProgressRelativeLayout) findViewById(R.id.details_progressActivity);
        progressBar = (DilatingDotsProgressBar) findViewById(R.id.extraProgressBar);

        mainView = back_iv;

        if (getIntent().hasExtra(BUNDLE_NOTIFICATION_MAIN_MODEL)) {
            mNotificationMainModel = (NotificationMainModel) getIntent().getSerializableExtra(BUNDLE_NOTIFICATION_MAIN_MODEL);
        } else {
            mNotificationMainModel = null;
        }

        clear_all_tv.setVisibility(View.GONE);

        initRecyclerView();

        seenNotificationAllData(context);

        /*if (mNotificationMainModel != null) {
            setUpList(mNotificationMainModel);
        } else {
            refreshList();
        }*/

        swipe_container.setOnRefreshListener(this);
        swipe_container.setColorSchemeResources(R.color.fab_color_normal,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        clear_all_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog alertDialog = new AlertDialog(context);
                alertDialog.setCancelable(false);
                alertDialog.setIcon(R.drawable.ic_logo_dark);
                alertDialog.setTitle(getString(R.string.app_name));
                alertDialog.setMessage(getResources().getString(R.string.clear_all_notifications));
                alertDialog.setPositiveButton(getString(R.string.yes), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        clearAllNotifications(context);
                    }
                });

                alertDialog.setNegativeButton(getString(R.string.cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                alertDialog.show();
            }
        });
    }

    private void seenNotificationAllData(final Context context) {
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.seenNotificationAllData(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                if (context != null) {
                    Log.d(TAG, "Response: " + arg0.toString());
//                    validateClearAllOutput(arg0);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
            }

            @Override
            public void OnError(String msg) {
            }
        }, "seenNotificationAllData");
    }

    private void clearAllNotifications(final Context context) {
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.clearAllNotifications(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                if (context != null) {
                    Log.d(TAG, "Response: " + arg0.toString());
                    validateClearAllOutput(arg0);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
            }

            @Override
            public void OnError(String msg) {
            }
        }, "ClearAllNotifications");
    }

    private void validateClearAllOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            if (statusModel != null) {
                if (!statusModel.isStatus()) {
                    globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
                } else {
                    MainActivity.closeThisActivity();
                    Intent intent = new Intent(NotificationActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        }
    }


    @Override
    public void onResume() {
        refreshList();
        super.onResume();
    }

    private void getNotificationList(final Context context) {
        if (!isLoading()) {
            showLoading();
            ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
            servicesMethodsManager.getNotificationList(context, orderPostModel, new ServerResponseInterface() {
                @Override
                public void OnSuccessFromServer(Object arg0) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        Log.d(TAG, "Response: " + arg0.toString());
                        showContent();
                        NotificationMainModel model = (NotificationMainModel) arg0;
                        setUpList(model);
                    }
                }

                @Override
                public void OnFailureFromServer(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        Log.d(TAG, "Failure : " + msg);
                    }
                }

                @Override
                public void OnError(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        Log.d(TAG, "Error : " + msg);
                    }
                }
            }, "NotificationList");
        }
    }

    private void setUpList(NotificationMainModel notificationMainModel) {
        if (notificationMainModel != null) {
//            setNotificationCount(notificationMainModel);
            if (notificationMainModel.getNotificationListModel() != null) {
                list.addAll(notificationMainModel.getNotificationListModel().getNotificationList());
                if (adapter != null) {
                    synchronized (adapter) {
                        adapter.notifyDataSetChanged();
                    }
                }
                if (list.size() <= 0) {
                    clear_all_tv.setVisibility(View.GONE);
                    showEmptyPage();
                } else {
                    clear_all_tv.setVisibility(View.VISIBLE);
                    initRecyclerView();
                    showContent();
                }
            }
        }
    }

    private void setNotificationCount(NotificationMainModel notificationMainModel) {
        if (notificationMainModel != null) {
            int notificationCount = 0;
            if (globalFunctions.isNotNullValue(notificationMainModel.getCount())) {
                try {
                    notificationCount = Integer.parseInt(notificationMainModel.getCount());
                } catch (Exception exc) {
                    notificationCount = 0;
                }
            }
            globalFunctions.setMyNotificationCount(context, notificationCount);
        }
    }

    public void initRecyclerView() {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager.onSaveInstanceState();
        Parcelable state = linearLayoutManager.onSaveInstanceState();
        adapter = new NotificationListAdapter(activity, list, this);
        recyclerView.setAdapter(adapter);
        linearLayoutManager.onRestoreInstanceState(state);
    }

    private void showLoading() {
        if (progressActivity != null) {
            progressActivity.showLoading();
        }
    }

    private boolean isLoading() {
        if (progressActivity != null) {
            return progressActivity.isLoadingCurrentState();
        }
        return false;
    }

    private void showContent() {
        if (progressActivity != null) {
            progressActivity.showContent();
        }
    }

    private void showEmptyPage() {
        if (progressActivity != null) {
            progressActivity.showEmpty(getResources().getDrawable(R.drawable.ic_logo), getString(R.string.emptyList),
                    "");
        }
    }

    private void showErrorPage() {
        if (progressActivity != null) {
            progressActivity.showError(getResources().getDrawable(R.drawable.app_icon), getString(R.string.noConnection),
                    getString(R.string.noConnectionErrorMessage),
                    getString(R.string.tryAgain), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            refreshList();
                        }
                    });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void refreshList() {
        list.clear();
        if (orderPostModel == null) {
            orderPostModel = new OrderPostModel();
        }
        orderPostModel.setIndex("0");
        orderPostModel.setSize("200");
        getNotificationList(context);
    }

    @Override
    public void OnClickInvoke(int position, MyNotificationModel model) {
        if (model != null) {
            //clear notification..
            clearNotification(context, model.getId());

            if (globalFunctions.isNotNullValue(model.getType())) {
                if (model.getType().equalsIgnoreCase(globalVariables.WAITING_FOR_SPARE_PARTS_APPROVAL) ||
                        model.getType().equalsIgnoreCase(globalVariables.JOB_COMPLETED)) {
                    Intent intent = MainActivity.newInstance(context, globalVariables.GO_TO_ACTIVE_TAB);
                    startActivity(intent);
                } else if (model.getType().equalsIgnoreCase(globalVariables.ORDER_CONFIRMED_BY_TECHNICIAN)) {
                    Intent intent = MainActivity.newInstance(context, globalVariables.GO_TO_UPCOMING_TAB);
                    startActivity(intent);
                } else if (model.getType().equalsIgnoreCase(globalVariables.WAITING_FOR_CONFIRMATION)) {
                    Intent intent = MainActivity.newInstance(context, globalVariables.GO_TO_NEW_TAB);
                    startActivity(intent);
                } else if (model.getType().equalsIgnoreCase(globalVariables.WALLET_STATUS)) {
                    Intent intent = new Intent(activity, MyWalletActivity.class);
                    activity.startActivity(intent);
                } else if (model.getType().equalsIgnoreCase(globalVariables.USER_CANCELLED) || model.getType().equalsIgnoreCase(globalVariables.CANCELLED) || model.getType().equalsIgnoreCase(globalVariables.ORDER_COMPLETED)
                        || model.getType().equalsIgnoreCase(globalVariables.CUSTOMER_NOT_RESPONDED)) {
                    Intent intent = new Intent(activity, CompletedRequestsActivity.class);
                    startActivity(intent);
                } else if (model.getType().equalsIgnoreCase(globalVariables.CHAT_STATUS)) {
                    Intent intent = MainActivity.newInstance(context, globalVariables.GO_TO_CHAT);
                    startActivity(intent);
                }
            }
        }
    }

    private void clearNotification(final Context context, String id) {
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.clearNotification(context, id, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                if (context != null) {
                    Log.d(TAG, "Response: " + arg0.toString());
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
            }

            @Override
            public void OnError(String msg) {
            }
        }, "ClearNotification");
    }
}