package sa.quickfix.vendor.flows.registration.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.SelectServicesListAdapter;
import sa.quickfix.vendor.flows.registration.activities.individual_registration.IndividualReviewApplicationActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.CategoryListModel;
import sa.quickfix.vendor.services.model.CategoryModel;
import sa.quickfix.vendor.services.model.IndividualRegisterModel;
import sa.quickfix.vendor.R;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class SelectServicesActivity extends AppCompatActivity {

    public static final String TAG = "SelectServicesActivity";

    public static final String BUNDLE_INDIVIDUAL_REGISTER_MODEL = "BundleIndividualRegisterModel";
    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";

    Context context;
    private static Activity activity;
    View mainView;

    List<Uri>
            uriProfileImageList,
            uriIdImageList,
            uriCertificatesImageList;

    IndividualRegisterModel individualRegisterModel = null;

    private CardView review_your_application_cardview;
    private TextView vendor_name_tv;
    private CircleImageView profile_iv;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    CategoryListModel categoryListModel = new CategoryListModel();
    List<CategoryModel> list = new ArrayList<>();

    LinearLayoutManager categoryLayoutManager;
    RecyclerView category_recycler_view;
    SelectServicesListAdapter selectServicesListAdapter;

    String fromPage = null;

    public static Intent newInstance(Context context, IndividualRegisterModel model, String fromPage) {
        Intent intent = new Intent(context, SelectServicesActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_INDIVIDUAL_REGISTER_MODEL, model);
        args.putString(BUNDLE_KEY_FROM_PAGE, fromPage);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.select_services_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        uriProfileImageList = new ArrayList<>();
        uriIdImageList = new ArrayList<>();
        uriCertificatesImageList = new ArrayList<>();

        uriProfileImageList.clear();
        uriIdImageList.clear();
        uriCertificatesImageList.clear();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        if (getIntent().hasExtra(BUNDLE_INDIVIDUAL_REGISTER_MODEL)) {
            individualRegisterModel = (IndividualRegisterModel) getIntent().getSerializableExtra(BUNDLE_INDIVIDUAL_REGISTER_MODEL);
        } else {
            individualRegisterModel = null;
        }

        Intent i = getIntent();
        uriProfileImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE);
        uriIdImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_LIST_CODE);
        uriCertificatesImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_CERTIFICATES_LIST_CODE);

        categoryLayoutManager = new LinearLayoutManager(context);

        vendor_name_tv = (TextView) findViewById(R.id.vendor_name_tv);
        profile_iv = (CircleImageView) findViewById(R.id.profile_iv);

        review_your_application_cardview = (CardView) findViewById(R.id.review_your_application_cardview);
        category_recycler_view = (RecyclerView) findViewById(R.id.category_recycler_view);

        mainView = review_your_application_cardview;

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_PAGE)) {
            fromPage = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_PAGE);
        } else {
            fromPage = null;
        }

        //category recycler view....

        initCategoryList();

        if (individualRegisterModel != null) {
            vendor_name_tv.setText(individualRegisterModel.getFirstName() + " " + individualRegisterModel.getLastName());

            if (uriProfileImageList != null) {
                if (uriProfileImageList.size() > 0) {
                    try {
                        Glide.with(this).load(uriProfileImageList.get(0)).into(profile_iv);
                    } catch (Exception exccc) {

                    }

                }
            }
        }

        if (fromPage != null) {
            if (fromPage.equalsIgnoreCase(globalVariables.FROM_PAGE_REVIEW_INDIVIDUAL_REGISTRATION)) {
                //from edit....
                if (individualRegisterModel != null) {

                    if (individualRegisterModel.getCategoryList() != null) {
                        //already selected list data..
                        setUpList(individualRegisterModel.getCategoryList());
                    }
                }
            } else {
                loadList(context);
            }
        } else {
            loadList(context);
        }

        review_your_application_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

    }

    private void validateInput() {
        if (context != null) {

            if (!isAnyServiceSelected()) {
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.select_atleast_one_service));
            } else {
                if (individualRegisterModel == null) {
                    individualRegisterModel = new IndividualRegisterModel();
                }
                categoryListModel.setRESPONSE("category");
                individualRegisterModel.setCategoryList(categoryListModel);
                Intent intent = IndividualReviewApplicationActivity.newInstance(activity, individualRegisterModel);
                intent.putExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE, (Serializable) uriProfileImageList);
                intent.putExtra(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_LIST_CODE, (Serializable) uriIdImageList);
                intent.putExtra(GlobalVariables.UPLOAD_CERTIFICATES_LIST_CODE, (Serializable) uriCertificatesImageList);
                activity.startActivity(intent);
            }

        }
    }

    private boolean isAnyServiceSelected() {
        boolean
                isAnyServiceSelected = false;

        if (categoryListModel != null) {

            int listSizeMain = categoryListModel.getCategoryList().size();
            for (int i = 0; i < listSizeMain; i++) {
                boolean isAdded = false;
                if(categoryListModel.getCategoryList().get(i).getSubCategoryListModel() != null){
                    int listSizeSub = categoryListModel.getCategoryList().get(i).getSubCategoryListModel().getSubCategoryListModels().size();
                    if (listSizeSub > 0) {
                        for (int j = 0; j < listSizeSub; j++) {

                            if (!isAdded && categoryListModel.getCategoryList().get(i).getSubCategoryListModel().getSubCategoryListModels().get(j).getSelected().equalsIgnoreCase("1")) {
                                isAnyServiceSelected = true;
                                isAdded = true;
                                return isAnyServiceSelected;
                            }
                        }
                    } else if (categoryListModel.getCategoryList().get(i).getSelected().equalsIgnoreCase("1")) {
                        isAnyServiceSelected = true;
                        return isAnyServiceSelected;
                    }
                }else if (categoryListModel.getCategoryList().get(i).getSelected().equalsIgnoreCase("1")) {
                    isAnyServiceSelected = true;
                    return isAnyServiceSelected;
                }
            }

        }
        return isAnyServiceSelected;

    }

    private void initCategoryList() {
        selectServicesListAdapter = new SelectServicesListAdapter(activity, list);
        category_recycler_view.setLayoutManager(categoryLayoutManager);
        category_recycler_view.setAdapter(selectServicesListAdapter);

        synchronized (selectServicesListAdapter) {
            selectServicesListAdapter.notifyDataSetChanged();
        }
    }

    private void loadList(final Context context) {
        globalFunctions.showProgress(context, context.getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getCategoryList(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                CategoryListModel model = (CategoryListModel) arg0;
                setUpList(model);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                Log.d(TAG, "Failure : " + msg);
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                Log.d(TAG, "Error : " + msg);
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
            }
        }, "GetCategoryList");
    }

    private void setUpList(CategoryListModel categoryListModel1) {
        if (categoryListModel1 != null && list != null) {
            list.clear();
            if (categoryListModel == null) {
                categoryListModel = new CategoryListModel();
            }
            categoryListModel.setCategoryList(categoryListModel1.getCategoryList());
            list = categoryListModel1.getCategoryList();
            initCategoryList();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}