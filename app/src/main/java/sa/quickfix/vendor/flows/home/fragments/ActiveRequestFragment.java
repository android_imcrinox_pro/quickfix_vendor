package sa.quickfix.vendor.flows.home.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;

import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.adapters.ReqSubListAdapter;
import sa.quickfix.vendor.adapters.StatusListAdapter;
import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.ActiveStatusListAdapter;
import sa.quickfix.vendor.adapters.AddedPartsStatusListAdapter;
import sa.quickfix.vendor.adapters.CommentListAdapter;
import sa.quickfix.vendor.adapters.OrderImagesListAdapter;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnActiveStatusItemClickInvoke;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnPartTimeEditClickInvoke;
import sa.quickfix.vendor.flows.add_inspection.AddInspectionActivity;
import sa.quickfix.vendor.flows.add_parts.activities.AddPartsActivity;
import sa.quickfix.vendor.flows.location_service.LocationMonitoringService;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.AddressModel;
import sa.quickfix.vendor.services.model.CommentModel;
import sa.quickfix.vendor.services.model.CommentsListModel;
import sa.quickfix.vendor.services.model.FixingTimePostModel;
import sa.quickfix.vendor.services.model.OrderDetailListModel;
import sa.quickfix.vendor.services.model.OrderDetailMainModel;
import sa.quickfix.vendor.services.model.OrderDetailModel;
import sa.quickfix.vendor.services.model.OrderModel;
import sa.quickfix.vendor.services.model.OrderStatusModel;
import sa.quickfix.vendor.services.model.OrderStatusUpdateModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.SparePartModel;
import sa.quickfix.vendor.services.model.SparePartsListModel;
import sa.quickfix.vendor.services.model.StatusListModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

public class ActiveRequestFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnActiveStatusItemClickInvoke, OnPartTimeEditClickInvoke {

    public static final String TAG = "ActiveRequestFragment";

    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    Activity activity;
    Context context;
    View mainView;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    boolean isJustCommentAdded = false;

    private boolean
            isIdImageVerified = false,
            isCertificateImageVerified = false;

    SwipeRefreshLayout swipe_container;

    OrderModel orderModel = null;
    String fromPage = null;
    String orderId = "";
    String amountToBePaid = "";
    String paymentType = "";
    String workActive = "";
    String orderStatus = "";
    boolean isOrderCompletedClicked = false;
    private LayoutInflater layoutInflater;

    AddressModel pickUpAddressModel = new AddressModel();

    StatusListModel statusListModel = new StatusListModel();
    List<OrderStatusModel> list = new ArrayList<>();
    ArrayList<CommentModel> commentList = new ArrayList<>();

    LinearLayoutManager layoutManager, details_linearLayoutManager, orderImagelayoutManager, commemntsLayoutManager;
    RecyclerView status_recycler_view, orderImage_recycler_view, comments_recycler_view, services_rv;
    ActiveStatusListAdapter statusListAdapter;
    OrderImagesListAdapter orderImagesListAdapter;
    CommentListAdapter commentListAdapter;

    private CircleImageView user_iv;
    private ImageView category_iv, call_iv;
    private LinearLayout find_on_map_ll;
    private CardView cancel_cardview, status_cardview;
    private RelativeLayout complete_rl;
    private TextView support_number_tv, user_name_tv, date_tv, address_tv, category_title_tv, services_tv, no_of_tv, items_count_tv, empty_request_tv, client_message_tv, inspection_time_tv,
            price_per_hour_tv, total_tv, inspection_status_pending_tv, inspection_status_completed_tv, inspection_status_rejected_tv, order_number_tv, view_comments_tv,
            add_comments_tv, vendor_comments_tv, close_comments_tv, job_status_title_tv, comments_title_tv, customer_not_available_tv, customer_not_paid_tv, add_more_comments_tv,
            req_to_cancel_tv, job_status_tv,
            sub_total_tv, spare_part_price_tv, vat_tv, spare_part_vat_tv, discount_tv, total_price_tv;
    private LinearLayout empty_request_ll, add_fixing_time_ll, enter_fixing_time_ll, added_parts_ll, add_parts_ll,
            add_inspection_ll, client_message_ll, client_picture_ll, inspection_status_ll, comments_list_ll,
            spare_part_price_ll, vat_ll, spare_part_vat_ll, discount_ll;
    private View req_to_cancel_view;

    boolean isCancelShow = false;

    List<SparePartModel> addedPartList = new ArrayList<>();

    private boolean
            isCommentsGiven = false,
            isImagesGiven = false;

    RecyclerView added_parts_recycler_view;
    AddedPartsStatusListAdapter addedPartsAdapter;
    LinearLayoutManager linearLayoutManager;

    private View client_description_and_images_view;
    private LinearLayout comments_ll;
    private NestedScrollView scroll_view;

    boolean
            isHourSelected = false,
            isMinuteSelected = false;
    private boolean mAlreadyStartedService = false;

    ReqSubListAdapter reqSubListAdapter;
    List<OrderDetailModel> orderDetailList = new ArrayList<>();
    OrderStatusModel orderStatusModel = null;

    public static Bundle getBundle(Activity activity) {
        Bundle bundle = new Bundle();
        return bundle;
    }

    public static Bundle getBundle(@Nullable String fromPage) {
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_FROM_PAGE, fromPage);
        return bundle;
    }

    static Intent locationintent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.active_request_fragment, container, false);
        activity = getActivity();
        context = getActivity();

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        this.layoutInflater = activity.getLayoutInflater();

        swipe_container = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);

        category_iv = (ImageView) view.findViewById(R.id.category_iv);
        call_iv = (ImageView) view.findViewById(R.id.call_iv);
        user_iv = (CircleImageView) view.findViewById(R.id.user_iv);
        complete_rl = (RelativeLayout) view.findViewById(R.id.complete_rl);

        support_number_tv = (TextView) view.findViewById(R.id.support_number_tv);
        user_name_tv = (TextView) view.findViewById(R.id.user_name_tv);
        date_tv = (TextView) view.findViewById(R.id.date_tv);
        address_tv = (TextView) view.findViewById(R.id.address_tv);
        category_title_tv = (TextView) view.findViewById(R.id.category_title_tv);
        services_tv = (TextView) view.findViewById(R.id.services_tv);
        no_of_tv = (TextView) view.findViewById(R.id.no_of_tv);
        items_count_tv = (TextView) view.findViewById(R.id.items_count_tv);
        empty_request_tv = (TextView) view.findViewById(R.id.empty_request_tv);
        client_message_tv = (TextView) view.findViewById(R.id.client_message_tv);
        inspection_time_tv = (TextView) view.findViewById(R.id.inspection_time_tv);
        price_per_hour_tv = (TextView) view.findViewById(R.id.price_per_hour_tv);
        total_tv = (TextView) view.findViewById(R.id.total_tv);
        inspection_status_pending_tv = (TextView) view.findViewById(R.id.inspection_status_pending_tv);
        inspection_status_completed_tv = (TextView) view.findViewById(R.id.inspection_status_completed_tv);
        inspection_status_rejected_tv = (TextView) view.findViewById(R.id.inspection_status_rejected_tv);
        order_number_tv = (TextView) view.findViewById(R.id.order_number_tv);
        view_comments_tv = (TextView) view.findViewById(R.id.view_comments_tv);
        add_comments_tv = (TextView) view.findViewById(R.id.add_comments_tv);
        // vendor_comments_tv = (TextView) view.findViewById(R.id.vendor_comments_tv);
        close_comments_tv = (TextView) view.findViewById(R.id.close_comments_tv);
        job_status_title_tv = (TextView) view.findViewById(R.id.job_status_title_tv);
        comments_title_tv = (TextView) view.findViewById(R.id.comments_title_tv);
        customer_not_available_tv = (TextView) view.findViewById(R.id.customer_not_available_tv);
        customer_not_paid_tv = (TextView) view.findViewById(R.id.customer_not_paid_tv);
        add_more_comments_tv = (TextView) view.findViewById(R.id.add_more_comments_tv);
        sub_total_tv = (TextView) view.findViewById(R.id.sub_total_tv);
        spare_part_price_tv = (TextView) view.findViewById(R.id.spare_part_price_tv);
        vat_tv = (TextView) view.findViewById(R.id.vat_tv);
        spare_part_vat_tv = (TextView) view.findViewById(R.id.spare_part_vat_tv);
        discount_tv = (TextView) view.findViewById(R.id.discount_tv);
        total_price_tv = (TextView) view.findViewById(R.id.total_price_tv);
        req_to_cancel_tv = (TextView) view.findViewById(R.id.req_to_cancel_tv);
        job_status_tv = (TextView) view.findViewById(R.id.job_status_tv);

        find_on_map_ll = (LinearLayout) view.findViewById(R.id.find_on_map_ll);
        status_cardview = (CardView) view.findViewById(R.id.status_cardview);
        cancel_cardview = (CardView) view.findViewById(R.id.cancel_cardview);
        status_recycler_view = (RecyclerView) view.findViewById(R.id.status_recycler_view);
        orderImage_recycler_view = (RecyclerView) view.findViewById(R.id.pictures_recycler_view);
        comments_recycler_view = (RecyclerView) view.findViewById(R.id.comments_recycler_view);

        commemntsLayoutManager = new LinearLayoutManager(context);
        layoutManager = new LinearLayoutManager(context);
        orderImagelayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        services_rv = (RecyclerView) view.findViewById(R.id.services_rv);
        details_linearLayoutManager = new LinearLayoutManager(context);


        empty_request_ll = (LinearLayout) view.findViewById(R.id.empty_request_ll);
        // add_fixing_time_ll = (LinearLayout) view.findViewById(R.id.add_fixing_time_ll);
        // enter_fixing_time_ll = (LinearLayout) view.findViewById(R.id.enter_fixing_time_ll);
        added_parts_ll = (LinearLayout) view.findViewById(R.id.added_parts_ll);
        add_parts_ll = (LinearLayout) view.findViewById(R.id.add_parts_ll);
        add_inspection_ll = (LinearLayout) view.findViewById(R.id.add_inspection_ll);
        client_message_ll = (LinearLayout) view.findViewById(R.id.client_message_ll);
        client_picture_ll = (LinearLayout) view.findViewById(R.id.client_picture_ll);
        inspection_status_ll = (LinearLayout) view.findViewById(R.id.inspection_status_ll);
        comments_list_ll = (LinearLayout) view.findViewById(R.id.comments_list_ll);
        spare_part_price_ll = (LinearLayout) view.findViewById(R.id.spare_part_price_ll);
        vat_ll = (LinearLayout) view.findViewById(R.id.vat_ll);
        spare_part_vat_ll = (LinearLayout) view.findViewById(R.id.spare_part_vat_ll);
        discount_ll = (LinearLayout) view.findViewById(R.id.discount_ll);
        req_to_cancel_view = (View) view.findViewById(R.id.req_to_cancel_view);

        client_description_and_images_view = (View) view.findViewById(R.id.client_description_and_images_view);
        comments_ll = (LinearLayout) view.findViewById(R.id.comments_ll);

        scroll_view = (NestedScrollView) view.findViewById(R.id.scroll_view);

        added_parts_recycler_view = (RecyclerView) view.findViewById(R.id.added_parts_recycler_view);
        linearLayoutManager = new LinearLayoutManager(activity);

        mainView = swipe_container;

        try {
            if (getArguments() != null) {
                fromPage = (String) getArguments().getString(BUNDLE_KEY_FROM_PAGE);
            }
        } catch (Exception exccc) {
            fromPage = null;
        }

//        comments_ll.setVisibility(View.GONE);
        close_comments_tv.setVisibility(View.GONE);
        comments_list_ll.setVisibility(View.GONE);
        add_more_comments_tv.setVisibility(View.GONE);
        add_comments_tv.setVisibility(View.GONE);
        view_comments_tv.setVisibility(View.GONE);
        add_parts_ll.setVisibility(View.GONE);
        add_inspection_ll.setVisibility(View.GONE);
        complete_rl.setVisibility(View.GONE);
        empty_request_ll.setVisibility(View.GONE);
        comments_title_tv.setVisibility(View.GONE);
        customer_not_available_tv.setVisibility(View.GONE);
        customer_not_paid_tv.setVisibility(View.GONE);
        job_status_title_tv.setVisibility(View.VISIBLE);

        // enter_fixing_time_ll.setVisibility(View.GONE);
        added_parts_ll.setVisibility(View.GONE);

        // initStatusList();
        initOrderList();
        initAddedPartsRecyclerView();
        orderId = "";
//        getOrderDetails(context, orderId);

       /* add_fixing_time_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_fixing_time_ll.setVisibility(View.GONE);
                enter_fixing_time_ll.setVisibility(View.VISIBLE);
            }
        });*/

        locationintent = new Intent(activity, LocationMonitoringService.class);

        swipe_container.setOnRefreshListener(this);
        swipe_container.setColorSchemeResources(R.color.fab_color_normal,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                orderId = "";
                isJustCommentAdded = false;
                getProfile(orderId);
                // getOrderDetails(context, orderId);
            }
        });

        support_number_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileModel profileModel = GlobalFunctions.getProfile(context);
                if (GlobalFunctions.isNotNullValue(profileModel.getSupportNumber())) {
                    GlobalFunctions.callPhone(activity, profileModel.getSupportNumber());
                }
            }
        });

        job_status_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderStatusModel != null) {
                    if (orderStatusModel.getId() != null && orderId != null) {
                        if (orderStatusModel.getId().equalsIgnoreCase(globalVariables.ORDER_COMPLETED)) {
                            //show deduct money from wallet popup....
                            //job is completed....check for payment type..
                            isOrderCompletedClicked = true;
                            // refresh the page....
                            orderId = "";
                            getProfile(orderId);
                            // getOrderDetails(context, orderId);
                   /* if (paymentType != null) {
                        if (paymentType.equalsIgnoreCase(globalVariables.PAYMENT_TYPE_COD)) {
                            showDeductMoneyFromWalletPopup(amountToBePaid);
                        } else if (paymentType.equalsIgnoreCase(globalVariables.PAYMENT_TYPE_NOT_SET)) {
                            //not set payment type yet...
                            showPaymentNotSetPopup();
                        }*//* else if (paymentType.equalsIgnoreCase(globalVariables.PAYMENT_TYPE_ONLINE)) {
                            //direct complete order.
                            OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                            orderStatusUpdateModel.setOrderId(orderId);
                            orderStatusUpdateModel.setStatus(orderStatusModel.getId());
                            updateOrderStatus(context, orderStatusUpdateModel);
                        }*//*
                    }*/
                        } else {
                            OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                            orderStatusUpdateModel.setOrderId(orderId);
                            orderStatusUpdateModel.setStatus(orderStatusModel.getId());
                            updateOrderStatus(context, orderStatusUpdateModel, false);
                        }
                    }
                }
            }
        });

//        startCountDownTimer();

        return view;
    }

    private void getProfile(final String orderId) {
        //globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getProfile(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                Log.d(TAG, "Response : " + arg0.toString());
                // globalFunctions.hideProgress();
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                if (arg0 instanceof ProfileModel) {
                    ProfileModel profileModel = (ProfileModel) arg0;
                    globalFunctions.setProfile(context, profileModel);
                    setThisPage(orderId);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                //globalFunctions.hideProgress();
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                // globalFunctions.hideProgress();
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "Get Profile");
    }

    private void setThisPage(String orderId) {
        ProfileModel profileModel = globalFunctions.getProfile(context);
        if (profileModel != null) {

            /*if (profileModel.getIdImageStatus() != null) {
                if (profileModel.getIdImageStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_VERIFIED)) {
                    isIdImageVerified = true;
                }
            }

            if (profileModel.getCertificateImgStatus() != null) {
                if (profileModel.getCertificateImgStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_VERIFIED)) {
                    isCertificateImageVerified = true;
                }
            }*/

            if (!GlobalFunctions.isNotNullValue(profileModel.getMessage())) {
                //verified....
                //nothing.... normal flow....
                getOrderDetails(context, orderId);
            } else {
                //show message...
                String message = "";
                message = profileModel.getMessage();
                showEmptyView(message);
            }
        }
    }

    private void getOrderDetails(final Context context, String orderId) {
        //globalFunctions.showProgress(context, context.getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getOrderDetails(context, orderId, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                // globalFunctions.hideProgress();
                validateOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                Log.d(TAG, "Failure : " + msg);
                // globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                Log.d(TAG, "Error : " + msg);
                // globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
            }
        }, "GetOrderDetails");
    }

    private void validateOutput(Object model) {
        if (model instanceof StatusModel) {
            MainActivity.setActiveRequestCount(0);
            StatusModel statusModel = (StatusModel) model;
            if (!statusModel.isStatus()) {
                showEmptyView(statusModel.getMessage());
            }
        } else {
            OrderDetailMainModel orderDetailMainModel = (OrderDetailMainModel) model;
            setThisPage(orderDetailMainModel);
        }
    }

    private void setThisPage(final OrderDetailMainModel model) {

        if (model != null) {

            MainActivity.setActiveRequestCount(1);

            complete_rl.setVisibility(View.VISIBLE);
            empty_request_ll.setVisibility(View.GONE);

            if (model.getOrderId() != null) {
                orderId = model.getOrderId();
            }

            if (GlobalFunctions.isNotNullValue(model.getOrderNumber())) {
                order_number_tv.setText(activity.getString(R.string.order_number) + " " + model.getOrderNumber());
            }

            if (model.getPayableAmount() != null) {
                amountToBePaid = model.getPayableAmount();
            }

            if (model.getPaymentType() != null) {
                paymentType = model.getPaymentType();
            }

            if (model.getCommentsListModel() != null && model.getCommentsListModel().getCommentList() != null && model.getCommentsListModel().getCommentList().size() > 0) {

                if (isJustCommentAdded) {
                    status_recycler_view.setVisibility(View.GONE);
                    job_status_title_tv.setVisibility(View.GONE);
//                    close_comments_tv.setVisibility(View.VISIBLE);
                    comments_list_ll.setVisibility(View.VISIBLE);
                    comments_title_tv.setVisibility(View.VISIBLE);
                    view_comments_tv.setVisibility(View.GONE);
                } else {
//                    close_comments_tv.setVisibility(View.GONE);
                    comments_list_ll.setVisibility(View.GONE);
                    comments_title_tv.setVisibility(View.GONE);
                    view_comments_tv.setVisibility(View.VISIBLE);
                    job_status_title_tv.setVisibility(View.VISIBLE);
                    status_recycler_view.setVisibility(View.VISIBLE);
                }

                //show comment list...
                add_more_comments_tv.setVisibility(View.VISIBLE);
                // comments_list_ll.setVisibility(View.VISIBLE);
                if (isJustCommentAdded) {
                    view_comments_tv.setVisibility(View.GONE);
//                    close_comments_tv.setVisibility(View.VISIBLE);
                } else {
                    view_comments_tv.setVisibility(View.VISIBLE);
//                    close_comments_tv.setVisibility(View.GONE);
                }
                add_comments_tv.setVisibility(View.GONE);
                if (model.getCommentsListModel() != null) {
                    setCommentList(model.getCommentsListModel());
                }
            } else {
                //hide comment list...
                add_more_comments_tv.setVisibility(View.GONE);
//                close_comments_tv.setVisibility(View.GONE);
                // comments_list_ll.setVisibility(View.GONE);
                view_comments_tv.setVisibility(View.GONE);
                add_comments_tv.setVisibility(View.VISIBLE);
            }

            view_comments_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    status_recycler_view.setVisibility(View.GONE);
                    job_status_title_tv.setVisibility(View.GONE);
                    close_comments_tv.setVisibility(View.VISIBLE);
                    comments_list_ll.setVisibility(View.VISIBLE);
                    comments_title_tv.setVisibility(View.VISIBLE);
                    view_comments_tv.setVisibility(View.GONE);
                }
            });

            close_comments_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    close_comments_tv.setVisibility(View.GONE);
                    comments_list_ll.setVisibility(View.GONE);
                    comments_title_tv.setVisibility(View.GONE);
                    view_comments_tv.setVisibility(View.VISIBLE);
                    job_status_title_tv.setVisibility(View.VISIBLE);
                    status_recycler_view.setVisibility(View.VISIBLE);
                }
            });

            if (model.getStatus() != null && model.getPaymentType() != null) {
                if (model.getStatus().equalsIgnoreCase(globalVariables.JOB_COMPLETED) && model.getPaymentType().equalsIgnoreCase(globalVariables.PAYMENT_TYPE_COD)) {
                    //show popup
                    if (model.getPayableAmount() != null) {
                        showDeductMoneyFromWalletPopup(model.getPayableAmount());
                    }
                } else if (model.getStatus().equalsIgnoreCase(globalVariables.JOB_COMPLETED) && model.getPaymentType().equalsIgnoreCase(globalVariables.PAYMENT_TYPE_ONLINE)) {
                    //directly complete the order...
                    if (orderId != null) {
                        //update status...
                        OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                        orderStatusUpdateModel.setOrderId(orderId);
                        orderStatusUpdateModel.setStatus(globalVariables.ORDER_COMPLETED);
                        updateOrderStatus(context, orderStatusUpdateModel, false);
                    }
                }
            }

            if (isOrderCompletedClicked) {
                //called order completed status...
                if (paymentType != null) {
                    if (paymentType.equalsIgnoreCase(globalVariables.PAYMENT_TYPE_COD)) {
                        showDeductMoneyFromWalletPopup(amountToBePaid);
                    } else if (paymentType.equalsIgnoreCase(globalVariables.PAYMENT_TYPE_ONLINE)) {
                        //directly complete the order...
                        if (orderId != null) {
                            //update status...
                            OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                            orderStatusUpdateModel.setOrderId(orderId);
                            orderStatusUpdateModel.setStatus(globalVariables.ORDER_COMPLETED);
                            updateOrderStatus(context, orderStatusUpdateModel, false);
                        }
                    } else if (paymentType.equalsIgnoreCase(globalVariables.PAYMENT_TYPE_NOT_SET)) {
                        //not set payment type yet...
                        showPaymentNotSetPopup();
                    }
                }
                isOrderCompletedClicked = false;
            }

            workActive = model.getWorkActive();

            if (model.getStatus() != null) {
                orderStatus = model.getStatus();
                if (model.getStatus().equalsIgnoreCase(globalVariables.WORK_IN_PROGRESS)) {
                    add_parts_ll.setVisibility(View.VISIBLE);

                    if (model.getOrderDetailList() != null && model.getOrderDetailList().getOrderDetailList().size() > 0) {
                        if (GlobalFunctions.isNotNullValue(model.getOrderDetailList().getOrderDetailList().get(0).getIsInspection()) && model.getOrderDetailList().getOrderDetailList().get(0).getIsInspection().equalsIgnoreCase("1")) {
                            add_inspection_ll.setVisibility(View.VISIBLE);
                        } else {
                            add_inspection_ll.setVisibility(View.GONE);
                        }
                    } else {
                        add_inspection_ll.setVisibility(View.GONE);
                    }

                } else {
                    add_parts_ll.setVisibility(View.GONE);
                    add_inspection_ll.setVisibility(View.GONE);
                }
            } else {
                add_parts_ll.setVisibility(View.GONE);
                add_inspection_ll.setVisibility(View.GONE);
            }

            String
                    date = "",
                    time = "";

            if (GlobalFunctions.isNotNullValue(model.getUserImage())) {
                SeparateProgress.loadImage(user_iv, model.getUserImage(), SeparateProgress.setProgress(activity));
            }


            if (model.getUserName() != null) {
                user_name_tv.setText(model.getUserName());
            }

            if (model.getDate() != null) {
                date = GlobalFunctions.getDayMonthYearFromDate(model.getDate());
            }

            if (model.getTime() != null) {
                time = GlobalFunctions.getTimeFromDate(model.getTime());
            }

            date_tv.setText(date + " " + "|" + " " + time);

            if (model.getAddress() != null) {
                address_tv.setText(model.getAddress());
            }

            find_on_map_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (model.getLatitude() != null && model.getLongitude() != null) {
                        // GlobalFunctions.showOnMap(activity, "", model.getLatitude(), model.getLongitude());
                        GlobalFunctions.openMapView(activity, model.getLatitude(), model.getLongitude());
//                        setAdsLocation(model.getLatitude(), model.getLongitude());
//                        Intent mIntent = SearchPlaceOnMapActivity.newInstance(context, pickUpAddressModel, GlobalVariables.LOCATION_TYPE.NONE);
//                        startActivity(mIntent);
                    }
                }
            });

            call_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (globalFunctions.isNotNullValue(model.getCommunicationNumber())) {
                        globalFunctions.callPhone(activity, model.getCommunicationNumber());
                    } else if (globalFunctions.isNotNullValue(model.getUserMobileNumber())) {
                        globalFunctions.callPhone(activity, model.getUserMobileNumber());
                    }
                }
            });

            if (model.getCategoryTitle() != null) {
                category_title_tv.setText(model.getCategoryTitle());
                no_of_tv.setText(getString(R.string.number_of) + " " + model.getCategoryTitle());
            }

            if (GlobalFunctions.isNotNullValue(model.getCategoryIcon())) {
                SeparateProgress.loadImage(category_iv, model.getCategoryIcon(), SeparateProgress.setProgress(activity));
            }

            if (model.getOrderDetailList() != null) {
                if (model.getOrderDetailList().getOrderDetailList() != null) {
                    String number = String.format("%02d", model.getOrderDetailList().getOrderDetailList().size());
                    items_count_tv.setText(number);
                }
            }

            setServicesNames(model);

            if (model.getStatusList() != null) {
                setStatusList(model.getStatusList(), model);
            }
            if (model.getOrderDetailList() != null) {
                setOrderList(model.getOrderDetailList());
            }

            if (GlobalFunctions.isNotNullValue(model.getComments())) {
                client_message_ll.setVisibility(View.VISIBLE);
                client_message_tv.setText(model.getComments());
                isCommentsGiven = true;
            } else {
                client_message_ll.setVisibility(View.GONE);
                isCommentsGiven = false;
            }

            if (model.getOrderImageList() != null) {
                setImageList(model.getOrderImageList().getImages());
            }

            add_parts_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = AddPartsActivity.newInstance(activity, model);
                    activity.startActivity(intent);
                }
            });

            add_inspection_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = AddInspectionActivity.newInstance(activity, model);
                    activity.startActivity(intent);
                }
            });

            cancel_cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openCancelJobAlertDialog(context);
                }
            });

            req_to_cancel_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openCancelJobAlertDialog(context);
                }
            });

            add_comments_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openAddCommentsAlertDialog(context);
                }
            });

            add_more_comments_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openAddCommentsAlertDialog(context);
                }
            });

            customer_not_available_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openCustomerNotAvailableAlertDialog(context);
                }
            });

            customer_not_paid_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openCustomerNotPaidAlertDialog(context);
                }
            });

            if (model.getSparePartsList() != null) {
                setAddedPartsList(model.getSparePartsList());
            }

            if (model.getStatus().equalsIgnoreCase(globalVariables.JOB_COMPLETED)) {
                cancel_cardview.setVisibility(View.GONE);
                req_to_cancel_tv.setVisibility(View.GONE);
//                req_to_cancel_view.setVisibility(View.GONE);
                isCancelShow = false;
            } else {
//                cancel_cardview.setVisibility(View.VISIBLE);
                req_to_cancel_tv.setVisibility(View.VISIBLE);
//                req_to_cancel_view.setVisibility(View.GONE);
                cancel_cardview.setVisibility(View.GONE);
                isCancelShow = true;
            }

            if (model.getStatus().equalsIgnoreCase(globalVariables.WAITING_FOR_SPARE_PARTS_APPROVAL)) {
                status_cardview.setVisibility(View.GONE);
            } else {
                status_cardview.setVisibility(View.VISIBLE);
            }

            if ((model.getStatus().equalsIgnoreCase(globalVariables.WORK_IN_PROGRESS) || model.getStatus().equalsIgnoreCase(globalVariables.WAITING_FOR_SPARE_PARTS_APPROVAL)) && GlobalFunctions.isNotNullValue(model.getOrderDetailList().getOrderDetailList().get(0).getIsInspection())
                    && model.getOrderDetailList().getOrderDetailList().get(0).getIsInspection().equalsIgnoreCase("1") && GlobalFunctions.isNotNullValue(model.getOrderDetailList().getOrderDetailList().get(0).getInspectionTime())) {
                inspection_status_ll.setVisibility(View.VISIBLE);

                if (GlobalFunctions.isNotNullValue(model.getOrderDetailList().getOrderDetailList().get(0).getInspectionTime())) {
                    inspection_time_tv.setText(model.getOrderDetailList().getOrderDetailList().get(0).getInspectionTime());
                }

                if (GlobalFunctions.isNotNullValue(model.getOrderDetailList().getOrderDetailList().get(0).getPricePerHour())) {
                    price_per_hour_tv.setText(activity.getString(R.string.sar) + " " + model.getOrderDetailList().getOrderDetailList().get(0).getPricePerHour());
                }
                if (GlobalFunctions.isNotNullValue(model.getOrderDetailList().getOrderDetailList().get(0).getInspectionPrice())) {
                    total_tv.setText(activity.getString(R.string.sar) + " " + model.getOrderDetailList().getOrderDetailList().get(0).getInspectionPrice());
                }

                if (model.getOrderDetailList().getOrderDetailList().get(0).getStatus().equalsIgnoreCase(GlobalVariables.PART_STATUS_PENDING)) {
                    inspection_status_pending_tv.setText(model.getOrderDetailList().getOrderDetailList().get(0).getStatusTitle());
                    inspection_status_pending_tv.setVisibility(View.VISIBLE);
                    inspection_status_completed_tv.setVisibility(View.GONE);
                    inspection_status_rejected_tv.setVisibility(View.GONE);
                } else if (model.getOrderDetailList().getOrderDetailList().get(0).getStatus().equalsIgnoreCase(GlobalVariables.PART_STATUS_ACCEPTED)) {
                    inspection_status_completed_tv.setText(model.getOrderDetailList().getOrderDetailList().get(0).getStatusTitle());
                    inspection_status_pending_tv.setVisibility(View.GONE);
                    inspection_status_rejected_tv.setVisibility(View.GONE);
                    inspection_status_completed_tv.setVisibility(View.VISIBLE);
                } else if (model.getOrderDetailList().getOrderDetailList().get(0).getStatus().equalsIgnoreCase(GlobalVariables.PART_STATUS_REJECTED)) {
                    inspection_status_rejected_tv.setText(model.getOrderDetailList().getOrderDetailList().get(0).getStatusTitle());
                    inspection_status_pending_tv.setVisibility(View.GONE);
                    inspection_status_completed_tv.setVisibility(View.GONE);
                    inspection_status_rejected_tv.setVisibility(View.VISIBLE);
                } else {
                    inspection_status_pending_tv.setVisibility(View.GONE);
                    inspection_status_completed_tv.setVisibility(View.GONE);
                    inspection_status_rejected_tv.setVisibility(View.GONE);
                }

            } else {
                inspection_status_ll.setVisibility(View.GONE);
            }

            if (isCommentsGiven || isImagesGiven) {
                client_description_and_images_view.setVisibility(View.VISIBLE);
            } else {
                client_description_and_images_view.setVisibility(View.GONE);
            }

            if (model.getStatus() != null && model.getStatus().equalsIgnoreCase(globalVariables.ON_THE_WAY)) {
                customer_not_available_tv.setVisibility(View.VISIBLE);
//                comments_ll.setVisibility(View.GONE);
                add_comments_tv.setVisibility(View.GONE);
                view_comments_tv.setVisibility(View.GONE);
//                close_comments_tv.setVisibility(View.GONE);
                    req_to_cancel_view.setVisibility(View.GONE);
            } else {
                customer_not_available_tv.setVisibility(View.GONE);
//                comments_ll.setVisibility(View.VISIBLE);
//                req_to_cancel_view.setVisibility(View.VISIBLE);
                add_comments_tv.setVisibility(View.VISIBLE);
                view_comments_tv.setVisibility(View.VISIBLE);
                if (isCancelShow){
                    req_to_cancel_view.setVisibility(View.VISIBLE);
                }else {
                    req_to_cancel_view.setVisibility(View.GONE);
                }
//                close_comments_tv.setVisibility(View.VISIBLE);
            }

            //  status 106 and paymentype is 0
            if (model.getStatus() != null && model.getPaymentType() != null) {
                if (model.getStatus().equalsIgnoreCase(globalVariables.JOB_COMPLETED) && model.getPaymentType().equalsIgnoreCase("0")) {
                    customer_not_paid_tv.setVisibility(View.VISIBLE);
                } else {
                    customer_not_paid_tv.setVisibility(View.GONE);
                }
            }

            if (fromPage != null && fromPage.equalsIgnoreCase(globalVariables.GO_TO_CHAT)) {
                //give view comments....functionality
                status_recycler_view.setVisibility(View.GONE);
                job_status_title_tv.setVisibility(View.GONE);
//                close_comments_tv.setVisibility(View.VISIBLE);
                comments_list_ll.setVisibility(View.VISIBLE);
                comments_title_tv.setVisibility(View.VISIBLE);
                view_comments_tv.setVisibility(View.GONE);
                if (scroll_view != null) {
                    scroll_view.fullScroll(View.FOCUS_DOWN);
                }
            }

            if (GlobalFunctions.isNotNullValue(model.getSubTotal())) {
                sub_total_tv.setText(model.getSubTotal() + " " + activity.getString(R.string.sar));

            }
            if (GlobalFunctions.isNotNullValue(model.getSparePartPrice()) && GlobalFunctions.isNotZeroValue(model.getSparePartPrice())) {
                spare_part_price_tv.setText(model.getSparePartPrice() + " " + activity.getString(R.string.sar));
                spare_part_price_ll.setVisibility(View.VISIBLE);
            } else {
                spare_part_price_ll.setVisibility(View.GONE);
            }

            if (GlobalFunctions.isNotNullValue(model.getVat()) && GlobalFunctions.isNotZeroValue(model.getVat())) {
                vat_tv.setText(model.getVat() + " " + activity.getString(R.string.sar));
                vat_ll.setVisibility(View.VISIBLE);
            } else {
                vat_ll.setVisibility(View.GONE);
            }

            if (GlobalFunctions.isNotNullValue(model.getSparePartVat()) && GlobalFunctions.isNotZeroValue(model.getSparePartVat())) {
                spare_part_vat_tv.setText(model.getSparePartVat() + " " + activity.getString(R.string.sar));
                spare_part_vat_ll.setVisibility(View.VISIBLE);
            } else {
                spare_part_vat_ll.setVisibility(View.GONE);
            }

            if (GlobalFunctions.isNotNullValue(model.getPayableAmount())) {
                total_price_tv.setText(model.getPayableAmount() + " " + activity.getString(R.string.sar));

            }
            if (GlobalFunctions.isNotNullValue(model.getDiscountTotal())) {
                String totalDiscount = GlobalFunctions.getCalculateTotalAmount(model.getDiscountTotal(), model.getSubscriptionDiscount());
//                if ( GlobalFunctions.isNotZeroValue(totalDiscount)){
//                    discount_ll.setVisibility(View.VISIBLE);
                discount_tv.setText(GlobalFunctions.getPriceStringFromDecimal(totalDiscount) + " " + activity.getString(R.string.sar));
//                }else {
//                    discount_ll.setVisibility(View.VISIBLE);
//                }
//            }else {
//                discount_ll.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setOrderList(OrderDetailListModel orderDetailListModel) {
        if (orderDetailListModel != null && orderDetailList != null) {
            orderDetailList.clear();
            orderDetailList.addAll(orderDetailListModel.getOrderDetailList());
            if (orderDetailList.size() > 0) {
                initOrderList();
            }
        }
    }

    private void openCustomerNotPaidAlertDialog(final Context context) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.confirm));
        alertDialog.setPositiveButton(getString(R.string.yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (orderId != null) {
                    //update status...
                    OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                    orderStatusUpdateModel.setOrderId(orderId);
                    updateCustomerNotPaid(context, orderStatusUpdateModel);
                }
            }
        });

        alertDialog.setNegativeButton(getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void showDeductMoneyFromWalletPopup(String totalAmount) {
        String wallet_deduct_message = "";
        if (totalAmount != null) {
            wallet_deduct_message = getString(R.string.customer_has_opted_for_cod) + " " + totalAmount + " " + getString(R.string.will_be_deducted_from_your_wallet);
        }
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(wallet_deduct_message);
        alertDialog.setPositiveButton(getString(R.string.proceed), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                //update 109....
                if (orderId != null) {
                    //update status...
                    OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                    orderStatusUpdateModel.setOrderId(orderId);
                    orderStatusUpdateModel.setStatus(globalVariables.ORDER_COMPLETED);
                    updateOrderStatus(context, orderStatusUpdateModel, false);
                }
                //  alertDialog.dismiss();
            }
        });

        alertDialog.setNegativeButton(getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void setAddedPartsList(SparePartsListModel sparePartsList) {
        if (sparePartsList != null && addedPartList != null) {
            addedPartList.clear();

            if (sparePartsList.getSparePartList().size() > 0) {
                for (int i = 0; i < sparePartsList.getSparePartList().size(); i++) {
                    if (!sparePartsList.getSparePartList().get(i).getStatus().equalsIgnoreCase(GlobalVariables.PART_STATUS_ADDED)) {
                        addedPartList.add(sparePartsList.getSparePartList().get(i));
                    }
                }
                added_parts_ll.setVisibility(View.VISIBLE);
                initAddedPartsRecyclerView();
            } else {
                added_parts_ll.setVisibility(View.GONE);
            }
        }
    }

    private void setCommentList(CommentsListModel mCommentList) {
        if (mCommentList != null && commentList != null) {
            commentList.clear();
            commentList.addAll(mCommentList.getCommentList());
            if (commentList.size() > 0) {
                initCommentList();
            }
        }
    }

    private void setStatusList(StatusListModel statusList, OrderDetailMainModel mainModel) {
        if (statusList != null && list != null && mainModel != null) {
            list.clear();
            list.addAll(statusList.getStatusList());
            if (list.size() > 0) {
                initStatusList(mainModel);
                this.orderStatusModel = GlobalFunctions.getNameFromList(list);
                if (orderStatusModel != null) {
                    job_status_tv.setText(orderStatusModel.getTitle());
                }
            }
        }
    }

    private void setImageList(ArrayList<String> imagelist) {
        if (imagelist.size() > 0) {
            client_picture_ll.setVisibility(View.VISIBLE);
            orderImage_recycler_view.setVisibility(View.VISIBLE);
            initImageList(imagelist);
            isImagesGiven = true;
        } else {
            client_picture_ll.setVisibility(View.GONE);
            orderImage_recycler_view.setVisibility(View.GONE);
            isImagesGiven = false;
        }
    }

    private void initImageList(ArrayList<String> imagelist) {
        orderImagesListAdapter = new OrderImagesListAdapter(activity, imagelist);
        orderImage_recycler_view.setLayoutManager(orderImagelayoutManager);
        orderImage_recycler_view.setAdapter(orderImagesListAdapter);

        synchronized (orderImagesListAdapter) {
            orderImagesListAdapter.notifyDataSetChanged();
        }
    }

    private void initCommentList() {
        commentListAdapter = new CommentListAdapter(activity, commentList);
        comments_recycler_view.setLayoutManager(commemntsLayoutManager);
        comments_recycler_view.setAdapter(commentListAdapter);

        synchronized (commentListAdapter) {
            commentListAdapter.notifyDataSetChanged();
        }
    }

    private void initOrderList() {
        reqSubListAdapter = new ReqSubListAdapter(activity, orderDetailList);
        services_rv.setLayoutManager(details_linearLayoutManager);
        services_rv.setAdapter(reqSubListAdapter);

        synchronized (reqSubListAdapter) {
            reqSubListAdapter.notifyDataSetChanged();
        }
    }

    private void initStatusList(OrderDetailMainModel orderDetailMainModel) {
        statusListAdapter = new ActiveStatusListAdapter(activity, list, this, orderDetailMainModel);
        status_recycler_view.setLayoutManager(layoutManager);
        status_recycler_view.setAdapter(statusListAdapter);

        synchronized (statusListAdapter) {
            statusListAdapter.notifyDataSetChanged();
        }
    }

    private void openCustomerNotAvailableAlertDialog(final Context context) {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.alert_custom_dialog_for_customer_not_responded, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        TextView submit_tv, cancel_tv;
        final EditText reason_etv;

        reason_etv = alertView.findViewById(R.id.customer_not_available_reason_etv);
        submit_tv = alertView.findViewById(R.id.customer_not_available_submit_tv);
        cancel_tv = alertView.findViewById(R.id.customer_not_available_cancel_tv);

        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        submit_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = false;
                String reason = reason_etv.getText().toString().trim();
                if (reason.isEmpty()) {
                    reason_etv.setError(activity.getString(R.string.please_enter_reason));
                    reason_etv.setFocusableInTouchMode(true);
                    reason_etv.requestFocus();
                } else {
                    isValid = true;
                    if (orderId != null) {
                        //update status...
                        OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                        orderStatusUpdateModel.setOrderId(orderId);
                        orderStatusUpdateModel.setStatus(globalVariables.CUSTOMER_NOT_RESPONDED);
                        orderStatusUpdateModel.setReason(reason);
                        updateOrderStatus(context, orderStatusUpdateModel, true);
                    }
                }
                if (isValid) {
                    dialog.dismiss();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void openAddCommentsAlertDialog(final Context context) {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.alert_custom_dialog_for_add_comments, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        TextView submit_tv, cancel_tv;
        final EditText comments_etv;

        comments_etv = alertView.findViewById(R.id.comments_etv);
        submit_tv = alertView.findViewById(R.id.submit_comment_tv);
        cancel_tv = alertView.findViewById(R.id.cancel_comment_tv);

        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        submit_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = false;
                String comment = comments_etv.getText().toString().trim();
                if (comment.isEmpty()) {
                    comments_etv.setError(activity.getString(R.string.please_enter_your_comments));
                    comments_etv.setFocusableInTouchMode(true);
                    comments_etv.requestFocus();
                } else {
                    isValid = true;
                    if (orderId != null) {
                        //send comment...
                        OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                        orderStatusUpdateModel.setOrderId(orderId);
                        orderStatusUpdateModel.setComment(comment);
                        addComments(context, orderStatusUpdateModel);
                    }
                }
                if (isValid) {
                    dialog.dismiss();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void openCancelJobAlertDialog(final Context context) {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.alert_custom_dialog_for_cancel_request, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        TextView submit_tv, cancel_tv;
        final EditText reason_etv;

        reason_etv = alertView.findViewById(R.id.reason_etv);
        submit_tv = alertView.findViewById(R.id.submit_tv);
        cancel_tv = alertView.findViewById(R.id.cancel_tv);

        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        submit_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = false;
                String reason = reason_etv.getText().toString().trim();
                if (reason.isEmpty()) {
                    reason_etv.setError(activity.getString(R.string.please_enter_reason));
                    reason_etv.setFocusableInTouchMode(true);
                    reason_etv.requestFocus();
                } else {
                    isValid = true;
                    if (orderId != null) {
                        //update status...
                        OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                        orderStatusUpdateModel.setOrderId(orderId);
                        orderStatusUpdateModel.setStatus(globalVariables.CANCELLED);
                        orderStatusUpdateModel.setReason(reason);
                        updateOrderStatus(context, orderStatusUpdateModel, false);
                    }
                }
                if (isValid) {
                    dialog.dismiss();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void addComments(final Context context, final OrderStatusUpdateModel model) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.addComment(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateCommentsOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "addComment");
    }

    private void validateCommentsOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            if (statusModel != null) {
                globalFunctions.displayMessageBottom(context, mainView, statusModel.getMessage());
                if (statusModel.isStatus()) {
                    // vendor_comments_tv.setText("");
                    isJustCommentAdded = true;
                    getProfile(orderId);
                }
            }
        }
    }

    private void updateCustomerNotPaid(final Context context, final OrderStatusUpdateModel model) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.updateCustomerNotPaid(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateCustomerNotPaidOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "UpdateCustomerNotPaid");
    }

    private void updateOrderStatus(final Context context, final OrderStatusUpdateModel model, final boolean isCustomerNotAvailable) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.updateOrderStatus(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validatOrderStatusUpdateOutput(arg0, model, isCustomerNotAvailable);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "UpdateOrderStatus");
    }

    private void validateCustomerNotPaidOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            showAlertMessage(statusModel);
        }
    }

    private void validatOrderStatusUpdateOutput(Object model, OrderStatusUpdateModel orderStatusUpdateModel, boolean isCustomerNotAvailable) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            showAlertMessage(statusModel, orderStatusUpdateModel, isCustomerNotAvailable);
        }
    }

    private void showAlertMessage(final StatusModel statusModel) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(statusModel.getMessage());
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (statusModel.isStatus()) {
                    MainActivity.setActiveRequestCount(0);
                    Fragment newRequestListFragment = new NewRequestListFragment();
                    ((MainActivity) activity).replaceFragment(newRequestListFragment, NewRequestListFragment.TAG, "", 0, 0);
                }
            }
        });

        alertDialog.show();
    }

    private void showAlertMessage(final StatusModel statusModel, final OrderStatusUpdateModel orderStatusUpdateModel, final boolean isCustomerNotAvailable) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(statusModel.getMessage());
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (statusModel.isStatus()) {
                    if (isCustomerNotAvailable) {
                        MainActivity.setActiveRequestCount(0);
                        Fragment newRequestListFragment = new NewRequestListFragment();
                        ((MainActivity) activity).replaceFragment(newRequestListFragment, NewRequestListFragment.TAG, "", 0, 0);
                    } else {
                        if (orderStatusUpdateModel != null) {

                            if (orderStatusUpdateModel.getStatus() != null) {
                                if (orderStatusUpdateModel.getStatus().equalsIgnoreCase(globalVariables.WORK_IN_PROGRESS)) {
                                    //reload the page....
                                    //orderId = "";
                                    //  getOrderDetails(context, orderId);
                                    getProfile(orderId);
                                } else if (orderStatusUpdateModel.getStatus().equalsIgnoreCase(globalVariables.ARRIVED_TO_CUSTOMER)) {
                                    //reload the page....
                                    //orderId = "";
                                    //  getOrderDetails(context, orderId);
                                    getProfile(orderId);
                                } else if (orderStatusUpdateModel.getStatus().equalsIgnoreCase(globalVariables.CANCELLED)) {
                                    orderId = "";
                                    stopLocationService();
                                    GlobalFunctions.setSharedPreferenceString(context, "extra_order_Id", orderId);
                                    // getOrderDetails(context, orderId);
                                    getProfile(orderId);
                               /* Intent intent = new Intent(activity, CompletedRequestsActivity.class);
                                activity.startActivity(intent);*/
                                    Fragment newRequestListFragment = new NewRequestListFragment();
                                    ((MainActivity) activity).replaceFragment(newRequestListFragment, NewRequestListFragment.TAG, "", 0, 0);
                                } else if (orderStatusUpdateModel.getStatus().equalsIgnoreCase(globalVariables.JOB_COMPLETED)) {
                                    //reload the page....
                                    // orderId = "";
                                    // getOrderDetails(context, orderId);
                                    getProfile(orderId);
                                } else if (orderStatusUpdateModel.getStatus().equalsIgnoreCase(globalVariables.ORDER_COMPLETED)) {
                                    Fragment newRequestListFragment = new NewRequestListFragment();
                                    ((MainActivity) activity).replaceFragment(newRequestListFragment, NewRequestListFragment.TAG, "", 0, 0);
                                }
                            }

                        }
                    }
                }
            }
        });

        alertDialog.show();
    }

    private void setServicesNames(OrderDetailMainModel model) {
        if (model != null) {
            List<String> servicesNames = new ArrayList<>();
            servicesNames.clear();
            if (model.getOrderDetailList() != null) {
                if (model.getOrderDetailList().getServiceTitle() != null) {
                    servicesNames.addAll(model.getOrderDetailList().getServiceTitle());
                    services_tv.setText(globalFunctions.getStringFromList(servicesNames));
                }
            }
        }
    }

    private void setAdsLocation(String myLatitude, String myLongitude) {
        if (pickUpAddressModel == null) {
            pickUpAddressModel = new AddressModel();
        }
        double latitude = 0.0;
        double longitude = 0.0;
        try {
            latitude = Double.parseDouble(myLatitude);
            longitude = Double.parseDouble(myLongitude);
        } catch (Exception e) {
            latitude = 0.0;
            longitude = 0.0;
        }

        LatLng startLatLng = new LatLng(latitude, longitude);
        LatLng endLatLng = new LatLng(latitude, longitude);

        pickUpAddressModel.setLatitude(latitude);
        pickUpAddressModel.setLongitude(longitude);
    }

    private void showEmptyView(String message) {
        complete_rl.setVisibility(View.GONE);
        empty_request_ll.setVisibility(View.VISIBLE);
        empty_request_tv.setText(message);
    }

    public void initAddedPartsRecyclerView() {
        added_parts_recycler_view.setLayoutManager(linearLayoutManager);
        added_parts_recycler_view.setHasFixedSize(true);
        addedPartsAdapter = new AddedPartsStatusListAdapter(activity, addedPartList, this);
        added_parts_recycler_view.setAdapter(addedPartsAdapter);
        if (addedPartsAdapter != null) {
            synchronized (addedPartsAdapter) {
                addedPartsAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onResume() {
        ((MainActivity) activity).setSelectionTabIcons(getString(R.string.active));
        orderId = "";
        getProfile(orderId);
//        stopCountDownTimer();

        // getOrderDetails(context, orderId);
        super.onResume();
    }

    @Override
    public void OnItemClickInvoke(int position, OrderStatusModel orderStatusModel) {
        //update status...
        if (orderStatusModel != null) {
            if (orderStatusModel.getId() != null && orderId != null) {
                if (orderStatusModel.getId().equalsIgnoreCase(globalVariables.ORDER_COMPLETED)) {
                    //show deduct money from wallet popup....
                    //job is completed....check for payment type..
                    isOrderCompletedClicked = true;
                    // refresh the page....
                    orderId = "";
                    getProfile(orderId);
                    // getOrderDetails(context, orderId);
                   /* if (paymentType != null) {
                        if (paymentType.equalsIgnoreCase(globalVariables.PAYMENT_TYPE_COD)) {
                            showDeductMoneyFromWalletPopup(amountToBePaid);
                        } else if (paymentType.equalsIgnoreCase(globalVariables.PAYMENT_TYPE_NOT_SET)) {
                            //not set payment type yet...
                            showPaymentNotSetPopup();
                        }*//* else if (paymentType.equalsIgnoreCase(globalVariables.PAYMENT_TYPE_ONLINE)) {
                            //direct complete order.
                            OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                            orderStatusUpdateModel.setOrderId(orderId);
                            orderStatusUpdateModel.setStatus(orderStatusModel.getId());
                            updateOrderStatus(context, orderStatusUpdateModel);
                        }*//*
                    }*/
                } else {
                    OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                    orderStatusUpdateModel.setOrderId(orderId);
                    orderStatusUpdateModel.setStatus(orderStatusModel.getId());
                    updateOrderStatus(context, orderStatusUpdateModel, false);
                }
            }
        }
    }

    @Override
    public void OnTroubleInvoke(int position, OrderStatusModel orderStatusModel) {
        ProfileModel profileModel = GlobalFunctions.getProfile(context);
        if (GlobalFunctions.isNotNullValue(profileModel.getSupportNumber())) {
            GlobalFunctions.callPhone(activity, profileModel.getSupportNumber());
        }
    }


    private void showPaymentNotSetPopup() {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getString(R.string.customer_not_set_payment_mentod));
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onRefresh() {
        orderId = "";
        getProfile(orderId);
        //getOrderDetails(context, orderId);
    }

    @Override
    public void OnEditClickInvoke(int position, SparePartModel sparePartModel) {
        if (sparePartModel != null) {
            //edit time...
            openTimeDialog(context, sparePartModel);
        }
    }

    private void openTimeDialog(final Context context, final SparePartModel sparePartModel) {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.alert_custom_dialog_for_time_update, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        List<String> hoursStringList = new ArrayList<>();
        List<String> minutesStringList = new ArrayList<>();

        hoursStringList.clear();
        hoursStringList.add("00");
        hoursStringList.add("01");
        hoursStringList.add("02");
        hoursStringList.add("03");
        hoursStringList.add("04");
        hoursStringList.add("05");
        hoursStringList.add("06");
        hoursStringList.add("07");
        hoursStringList.add("08");
        hoursStringList.add("09");
        hoursStringList.add("10");
        hoursStringList.add("11");
        hoursStringList.add("12");

        minutesStringList.clear();
        minutesStringList.add("00");
        minutesStringList.add("01");
        minutesStringList.add("02");
        minutesStringList.add("03");
        minutesStringList.add("04");
        minutesStringList.add("05");
        minutesStringList.add("06");
        minutesStringList.add("07");
        minutesStringList.add("08");
        minutesStringList.add("09");
        minutesStringList.add("10");
        minutesStringList.add("11");
        minutesStringList.add("12");
        minutesStringList.add("13");
        minutesStringList.add("14");
        minutesStringList.add("15");
        minutesStringList.add("16");
        minutesStringList.add("17");
        minutesStringList.add("18");
        minutesStringList.add("19");
        minutesStringList.add("20");
        minutesStringList.add("21");
        minutesStringList.add("22");
        minutesStringList.add("23");
        minutesStringList.add("24");
        minutesStringList.add("25");
        minutesStringList.add("26");
        minutesStringList.add("27");
        minutesStringList.add("28");
        minutesStringList.add("29");
        minutesStringList.add("30");
        minutesStringList.add("31");
        minutesStringList.add("32");
        minutesStringList.add("33");
        minutesStringList.add("34");
        minutesStringList.add("35");
        minutesStringList.add("36");
        minutesStringList.add("37");
        minutesStringList.add("38");
        minutesStringList.add("39");
        minutesStringList.add("40");
        minutesStringList.add("41");
        minutesStringList.add("42");
        minutesStringList.add("43");
        minutesStringList.add("44");
        minutesStringList.add("45");
        minutesStringList.add("46");
        minutesStringList.add("47");
        minutesStringList.add("48");
        minutesStringList.add("47");
        minutesStringList.add("50");
        minutesStringList.add("51");
        minutesStringList.add("51");
        minutesStringList.add("52");
        minutesStringList.add("53");
        minutesStringList.add("54");
        minutesStringList.add("55");
        minutesStringList.add("56");
        minutesStringList.add("57");
        minutesStringList.add("58");
        minutesStringList.add("59");
        minutesStringList.add("60");

        final TextView update_tv, cancel_tv, hours_tv, minutes_tv;
        final SpinnerDialog spinnerDialogHours, spinnerDialogMinutes;

        update_tv = alertView.findViewById(R.id.update_tv);
        cancel_tv = alertView.findViewById(R.id.cancel_tv);
        hours_tv = alertView.findViewById(R.id.hours_tv);
        minutes_tv = alertView.findViewById(R.id.minutes_tv);

        spinnerDialogHours = new SpinnerDialog(activity, (ArrayList<String>) hoursStringList, getString(R.string.select_hour));
        spinnerDialogHours.setCancellable(true); // for cancellable
        spinnerDialogHours.setShowKeyboard(false);// for open keyboard by default
        spinnerDialogHours.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                hours_tv.setText(item);
                minutes_tv.setText("00");
            }
        });

        spinnerDialogMinutes = new SpinnerDialog(activity, (ArrayList<String>) minutesStringList, getString(R.string.select_minute));
        spinnerDialogMinutes.setCancellable(true); // for cancellable
        spinnerDialogMinutes.setShowKeyboard(false);// for open keyboard by default
        spinnerDialogMinutes.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                minutes_tv.setText(item);
            }
        });

        if (sparePartModel != null) {
            hours_tv.setText(sparePartModel.getHour());
            minutes_tv.setText(sparePartModel.getMinute());
        }

        hours_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerDialogHours.showSpinerDialog();
            }
        });

        minutes_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerDialogMinutes.showSpinerDialog();
            }
        });

        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        update_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = false;
                String
                        hours = hours_tv.getText().toString().trim(),
                        minutes = minutes_tv.getText().toString().trim();

                if (hours.isEmpty()) {
                    globalFunctions.displayMessaage(context, mainView, getString(R.string.select_time));
                } else {
                    isValid = true;
                    if (sparePartModel != null) {
                        //update time...
                        FixingTimePostModel fixingTimePostModel = new FixingTimePostModel();
                        fixingTimePostModel.setSparePartId(sparePartModel.getId());
                        fixingTimePostModel.setSparePartTime(hours + ":" + minutes + ":" + "00");
                        updateFixingTime(context, fixingTimePostModel);
                    }
                }
                if (isValid) {
                    dialog.dismiss();
                }
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void updateFixingTime(final Context context, final FixingTimePostModel model) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.updateFixingTime(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validatFixingTimeOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "UpdateFixingTime");
    }

    private void validatFixingTimeOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            if (statusModel.isStatus()) {
                //refresh the page...
                orderId = "";
                getProfile(orderId);
                // getOrderDetails(context, orderId);
            }
        }
    }

    @Override
    public void OnLocationServiceInvoke(int position, boolean isStarted) {
        if (isStarted && !mAlreadyStartedService) {
            startStep1();
        } else {
            //Intent intent = new Intent(activity, LocationMonitoringService.class);
            activity.stopService(locationintent);
            // stopService(new Intent(SummaryActivity.this, LocationMonitoringService.class));
            mAlreadyStartedService = false;
        }
    }

    private void startStep1() {
        //Check whether this user has installed Google play service which is being used by Location updates.
        if (isGooglePlayServicesAvailable()) {
            //Passing null to indicate that it is executing for the first time.
            startStep2(null);
        } else {
            Toast.makeText(activity, R.string.no_google_playservice_available, Toast.LENGTH_LONG).show();
        }
    }

    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(context);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    private Boolean startStep2(DialogInterface dialog) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            promptInternetConnect();
            return false;
        }

        if (dialog != null) {
            dialog.dismiss();
        }

        //Yes there is active internet connection. Next check Location is granted by user or not.

        if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.
            startStep3();
        } else {  //No user has not granted the permissions yet. Request now.
            requestPermissions();
        }
        return true;
    }

    private void requestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.ACCESS_COARSE_LOCATION);


        // Provide an additional rationale to the img_user. This would happen if the img_user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale || shouldProvideRationale2) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                activity.findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    private void startStep3() {
        //And it will be keep running until you close the entire application from task manager.
        //This method will executed only once.

        if (!mAlreadyStartedService) {
            //Start location sharing service to app server.........
            // Intent intent = new Intent(activity, LocationMonitoringService.class);
            GlobalFunctions.setSharedPreferenceString(context, "extra_order_Id", orderId);
            activity.startService(locationintent);

            mAlreadyStartedService = true;
            //Ends................................................
        }
    }

    private void promptInternetConnect() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setTitle(R.string.no_internet);
        builder.setMessage(R.string.no_internet);

        String positiveText = getString(R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Block the Application Execution until user grants the permissions
                        if (startStep2(dialog)) {

                            //Now make sure about location permission.
                            if (checkPermissions()) {

                                //Step 2: Start the Location Monitor Service
                                //Everything is there to start the service.
                                startStep3();
                            } else if (!checkPermissions()) {
                                requestPermissions();
                            }
                        }
                    }
                });

        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void stopLocationService() {
        if (activity != null) {
            activity.stopService(locationintent);
        }
    }

    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionState2 = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;
    }
}


