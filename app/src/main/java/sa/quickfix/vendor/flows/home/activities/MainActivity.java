package sa.quickfix.vendor.flows.home.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;

import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.SplashScreen;
import sa.quickfix.vendor.flows.bank_details.activities.BankDetailsActivity;
import sa.quickfix.vendor.flows.completed_requests.activities.CompletedRequestsActivity;
import sa.quickfix.vendor.flows.contact_us.activities.ContactUsActivity;
import sa.quickfix.vendor.flows.my_ratings.activities.MyRatingsActivity;
import sa.quickfix.vendor.flows.my_services.activities.MyServicesActivity;
import sa.quickfix.vendor.flows.notification.activities.NotificationActivity;
import sa.quickfix.vendor.flows.profile.activities.ProfileActivity;
import sa.quickfix.vendor.flows.schedule_date.ScheduleDateActivity;
import sa.quickfix.vendor.flows.wallet.activities.MyWalletActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.LocationUpdateModel;
import sa.quickfix.vendor.services.model.NotificationMainModel;
import sa.quickfix.vendor.services.model.NotificationModel;
import sa.quickfix.vendor.services.model.OrderDetailMainModel;
import sa.quickfix.vendor.services.model.OrderListMainModel;
import sa.quickfix.vendor.services.model.OrderModel;
import sa.quickfix.vendor.services.model.OrderPostModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.PushNotificationModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.services.model.UpdateLanguageModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.flows.home.fragments.ActiveRequestFragment;
import sa.quickfix.vendor.flows.home.fragments.NewRequestListFragment;
import sa.quickfix.vendor.flows.home.fragments.UpcomingRequestListFragment;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.view.SeparateProgress;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, LocationListener {

    public static String TAG = "MainActivity";

    public static final String BUNDLE_MAIN_NOTIFICATION_MODEL = "BundleMainModelNotificationModel";
    public static final String BUNDLE_KEY_TO_PAGE = "BundleKeyToPage";

    public static Context mainContext = null;
    static Activity activity = null;
    Window mainWindow = null;
    FragmentManager mainActivityFM = null;

    GlobalFunctions globalFunctions;
    GlobalVariables globalVariables;

    private LinearLayout new_ll, active_ll, upcoming_ll;
    private static AppCompatTextView new_tv, active_tv, upcoming_tv;
    private View new_line_view, active_line_view, upcoming_line_view;

    View mainView;

    NavigationView navigationView;
    View navigationHeaderView;

    private LayoutInflater layoutInflater;

    static Toolbar toolbar;
    static ActionBar actionBar;
    static String mTitle;
    static int mResourceID, titleResourseID;
    static Menu menu;
    static TextView toolbar_title;
    static ImageView toolbar_logo;

    private NotificationModel notificationModel = null;
    String toPage = null;

    public static RelativeLayout user_profile_layout;

    static TextView arabic_language_tv, english_language_tv;

    static View arabic_language_iv, english_language_iv;
    int gravity = 0;

    public static RelativeLayout profile_photo_layout, notificationLayout;

    LocationManager locationManager;
    String provider;

    OrderPostModel orderPostModel = null;
    NotificationMainModel notificationMainModel = null;

    public static Intent newInstance(Context mainContext, NotificationModel notificationModel) {
        Intent intent = new Intent(mainContext, MainActivity.class);
        intent.putExtra(BUNDLE_MAIN_NOTIFICATION_MODEL, notificationModel);
        return intent;
    }

    public static Intent newInstance(Context context, String toPage) {
        Intent intent = new Intent(context, MainActivity.class);
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_TO_PAGE, toPage);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;
        mainContext = this;
        mainWindow = getWindow();
        mainActivityFM = getSupportFragmentManager();

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        this.layoutInflater = activity.getLayoutInflater();

        Drawable navIconDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu, getTheme());

        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        //toolbar.setPadding(0, globalFunctions.getStatusBarHeight(this), 0, 0);
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setNavigationIcon(navIconDrawable);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_logo = (ImageView) toolbar.findViewById(R.id.tool_bar_logo);

        // filter_iv = (ImageView) findViewById(R.id.filter_iv);
        new_ll = (LinearLayout) findViewById(R.id.new_ll);
        active_ll = (LinearLayout) findViewById(R.id.active_ll);
        upcoming_ll = (LinearLayout) findViewById(R.id.upcoming_ll);

        new_tv = (AppCompatTextView) findViewById(R.id.new_tv);
        active_tv = (AppCompatTextView) findViewById(R.id.active_tv);
        upcoming_tv = (AppCompatTextView) findViewById(R.id.upcoming_tv);

        new_line_view = (View) findViewById(R.id.new_line_view);
        active_line_view = (View) findViewById(R.id.active_line_view);
        upcoming_line_view = (View) findViewById(R.id.upcoming_line_view);

        mainView = toolbar;

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(navIconDrawable);
        setOptionsMenuVisiblity(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        if (FirebaseInstanceId.getInstance().getToken() != null) {
            PushNotificationModel pushNotificationModel = new PushNotificationModel();
            pushNotificationModel.setRegistration_id(FirebaseInstanceId.getInstance().getToken());
            sendPushNotificationID(mainContext, pushNotificationModel);
        }

//        GPSTracker gpsTracker = new GPSTracker(this);

        /*if (gpsTracker.getIsGPSTrackingEnabled()) {
            LocationUpdateModel locationUpdateModel = new LocationUpdateModel();
            locationUpdateModel.setLatitude(gpsTracker.getLatitude() + "");
            locationUpdateModel.setLongitude(gpsTracker.getLongitude() + "");

            updateLocation(locationUpdateModel);
        }*/


        if (getIntent().hasExtra(BUNDLE_MAIN_NOTIFICATION_MODEL)) {
            notificationModel = (NotificationModel) getIntent().getSerializableExtra(BUNDLE_MAIN_NOTIFICATION_MODEL);
        } else {
            notificationModel = null;
        }

        if (getIntent().hasExtra(BUNDLE_KEY_TO_PAGE)) {
            toPage = (String) getIntent().getStringExtra(BUNDLE_KEY_TO_PAGE);
        } else {
            toPage = null;
        }

        // user_profile_iv = (CircleImageView) findViewById(R.id.user_profile_iv);
        arabic_language_iv = (View) findViewById(R.id.arabic_language_iv);
        english_language_iv = (View) findViewById(R.id.english_language_iv);

        english_language_tv = (TextView) findViewById(R.id.english_language_tv);
        arabic_language_tv = (TextView) findViewById(R.id.arabic_language_tv);

        gravity = globalFunctions.getLanguage(mainContext) == GlobalVariables.LANGUAGE.ARABIC ? GravityCompat.START : GravityCompat.START;

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(navIconDrawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(gravity)) {
                    drawer.closeDrawer(gravity);
                } else {
                    drawer.openDrawer(gravity);
                }
            }
        });
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationHeaderView = navigationView.getHeaderView(0);

        //setProfileImage();

        // openNewRequestDialog(mainContext);

        if (toPage != null) {
            if (toPage.equalsIgnoreCase(globalVariables.TO_PAGE_ACTIVE_REQUESTS) || toPage.equalsIgnoreCase(globalVariables.GO_TO_ACTIVE_TAB)) {
                Fragment activeRequestFragment = new ActiveRequestFragment();
                replaceFragment(activeRequestFragment, ActiveRequestFragment.TAG, "", 0, 0);
            } else if (toPage.equalsIgnoreCase(globalVariables.GO_TO_CHAT)) {
                Bundle bundle = new Bundle();
                bundle.putString("BundleKeyFromPage", globalVariables.GO_TO_CHAT);
                Fragment activeRequestFragment = new ActiveRequestFragment();
                activeRequestFragment.setArguments(bundle);
                replaceFragment(activeRequestFragment, ActiveRequestFragment.TAG, "", 0, 0);
            } else if (toPage.equalsIgnoreCase(globalVariables.GO_TO_NEW_TAB)) {
                Fragment newRequestListFragment = new NewRequestListFragment();
                replaceFragment(newRequestListFragment, NewRequestListFragment.TAG, "", 0, 0);
            } else if (toPage.equalsIgnoreCase(globalVariables.GO_TO_UPCOMING_TAB)) {
                Fragment upcomingRequestListFragment = new UpcomingRequestListFragment();
                replaceFragment(upcomingRequestListFragment, UpcomingRequestListFragment.TAG, "", 0, 0);
            } else {
                Fragment newRequestListFragment = new NewRequestListFragment();
                replaceFragment(newRequestListFragment, NewRequestListFragment.TAG, "", 0, 0);
            }
        } else {
          /*  Fragment newRequestListFragment = new NewRequestListFragment();
            replaceFragment(newRequestListFragment, NewRequestListFragment.TAG, "", 0, 0);*/
        }

        if (notificationModel != null) {
            if (notificationModel.getPushId() != null) {
                //clear this notification...
                clearNotification(mainContext, notificationModel.getPushId());
            }
            goToRedirectScreen(notificationModel);
        }

        if (toPage == null && notificationModel == null) {
            Fragment newRequestListFragment = new NewRequestListFragment();
            replaceFragment(newRequestListFragment, NewRequestListFragment.TAG, "", 0, 0);
        }

        notificationModel = null;
        toPage = null;

        new_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.ColorAccent));
        active_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.transparent));
        upcoming_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.transparent));

        new_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnClickEvent(GlobalVariables.TYPE_NEW);
            }
        });

        active_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnClickEvent(GlobalVariables.TYPE_ACTIVE);
            }
        });

        upcoming_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnClickEvent(GlobalVariables.TYPE_UPCOMING);
            }
        });

        mainActivityFM.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {

            @Override
            public void onBackStackChanged() {
                if (mainActivityFM != null) {
                    Fragment currentFragment = mainActivityFM.findFragmentById(R.id.container);
                    if (currentFragment != null) {
                        currentFragment.onResume();
                    }
                }
            }
        });

        // registerBroadCast();


        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    0);
        }
        // Getting LocationManager object
        statusCheck();

        locationManager = (LocationManager) getSystemService(
                Context.LOCATION_SERVICE);

        // Creating an empty criteria object
        Criteria criteria = new Criteria();

        // Getting the name of the provider that meets the criteria
        provider = locationManager.getBestProvider(criteria, false);


        if (provider != null && !provider.equals("")) {
            if (!provider.contains("gps")) { // if gps is disabled
                final Intent poke = new Intent();
                poke.setClassName("com.android.settings",
                        "com.android.settings.widget.SettingsAppWidgetProvider");
                poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
                poke.setData(Uri.parse("3"));
                sendBroadcast(poke);
            }
            // Get the location from the given provider
            Location location = locationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 500, 0, (android.location.LocationListener) this);

            if (location != null)
                onLocationChanged(location);
            else
                location = locationManager.getLastKnownLocation(provider);
            if (location != null)
                onLocationChanged(location);
            else
                Toast.makeText(getBaseContext(), "Location can't be retrieved",
                        Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(getBaseContext(), "No Provider Found",
                    Toast.LENGTH_SHORT).show();
        }

    }

    private void clearNotification(final Context context, String id) {
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.clearNotification(context, id, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                if (context != null) {
                    Log.d(TAG, "Response: " + arg0.toString());
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
            }

            @Override
            public void OnError(String msg) {
            }
        }, "ClearNotification");
    }

    private void loadNotificationList() {
        if (orderPostModel == null) {
            orderPostModel = new OrderPostModel();
        }
        orderPostModel.setIndex("0");
        orderPostModel.setSize("200");
        getNotificationList(mainContext);
    }

    private void getNotificationList(final Context context) {
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getNotificationList(context, orderPostModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                if (context != null) {
                    NotificationMainModel model = (NotificationMainModel) arg0;
                    setUpNotificationList(model);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
            }

            @Override
            public void OnError(String msg) {
            }
        }, "NotificationList");
    }

    private void setUpNotificationList(NotificationMainModel mNotificationMainModel) {
        if (mNotificationMainModel != null) {
            setMyNotificationCount(mNotificationMainModel);
            if (notificationMainModel == null) {
                notificationMainModel = new NotificationMainModel();
            }
            notificationMainModel = mNotificationMainModel;
        }
    }

    private void setMyNotificationCount(NotificationMainModel notificationMainModel) {
        if (notificationMainModel != null) {
            int notificationCount = 0;
            if (globalFunctions.isNotNullValue(notificationMainModel.getCount())) {
                try {
                    notificationCount = Integer.parseInt(notificationMainModel.getCount());
                } catch (Exception exc) {
                    notificationCount = 0;
                }
            }
            globalFunctions.setMyNotificationCount(mainContext, notificationCount);
        }
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(
                Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setMessage(
                activity.getString(R.string.enable_gps_text))
                .setCancelable(false).setPositiveButton(activity.getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,
                                        final int id) {
                        startActivity(new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,
                                        final int id) {
                        dialog.cancel();
                    }
                });
        final androidx.appcompat.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void goToRedirectScreen(NotificationModel mNotificationModel) {
        if (mNotificationModel != null) {
            if (mNotificationModel.getType() != null) {
                if (mNotificationModel.getType().equalsIgnoreCase(GlobalVariables.NEW_REQUEST_STATUS)) {
                    setOnClickEvent(GlobalVariables.TYPE_NEW);
                } else if (mNotificationModel.getType().equalsIgnoreCase(globalVariables.CHAT_STATUS)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("BundleKeyFromPage", globalVariables.GO_TO_CHAT);
                    Fragment activeRequestFragment = new ActiveRequestFragment();
                    activeRequestFragment.setArguments(bundle);
                    replaceFragment(activeRequestFragment, ActiveRequestFragment.TAG, "", 0, 0);
                } else if (mNotificationModel.getType().equalsIgnoreCase(GlobalVariables.ORDER_CONFIRMED_BY_TECHNICIAN)) {
                    setOnClickEvent(GlobalVariables.TYPE_UPCOMING);
                } else if (mNotificationModel.getType().equalsIgnoreCase(GlobalVariables.ORDER_ACTIVE_STATUS) ||
                        mNotificationModel.getType().equalsIgnoreCase(GlobalVariables.ORDER_ACTIVE_PART_STATUS)) {
                    setOnClickEvent(GlobalVariables.TYPE_ACTIVE);
                } else if (mNotificationModel.getType().equalsIgnoreCase(GlobalVariables.ORDER_HISTORY_CANCEL_STATUS) || mNotificationModel.getType().equalsIgnoreCase(GlobalVariables.ORDER_HISTORY_STATUS)
                        || mNotificationModel.getType().equalsIgnoreCase(GlobalVariables.ORDER_HISTORY_COMPLETED_STATUS) || mNotificationModel.getType().equalsIgnoreCase(GlobalVariables.CUSTOMER_NOT_RESPONDED)) {
                    Intent intent = new Intent(mainContext, CompletedRequestsActivity.class);
                    startActivity(intent);
                    closeThisActivity();
                } else if (mNotificationModel.getType().equalsIgnoreCase(GlobalVariables.WALLET_STATUS)) {
                    Intent intent = new Intent(mainContext, MyWalletActivity.class);
                    startActivity(intent);
                }/* else if (mNotificationModel.getType().equalsIgnoreCase(GlobalVariables.BANK_STATUS)) {
                Intent intent = new Intent(mainContext, BankDetailsActivity.class);
                startActivity(intent);
            }*/
                /*notificationModel = null;*/
            }
        }
    }

    private void updateLocation(LocationUpdateModel locationUpdateModel) {
        //Riyad location..
//        locationUpdateModel.setLatitude("24.7136");
//        locationUpdateModel.setLongitude("46.6753");
        updateMyLocation(mainContext, locationUpdateModel);
    }

    private void updateMyLocation(final Context context, final LocationUpdateModel model) {
        //  globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.updateLocation(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                //   globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateLocationUpdateOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                // globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                // globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "UpdateMyLocation");
    }

    private void validateLocationUpdateOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            //globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
        }
    }


    public void setSelectionTabIcons(String tabName) {

        new_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.transparent));
        active_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.transparent));
        upcoming_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.transparent));

        if (tabName.equalsIgnoreCase(mainContext.getString(R.string.new_title))) {
            new_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.ColorAccent));
        } else if (tabName.equalsIgnoreCase(mainContext.getString(R.string.active))) {
            active_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.ColorAccent));
        } else if (tabName.equalsIgnoreCase(mainContext.getString(R.string.upcoming))) {
            upcoming_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.ColorAccent));
        }
    }

    private void setOnClickEvent(int type) {

        new_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.transparent));
        active_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.transparent));
        upcoming_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.transparent));

        if (type == GlobalVariables.TYPE_NEW) {
            new_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.ColorAccent));
            Fragment newRequestListFragment = new NewRequestListFragment();
            replaceFragment(newRequestListFragment, NewRequestListFragment.TAG, "", 0, 0);
        } else if (type == GlobalVariables.TYPE_ACTIVE) {
            active_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.ColorAccent));
            Fragment activeRequestFragment = new ActiveRequestFragment();
            replaceFragment(activeRequestFragment, ActiveRequestFragment.TAG, "", 0, 0);
        } else if (type == GlobalVariables.TYPE_UPCOMING) {
            upcoming_line_view.setBackgroundColor(globalFunctions.getColor(mainContext, R.color.ColorAccent));
            Fragment upcomingRequestListFragment = new UpcomingRequestListFragment();
            replaceFragment(upcomingRequestListFragment, UpcomingRequestListFragment.TAG, "", 0, 0);
        }
    }

    private void sendPushNotificationID(final Context mainContext, PushNotificationModel pushNotificationModel) {
        if (globalFunctions.getSharedPreferenceString(mainContext, globalVariables.SHARED_PREFERENCE_TOKEN) != null) {
            ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
            servicesMethodsManager.sendPushNotificationID(mainContext, pushNotificationModel, new ServerResponseInterface() {
                @Override
                public void OnSuccessFromServer(Object arg0) {
                    Log.d(TAG, "Response : " + arg0.toString());
                }

                @Override
                public void OnFailureFromServer(String msg) {
                    Log.d(TAG, "Failure : " + msg);
                }

                @Override
                public void OnError(String msg) {
                    Log.d(TAG, "Error : " + msg);
                }
            }, "Send_Push_Notification_ID");
        }
    }

    public void replaceFragment(@NonNull Fragment fragment, @NonNull String tag, @NonNull String title, int titleImageID, @NonNull int bgResID) {
        if (fragment != null && mainActivityFM != null) {
            Fragment tempFrag = mainActivityFM.findFragmentByTag(tag);
            if (tempFrag != null) {
                mainActivityFM.beginTransaction().remove(tempFrag).commitAllowingStateLoss();
            }
            setTitle(title, titleImageID, bgResID);
            FragmentTransaction ft = mainActivityFM.beginTransaction();
            //ft.setCustomAnimations(R.anim.slide_out_left, R.anim.slide_out_right);
            ft.add(R.id.container, fragment, tag).addToBackStack(tag).commitAllowingStateLoss();
            //ft.replace(R.id.container, fragment, tag).disallowAddToBackStack().commitAllowingStateLoss();
        }
    }

    public void addFragment(@NonNull Fragment fragment, @NonNull String tag, @NonNull String title, int titleImageID, @NonNull int bgResID) {
        if (fragment != null && mainActivityFM != null) {
            Fragment tempFrag = mainActivityFM.findFragmentByTag(tag);
            if (tempFrag != null) {
                fragment = tempFrag;
            }
            setTitle(title, titleImageID, bgResID);
            FragmentTransaction ft = mainActivityFM.beginTransaction();
            //ft.setCustomAnimations(R.anim.slide_out_left, R.anim.slide_out_right);
            ft.replace(R.id.container, fragment).addToBackStack(tag).commitAllowingStateLoss();
        }
    }

    public static void setNewRequestCount(int count) {
        if (count > 0) {
            new_tv.setText(activity.getString(R.string.new_title) + "(" + count + ")");
        } else {
            new_tv.setText(activity.getString(R.string.new_title));
        }
    }

    public static void setActiveRequestCount(int count) {
        if (count > 0) {
            active_tv.setText(activity.getString(R.string.active) + "(" + count + ")");
        } else {
            active_tv.setText(activity.getString(R.string.active));
        }
    }

    public static void setUpcomingRequestCount(int count) {
        if (count > 0) {
            upcoming_tv.setText(activity.getString(R.string.upcoming) + "(" + count + ")");
        } else {
            upcoming_tv.setText(activity.getString(R.string.upcoming));
        }
    }

    public static void setTitle(String title, int titleImageID, int backgroundResourceID) {
        mTitle = title;
        if (backgroundResourceID != 0) {
            mResourceID = backgroundResourceID;
        } else {
            mResourceID = 0;
        }
        if (titleImageID != 0) {
            titleResourseID = titleImageID;
        } else {
            titleResourseID = 0;
        }
        restoreToolbar();
    }

    private static void restoreToolbar() {
        //toolbar = (Toolbar) findViewById(R.id.tool_bar);
        Log.d(TAG, "Restore Tool Bar");
        if (actionBar != null) {
            Log.d(TAG, "Restore Action Bar not Null");
            Log.d(TAG, "Title : " + mTitle);
            if (titleResourseID != 0) {
                toolbar_logo.setVisibility(View.VISIBLE);
                toolbar_title.setVisibility(View.GONE);
                toolbar_logo.setImageResource(titleResourseID);
            } else {
                toolbar_logo.setVisibility(View.GONE);
                toolbar_title.setVisibility(View.VISIBLE);
                toolbar_title.setText(mTitle);
            }
            if (mResourceID != 0) toolbar.setBackgroundResource(mResourceID);
            //actionBar.setTitle("");
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    public static int getTitleResourseID() {
        return titleResourseID;
    }

    public static void setTitleResourseID(int titleResourseID) {
        MainActivity.titleResourseID = titleResourseID;
        restoreToolbar();
    }

    public static void setProfileImage() {
        if (profile_photo_layout != null) {
            final CircleImageView profile_iv = (CircleImageView) profile_photo_layout.findViewById(R.id.user_profile_iv);
            ProfileModel profileModel = GlobalFunctions.getProfile(mainContext);
            if (profileModel != null) {
                if (GlobalFunctions.isNotNullValue(profileModel.getProfileImage())) {
                    SeparateProgress.loadImage(profile_iv, profileModel.getProfileImage(), SeparateProgress.setProgress(activity));
                }
            }
        }
    }

    private void openNewRequestDialog(final Context mainContext) {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.new_request_popup, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        TextView accept_tv, reject_tv;

        accept_tv = (TextView) alertView.findViewById(R.id.accept_tv);
        reject_tv = (TextView) alertView.findViewById(R.id.reject_tv);

        accept_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
             /*   Intent intent = PaymentMethodActivity.newInstance(activity, "3");
                startActivity(intent);*/
            }
        });

        reject_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        setNavigationHeaders();
        setActiveAndUpcomingCount();
//        loadNotificationList();
        getProfile();
        super.onResume();
    }

    private void setActiveAndUpcomingCount() {
        //upcoming...
        OrderPostModel upcomingOrderPostModel = new OrderPostModel();
        upcomingOrderPostModel.setIndex("0");
        upcomingOrderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
        upcomingOrderPostModel.setStatus(globalVariables.UPCOMING_REQUEST);
        getUpcomingRequestList(mainContext, upcomingOrderPostModel);

        //active...
        getOrderDetails(mainContext, "");
    }

    private void getOrderDetails(final Context context, String orderId) {
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getOrderDetails(context, orderId, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                setActiveCount(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
            }
        }, "GetOrderDetails");
    }

    private void setActiveCount(Object model) {
        if (model instanceof StatusModel) {
            MainActivity.setActiveRequestCount(0);
        } else {
            OrderDetailMainModel orderDetailMainModel = (OrderDetailMainModel) model;
            MainActivity.setActiveRequestCount(1);
        }
    }

    private void getUpcomingRequestList(final Context context, OrderPostModel upcomingOrderPostModel) {
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getOrderList(context, upcomingOrderPostModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                if (context != null) {
                    OrderListMainModel model = (OrderListMainModel) arg0;
                    setUpcomingListCount(model);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
            }

            @Override
            public void OnError(String msg) {
            }
        }, "GetUpcomingRequestList");
    }

    private void setUpcomingListCount(OrderListMainModel orderListMainModel) {
        if (orderListMainModel != null) {
            if (orderListMainModel.getOrderList() != null) {
                List<OrderModel> list = new ArrayList<>();
                list.clear();
                list.addAll(orderListMainModel.getOrderList().getOrderList());
                setUpcomingRequestCount(list.size());
            }
        }
    }

    public void setOptionsMenuVisiblity(boolean showMenu) {
        if (menu == null)
            return;
        //menu.setGroupVisible(R.id.menu, showMenu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        this.menu = menu;
        setOptionsMenuVisiblity(false);
        MenuItem item = menu.findItem(R.id.action_profile_photo);
        MenuItemCompat.setActionView(item, R.layout.menu_profile_photo_layout);
        profile_photo_layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        setProfileImage();
        profile_photo_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        MenuItem notificationItem = menu.findItem(R.id.action_notification);
        MenuItemCompat.setActionView(notificationItem, R.layout.actionbar_badge_notification_layout);
        notificationLayout = (RelativeLayout) MenuItemCompat.getActionView(notificationItem);

        notificationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = NotificationActivity.newInstance(activity, notificationMainModel);
                activity.startActivity(intent);
            }
        });

        globalFunctions.setMyNotificationCount(mainContext, globalFunctions.getMyNotificationCount(mainContext));

        return true;
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        this.menu = menu;
        setOptionsMenuVisiblity(false);
        MenuItem item = menu.findItem(R.id.action_profile_photo);
        MenuItemCompat.setActionView(item, R.layout.menu_profile_photo_layout);
        profile_photo_layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        setProfileImage();
        profile_photo_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        MenuItem notificationItem = menu.findItem(R.id.action_notification);
        MenuItemCompat.setActionView(notificationItem, R.layout.actionbar_badge_notification_layout);
        notificationLayout = (RelativeLayout) MenuItemCompat.getActionView(notificationItem);

        notificationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = NotificationActivity.newInstance(activity, notificationMainModel);
                activity.startActivity(intent);
            }
        });

        globalFunctions.setMyNotificationCount(mainContext, globalFunctions.getMyNotificationCount(mainContext));

        return true;
    }

    public static void setNotificationCount(final int count) {
        if (notificationLayout != null) {
            final TextView notification_count_tv = (TextView) notificationLayout.findViewById(R.id.actionbar_badge_notification_textview);
            if (activity != null) {
                if (count == 0) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            notification_count_tv.setVisibility(View.GONE);
                        }
                    });
                } else {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            notification_count_tv.setVisibility(View.VISIBLE);
                            notification_count_tv.setText(count + "");
                        }
                    });
                }
            }
        }
    }

    public void setNavigationHeaders() {
        if (navigationHeaderView != null && mainContext != null) {
            TextView
                    header_name_tv = (TextView) navigationHeaderView.findViewById(R.id.name_tv);

            RelativeLayout
                    profile_rl = (RelativeLayout) navigationHeaderView.findViewById(R.id.profile_rl);

            CircleImageView
                    header_profile_iv = (CircleImageView) navigationHeaderView.findViewById(R.id.profile_iv);

            RatingBar ratingBar = (RatingBar) navigationHeaderView.findViewById(R.id.ratingBar);
            final SwitchCompat available_switch = (SwitchCompat) navigationHeaderView.findViewById(R.id.swOnOff);

            ProfileModel profileModel = globalFunctions.getProfile(mainContext);
            if (profileModel != null && mainContext != null) {
                try {
                    String
                            firstName = null,
                            lastName = null,
                            fullName = null;

                    firstName = profileModel.getFirstName();
                    lastName = profileModel.getLastName();
                    fullName = firstName + " " + lastName;
                    header_name_tv.setText(fullName != null ? fullName : getString(R.string.guest));

                    if (GlobalFunctions.isNotNullValue(profileModel.getProfileImage())) {
                        SeparateProgress.loadImage(header_profile_iv, profileModel.getProfileImage(), SeparateProgress.setProgress(activity));
                    }

                    if (profileModel.getRating() != null) {
                        float value;
                        value = Float.parseFloat(profileModel.getRating());
                        ratingBar.setVisibility(View.VISIBLE);
                        ratingBar.setRating(value);
                    } else {
                        ratingBar.setVisibility(View.GONE);
                    }

                    if (GlobalFunctions.isNotNullValue(profileModel.getIs_available()) && profileModel.getIs_available().equalsIgnoreCase(GlobalVariables.DRIVER_AVAILABILITY_ON_STATUS)) {
                        available_switch.setChecked(true);
                    } else {
                        available_switch.setChecked(false);
                    }

                } catch (Exception exxx) {
                    Log.e(TAG, exxx.getMessage());
                }

            } else {
                if (mainContext != null) {
                    try {
                        header_name_tv.setText(mainContext.getString(R.string.guest));
                    } catch (Exception exxx) {
                        Log.e(TAG, exxx.getMessage());
                    }
                }
            }

            final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

            profile_rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mainContext, ProfileActivity.class);
                    startActivity(intent);
                    drawer.closeDrawer(gravity);
                }
            });

            if (globalFunctions.getLanguage(mainContext) == GlobalVariables.LANGUAGE.ENGLISH) {
                english_language_iv.setVisibility(View.VISIBLE);
                arabic_language_iv.setVisibility(View.GONE);
            } else {
                english_language_iv.setVisibility(View.GONE);
                arabic_language_iv.setVisibility(View.VISIBLE);
            }

            available_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (available_switch.isChecked()) {
                        setAvailable(mainContext, GlobalVariables.DRIVER_AVAILABILITY_ON_STATUS);
                    } else {
                        setAvailable(mainContext, GlobalVariables.DRIVER_AVAILABILITY_OFF_STATUS);
                    }
                }
            });

            arabic_language_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (globalFunctions.getLanguage(mainContext) == GlobalVariables.LANGUAGE.ENGLISH) {
                        //LanguageChange(mainContext);
                        RestartEntireApp(mainContext, true);
                    } else {
                        ///nothing...
                    }
                    drawer.closeDrawer(gravity);
                }
            });

            english_language_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (globalFunctions.getLanguage(mainContext) == GlobalVariables.LANGUAGE.ENGLISH) {
                        //nothing
                    } else {
                        // LanguageChange(mainContext);
                        RestartEntireApp(mainContext, true);
                    }
                    drawer.closeDrawer(gravity);
                }
            });

        }
    }

    private void setAvailable(final Context mainContext, String status) {
        //globalFunctions.showProgress(context, context.getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getIsAvailable(mainContext, status, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                // globalFunctions.hideProgress();
                validateAvailOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                Log.d(TAG, "Failure : " + msg);
                // globalFunctions.hideProgress();
                globalFunctions.displayMessaage(mainContext, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                Log.d(TAG, "Error : " + msg);
                // globalFunctions.hideProgress();
                globalFunctions.displayMessaage(mainContext, mainView, msg);
            }
        }, "GetOrderAvailStatus");
    }


    public void LanguageChange(Context context) {
        ShowPopUpLanguage(context);
    }

    public void ShowPopUpLanguage(final Context context) {
        final AlertDialog alertDialog = new AlertDialog(activity);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.app_icon);
        alertDialog.setTitle(activity.getString(R.string.app_name));
        alertDialog.setMessage(activity.getString(R.string.lang_change));
        alertDialog.setPositiveButton(activity.getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                RestartEntireApp(context, true);
            }
        });

        alertDialog.setNegativeButton(activity.getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }


    public void RestartEntireApp(Context context, boolean isLanguageChange) {
        if (isLanguageChange) {
            SharedPreferences shared_preference = PreferenceManager.getDefaultSharedPreferences(this
                    .getApplicationContext());

            String mCustomerLanguage = shared_preference.getString(
                    globalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE, "null");
            String mCurrentlanguage;
            if ((mCustomerLanguage.equalsIgnoreCase("en"))) {
                globalFunctions.setLanguage(context, GlobalVariables.LANGUAGE.ARABIC);

                mCurrentlanguage = "ar";
            } else {
                mCurrentlanguage = "en";
                globalFunctions.setLanguage(context, GlobalVariables.LANGUAGE.ENGLISH);

            }
            SharedPreferences.Editor editor = shared_preference.edit();
            editor.putString(globalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE, mCurrentlanguage);
            editor.commit();
        }
        globalFunctions.closeAllActivities();
        Intent i = new Intent(this, SplashScreen.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        System.exit(0);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        }
        return false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(gravity)) {
            drawer.closeDrawers();
        }

        /*Exit Alert Box*/
        final AlertDialog alertDialog = new AlertDialog(mainContext);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getResources().getString(R.string.appExitText));
        alertDialog.setPositiveButton(getString(R.string.yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                // stopLocationService();
                // globalFunctions.closeAllActivities();
                finishAffinity();
                System.exit(0);
            }
        });

        alertDialog.setNegativeButton(getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }

    public void stimulateOnResumeFunction() {
        mainActivityFM.findFragmentById(R.id.container).onResume();
    }

    public void releseFragments() {
        for (int i = 0; i < mainActivityFM.getBackStackEntryCount(); ++i) {
            mainActivityFM.popBackStack();
        }
    }

    @Override
    protected void onDestroy() {
        //deRegisterBroadcast();
        super.onDestroy();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            Fragment newRequestListFragment = new NewRequestListFragment();
            replaceFragment(newRequestListFragment, NewRequestListFragment.TAG, "", 0, 0);
        } else if (id == R.id.nav_profile) {
            Intent intent = new Intent(mainContext, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_my_services) {
            Intent intent = new Intent(mainContext, MyServicesActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_completed_service) {
            Intent intent = new Intent(mainContext, CompletedRequestsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_bank_details) {
            Intent intent = new Intent(mainContext, BankDetailsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_wallet) {
            Intent intent = new Intent(mainContext, MyWalletActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_my_ratings) {
            Intent intent = new Intent(mainContext, MyRatingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_contact_us) {
            Intent intent = new Intent(mainContext, ContactUsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_schedule_date) {
            Intent intent = new Intent(mainContext, ScheduleDateActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_logout) {
            logout();
        } else if (id == R.id.nav_share) {
            globalFunctions.openPlayStore(activity);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(gravity);
        return false;
    }

    private void logout() {
        final AlertDialog alertDialog = new AlertDialog(activity);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(activity.getString(R.string.app_name));
        alertDialog.setMessage(activity.getResources().getString(R.string.appLogoutText));
        alertDialog.setPositiveButton(activity.getString(R.string.yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                /*Logout the Application*/
                UpdateLanguageModel updateLanguageModel = new UpdateLanguageModel();
                if (FirebaseInstanceId.getInstance().getToken() != null) {
                    updateLanguageModel.setPushToken(FirebaseInstanceId.getInstance().getToken());
                }
                logoutUser(mainContext, updateLanguageModel);
            }
        });

        alertDialog.setNegativeButton(activity.getString(R.string.no), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void logoutUser(final Context mainContext, UpdateLanguageModel updateLanguageModel) {
        globalFunctions.showProgress(activity, getString(R.string.logingout));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.logout(mainContext, updateLanguageModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(mainContext, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(mainContext, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(mainContext, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(mainContext, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "Logout_User");
    }

    private void validateOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            if (statusModel.isStatus()) {
                /*Logout success, Clear all cache and reload the home page*/

            }
            globalFunctions.closeAllActivities();
            globalFunctions.logoutApplication(mainContext, false);
            restartEntireApp(mainContext, false);
        }
    }

    private void validateAvailOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
//            globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            if (statusModel.isStatus()) {
                getProfile();
            }
        }
    }

    private void getProfile() {
//        globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getProfile(activity, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                Log.d(TAG, "Response : " + arg0.toString());
//                globalFunctions.hideProgress();
                if (arg0 instanceof ProfileModel) {
                    ProfileModel profileModel = (ProfileModel) arg0;
                    globalFunctions.setProfile(mainContext, profileModel);
                    if (GlobalFunctions.isNotNullValue(profileModel.getNotificationCount())){
                        NotificationMainModel notificationMainModel=new NotificationMainModel();
                        notificationMainModel.setCount(profileModel.getNotificationCount());
                        setMyNotificationCount(notificationMainModel);
                    }else {
                        NotificationMainModel notificationMainModel=new NotificationMainModel();
                        notificationMainModel.setCount("0");
                        setMyNotificationCount(notificationMainModel);
                    }
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
//                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(mainContext, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
//                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(mainContext, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "Get Profile");
    }

    public void restartEntireApp(Context mainContext, boolean isLanguageChange) {
        if (isLanguageChange) {
            SharedPreferences shared_preference = PreferenceManager.getDefaultSharedPreferences(this
                    .getApplicationContext());

            String mCustomerLanguage = shared_preference.getString(
                    globalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE, "null");
            String mCurrentlanguage;
            if ((mCustomerLanguage.equalsIgnoreCase("en"))) {
                globalFunctions.setLanguage(mainContext, GlobalVariables.LANGUAGE.ARABIC);

                mCurrentlanguage = "ar";
            } else {
                mCurrentlanguage = "en";
                globalFunctions.setLanguage(mainContext, GlobalVariables.LANGUAGE.ENGLISH);

            }
            SharedPreferences.Editor editor = shared_preference.edit();
            editor.putString(globalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE, mCurrentlanguage);
            editor.commit();
        }
        globalFunctions.closeAllActivities();
        Intent i = new Intent(this, SplashScreen.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        System.exit(0);
    }

    @Override
    public void onLocationChanged(Location location) {

        LocationUpdateModel locationUpdateModel = new LocationUpdateModel();
        locationUpdateModel.setLatitude(location.getLatitude() + "");
        locationUpdateModel.setLongitude(location.getLongitude() + "");
        updateLocation(locationUpdateModel);

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}

