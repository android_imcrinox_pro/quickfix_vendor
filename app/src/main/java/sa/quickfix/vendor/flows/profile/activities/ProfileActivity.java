package sa.quickfix.vendor.flows.profile.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.ProfileCertificateImageListAdapter;
import sa.quickfix.vendor.adapters.ProfileIdImageListAdapter;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.CertificatesListModel;
import sa.quickfix.vendor.services.model.CertificatesModel;
import sa.quickfix.vendor.services.model.IdImageModel;
import sa.quickfix.vendor.services.model.IdImagesListModel;
import sa.quickfix.vendor.services.model.LanguageListModel;
import sa.quickfix.vendor.services.model.LanguageModel;
import sa.quickfix.vendor.services.model.MyCityListModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;


public class ProfileActivity extends AppCompatActivity {

    public static final String TAG = "ProfileActivity";

    public static final String BUNDLE_FROM_PAGE = "BundleFromPage";

    static Activity activity;
    Context context;

    View mainView;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;
    ProfileModel profileModel = null;

    LinearLayoutManager certificateLayoutManager;
    RecyclerView certificates_recycler_view;
    ProfileCertificateImageListAdapter certficateListAdapter;

    List<CertificatesModel> certificatesImageList = new ArrayList<>();
    List<IdImageModel> idImageList = new ArrayList<>();

    LinearLayoutManager idLayoutManager;
    RecyclerView id_images_recycler_view;
    ProfileIdImageListAdapter idListAdapter;

    LanguageListModel languageListModel = null;
    MyCityListModel myCityListModel = null;
    List<LanguageModel> seletedLanguagesList = new ArrayList<>();

    private static String fromPage = null;

    private ImageView back_iv, edit_iv;
    private CircleImageView vendor_iv;
    private RatingBar ratingBar;
    private TextView vendor_name_tv, vendor_email_tv, first_name_tv, last_name_tv, gender_tv, email_id_tv, mobile_no_tv, address_tv, city_tv, national_id_tv,
            nationality_tv, language_known_tv, total_experiance_tv, verification_documents_header_tv, certificates_header_tv;

    public static Intent newInstance(Context context) {
        Intent intent = new Intent(context, ProfileActivity.class);
        return intent;
    }

    public static Intent newInstance(Context context, String fromPage) {
        Intent intent = new Intent(context, ProfileActivity.class);
        Bundle args = new Bundle();
        args.putString(BUNDLE_FROM_PAGE, fromPage);
        intent.putExtras(args);
        return intent;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        activity = this;
        context = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        certificateLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        idLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        certificates_recycler_view = (RecyclerView) findViewById(R.id.certificates_recycler_view);
        id_images_recycler_view = (RecyclerView) findViewById(R.id.id_images_recycler_view);

        back_iv = (ImageView) findViewById(R.id.back_iv);
        edit_iv = (ImageView) findViewById(R.id.edit_iv);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        vendor_iv = (CircleImageView) findViewById(R.id.vendor_iv);

        vendor_name_tv = (TextView) findViewById(R.id.vendor_name_tv);
        vendor_email_tv = (TextView) findViewById(R.id.vendor_email_tv);
        first_name_tv = (TextView) findViewById(R.id.first_name_tv);
        last_name_tv = (TextView) findViewById(R.id.last_name_tv);
        gender_tv = (TextView) findViewById(R.id.gender_tv);
        email_id_tv = (TextView) findViewById(R.id.email_id_tv);
        mobile_no_tv = (TextView) findViewById(R.id.mobile_no_tv);
        address_tv = (TextView) findViewById(R.id.address_tv);
        city_tv = (TextView) findViewById(R.id.city_tv);
        national_id_tv = (TextView) findViewById(R.id.national_id_tv);
        nationality_tv = (TextView) findViewById(R.id.nationality_tv);
        language_known_tv = (TextView) findViewById(R.id.language_known_tv);
        total_experiance_tv = (TextView) findViewById(R.id.total_experiance_tv);
        verification_documents_header_tv = (TextView) findViewById(R.id.verification_documents_header_tv);
        certificates_header_tv = (TextView) findViewById(R.id.certificates_header_tv);

        mainView = vendor_iv;

        if (getIntent().hasExtra(BUNDLE_FROM_PAGE)) {
            fromPage = (String) getIntent().getStringExtra(BUNDLE_FROM_PAGE);
        } else {
            fromPage = null;
        }

        initCertificateImageList();
        initIdImageList();

        getSpokenLanguagesList(context);
       /* getCityList(context);
        getProfile();*/

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void getCityList(final Context context) {
        //globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getCityList(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                // globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof MyCityListModel) {
                    MyCityListModel myCityListModel1 = (MyCityListModel) arg0;
                    myCityListModel = myCityListModel1;
                    getProfile();
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                //globalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                // globalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "GetCityList");
    }

    private void getSpokenLanguagesList(final Context context) {
        // globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getSpokenLanguagesList(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                //globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof LanguageListModel) {
                    LanguageListModel languageListModel1 = (LanguageListModel) arg0;
                    languageListModel = languageListModel1;
                    getCityList(context);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                // globalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                // globalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "GetSpokenLanguagesList");
    }

    private void getProfile() {
        globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getProfile(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                Log.d(TAG, "Response : " + arg0.toString());
                globalFunctions.hideProgress();
                if (arg0 instanceof ProfileModel) {
                    ProfileModel profileModel = (ProfileModel) arg0;
                    globalFunctions.setProfile(context, profileModel);
                    setThisPage();
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "Get Profile");
    }

    private void setThisPage() {

        String
                year = "",
                month = "";

        if (profileModel == null) {
            profileModel = new ProfileModel();
        }

        profileModel = globalFunctions.getProfile(context);
        if (profileModel != null) {

            if (GlobalFunctions.isNotNullValue(profileModel.getProfileImage())) {
                SeparateProgress.loadImage(vendor_iv, profileModel.getProfileImage(), SeparateProgress.setProgress(activity));
            }

            if (profileModel.getIdImageStatus() != null) {
                if (profileModel.getIdImageStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_NOT_VERIFIED)) {
                    verification_documents_header_tv.setText(activity.getString(R.string.verification_documents_not_verified));
                } else if (profileModel.getIdImageStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_BLOCKED)) {
                    verification_documents_header_tv.setText(activity.getString(R.string.verification_documents_blocked));
                } else {
                    verification_documents_header_tv.setText(activity.getString(R.string.verification_documents));
                }
            } else {
                verification_documents_header_tv.setText(activity.getString(R.string.verification_documents));
            }

            if (profileModel.getCertificateImgStatus() != null) {
                if (profileModel.getCertificateImgStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_NOT_VERIFIED)) {
                    certificates_header_tv.setText(activity.getString(R.string.certificates_not_verified));
                } else if (profileModel.getCertificateImgStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_BLOCKED)) {
                    certificates_header_tv.setText(activity.getString(R.string.certificates_blocked));
                } else {
                    certificates_header_tv.setText(activity.getString(R.string.certificates));
                }
            } else {
                certificates_header_tv.setText(activity.getString(R.string.certificates));
            }

            if (profileModel.getFirstName() != null && profileModel.getLastName() != null) {
                vendor_name_tv.setText(profileModel.getFirstName() + " " + profileModel.getLastName());
            }

            if (profileModel.getEmailId() != null) {
                email_id_tv.setText(profileModel.getEmailId());
                vendor_email_tv.setText(profileModel.getEmailId());
            }

            if (profileModel.getRating() != null) {
                float value;
                value = Float.parseFloat(profileModel.getRating());
                ratingBar.setVisibility(View.VISIBLE);
                ratingBar.setRating(value);
            } else {
                ratingBar.setVisibility(View.GONE);
            }

            if (profileModel.getFirstName() != null) {
                first_name_tv.setText(profileModel.getFirstName());
            }

            if (profileModel.getLastName() != null) {
                last_name_tv.setText(profileModel.getLastName());
            }

            if (profileModel.getGender() != null) {
                if (profileModel.getGender().equalsIgnoreCase(globalVariables.GENDER_MALE)) {
                    gender_tv.setText(getString(R.string.male));
                } else if (profileModel.getGender().equalsIgnoreCase(globalVariables.GENDER_FEMALE)) {
                    gender_tv.setText(getString(R.string.female));
                }
            }

            if (profileModel.getCountryCode() != null && profileModel.getMobileNumber() != null) {
                mobile_no_tv.setText(profileModel.getCountryCode()+profileModel.getMobileNumber());
            }

            if (profileModel.getAddress() != null) {
                address_tv.setText(profileModel.getAddress());
            }

            if (profileModel.getCityId() != null) {
                setSelectedCity(profileModel.getCityId());
            }

            if (profileModel.getIdNumber() != null) {
                national_id_tv.setText(profileModel.getIdNumber());
            }

            if (profileModel.getNationality() != null) {
                nationality_tv.setText(profileModel.getNationality());
            }

            if (profileModel.getSpokenLanguages() != null) {
                setSelectedLanguages(profileModel.getSpokenLanguages());
            }

            if (profileModel.getExperience() != null) {
                year = profileModel.getExperience();
            }

            if (profileModel.getExperienceM() != null) {
                month = profileModel.getExperienceM();
            }
            total_experiance_tv.setText(year + " " + getString(R.string.years) + " " + month + " " + getString(R.string.months));

            if (profileModel.getIdImageList() != null) {
                IdImagesListModel idImagesListModel = profileModel.getIdImageList();
                if (idImagesListModel != null) setIdImageList(idImagesListModel);
            }

            if (profileModel.getCertificatesList() != null) {
                CertificatesListModel certificatesList = profileModel.getCertificatesList();
                if (certificatesList != null) setCertificateImageList(certificatesList);
            }

            edit_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = EditProfileActivity.newInstance(context, profileModel);
                    startActivity(intent);
                }
            });


        }
    }

    private void setSelectedCity(String cityId) {
        if (cityId != null) {
            String selectedCityName = "";
            if (myCityListModel != null) {
                int listSizeMain = myCityListModel.getCityList().size();
                for (int i = 0; i < listSizeMain; i++) {
                    if (cityId.equalsIgnoreCase(myCityListModel.getCityList().get(i).getId())) {
                        selectedCityName = myCityListModel.getCityList().get(i).getTitle();
                        city_tv.setText(selectedCityName);
                        profileModel.setCityName(selectedCityName);
                        // return;
                    }
                }
            }
           /* city_tv.setText(selectedCityName);
            profileModel.setCityName(selectedCityName);*/
        }
    }

    private void setSelectedLanguages(String spokenLanguages) {
        if (spokenLanguages != null) {
            //spokenLanguages have comma separated selected languages  ids...
            ArrayList<Integer> selectedLanguageIds = new ArrayList<>();
            ArrayList<String> selectedLanguageNames = new ArrayList<>();
            selectedLanguageIds = globalFunctions.getListFromInteger(spokenLanguages);

            if (languageListModel != null) {

                int listSizeMain = languageListModel.getLanguageList().size();

                for (int i = 0; i < listSizeMain; i++) {
                    int listSizeSub = selectedLanguageIds.size();
                    if (listSizeSub > 0) {
                        for (int j = 0; j < listSizeSub; j++) {
                            if (selectedLanguageIds.get(j) == Integer.parseInt(languageListModel.getLanguageList().get(i).getId())) {
                                selectedLanguageNames.add(languageListModel.getLanguageList().get(i).getTitle());
                            }
                        }
                    }
                }
                if (selectedLanguageNames != null) {
                    language_known_tv.setText(globalFunctions.getStringFromList(selectedLanguageNames));
                    profileModel.setSpokenLanguageNames(globalFunctions.getStringFromList(selectedLanguageNames));
                }
            }
        }
    }

    private void initCertificateImageList() {
        certficateListAdapter = new ProfileCertificateImageListAdapter(activity, certificatesImageList);
        certificates_recycler_view.setLayoutManager(certificateLayoutManager);
        certificates_recycler_view.setAdapter(certficateListAdapter);
        synchronized (certficateListAdapter) {
            certficateListAdapter.notifyDataSetChanged();
        }
    }

    private void initIdImageList() {
        idListAdapter = new ProfileIdImageListAdapter(activity, idImageList);
        id_images_recycler_view.setLayoutManager(idLayoutManager);
        id_images_recycler_view.setAdapter(idListAdapter);
        synchronized (idListAdapter) {
            idListAdapter.notifyDataSetChanged();
        }
    }

    private void setIdImageList(IdImagesListModel idImagesListModel1) {
        if (idImagesListModel1 != null) {
            idImageList.clear();
            idImageList.addAll(idImagesListModel1.getIdImageList());
            initIdImageList();
        }
    }

    private void setCertificateImageList(CertificatesListModel certificateImageList1) {
        if (certificateImageList1 != null) {
            certificatesImageList.clear();
            certificatesImageList.addAll(certificateImageList1.getCertificateList());
            initCertificateImageList();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        closeThisActivity();
        // super.onBackPressed();
    }

    public static void closeThisActivity() {

        if (fromPage != null) {
            if (fromPage.equalsIgnoreCase(GlobalVariables.FROM_DOCUMENTS_REJECT_PAGE)) {
                MainActivity.closeThisActivity();
                Intent intent = new Intent(activity, MainActivity.class);
                activity.startActivity(intent);
            } else {
                if (activity != null) {
                    activity.finish();
                }
            }
        } else {
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}


