package sa.quickfix.vendor.flows.home.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.UpcomingRequestListAdapter;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.OrderListMainModel;
import sa.quickfix.vendor.services.model.OrderListModel;
import sa.quickfix.vendor.services.model.OrderModel;
import sa.quickfix.vendor.services.model.OrderPostModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.services.model.ProfileModel;

import com.vlonjatg.progressactivity.ProgressRelativeLayout;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.List;

public class UpcomingRequestListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "UpcomingListFragment";

    Activity activity;
    Context context;

    private boolean
            isIdImageVerified = false,
            isCertificateImageVerified = false;

    ProgressRelativeLayout progressActivity;
    DilatingDotsProgressBar progressBar;

    SwipeRefreshLayout swipe_container;
    RecyclerView recyclerView;
    UpcomingRequestListAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    OrderListModel detail = null;
    List<OrderModel> list = new ArrayList<>();

    OrderPostModel orderPostModel = null;

    int index = 0;
    int preLast = 0;
    boolean dataEnd = false;
    boolean loadingList = false;

    int visibleItemCount = 0, totalItemCount = 0, pastVisiblesItems = 0;

    GlobalFunctions globalFunctions = null;
    GlobalVariables globalVariables = null;
    View mainView;

    public static Bundle getBundle(Activity activity) {
        Bundle bundle = new Bundle();
        return bundle;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.upcoming_request_list_fragment, container, false);
        activity = getActivity();
        context = getActivity();

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        swipe_container = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(activity);
        progressActivity = (ProgressRelativeLayout) view.findViewById(R.id.details_progressActivity);
        progressBar = (DilatingDotsProgressBar) view.findViewById(R.id.extraProgressBar);

        mainView = recyclerView;

        if (detail == null) {
            index = 0;
            preLast = 0;
            dataEnd = false;
            detail = null;
        }

        initRecyclerView();

        swipe_container.setOnRefreshListener(this);
        swipe_container.setColorSchemeResources(R.color.fab_color_normal,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProfile();
                //refreshList();
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (!isLoadingList()) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            //Log.v("...", "Last Item Wow !");
                            final int lastItem = pastVisiblesItems + visibleItemCount;
                            if (lastItem == totalItemCount) {
                                if (preLast != lastItem) { //to avoid multiple calls for last item
                                    preLast = lastItem;
                                    orderPostModel.setIndex(String.valueOf(getIndex()));
                                    orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
                                    orderPostModel.setStatus(globalVariables.UPCOMING_REQUEST);
                                    getUpcomingRequestList(context);
                                }
                            }
                        }

                    }
                }
            }
        });

        return view;
    }

    public void initRecyclerView() {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager.onSaveInstanceState();
        Parcelable state = linearLayoutManager.onSaveInstanceState();
        adapter = new UpcomingRequestListAdapter(activity, list);
        recyclerView.setAdapter(adapter);
        linearLayoutManager.onRestoreInstanceState(state);
    }

    private void showExtraLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.showNow();
        }
    }

    private void hideExtraLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            progressBar.hideNow();
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isDataEnd() {
        return dataEnd;
    }

    public void setDataEnd(boolean dataEnd) {
        this.dataEnd = dataEnd;
    }

    public boolean isLoadingList() {
        return loadingList;
    }

    public void setLoadingList(boolean loadingList) {
        this.loadingList = loadingList;
    }

    private void showLoading() {
        if (progressActivity != null) {
            progressActivity.showLoading();
        }
    }

    private boolean isLoading() {
        if (progressActivity != null) {
            return progressActivity.isLoadingCurrentState();
        }
        return false;
    }

    private void showContent() {
        if (progressActivity != null) {
            progressActivity.showContent();
        }
    }

    private void showEmptyPage(String message) {
        if (progressActivity != null) {
            progressActivity.showEmpty(getResources().getDrawable(R.drawable.ic_logo), message,
                    "");
        }

    }

    private void showErrorPage() {
        if (progressActivity != null) {
            progressActivity.showError(getResources().getDrawable(R.drawable.app_icon), getString(R.string.noConnection),
                    getString(R.string.noConnectionErrorMessage),
                    getString(R.string.tryAgain), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getProfile();
                            // refreshList();
                        }
                    });
        }
    }

    @Override
    public void onResume() {
        ((MainActivity) activity).setSelectionTabIcons(getString(R.string.upcoming));
        getProfile();
        /*if (orderPostModel == null) {
            orderPostModel = new OrderPostModel();
        }
        orderPostModel.setIndex("0");
        orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
        orderPostModel.setStatus(globalVariables.UPCOMING_REQUEST);
        list.clear();
        getUpcomingRequestList(context);*/
        super.onResume();
    }

    private void refreshList() {
        index = 0;
        preLast = 0;
        dataEnd = false;
        detail = null;
        list.clear();
        if (orderPostModel == null) {
            orderPostModel = new OrderPostModel();
        }
        orderPostModel.setIndex("0");
        orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
        orderPostModel.setStatus(globalVariables.UPCOMING_REQUEST);
        getUpcomingRequestList(context);
    }

    private void getUpcomingRequestList(final Context context) {
        if (!isLoading()) {
            if (index == 0) {
                showLoading();
            } else {
                showExtraLoading();
            }
            //globalFunctions.showProgress(activity, getString(R.string.loading));
            ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
            servicesMethodsManager.getOrderList(context, orderPostModel, new ServerResponseInterface() {
                @Override
                public void OnSuccessFromServer(Object arg0) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        Log.d(TAG, "Response: " + arg0.toString());
                        if (index == 0) {
                            showContent();
                        } else {
                            hideExtraLoading();
                        }
                        setLoadingList(false);
                        setIndex(getIndex() + 1);
                        OrderListMainModel model = (OrderListMainModel) arg0;
                        setUpList(model);
                    }
                }

                @Override
                public void OnFailureFromServer(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        if (index == 0) {
                            showErrorPage();
                        } else {
                            hideExtraLoading();
                        }
                        Log.d(TAG, "Failure : " + msg);
                    }
                    // globalFunctions.hideProgress();
                    //globalFunctions.displayMessaage(activity, mainView, msg);
                    //Log.d(TAG, "Failure : " + msg);
                }

                @Override
                public void OnError(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        if (index == 0) {
                            showErrorPage();
                        } else {
                            hideExtraLoading();
                        }
                        Log.d(TAG, "Error : " + msg);
                    }
                    //globalFunctions.hideProgress();
                    // globalFunctions.displayMessaage(activity, mainView, msg);
                    //Log.d(TAG, "Error : " + msg);
                }
            }, "GetUpcomingRequestList");
        }
    }

    private void getProfile() {
        //globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getProfile(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                Log.d(TAG, "Response : " + arg0.toString());
                // globalFunctions.hideProgress();
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                if (arg0 instanceof ProfileModel) {
                    ProfileModel profileModel = (ProfileModel) arg0;
                    globalFunctions.setProfile(context, profileModel);
                    setThisPage();
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                //globalFunctions.hideProgress();
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                // globalFunctions.hideProgress();
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "Get Profile");
    }

    private void setThisPage() {
        ProfileModel profileModel = globalFunctions.getProfile(context);
        if (profileModel != null) {

            /*if (profileModel.getIdImageStatus() != null) {
                if (profileModel.getIdImageStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_VERIFIED)) {
                    isIdImageVerified = true;
                }
            }

            if (profileModel.getCertificateImgStatus() != null) {
                if (profileModel.getCertificateImgStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_VERIFIED)) {
                    isCertificateImageVerified = true;
                }
            }*/

            if (!GlobalFunctions.isNotNullValue(profileModel.getMessage())) {
                //verified....
                //nothing.... normal flow....
                index = 0;
                preLast = 0;
                dataEnd = false;
                detail = null;
                list.clear();
                if (orderPostModel == null) {
                    orderPostModel = new OrderPostModel();
                }
                orderPostModel.setIndex("0");
                orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
                orderPostModel.setStatus(globalVariables.UPCOMING_REQUEST);
                getUpcomingRequestList(context);
            } else {
                //show message...
                String message = "";
                message = profileModel.getMessage();
                showEmptyPage(message);
            }
        }
    }

    private void setUpList(OrderListMainModel orderListMainModel) {
        if (orderListMainModel != null) {
            if (orderListMainModel.getOrderList() != null) {
                detail = orderListMainModel.getOrderList();
                list.addAll(orderListMainModel.getOrderList().getOrderList());
                if (adapter != null) {
                    synchronized (adapter) {
                        adapter.notifyDataSetChanged();
                    }
                }
                if (list.size() <= 0) {
                    showEmptyPage(getString(R.string.emptyConfirmedJobListMessage));
                } else {
                    setUpDateWiseList();
                    initRecyclerView();
                    showContent();
                }
                MainActivity.setUpcomingRequestCount(list.size());
            }
        }
    }

    private void setUpDateWiseList() {
        ArrayList<String> updatedList = new ArrayList<String>();

        updatedList.clear();

        for (int i = 0; i < list.size(); i++) {
            if (!updatedList.contains(list.get(i).getDate())) {
                updatedList.add(list.get(i).getDate());
                list.get(i).setAddedDate(true);
            }
        }
    }

    @Override
    public void onRefresh() {
        getProfile();
        //refreshList();
    }
}


