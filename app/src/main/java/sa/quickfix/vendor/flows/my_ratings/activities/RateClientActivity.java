package sa.quickfix.vendor.flows.my_ratings.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.FeedbackPostModel;
import sa.quickfix.vendor.services.model.OrderListMainModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

import com.facebook.FacebookSdk;


public class RateClientActivity extends AppCompatActivity {

    public static final String TAG = "RateClientActivity";
    public static final String BUNDLE_ORDER_LIST_MAIN_MODEL = "BundleOrderListMainModel";

    Context context;
    private static Activity activity;
    View mainView;

    OrderListMainModel orderListMainModel = null;

    String orderId = "";

    GlobalFunctions globalFunctions;
    GlobalVariables globalVariables;

    FeedbackPostModel feedbackPostModel = null;

    private CardView submit_card_view;
    private ImageView back_iv;
    private CircleImageView user_iv;
    private TextView user_name_tv, rating_title_tv;
    private EditText feedback_etv;
    private ImageView rating_iv1, rating_iv2, rating_iv3, rating_iv4, rating_iv5;

    public static Intent newInstance(Context context, OrderListMainModel orderListMainModel) {
        Intent intent = new Intent(context, RateClientActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_ORDER_LIST_MAIN_MODEL, orderListMainModel);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.rate_client_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        user_iv = (CircleImageView) findViewById(R.id.user_iv);
        user_name_tv = (TextView) findViewById(R.id.user_name_tv);
        rating_title_tv = (TextView) findViewById(R.id.rating_title_tv);
        feedback_etv = (EditText) findViewById(R.id.feedback_etv);

        back_iv = (ImageView) findViewById(R.id.back_iv);
        rating_iv1 = (ImageView) findViewById(R.id.rating_iv1);
        rating_iv2 = (ImageView) findViewById(R.id.rating_iv2);
        rating_iv3 = (ImageView) findViewById(R.id.rating_iv3);
        rating_iv4 = (ImageView) findViewById(R.id.rating_iv4);
        rating_iv5 = (ImageView) findViewById(R.id.rating_iv5);

        mainView = back_iv;

        submit_card_view = (CardView) findViewById(R.id.submit_card_view);

        rating_iv1.setImageResource(R.drawable.ic_star_unselected_large);
        rating_iv2.setImageResource(R.drawable.ic_star_unselected_large);
        rating_iv3.setImageResource(R.drawable.ic_star_unselected_large);
        rating_iv4.setImageResource(R.drawable.ic_star_unselected_large);
        rating_iv5.setImageResource(R.drawable.ic_star_unselected_large);

        if (getIntent().hasExtra(BUNDLE_ORDER_LIST_MAIN_MODEL)) {
            orderListMainModel = (OrderListMainModel) getIntent().getSerializableExtra(BUNDLE_ORDER_LIST_MAIN_MODEL);
        } else {
            orderListMainModel = null;
        }

        if (orderListMainModel != null) {
            setThisPage(orderListMainModel);
        }

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setThisPage(OrderListMainModel orderListMainModel) {

        if (orderListMainModel != null) {

            if (orderListMainModel.getFeedback() != null) {
                orderId = orderListMainModel.getFeedback();
            }

            if (orderListMainModel.getUserName() != null) {
                user_name_tv.setText(orderListMainModel.getUserName());
            }

            if (GlobalFunctions.isNotNullValue(orderListMainModel.getUserImage())) {
                SeparateProgress.loadImage(user_iv, orderListMainModel.getUserImage(), SeparateProgress.setProgress(activity));
            }


            rating_iv1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setImageView(1);
                }

            });

            rating_iv2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setImageView(2);
                }

            });

            rating_iv3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setImageView(3);
                }

            });

            rating_iv4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setImageView(4);
                }

            });

            rating_iv5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setImageView(5);
                }

            });

            submit_card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (orderId != null) {
                        validateInput();
                    }
                }
            });

            setImageView(5);

        }
    }

    private void validateInput() {
        String
                performance = rating_title_tv.getText().toString().trim(),
                comment = feedback_etv.getText().toString().trim(),
                selected_rating_value = globalVariables.EXCELLENT;

        if (performance.isEmpty()) {
            globalFunctions.displayMessaage(context, mainView, getString(R.string.please_select_stars_to_rate));
        }/* else if (comment.isEmpty()) {
            feedback_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
            feedback_etv.setFocusableInTouchMode(true);
            feedback_etv.requestFocus();
        }*/ else {
            if (feedbackPostModel == null) {
                feedbackPostModel = new FeedbackPostModel();
            }
            if (performance.equalsIgnoreCase(context.getString(R.string.poor))) {
                selected_rating_value = globalVariables.POOR;
            } else if (performance.equalsIgnoreCase(context.getString(R.string.fair))) {
                selected_rating_value = globalVariables.FAIR;
            } else if (performance.equalsIgnoreCase(context.getString(R.string.good))) {
                selected_rating_value = globalVariables.GOOD;
            } else if (performance.equalsIgnoreCase(context.getString(R.string.very_good))) {
                selected_rating_value = globalVariables.VERY_GOOD;
            } else if (performance.equalsIgnoreCase(context.getString(R.string.excellent))) {
                selected_rating_value = globalVariables.EXCELLENT;
            }

            if (selected_rating_value != null) {
                feedbackPostModel.setRating(selected_rating_value);
            }

            if (!comment.isEmpty()) {
                feedbackPostModel.setComment(comment);
            }else {
                feedbackPostModel.setComment("");
            }

//            feedbackPostModel.setComment(comment);
            feedbackPostModel.setOrderId(orderId);
            giveFeedback(context, feedbackPostModel);
        }
    }

    private void giveFeedback(final Context context, final FeedbackPostModel model) {
        globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.feedbackPost(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Error : " + msg);
            }
        }, "FeedbackPost");
    }

    private void validateOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            if (statusModel.isStatus()) {
                showAlertMessage(statusModel.getMessage());
            } else {
                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            }
        }
    }

    private void showAlertMessage(String message) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name_sort));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(RateClientActivity.this, MainActivity.class);
                startActivity(intent);
                closeThisActivity();
            }
        });
        alertDialog.show();
    }

    private void setImageView(int position) {
        switch (position) {
            case 1:
                rating_iv1.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv2.setImageResource(R.drawable.ic_star_unselected_large);
                rating_iv3.setImageResource(R.drawable.ic_star_unselected_large);
                rating_iv4.setImageResource(R.drawable.ic_star_unselected_large);
                rating_iv5.setImageResource(R.drawable.ic_star_unselected_large);
                rating_title_tv.setText(context.getString(R.string.poor));
                break;

            case 2:
                rating_iv1.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv2.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv3.setImageResource(R.drawable.ic_star_unselected_large);
                rating_iv4.setImageResource(R.drawable.ic_star_unselected_large);
                rating_iv5.setImageResource(R.drawable.ic_star_unselected_large);
                rating_title_tv.setText(context.getString(R.string.fair));
                break;

            case 3:
                rating_iv1.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv2.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv3.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv4.setImageResource(R.drawable.ic_star_unselected_large);
                rating_iv5.setImageResource(R.drawable.ic_star_unselected_large);
                rating_title_tv.setText(context.getString(R.string.good));
                break;

            case 4:
                rating_iv1.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv2.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv3.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv4.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv5.setImageResource(R.drawable.ic_star_unselected_large);
                rating_title_tv.setText(context.getString(R.string.very_good));
                break;

            case 5:
                rating_iv1.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv2.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv3.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv4.setImageResource(R.drawable.ic_star_selected_large);
                rating_iv5.setImageResource(R.drawable.ic_star_selected_large);
                rating_title_tv.setText(context.getString(R.string.excellent));
                break;

            default:
                rating_iv1.setImageResource(R.drawable.ic_star_unselected_large);
                rating_iv2.setImageResource(R.drawable.ic_star_unselected_large);
                rating_iv3.setImageResource(R.drawable.ic_star_unselected_large);
                rating_iv4.setImageResource(R.drawable.ic_star_unselected_large);
                rating_iv5.setImageResource(R.drawable.ic_star_unselected_large);
                //rating_title_tv.setText("Very Bad");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}