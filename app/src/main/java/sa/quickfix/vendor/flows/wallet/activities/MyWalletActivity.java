package sa.quickfix.vendor.flows.wallet.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.WalletListAdapter;
import sa.quickfix.vendor.flows.payment_method.activities.PaymentMethodActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.OrderPostModel;
import sa.quickfix.vendor.services.model.WalletListModel;
import sa.quickfix.vendor.services.model.WalletMainModel;
import sa.quickfix.vendor.services.model.WalletModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.AlertDialog;

import com.facebook.FacebookSdk;
import com.vlonjatg.progressactivity.ProgressRelativeLayout;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.List;


public class MyWalletActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "MyWalletActivity";

    Context context;
    private static Activity activity;

    GlobalFunctions globalFunctions;
    GlobalVariables globalVariables;

    View mainView;

    int index = 0;
    int preLast = 0;
    boolean dataEnd = false;
    boolean loadingList = false;

    int visibleItemCount = 0, totalItemCount = 0, pastVisiblesItems = 0;

    SwipeRefreshLayout swipe_container;
    ProgressRelativeLayout progressActivity;
    DilatingDotsProgressBar progressBar;

    RecyclerView recyclerView;
    WalletListAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    WalletListModel detail = null;
    List<WalletModel> list = new ArrayList<>();

    OrderPostModel orderPostModel = null;

    private LayoutInflater layoutInflater;

    private ImageView back_iv;
    private TextView add_money_to_wallet_tv, wallet_balance_tv;
    String wallet_balance = null;
    String MIN_ADD_AMOUNT = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.my_wallet_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        this.layoutInflater = activity.getLayoutInflater();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);
        add_money_to_wallet_tv = (TextView) findViewById(R.id.add_money_to_wallet_tv);
        wallet_balance_tv = (TextView) findViewById(R.id.wallet_balance_tv);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(activity);

        swipe_container = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        progressActivity = (ProgressRelativeLayout) findViewById(R.id.details_progressActivity);
        progressBar = (DilatingDotsProgressBar) findViewById(R.id.extraProgressBar);

        mainView = recyclerView;

        if (detail == null) {
            index = 0;
            preLast = 0;
            dataEnd = false;
            detail = null;
        }

        initRecyclerView();
        refreshList();

        swipe_container.setOnRefreshListener(this);
        swipe_container.setColorSchemeResources(R.color.fab_color_normal,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (!isLoadingList()) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            //Log.v("...", "Last Item Wow !");
                            final int lastItem = pastVisiblesItems + visibleItemCount;
                            if (lastItem == totalItemCount) {
                                if (preLast != lastItem) { //to avoid multiple calls for last item
                                    preLast = lastItem;
                                    orderPostModel.setIndex(String.valueOf(getIndex()));
                                    orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
                                    getWalletHistoryList(context);
                                }
                            }
                        }

                    }
                }
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        add_money_to_wallet_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showComingSoonPopup(context);
                 openAddMoneyToWalletDialog(context);
            }
        });
    }

    private void showComingSoonPopup(Context context) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getResources().getString(R.string.coming_soon));
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void openAddMoneyToWalletDialog(final Context context) {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.add_money_to_wallet, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        ImageView close_iv;
        TextView add_money_tv;
        final EditText amount_etv;

        add_money_tv = (TextView) alertView.findViewById(R.id.add_money_tv);
        close_iv = (ImageView) alertView.findViewById(R.id.close_iv);
        amount_etv = (EditText) alertView.findViewById(R.id.amount_etv);

        amount_etv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().startsWith("0")) {
                    s.clear();
                }
            }
        });

        add_money_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = false;
                String
                        amount = amount_etv.getText().toString().trim();

                if (!GlobalFunctions.isNotNullValue(amount)) {
                    amount_etv.setError(getString(R.string.please_add_amount));
                    amount_etv.setFocusableInTouchMode(true);
                    amount_etv.requestFocus();
                } else if (amount.startsWith("0")) {
                    amount_etv.setError(activity.getString(R.string.valid_amount));
                    amount_etv.setFocusableInTouchMode(true);
                    amount_etv.requestFocus();
                } else if (!GlobalFunctions.isValidWalletAmount(MIN_ADD_AMOUNT, amount)) {
                    amount_etv.setError(activity.getString(R.string.min_amount) + " " + MIN_ADD_AMOUNT);
                    amount_etv.setFocusableInTouchMode(true);
                    amount_etv.requestFocus();
                } else {
                    isValid = true;
                    Intent intent = PaymentMethodActivity.newInstance(activity, amount, wallet_balance, "1");
                    startActivity(intent);
                    dialog.dismiss();
                }
                if (isValid) {
                    dialog.dismiss();
                }
            }
        });

        close_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    public void initRecyclerView() {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager.onSaveInstanceState();
        Parcelable state = linearLayoutManager.onSaveInstanceState();
        adapter = new WalletListAdapter(activity, list);
        recyclerView.setAdapter(adapter);
        linearLayoutManager.onRestoreInstanceState(state);
    }

    private void showExtraLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.showNow();
        }
    }

    private void hideExtraLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            progressBar.hideNow();
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isDataEnd() {
        return dataEnd;
    }

    public void setDataEnd(boolean dataEnd) {
        this.dataEnd = dataEnd;
    }

    public boolean isLoadingList() {
        return loadingList;
    }

    public void setLoadingList(boolean loadingList) {
        this.loadingList = loadingList;
    }

    private void showLoading() {
        if (progressActivity != null) {
            progressActivity.showLoading();
        }
    }

    private boolean isLoading() {
        if (progressActivity != null) {
            return progressActivity.isLoadingCurrentState();
        }
        return false;
    }

    private void showContent() {
        if (progressActivity != null) {
            progressActivity.showContent();
        }
    }

    private void showEmptyPage() {
        if (progressActivity != null) {
            progressActivity.showEmpty(getResources().getDrawable(R.drawable.ic_logo), getString(R.string.emptyWalletListMessage),
                    "");
        }

    }

    private void showErrorPage() {
        if (progressActivity != null) {
            progressActivity.showError(getResources().getDrawable(R.drawable.app_icon), getString(R.string.noConnection),
                    getString(R.string.noConnectionErrorMessage),
                    getString(R.string.tryAgain), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            refreshList();
                        }
                    });
        }
    }

    private void refreshList() {
        index = 0;
        preLast = 0;
        dataEnd = false;
        detail = null;
        list.clear();
        if (orderPostModel == null) {
            orderPostModel = new OrderPostModel();
        }
        orderPostModel.setIndex("0");
        orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
        getWalletHistoryList(context);
    }

    private void getWalletHistoryList(final Context context) {
        if (!isLoading()) {
            if (index == 0) {
                showLoading();
            } else {
                showExtraLoading();
            }
            //globalFunctions.showProgress(activity, getString(R.string.loading));
            ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
            servicesMethodsManager.getWalletHistoryList(context, orderPostModel, new ServerResponseInterface() {
                @Override
                public void OnSuccessFromServer(Object arg0) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        Log.d(TAG, "Response: " + arg0.toString());
                        if (index == 0) {
                            showContent();
                        } else {
                            hideExtraLoading();
                        }
                        setLoadingList(false);
                        setIndex(getIndex() + 1);
                        WalletMainModel model = (WalletMainModel) arg0;
                        if (model != null) {
                            setUpList(model.getWalletList());
                            setWalletBalance(model);
                        }
                    }
                }

                @Override
                public void OnFailureFromServer(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        if (index == 0) {
                            showErrorPage();
                        } else {
                            hideExtraLoading();
                        }
                        Log.d(TAG, "Failure : " + msg);
                    }
                }

                @Override
                public void OnError(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        if (index == 0) {
                            showErrorPage();
                        } else {
                            hideExtraLoading();
                        }
                        Log.d(TAG, "Error : " + msg);
                    }
                }
            }, "GetWalletHistoryList");
        }
    }

    private void setWalletBalance(WalletMainModel model) {

        if (model != null) {
            if (model.getCurrentWalletBalance() != null) {
                wallet_balance = model.getCurrentWalletBalance();
                MIN_ADD_AMOUNT = model.getMinAddAmount();
                wallet_balance_tv.setText(model.getCurrentWalletBalance());
            }
        }
    }

    private void setUpList(WalletListModel walletListModel) {
        if (walletListModel != null) {
            detail = walletListModel;
            list.addAll(walletListModel.getWalletList());
            if (adapter != null) {
                synchronized (adapter) {
                    adapter.notifyDataSetChanged();
                }
            }
            if (list.size() <= 0) {
                showEmptyPage();
            } else {
                initRecyclerView();
                showContent();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
}