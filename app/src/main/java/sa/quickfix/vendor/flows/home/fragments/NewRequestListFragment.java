package sa.quickfix.vendor.flows.home.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.NewRequestListAdapter;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnNewRequestItemClickInvoke;
import sa.quickfix.vendor.flows.my_ratings.activities.RateClientActivity;
import sa.quickfix.vendor.flows.profile.activities.ProfileActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.OrderListMainModel;
import sa.quickfix.vendor.services.model.OrderListModel;
import sa.quickfix.vendor.services.model.OrderModel;
import sa.quickfix.vendor.services.model.OrderPostModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;

import com.vlonjatg.progressactivity.ProgressRelativeLayout;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.List;

public class NewRequestListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnNewRequestItemClickInvoke {

    public static final String TAG = "NewRequestListFragment";

    private static int REQUEST_TOTAL_TIME_OUT = 60000;
    private static int REQUEST_TIME_INTERVAL = 1000;
    private static int REQUEST_COUNT_REFRESH_TIME = 15;
    private static int REQUEST_COUNT = 0;

    CountDownTimer countDownTimer;

    Activity activity;
    Context context;

    SwipeRefreshLayout swipe_container;
    ProgressRelativeLayout progressActivity;
    DilatingDotsProgressBar progressBar;

    RecyclerView recyclerView;
    NewRequestListAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    private boolean
            isIdImageVerified = false,
            isCertificateImageVerified = false;

    OrderListModel detail = null;
    List<OrderModel> list = new ArrayList<>();

    OrderPostModel orderPostModel = null;

    int index = 0;
    int preLast = 0;
    boolean dataEnd = false;
    boolean loadingList = false;

    int visibleItemCount = 0, totalItemCount = 0, pastVisiblesItems = 0;

    View mainView;

    GlobalFunctions globalFunctions = null;
    GlobalVariables globalVariables = null;

    public static Bundle getBundle(Activity activity) {
        Bundle bundle = new Bundle();
        return bundle;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.new_request_list_fragment, container, false);
        activity = getActivity();
        context = getActivity();

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        swipe_container = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(activity);
        progressActivity = (ProgressRelativeLayout) view.findViewById(R.id.details_progressActivity);
        progressBar = (DilatingDotsProgressBar) view.findViewById(R.id.extraProgressBar);

        mainView = recyclerView;

        if (detail == null) {
            index = 0;
            preLast = 0;
            dataEnd = false;
            detail = null;
        }

        initRecyclerView();

        swipe_container.setOnRefreshListener(this);
        swipe_container.setColorSchemeResources(R.color.fab_color_normal,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (!isLoadingList()) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            //Log.v("...", "Last Item Wow !");
                            final int lastItem = pastVisiblesItems + visibleItemCount;
                            if (lastItem == totalItemCount) {
                                if (preLast != lastItem) { //to avoid multiple calls for last item
                                    preLast = lastItem;
                                    orderPostModel.setIndex(String.valueOf(getIndex()));
                                    orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
                                    orderPostModel.setStatus(globalVariables.NEW_REQUEST);
                                    getNewRequestList(context);
                                }
                            }
                        }

                    }
                }
            }
        });

        //   swipe_container.add

        return view;
    }

    private void startCountDownTimer() {
        countDownTimer = new CountDownTimer(REQUEST_TOTAL_TIME_OUT, REQUEST_TIME_INTERVAL) {

            public void onTick(long millisUntilFinished) {
                if (activity != null) {
                    REQUEST_COUNT++;
                    if (REQUEST_COUNT == REQUEST_COUNT_REFRESH_TIME) {
                        REQUEST_COUNT = 0;
                        refreshList();
                    }

                }
            }

            @Override
            public void onFinish() {
//               startCountDownTimer();
            }
        };

        countDownTimer.start();
    }

    private void stopCountDownTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    private void refreshList() {
        getProfile();
     /*   index = 0;
        preLast = 0;
        dataEnd = false;
        detail = null;
        list.clear();
        if (orderPostModel == null) {
            orderPostModel = new OrderPostModel();
        }
        orderPostModel.setIndex("0");
        orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
        orderPostModel.setStatus(globalVariables.NEW_REQUEST);
        getNewRequestList(context);*/
    }

    private void getProfile() {
        //globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getProfile(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                Log.d(TAG, "Response : " + arg0.toString());
                // globalFunctions.hideProgress();
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                if (arg0 instanceof ProfileModel) {
                    ProfileModel profileModel = (ProfileModel) arg0;
                    globalFunctions.setProfile(context, profileModel);
                    setThisPage();
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                //globalFunctions.hideProgress();
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                // globalFunctions.hideProgress();
                if (swipe_container.isRefreshing()) {
                    swipe_container.setRefreshing(false);
                }
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "Get Profile");
    }

    private void setThisPage() {
        ProfileModel profileModel = globalFunctions.getProfile(context);
        if (profileModel != null) {

            if (profileModel.getMessage() != null) {
                if (globalFunctions.isNotNullValue(profileModel.getMessage())) {
                    //show message...
                    String message = "";
                    message = profileModel.getMessage();
                    // showEmptyPage(message);
                    showAlertPopup(context, message);
                } else {
                    refreshRequestList();
                }
            } else {
                refreshRequestList();
            }

            /*if (profileModel.getIdImageStatus() != null) {
                if (profileModel.getIdImageStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_VERIFIED)) {
                    isIdImageVerified = true;
                }
            }

            if (profileModel.getCertificateImgStatus() != null) {
                if (profileModel.getCertificateImgStatus().equalsIgnoreCase(globalVariables.DOCUMENTS_VERIFIED)) {
                    isCertificateImageVerified = true;
                }
            }*/

          /*  if (!GlobalFunctions.isNotNullValue(profileModel.getMessage())) {
                //verified....
                //nothing.... normal flow....
                refreshRequestList();
            } else {
                //show message...
                String message = "";
                message = profileModel.getMessage();
                // showEmptyPage(message);
                showAlertPopup(context, message);
            }*/
        }
    }

    private void refreshRequestList() {
        index = 0;
        preLast = 0;
        dataEnd = false;
        detail = null;
        list.clear();
        if (orderPostModel == null) {
            orderPostModel = new OrderPostModel();
        }
        orderPostModel.setIndex("0");
        orderPostModel.setSize(String.valueOf(globalVariables.LIST_REQUEST_SIZE));
        orderPostModel.setStatus(globalVariables.NEW_REQUEST);
        getNewRequestList(context);
    }

    private void showAlertPopup(Context context, String message) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                //Intent intent = new Intent(activity, ProfileActivity.class);
                Intent intent = ProfileActivity.newInstance(activity, globalVariables.FROM_DOCUMENTS_REJECT_PAGE);
                startActivity(intent);
            }
        });

        alertDialog.show();
    }

    public void initRecyclerView() {
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager.onSaveInstanceState();
        Parcelable state = linearLayoutManager.onSaveInstanceState();
        adapter = new NewRequestListAdapter(activity, list, this);
        recyclerView.setAdapter(adapter);
        linearLayoutManager.onRestoreInstanceState(state);
    }

    private void showExtraLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.showNow();
        }
    }

    private void hideExtraLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            progressBar.hideNow();
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isDataEnd() {
        return dataEnd;
    }

    public void setDataEnd(boolean dataEnd) {
        this.dataEnd = dataEnd;
    }

    public boolean isLoadingList() {
        return loadingList;
    }

    public void setLoadingList(boolean loadingList) {
        this.loadingList = loadingList;
    }

    private void showLoading() {
        if (progressActivity != null) {
            progressActivity.showLoading();
        }
    }

    private boolean isLoading() {
        if (progressActivity != null) {
            return progressActivity.isLoadingCurrentState();
        }
        return false;
    }

    private void showContent() {
        if (progressActivity != null) {
            progressActivity.showContent();
        }
    }

    private void showEmptyPage(String message) {
        if (progressActivity != null) {
            progressActivity.showEmpty(getResources().getDrawable(R.drawable.ic_logo), message,
                    "");
        }
    }

    private void showErrorPage() {
        if (progressActivity != null) {
            progressActivity.showError(getResources().getDrawable(R.drawable.app_icon), getString(R.string.noConnection),
                    getString(R.string.noConnectionErrorMessage),
                    getString(R.string.tryAgain), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            refreshList();
                        }
                    });
        }
    }

    @Override
    public void onResume() {
        ((MainActivity) activity).setSelectionTabIcons(getString(R.string.new_title));
        refreshList();
        startCountDownTimer();
        super.onResume();
    }

    private void getNewRequestList(final Context context) {
        if (!isLoading()) {
            if (index == 0) {
                showLoading();
            } else {
                showExtraLoading();
            }
            //globalFunctions.showProgress(activity, getString(R.string.loading));
            ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
            servicesMethodsManager.getOrderList(context, orderPostModel, new ServerResponseInterface() {
                @Override
                public void OnSuccessFromServer(Object arg0) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        Log.d(TAG, "Response: " + arg0.toString());
                        if (index == 0) {
                            showContent();
                        } else {
                            hideExtraLoading();
                        }
                        setLoadingList(false);
                        setIndex(getIndex() + 1);
                        OrderListMainModel model = (OrderListMainModel) arg0;
                        setUpList(model);
                        checkForFeedback(model);
                        checkUpdateStatus(model);
                    }
                }

                @Override
                public void OnFailureFromServer(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        if (index == 0) {
                            showErrorPage();
                        } else {
                            hideExtraLoading();
                        }
                        Log.d(TAG, "Failure : " + msg);
                    }
                    // globalFunctions.hideProgress();
                    //globalFunctions.displayMessaage(activity, mainView, msg);
                    //Log.d(TAG, "Failure : " + msg);
                }

                @Override
                public void OnError(String msg) {
                    if (context != null) {
                        if (swipe_container.isRefreshing()) {
                            swipe_container.setRefreshing(false);
                        }
                        showErrorPage();
                        if (index == 0) {
                            showErrorPage();
                        } else {
                            hideExtraLoading();
                        }
                        Log.d(TAG, "Error : " + msg);
                    }
                    //globalFunctions.hideProgress();
                    // globalFunctions.displayMessaage(activity, mainView, msg);
                    //Log.d(TAG, "Error : " + msg);
                }
            }, "GetNewRequestList");
        }
    }

    private void checkUpdateStatus(OrderListMainModel mainModel) {
        if (mainModel != null) {

            //check force update....
            if (mainModel.getForceAndroidUpdate() != null) {
                if (mainModel.getForceAndroidUpdate().equalsIgnoreCase(globalVariables.UPDATE_APP)) {
                    //force update to playstore...
                    showForceUpdatePopup();
                }
            }

            //check soft update....
            if (mainModel.getAndroidUpdate() != null) {
                if (mainModel.getAndroidUpdate().equalsIgnoreCase(globalVariables.UPDATE_APP)) {
                    //show popup update to playstore...
                    //globalFunctions.displayMessageBottom(activity, mainView, activity.getString(R.string.update_app_text));
                    globalFunctions.displayMessageBottom(activity, mainView, activity.getString(R.string.new_version_available_text));
                }
            }
        }
    }

    private void showForceUpdatePopup() {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(getResources().getString(R.string.update_app_text));
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                //navigate to play store for update
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                        ("market://details?id=" + activity.getString(R.string.playstore_ID))));
                // alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void checkForFeedback(OrderListMainModel model) {
        if (model != null) {
            if (model.getFeedback() == null || model.getFeedback().equalsIgnoreCase("0") || model.getFeedback().equalsIgnoreCase("")) {

            } else {
                //give feedback....
                Intent intent = RateClientActivity.newInstance(context, model);
                activity.startActivity(intent);
            }
        }
    }

    private void setUpList(OrderListMainModel orderListMainModel) {
        if (orderListMainModel != null) {
            if (orderListMainModel.getOrderList() != null) {
                detail = orderListMainModel.getOrderList();
                list.clear();
                list.addAll(orderListMainModel.getOrderList().getOrderList());
                if (adapter != null) {
                    synchronized (adapter) {
                        adapter.notifyDataSetChanged();
                    }
                }
                if (list.size() <= 0) {
                    showEmptyPage(getString(R.string.emptyNewRequestListMessage));
                } else {
                    initRecyclerView();
                    showContent();
                }
                MainActivity.setNewRequestCount(list.size());
            }
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void OnAccpetClickInvoke(int position, OrderModel orderModel) {

        if (orderModel != null) {
            if (orderModel.getOrderId() != null) {
                updatetRequest(context, orderModel.getOrderId(), globalVariables.REQUEST_ACCEPT, orderModel);
            }
        }

    }

    @Override
    public void OnRejectClickInvoke(int position, final OrderModel orderModel) {
        if (orderModel != null) {
            if (orderModel.getOrderId() != null) {

                final AlertDialog alertDialog = new AlertDialog(context);
                alertDialog.setCancelable(false);
                alertDialog.setIcon(R.drawable.ic_logo_dark);
                alertDialog.setTitle(getString(R.string.app_name));
                alertDialog.setMessage(getString(R.string.confirm_reject_text));
                alertDialog.setPositiveButton(getString(R.string.yes), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        updatetRequest(context, orderModel.getOrderId(), globalVariables.REQUEST_REJECT, orderModel);
                    }
                });

                alertDialog.setNegativeButton(getString(R.string.no), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                alertDialog.show();
            }
        }
    }

    private void updatetRequest(final Context context, String orderId, final String requestAcceptStatus, final OrderModel orderModel) {
        globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.updateRequestStatus(context, orderId, requestAcceptStatus, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                Log.d(TAG, "Response : " + arg0.toString());
                globalFunctions.hideProgress();
                validateRequestUpdateOutput(arg0, requestAcceptStatus, orderModel);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "UpdateRequestStatus");
    }

    private void validateRequestUpdateOutput(Object model, String requestAcceptStatus, OrderModel orderModel) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            showAlertMessageAefterUpdate(statusModel, requestAcceptStatus, orderModel);
        }
    }

    private void showAlertMessageAefterUpdate(final StatusModel statusModel, final String requestAcceptStatus, final OrderModel orderModel) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(statusModel.getMessage());
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                if (requestAcceptStatus != null) {
                    if (requestAcceptStatus.equalsIgnoreCase(globalVariables.REQUEST_ACCEPT)) {
                        if (statusModel != null) {
                            if (statusModel.isStatus()) {
                                if (GlobalFunctions.isNotNullValue(orderModel.getOrderType()) && orderModel.getOrderType().equalsIgnoreCase("1")) {
                                    Fragment activeRequestListFragment = new ActiveRequestFragment();
                                    ((MainActivity) activity).replaceFragment(activeRequestListFragment, ActiveRequestFragment.TAG, "", 0, 0);
                                } else if (GlobalFunctions.isNotNullValue(orderModel.getOrderType()) && orderModel.getOrderType().equalsIgnoreCase("2")) {
                                    Fragment upcomingRequestListFragment = new UpcomingRequestListFragment();
                                    ((MainActivity) activity).replaceFragment(upcomingRequestListFragment, UpcomingRequestListFragment.TAG, "", 0, 0);
                                } else if (GlobalFunctions.isNotNullValue(orderModel.getDate()) && GlobalFunctions.isTodayDate(activity, orderModel.getDate())) {
                                    Fragment activeRequestListFragment = new ActiveRequestFragment();
                                    ((MainActivity) activity).replaceFragment(activeRequestListFragment, ActiveRequestFragment.TAG, "", 0, 0);
                                } else {
                                    Fragment upcomingRequestListFragment = new UpcomingRequestListFragment();
//                                Fragment activeRequestListFragment = new ActiveRequestFragment();
                                    ((MainActivity) activity).replaceFragment(upcomingRequestListFragment, UpcomingRequestListFragment.TAG, "", 0, 0);
//                                ((MainActivity) activity).replaceFragment(activeRequestListFragment, ActiveRequestFragment.TAG, "", 0, 0);
                                }
                            }
                        }
                    } else {
                        refreshList();
                    }
                }

            }
        });

        alertDialog.show();
    }

    @Override
    public void onDestroyView() {

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }

        super.onDestroyView();
    }

}


