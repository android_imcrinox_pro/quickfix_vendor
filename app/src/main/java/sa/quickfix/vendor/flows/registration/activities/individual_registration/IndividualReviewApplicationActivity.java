package sa.quickfix.vendor.flows.registration.activities.individual_registration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.flows.registration.activities.RegistrationSuccessActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.LicenceImagesListAdapter;
import sa.quickfix.vendor.adapters.ScientificDocImagesListAdapter;
import sa.quickfix.vendor.adapters.SelectedServicesListAdapter;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.CategoryListModel;
import sa.quickfix.vendor.services.model.CategoryModel;
import sa.quickfix.vendor.services.model.CertificatesListModel;
import sa.quickfix.vendor.services.model.CertificatesModel;
import sa.quickfix.vendor.services.model.IdImageModel;
import sa.quickfix.vendor.services.model.IdImagesListModel;
import sa.quickfix.vendor.services.model.IndividualRegisterModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.upload.UploadImage;
import sa.quickfix.vendor.upload.UploadListener;
import sa.quickfix.vendor.R;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class IndividualReviewApplicationActivity extends AppCompatActivity implements UploadListener {

    public static final String TAG = "IndividualRevctivity";

    public static final String BUNDLE_INDIVIDUAL_REGISTER_MODEL = "BundleIndividualRegisterModel";

    Context context;
    private static Activity activity;
    View mainView;

    FirebaseStorage storage;
    StorageReference storageReference;

    ProfileModel mProfileModel = null;

    CategoryListModel selectedCategoryListModel = new CategoryListModel();
    List<CategoryModel> selectedCategoryList = new ArrayList<>();

    private CardView submit_cardview;
    private ImageView back_iv;
    private LinearLayout edit_details_ll;
    private CircleImageView vendor_iv;
    private TextView vendor_name_tv, vendor_email_tv, first_name_tv, last_name_tv, gender_tv, email_id_tv, mobile_no_tv, address_tv, city_tv, national_id_tv,
            nationality_tv, language_known_tv, total_experiance_tv, account_number_tv, iban_tv, holder_name_tv, branch_tv, bank_name_tv, swift_code_tv;

    List<Uri>
            uriProfileImageList,
            uriIdImageList,
            uriCertificatesImageList;

    IndividualRegisterModel individualRegisterModel = null;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    LinearLayoutManager servicesLayoutManager;
    RecyclerView selected_services_recycler_view;
    SelectedServicesListAdapter selectedServicesListAdapter;

    LinearLayoutManager certificateLayoutManager;
    RecyclerView certificates_recycler_view;
    LicenceImagesListAdapter certficateListAdapter;

    LinearLayoutManager idLayoutManager;
    RecyclerView id_images_recycler_view;
    ScientificDocImagesListAdapter idListAdapter;

    IdImagesListModel idImageListModel = new IdImagesListModel();
    List<IdImageModel> idImageList = new ArrayList<>();

    CertificatesListModel certificateListModel = new CertificatesListModel();
    List<CertificatesModel> certificateList = new ArrayList<>();

    public static Intent newInstance(Context context, IndividualRegisterModel model) {
        Intent intent = new Intent(context, IndividualReviewApplicationActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_INDIVIDUAL_REGISTER_MODEL, model);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.individual_review_application_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        uriProfileImageList = new ArrayList<>();
        uriIdImageList = new ArrayList<>();
        uriCertificatesImageList = new ArrayList<>();

        uriProfileImageList.clear();
        uriIdImageList.clear();
        uriCertificatesImageList.clear();

        idImageList.clear();
        certificateList.clear();

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        certificateLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        idLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        servicesLayoutManager = new LinearLayoutManager(context);

        certificates_recycler_view = (RecyclerView) findViewById(R.id.certificates_recycler_view);
        id_images_recycler_view = (RecyclerView) findViewById(R.id.id_images_recycler_view);

        back_iv = (ImageView) findViewById(R.id.back_iv);
        vendor_iv = (CircleImageView) findViewById(R.id.vendor_iv);

        vendor_name_tv = (TextView) findViewById(R.id.vendor_name_tv);
        vendor_email_tv = (TextView) findViewById(R.id.vendor_email_tv);
        first_name_tv = (TextView) findViewById(R.id.first_name_tv);
        last_name_tv = (TextView) findViewById(R.id.last_name_tv);
        gender_tv = (TextView) findViewById(R.id.gender_tv);
        email_id_tv = (TextView) findViewById(R.id.email_id_tv);
        mobile_no_tv = (TextView) findViewById(R.id.mobile_no_tv);
        address_tv = (TextView) findViewById(R.id.address_tv);
        city_tv = (TextView) findViewById(R.id.city_tv);
        national_id_tv = (TextView) findViewById(R.id.national_id_tv);
        nationality_tv = (TextView) findViewById(R.id.nationality_tv);
        language_known_tv = (TextView) findViewById(R.id.language_known_tv);
        total_experiance_tv = (TextView) findViewById(R.id.total_experiance_tv);
        account_number_tv = (TextView) findViewById(R.id.account_number_tv);
        iban_tv = (TextView) findViewById(R.id.iban_tv);
        holder_name_tv = (TextView) findViewById(R.id.holder_name_tv);
        branch_tv = (TextView) findViewById(R.id.branch_tv);
        bank_name_tv = (TextView) findViewById(R.id.bank_name_tv);
        swift_code_tv = (TextView) findViewById(R.id.swift_code_tv);

        edit_details_ll = (LinearLayout) findViewById(R.id.edit_details_ll);

        submit_cardview = (CardView) findViewById(R.id.submit_cardview);
        selected_services_recycler_view = (RecyclerView) findViewById(R.id.selected_services_recycler_view);

        mainView = submit_cardview;

        if (getIntent().hasExtra(BUNDLE_INDIVIDUAL_REGISTER_MODEL)) {
            individualRegisterModel = (IndividualRegisterModel) getIntent().getSerializableExtra(BUNDLE_INDIVIDUAL_REGISTER_MODEL);
        } else {
            individualRegisterModel = null;
        }

        Intent i = getIntent();
        uriProfileImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE);
        uriIdImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_LIST_CODE);
        uriCertificatesImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_CERTIFICATES_LIST_CODE);

        //category recycler view....
        initSeletedCategoryList();
        initCertificateImageList();
        initIdImageList();

        if (individualRegisterModel != null) {
            //set this page...
            setThisPage(individualRegisterModel);
        }

        submit_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage(GlobalVariables.UPLOAD_PROFILE_PHOTO_PATH_CODE);
            }
        });

        edit_details_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = IndividualRegistrationActivity.newInstance(activity, individualRegisterModel);
                intent.putExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE, (Serializable) uriProfileImageList);
                intent.putExtra(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_LIST_CODE, (Serializable) uriIdImageList);
                intent.putExtra(GlobalVariables.UPLOAD_CERTIFICATES_LIST_CODE, (Serializable) uriCertificatesImageList);
                activity.startActivity(intent);
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setUpSelectedCategoryList(CategoryListModel categoryListModel1) {
        if (categoryListModel1 != null && selectedCategoryList != null) {
            selectedCategoryList.clear();
            if (selectedCategoryListModel == null) {
                selectedCategoryListModel = new CategoryListModel();
            }
            selectedCategoryListModel.setCategoryList(categoryListModel1.getCategoryList());
            selectedCategoryList = categoryListModel1.getCategoryList();
            initSeletedCategoryList();
        }
    }

    private void initSeletedCategoryList() {
        selectedServicesListAdapter = new SelectedServicesListAdapter(activity, selectedCategoryList);
        selected_services_recycler_view.setLayoutManager(servicesLayoutManager);
        selected_services_recycler_view.setAdapter(selectedServicesListAdapter);

        synchronized (selectedServicesListAdapter) {
            selectedServicesListAdapter.notifyDataSetChanged();
        }
    }

    private void uploadImage(String type) {
        if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_PROFILE_PHOTO_PATH_CODE)) {
            if (uriProfileImageList != null) {
                if (uriProfileImageList.size() != 0) {
                    globalFunctions.showProgress(activity, context.getString(R.string.uploading_images));
                    for (int j = 0; j < uriProfileImageList.size(); j++) {
                        Uri uri = (uriProfileImageList.get(j));
                        UploadImage uploadImage = new UploadImage(context);
                        uploadImage.startUploading(uri, type, "image", this);
                    }
                } else {
                    uploadImage(GlobalVariables.UPLOAD_CERTIFICATE_PHOTO_PATH_CODE);
                }
            } else {
                uploadImage(GlobalVariables.UPLOAD_CERTIFICATE_PHOTO_PATH_CODE);
            }
        } else if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_CERTIFICATE_PHOTO_PATH_CODE)) {
            if (uriCertificatesImageList != null) {
                if (uriCertificatesImageList.size() != 0) {
                    // globalFunctions.showProgress(activity, context.getString(R.string.uploading_images));
                    for (int j = 0; j < uriCertificatesImageList.size(); j++) {
                        Uri uri = (uriCertificatesImageList.get(j));
                        UploadImage uploadImage = new UploadImage(context);
                        uploadImage.startUploading(uri, type, "image", this);
                    }
                } else {
                    uploadImage(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_PATH_CODE);
                }
            } else {
                uploadImage(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_PATH_CODE);
            }
        } else if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_PATH_CODE)) {
            if (uriIdImageList != null) {

                if (uriIdImageList.size() != 0) {
//                GlobalFunctions.showProgress(activity, context.getString(R.string.uploading_images));
                    for (int j = 0; j < uriIdImageList.size(); j++) {
                        Uri uri = (uriIdImageList.get(j));
                        UploadImage uploadImage = new UploadImage(context);
                        uploadImage.startUploading(uri, type, "image", this);
                    }
                } else {
                    //update the profile...
                /*if (mProfileModel != null) {
                    //from edit profile....update profile...
                    GalleryListModel mGalleryListModel1 = new GalleryListModel();
                    mProfileModel.setGalleryList(mGalleryListModel1);
                    updateProfile(context, mProfileModel);
                }*/
                }
            } else {
                //update the profile...
                /*if (mProfileModel != null) {
                    //from edit profile....update profile...
                    GalleryListModel mGalleryListModel1 = new GalleryListModel();
                    mProfileModel.setGalleryList(mGalleryListModel1);
                    updateProfile(context, mProfileModel);
                }*/
            }
        }
    }


    private void setThisPage(IndividualRegisterModel individualRegisterModel) {

        String
                year = "",
                month = "";

        if (individualRegisterModel != null) {

            if (uriProfileImageList != null) {
                if (uriProfileImageList.size() > 0) {
                    try {
                        Glide.with(this).load(uriProfileImageList.get(0)).into(vendor_iv);
                    } catch (Exception exccc) {

                    }
                }
            }

            if (individualRegisterModel.getFirstName() != null && individualRegisterModel.getLastName() != null) {
                vendor_name_tv.setText(individualRegisterModel.getFirstName() + " " + individualRegisterModel.getLastName());
            }

            if (individualRegisterModel.getFirstName() != null) {
                first_name_tv.setText(individualRegisterModel.getFirstName());
            }

            if (individualRegisterModel.getLastName() != null) {
                last_name_tv.setText(individualRegisterModel.getLastName());
            }

            if (individualRegisterModel.getEmailId() != null) {
                email_id_tv.setText(individualRegisterModel.getEmailId());
                vendor_email_tv.setText(individualRegisterModel.getEmailId());
            }

            if (individualRegisterModel.getCountryCode() != null && individualRegisterModel.getMobileNumber() != null) {
                mobile_no_tv.setText(individualRegisterModel.getCountryCode() + individualRegisterModel.getMobileNumber());
            }

            if (individualRegisterModel.getAddress() != null) {
                address_tv.setText(individualRegisterModel.getAddress());
            }

            if (individualRegisterModel.getGender() != null) {
                if (individualRegisterModel.getGender().equalsIgnoreCase(globalVariables.GENDER_MALE)) {
                    gender_tv.setText(getString(R.string.male));
                } else if (individualRegisterModel.getGender().equalsIgnoreCase(globalVariables.GENDER_FEMALE)) {
                    gender_tv.setText(getString(R.string.female));
                }
            }

            if (individualRegisterModel.getCityName() != null) {
                city_tv.setText(individualRegisterModel.getCityName());
            }

            if (individualRegisterModel.getIdNumber() != null) {
                national_id_tv.setText(individualRegisterModel.getIdNumber());
            }

            if (individualRegisterModel.getNationality() != null) {
                nationality_tv.setText(individualRegisterModel.getNationality());
            }

            if (individualRegisterModel.getLanguagesNames() != null) {
                language_known_tv.setText(individualRegisterModel.getLanguagesNames());
            }

            if (individualRegisterModel.getExperience() != null) {
                year = individualRegisterModel.getExperience();
            }

            if (individualRegisterModel.getExperienceM() != null) {
                month = individualRegisterModel.getExperienceM();
            }
            total_experiance_tv.setText(year + " " + getString(R.string.years) + " " + month + " " + getString(R.string.months));

            if (individualRegisterModel.getAccountNumber() != null) {
                account_number_tv.setText(individualRegisterModel.getAccountNumber());
            }

            if (individualRegisterModel.getIban() != null) {
                iban_tv.setText(individualRegisterModel.getIban());
            }

           /* if (individualRegisterModel.getAccountHolderName() != null) {
                holder_name_tv.setText(individualRegisterModel.getAccountHolderName());
            }

            if (individualRegisterModel.getBranch() != null) {
                branch_tv.setText(individualRegisterModel.getBranch());
            }

            if (individualRegisterModel.getBankName() != null) {
                bank_name_tv.setText(individualRegisterModel.getBankName());
            }

            if (individualRegisterModel.getSwiftCode() != null) {
                swift_code_tv.setText(individualRegisterModel.getSwiftCode());
            }*/

            //set selected category list..

            if (individualRegisterModel.getCategoryList() != null) {
                //already selected list data..
                // from this list filter only selected data....
                selectedCategoryList.clear();

                int listSizeMain = individualRegisterModel.getCategoryList().getCategoryList().size();

                for (int i = 0; i < listSizeMain; i++) {
                    boolean isAdded = false;
                    if (individualRegisterModel.getCategoryList().getCategoryList().get(i).getSubCategoryListModel() != null) {
                        int listSizeSub = individualRegisterModel.getCategoryList().getCategoryList().get(i).getSubCategoryListModel().getSubCategoryListModels().size();
                        if (listSizeSub > 0) {
                            for (int j = 0; j < listSizeSub; j++) {

                                if (!isAdded && individualRegisterModel.getCategoryList().getCategoryList().get(i).getSubCategoryListModel().getSubCategoryListModels().get(j).getSelected().equalsIgnoreCase("1")) {
//                        if (list.size() == 0) {
                                    selectedCategoryList.add(individualRegisterModel.getCategoryList().getCategoryList().get(i));
//                        }
                                    isAdded = true;
                                }
                            }
                        } else if (individualRegisterModel.getCategoryList().getCategoryList().get(i).getSelected().equalsIgnoreCase("1")) {
                            selectedCategoryList.add(individualRegisterModel.getCategoryList().getCategoryList().get(i));
                        }
                    } else if (individualRegisterModel.getCategoryList().getCategoryList().get(i).getSelected().equalsIgnoreCase("1")) {
                        selectedCategoryList.add(individualRegisterModel.getCategoryList().getCategoryList().get(i));
                    }
                }
                if (selectedCategoryList.size() > 0) {
                    initSeletedCategoryList();
                }

            }

        }
    }

    private void initCertificateImageList() {
        certficateListAdapter = new LicenceImagesListAdapter(activity, uriCertificatesImageList);
        certificates_recycler_view.setLayoutManager(certificateLayoutManager);
        certificates_recycler_view.setAdapter(certficateListAdapter);
        synchronized (certficateListAdapter) {
            certficateListAdapter.notifyDataSetChanged();
        }
    }

    private void initIdImageList() {
        idListAdapter = new ScientificDocImagesListAdapter(activity, uriIdImageList);
        id_images_recycler_view.setLayoutManager(idLayoutManager);
        id_images_recycler_view.setAdapter(idListAdapter);
        synchronized (idListAdapter) {
            idListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void OnSuccess(final String imgUrl, final String type, final String uploadingFile) {
        // activity.runOnUiThread(new Runnable() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                globalFunctions.displayMessaage(activity, mainView, "Uploaded");
                // GlobalFunctions.hideProgress();
                if (uploadingFile.equalsIgnoreCase("image")) {
                    setImagesToModel(type, imgUrl);
                }
            }
        });

    }

    private void setImagesToModel(String type, String imagePath) {
        if (type != null) {
            if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_PROFILE_PHOTO_PATH_CODE)) {
                if (mProfileModel != null) {
                    //from edit profile......
                    //  mProfileModel.setProfileImg(imagePath);
                } else {
                    individualRegisterModel.setProfileImage(imagePath);
                }
                uploadImage(GlobalVariables.UPLOAD_CERTIFICATE_PHOTO_PATH_CODE);
            } else if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_CERTIFICATE_PHOTO_PATH_CODE)) {
                if (mProfileModel != null) {
                    //from edit profile......
                    // mProfileModel.setLicenseImg(imagePath);
                } else {
                    setCertificatesImagesList(imagePath);
                }
                uploadImage(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_PATH_CODE);
            } else if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_PATH_CODE)) {
                if (mProfileModel != null) {
                    //from edit profile......
                    // mProfileModel.setLicenseImg(imagePath);
                } else {
                    setIdImagesList(imagePath);
                }
            }
        }

    }

    private void setCertificatesImagesList(String imagePath) {
        CertificatesModel licenceImageModel = new CertificatesModel();
        licenceImageModel.setImage(imagePath);
        certificateList.add(licenceImageModel);
        if (uriCertificatesImageList.size() == certificateList.size()) {
            certificateListModel.setCertificateList(certificateList);
            if (mProfileModel != null) {
                //from edit profile......
               /* CertificatesListModel mLicenceImagesListModel = new CertificatesListModel();
                mProfileModel.setGalleryList(mLicenceImagesListModel);
                mProfileModel.setGalleryList(certificateListModel);*/
            } else {
                individualRegisterModel.setCertificatesList(certificateListModel);
            }

        } else {

        }
    }

    private void setIdImagesList(String imagePath) {
        IdImageModel idImageModel = new IdImageModel();
        idImageModel.setImage(imagePath);
        idImageList.add(idImageModel);
        if (uriIdImageList.size() == idImageList.size()) {
            idImageListModel.setIdImageList(idImageList);
            if (mProfileModel != null) {
                //from edit profile......
               /* CertificatesListModel mLicenceImagesListModel = new CertificatesListModel();
                mProfileModel.setGalleryList(mLicenceImagesListModel);
                mProfileModel.setGalleryList(idImageListModel);*/
            } else {
                individualRegisterModel.setIdImageList(idImageListModel);
            }
            GlobalFunctions.hideProgress();

            if (mProfileModel != null) {
                //from edit profile....update profile...
                //  updateProfile(context, mProfileModel);
            } else {
                String password = "";
                password = individualRegisterModel.getConfirmPassword();
                individualRegisterModel.setPassword(globalFunctions.md5(password));
                registerUser(context, individualRegisterModel);
            }

        } else {
            GlobalFunctions.hideProgress();
        }
    }

    private void registerUser(final Context context, final IndividualRegisterModel model) {
        GlobalFunctions.showProgress(context, context.getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.registerAsIndividual(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                GlobalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateOutput(arg0, model);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                GlobalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                GlobalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Error : " + msg);
            }
        }, "Register_User");
    }

    private void validateOutput(Object model, IndividualRegisterModel registerModel) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            if (statusModel.isStatus()) {
                globalFunctions.closeAllActivities();
                Intent intent = RegistrationSuccessActivity.newInstance(activity, statusModel);
                activity.startActivity(intent);
                closeThisActivity();
            } else {
                globalFunctions.displayErrorDialog(activity, statusModel.getMessage());
            }

        }
    }

    @Override
    public void OnFailure() {
        GlobalFunctions.hideProgress();
        globalFunctions.displayErrorDialog(activity, context.getString(R.string.failed_upload_image));
    }
}