package sa.quickfix.vendor.flows.registration.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import sa.quickfix.vendor.flows.login.activities.LoginWithPhoneNumberActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.R;

import com.facebook.FacebookSdk;


public class RegistrationSuccessActivity extends AppCompatActivity {

    public static final String TAG = "RegistrationSuccessActivity";
    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";
    public static final String BUNDLE_STATUS_MODEL = "BundleStatusModel";

    Context context;
    private static Activity activity;
    View mainView;

    String pageType = null;

    private LinearLayout edit_details_ll;
    private CardView close_cardview;
    private TextView details_tv;

    StatusModel statusModel = null;

    public static Intent newInstance(Activity activity, String pageType) {
        Intent intent = new Intent(activity, RegistrationSuccessActivity.class);
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_FROM_PAGE, pageType);
        intent.putExtras(args);
        return intent;
    }

    public static Intent newInstance(Context context, StatusModel model) {
        Intent intent = new Intent(context, RegistrationSuccessActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_STATUS_MODEL, model);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.registration_success_activity);

        context = this;
        activity = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        edit_details_ll = (LinearLayout) findViewById(R.id.edit_details_ll);
        close_cardview = (CardView) findViewById(R.id.close_cardview);
        details_tv = (TextView) findViewById(R.id.details_tv);

        mainView = edit_details_ll;

        if (getIntent().hasExtra(BUNDLE_STATUS_MODEL)) {
            statusModel = (StatusModel) getIntent().getSerializableExtra(BUNDLE_STATUS_MODEL);
        }

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_PAGE)) {
            pageType = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_PAGE);
        } else {
            pageType = null;
        }

        if(statusModel != null){
            if(statusModel.getMessage() != null){
                details_tv.setText(statusModel.getMessage());
            }
        }

       /* edit_details_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageType != null) {
                    if (pageType.equalsIgnoreCase("1")) {
                        //from individual....
                        Intent intent = new Intent(RegistrationSuccessActivity.this, IndividualRegistrationActivity.class);
                        startActivity(intent);
                    } else if (pageType.equalsIgnoreCase("2")) {
                        ///from company...
                        Intent intent = new Intent(RegistrationSuccessActivity.this, CompanyRegistrationActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });*/

        close_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finishAffinity();
                GlobalFunctions.closeAllActivities();
                startActivity(new Intent(activity, LoginWithPhoneNumberActivity.class));
            }
        });

    }

    @Override
    public void onBackPressed() {
        finishAffinity();
       /* super.onBackPressed();
        closeThisActivity();*/

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}