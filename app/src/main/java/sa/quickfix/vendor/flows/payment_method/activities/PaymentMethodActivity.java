package sa.quickfix.vendor.flows.payment_method.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.checkout.meta.CheckoutStorePaymentDetailsMode;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;

import java.util.LinkedHashSet;
import java.util.Set;

import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.flows.wallet.activities.MyWalletActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.services.model.WalletAmountCreditModel;
import sa.quickfix.vendor.view.AlertDialog;

public class PaymentMethodActivity extends AppCompatActivity {

    public static final String TAG = "PaymentMethodActivity";

    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";
    public static final String BUNDLE_KEY_PAY_AMOUNT = "BundleKeyPayAMount";
    public static final String BUNDLE_KEY_FROM_WALLET_AMOUNT = "BundleKeyWalletAmount";

    Context context;
    private static Activity activity;
    private LayoutInflater layoutInflater;
    View mainView;

    TextView continue_tv;
    ImageView back_iv, menu_user_iv;

    RadioButton wallet_rb, online_rb, cod_rb;
    RadioGroup payment_rg;
    AppCompatCheckBox terms_checkbox;

    String pageType = null;
    String payAmount = null, walletAmount = null;

    private TextView amount_pay_tv, wallet_available_balance_tv;
    private CardView continue_view;
    private LinearLayout order_summary_ll;

    GlobalFunctions globalFunctions;
    GlobalVariables globalVariables;

    public static Intent newInstance(Activity activity, String payAmount, String walletAMount, String pageType) {
        Intent intent = new Intent(activity, PaymentMethodActivity.class);
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_FROM_PAGE, pageType);
        args.putString(BUNDLE_KEY_PAY_AMOUNT, payAmount);
        args.putString(BUNDLE_KEY_FROM_WALLET_AMOUNT, walletAMount);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_method_activity);

        context = this;
        activity = this;

        this.layoutInflater = activity.getLayoutInflater();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        back_iv = (ImageView) findViewById(R.id.back_iv);

        amount_pay_tv = (TextView) findViewById(R.id.amount_pay_tv);
        wallet_available_balance_tv = (TextView) findViewById(R.id.wallet_available_balance_tv);
        continue_view = (CardView) findViewById(R.id.continue_view);
        order_summary_ll = (LinearLayout) findViewById(R.id.order_summary_ll);
        payment_rg = (RadioGroup) findViewById(R.id.payment_rg);
        online_rb = (RadioButton) findViewById(R.id.online_rb);
        wallet_rb = (RadioButton) findViewById(R.id.wallet_rb);
        cod_rb = (RadioButton) findViewById(R.id.cod_rb);
        terms_checkbox = (AppCompatCheckBox) findViewById(R.id.terms_checkbox);

        continue_tv = (TextView) findViewById(R.id.continue_tv);

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_PAGE)) {
            pageType = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_PAGE);
        } else {
            pageType = null;
        }

        if (getIntent().hasExtra(BUNDLE_KEY_PAY_AMOUNT)) {
            payAmount = (String) getIntent().getStringExtra(BUNDLE_KEY_PAY_AMOUNT);
        } else {
            payAmount = null;
        }

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_WALLET_AMOUNT)) {
            walletAmount = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_WALLET_AMOUNT);
        } else {
            walletAmount = null;
        }

        payment_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.online_rb) {
                    online_rb.setChecked(true);
                    cod_rb.setChecked(false);
                    wallet_rb.setChecked(false);

                } else if (checkedId == R.id.cod_rb) {

                    wallet_rb.setChecked(false);
                    online_rb.setChecked(false);
                    cod_rb.setChecked(true);
                } else if (checkedId == R.id.wallet_rb) {
                    cod_rb.setChecked(false);
                    online_rb.setChecked(false);
                    wallet_rb.setChecked(true);
                }
            }
        });

        continue_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!cod_rb.isChecked() && !online_rb.isChecked() && !wallet_rb.isChecked()) {
                    GlobalFunctions.displayErrorDialog(context, getString(R.string.please_select_payment));
                } else if (!terms_checkbox.isChecked()) {
                    GlobalFunctions.displayErrorDialog(context, getString(R.string.please_accept_TC));
                } else {
                    openCartTypeDialog(context);
                }
//                validateInput();

            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setThisPage();

    }

    private void openCartTypeDialog(final Context context) {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.dialog_payment_card_type, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        ImageView close_iv;
        TextView submit_tv;

        final RadioButton mada_rb, visa_rb, master_rb, amex_rb;
        RadioGroup payment_rg;

        submit_tv = (TextView) alertView.findViewById(R.id.add_money_tv);
        close_iv = (ImageView) alertView.findViewById(R.id.close_iv);

        mada_rb = (RadioButton) alertView.findViewById(R.id.mada_rb);
        visa_rb = (RadioButton) alertView.findViewById(R.id.visa_rb);
        master_rb = (RadioButton) alertView.findViewById(R.id.master_rb);
        amex_rb = (RadioButton) alertView.findViewById(R.id.amex_rb);

        payment_rg = (RadioGroup) alertView.findViewById(R.id.payment_rg);

        submit_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mada_rb.isChecked() && !visa_rb.isChecked() && !master_rb.isChecked() && !amex_rb.isChecked()) {
                    GlobalFunctions.displayErrorDialog(context, activity.getString(R.string.please_select_card_type));
                } else {
                    dialog.dismiss();
                    String paymentCardType = null;
                    if (mada_rb.isChecked()) {
                        paymentCardType = GlobalVariables.PAYMENT_TYPE_MADA;
                    } else if (visa_rb.isChecked()) {
                        paymentCardType = GlobalVariables.PAYMENT_TYPE_VISA;
                    } else if (master_rb.isChecked()) {
                        paymentCardType = GlobalVariables.PAYMENT_TYPE_MASTER;
                    } else if (amex_rb.isChecked()) {
                        paymentCardType = GlobalVariables.PAYMENT_TYPE_AMERICAN;
                    }
                    validateInput(paymentCardType);
                }
            }
        });

        close_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    private void setThisPage() {

        if (GlobalFunctions.isNotNullValue(pageType) && pageType.equalsIgnoreCase("1")) {

            continue_view.setVisibility(View.VISIBLE);
            order_summary_ll.setVisibility(View.GONE);

            continue_tv.setClickable(true);
            continue_tv.setEnabled(true);

            online_rb.setChecked(true);
            cod_rb.setChecked(false);
            wallet_rb.setChecked(false);

            online_rb.setEnabled(false);
            online_rb.setClickable(false);
            wallet_rb.setClickable(false);

            cod_rb.setClickable(false);
            wallet_rb.setClickable(false);
            cod_rb.setEnabled(false);
            wallet_rb.setEnabled(false);

            amount_pay_tv.setText(payAmount + " "+activity.getString(R.string.sar));
//            wallet_rb.setText(activity.getString(R.string.wallet)+" ( "+ availWalletBalance+" SAR )");
            wallet_available_balance_tv.setText(" "+activity.getString(R.string.sar)+" " + walletAmount);

        }
    }

    private void validateInput(String cardType) {
        if (pageType.equalsIgnoreCase("1")) {
            WalletAmountCreditModel walletAmountCreditModel = new WalletAmountCreditModel();
            walletAmountCreditModel.setAmount(payAmount);
            walletAmountCreditModel.setType(cardType);
            onlineWalletCredit(context, walletAmountCreditModel,cardType);
        }
    }

    private void onlineWalletCredit(final Context context, final WalletAmountCreditModel model,String cardType) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.onlineWalletCredit(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validatWalletCreditOutput(arg0,cardType);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "OnlineWalletCredit");
    }

    private void validatWalletCreditOutput(Object model,String cardType) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;

            if (statusModel.isStatus() && GlobalFunctions.isNotNullValue(statusModel.getExtra())) {
                setUpPaymentConfiguration(statusModel.getExtra(),cardType);
            } else {
                showAddWalletPopup(statusModel);
            }
        }
    }


    private void setUpPaymentConfiguration(String token,String cardType) {
        String callbackScheme = "checkoutui";
        Set<String> paymentBrands = new LinkedHashSet<String>();

        paymentBrands.add(activity.getString(R.string.visa_payment));
        paymentBrands.add(activity.getString(R.string.master_payment));
        paymentBrands.add(activity.getString(R.string.mada_payment));
//        paymentBrands.add("DIRECTDEBIT_SEPA");

        CheckoutSettings checkoutSettings = new CheckoutSettings(token, paymentBrands, Connect.ProviderMode.LIVE);
        if (cardType.equalsIgnoreCase( GlobalVariables.PAYMENT_TYPE_MADA)) {
            checkoutSettings.setEntityId(activity.getString(R.string.Entity_Id_MADA));
        } else {
            checkoutSettings.setEntityId(activity.getString(R.string.Entity_Id));
        }

        if (GlobalFunctions.getLanguage(activity)== GlobalVariables.LANGUAGE.ARABIC){
            checkoutSettings.setLocale("ar_AR");
        }else {
            checkoutSettings.setLocale("en_US");
        }

//        checkoutSettings.setEntityId(activity.getString(R.string.Entity_Id));
        checkoutSettings.setCheckoutId(token);
        checkoutSettings.setStorePaymentDetailsMode(CheckoutStorePaymentDetailsMode.PROMPT);
        checkoutSettings.setShopperResultUrl(callbackScheme + "://callback");
        Intent intent = checkoutSettings.createCheckoutActivityIntent(this);
        startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT);
    }

    private void showAddWalletPopup(StatusModel model) {

        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        if (model.isStatus()) {
            alertDialog.setMessage(activity.getString(R.string.added_wallet));
        } else {
            alertDialog.setMessage(model.getMessage());
        }
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(PaymentMethodActivity.this, MainActivity.class);
                startActivity(intent);
                closeThisActivity();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case CheckoutActivity.RESULT_OK:
                /* transaction completed */
                Transaction transaction = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_TRANSACTION);

                /* resource path if needed */
                String resourcePath = data.getStringExtra(CheckoutActivity.CHECKOUT_RESULT_RESOURCE_PATH);
                if (pageType.equalsIgnoreCase("1")) {
                    checkWalletPayment(context, resourcePath);

                } else {
                    closeThisActivity();
                }
//                displayErrorDialog(context, resourcePath, true);

                if (transaction.getTransactionType() == TransactionType.SYNC) {
                    /* check the result of synchronous transaction */
                } else {
                    /* wait for the asynchronous transaction callback in the onNewIntent() */
                }

                break;
            case CheckoutActivity.RESULT_CANCELED:
                /* shopper canceled the checkout process */
                displayErrorDialog(context, activity.getString(R.string.cancelled), false);
                break;
            case CheckoutActivity.RESULT_ERROR:
                /* error occurred */
                PaymentError error = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_ERROR);
                displayErrorDialog(context, activity.getString(R.string.error) + error.getErrorMessage(), false);

        }
    }

    public void displayErrorDialog(final Context context, final String message, final boolean isSuccess) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(context.getString(R.string.app_name));

        if (isSuccess) {
            alertDialog.setMessage(activity.getString(R.string.payment_successfull));
        } else {
            alertDialog.setMessage(message);
        }

        alertDialog.setPositiveButton(context.getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (isSuccess && pageType.equalsIgnoreCase("1")) {
                    checkWalletPayment(context, message);

                } else {
                    closeThisActivity();
                }
            }
        });
        alertDialog.show();
    }

    private void checkWalletPayment(final Context context, String message) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.checkOnlineWalletCredit(context, message, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof StatusModel) {
                    StatusModel statusModel = (StatusModel) arg0;
                    displayCheckPaymentDialog(context, statusModel);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "OnlineWalletCredit");

    }

    public void displayCheckPaymentDialog(Context context, final StatusModel statusModel) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(context.getString(R.string.app_name));
        alertDialog.setMessage(statusModel.getMessage());

        alertDialog.setPositiveButton(context.getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (pageType.equalsIgnoreCase("1") && statusModel.isStatus()) {

                    MyWalletActivity.closeThisActivity();
                    Intent intent = new Intent(PaymentMethodActivity.this, MainActivity.class);
                    startActivity(intent);
                    closeThisActivity();

//                    showStatusPopup();
                } else {
                    closeThisActivity();
                }
            }
        });
        alertDialog.show();
    }

}