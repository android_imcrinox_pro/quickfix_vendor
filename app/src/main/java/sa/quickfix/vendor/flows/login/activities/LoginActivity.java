package sa.quickfix.vendor.flows.login.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import sa.quickfix.vendor.flows.registration.activities.RegisterActivity;

import com.facebook.FacebookSdk;

import sa.quickfix.vendor.R;


public class LoginActivity extends AppCompatActivity {

    public static final String TAG = "LoginActivity";

    Context context;
    private static Activity activity;
    View mainView;

    Toolbar toolbar;
    ActionBar actionBar;

    TextView login_with_mobile_number_tv, login_with_password_tv, register_tv;
    CardView register_card_view;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login_activity);

        context = this;
        activity = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        login_with_mobile_number_tv = (TextView) findViewById(R.id.login_with_mobile_number_tv);
        login_with_password_tv = (TextView) findViewById(R.id.login_with_password_tv);
        register_tv = (TextView) findViewById(R.id.register_tv);

        register_card_view = (CardView) findViewById(R.id.register_card_view);

        mainView = login_with_mobile_number_tv;

        login_with_mobile_number_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, LoginWithPhoneNumberActivity.class);
                startActivity(intent);
            }
        });

        login_with_password_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, LoginWithPasswordActivity.class);
                startActivity(intent);
            }
        });

        register_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}