package sa.quickfix.vendor.flows.contact_us.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.ContactPostModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.services.model.UtilityModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;

import com.facebook.FacebookSdk;


public class ContactUsActivity extends AppCompatActivity {

    public static final String TAG = "ContactUsActivity";

    Context context;
    private static Activity activity;
    View mainView;

    GlobalFunctions globalFunctions;
    GlobalVariables globalVariables;

    private CardView send_card_view;
    private ImageView back_iv;
    private EditText subject_etv, comments_etv;
    private TextView email_tv, mobile_number_tv;
    String phnNumber, mailId = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.contact_us_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);

        subject_etv = (EditText) findViewById(R.id.subject_etv);
        comments_etv = (EditText) findViewById(R.id.comments_etv);

        email_tv = (TextView) findViewById(R.id.email_tv);
        mobile_number_tv = (TextView) findViewById(R.id.mobile_number_tv);

        send_card_view = (CardView) findViewById(R.id.send_card_view);

        mainView = send_card_view;

        getUtility();

        send_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        mobile_number_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GlobalFunctions.isNotNullValue(phnNumber)) {
                    GlobalFunctions.callPhone(activity, phnNumber);
                }
            }
        });

        email_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GlobalFunctions.isNotNullValue(mailId)) {
                    GlobalFunctions.sendMail(activity, mailId, "", "");
                }
            }
        });

    }

    private void getUtility() {
        globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getUtility(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                Log.d(TAG, "Response : " + arg0.toString());
                globalFunctions.hideProgress();
                if (arg0 instanceof UtilityModel) {
                    UtilityModel utilityModel = (UtilityModel) arg0;
                    setThisPage(utilityModel);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "GetUtility");
    }

    private void setThisPage(final UtilityModel utilityModel) {

        if (utilityModel != null) {
            if (utilityModel.getContactNumber() != null) {
                mobile_number_tv.setText(utilityModel.getContactNumber());
                this.phnNumber = utilityModel.getContactNumber();
            }

            if (utilityModel.getEmailId() != null) {
                email_tv.setText(utilityModel.getEmailId());
                this.mailId = utilityModel.getEmailId();
            }

        }
    }

    private void validateInput() {
        String
                subject = subject_etv.getText().toString().trim(),
                comments = comments_etv.getText().toString().trim();

        if (subject.isEmpty()) {
            subject_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
            subject_etv.setFocusableInTouchMode(true);
            subject_etv.requestFocus();
        } else if (comments.isEmpty()) {
            comments_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
            comments_etv.setFocusableInTouchMode(true);
            comments_etv.requestFocus();
        } else {

            ContactPostModel contactPostModel = new ContactPostModel();
            contactPostModel.setSubject(subject);
            contactPostModel.setText(comments);
            contactUs(context, contactPostModel);
        }
    }

    private void contactUs(final Context context, final ContactPostModel model) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.contactUs(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "ContactUs");
    }

    private void validateOutput(Object model) {
        if (model instanceof StatusModel) {
            final StatusModel statusModel = (StatusModel) model;

            final AlertDialog alertDialog = new AlertDialog(activity);
            alertDialog.setCancelable(false);
            alertDialog.setIcon(R.drawable.ic_logo_dark);
            alertDialog.setTitle(activity.getString(R.string.app_name));
            alertDialog.setMessage(statusModel.getMessage());
            alertDialog.setPositiveButton(activity.getString(R.string.ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (statusModel.isStatus()) {
                        Intent intent = new Intent(ContactUsActivity.this, MainActivity.class);
                        startActivity(intent);
                        closeThisActivity();
                    }
                }
            });

            alertDialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}