package sa.quickfix.vendor.flows.reset_password.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.ForgotPasswordModel;
import sa.quickfix.vendor.services.model.RegisterModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;

import com.facebook.FacebookSdk;


public class ResetPasswordActivity extends AppCompatActivity {

    public static final String TAG = "ResetPasswordActivity";

    public static final String BUNDLE_REGISTER_MODEL = "BundleRegisterModel";

    Context context;
    private static Activity activity;
    View mainView;

    private TextView submit_tv;
    private EditText new_password_etv, confirm_password_etv;
    private ImageView back_iv;
    String phoneNumber = null;

    RegisterModel registerModel = null;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    public static Intent newInstance(Context context, RegisterModel registerModel) {
        Intent intent = new Intent(context, ResetPasswordActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_REGISTER_MODEL, registerModel);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.reset_password_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        if (getIntent().hasExtra(BUNDLE_REGISTER_MODEL)) {
            registerModel = (RegisterModel) getIntent().getSerializableExtra(BUNDLE_REGISTER_MODEL);
        } else {
            registerModel = null;
        }

        if(registerModel != null){
            if(registerModel.getMobileNumber() != null){
                phoneNumber = registerModel.getMobileNumber();
            }
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);

        submit_tv = (TextView) findViewById(R.id.submit_tv);

        new_password_etv = (EditText) findViewById(R.id.new_password_etv);
        confirm_password_etv = (EditText) findViewById(R.id.confirm_new_password_etv);

        mainView = submit_tv;

        submit_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
             /*   Intent intent = new Intent(ResetPasswordActivity.this, PasswordResetSuccessActivity.class);
                startActivity(intent);*/
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void validateInput() {
        if (phoneNumber != null && new_password_etv != null) {

            String
                    password = new_password_etv.getText().toString().trim(),
                    confirm_password = confirm_password_etv.getText().toString().trim();

            if (password.isEmpty()) {
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.pleaseFillMandatoryDetails));
                new_password_etv.setFocusableInTouchMode(true);
                new_password_etv.requestFocus();
            } else if (confirm_password.isEmpty()) {
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.pleaseFillMandatoryDetails));
                confirm_password_etv.setFocusableInTouchMode(true);
                confirm_password_etv.requestFocus();
            } else if (!password.equals(confirm_password)) {
                new_password_etv.setFocusableInTouchMode(true);
                new_password_etv.requestFocus();
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.passwordNotMatched));
            } else if (password.length() < 8) {
                new_password_etv.setFocusableInTouchMode(true);
                new_password_etv.requestFocus();
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.passwordContainsError));
            } else {
                ForgotPasswordModel model = new ForgotPasswordModel();
                model.setMobile(phoneNumber);
                model.setPassword(globalFunctions.md5(password));
                model.setOldPassword(globalFunctions.md5(password));
                changePassword(context, model);
            }
        }
    }

    private void changePassword(final Context context, final ForgotPasswordModel model) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.forgotMyPassword(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                checkChangePasswordAfter(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "ForgotMyPassword");
    }

    private void checkChangePasswordAfter(Object model) {
        if (model instanceof StatusModel) {
            final StatusModel statusModel = (StatusModel) model;
            if (!statusModel.isStatus()) {
                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            } else {
                final AlertDialog alertDialog = new AlertDialog(activity);
                alertDialog.setCancelable(false);
                alertDialog.setIcon(R.drawable.ic_logo_dark);
                alertDialog.setTitle(activity.getString(R.string.app_name));
                alertDialog.setMessage(statusModel.getMessage());
                alertDialog.setPositiveButton(activity.getString(R.string.ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                       // globalFunctions.closeAllActivities();
//                        globalFunctions.logoutApplication();
                        Intent i = new Intent(ResetPasswordActivity.this, PasswordResetSuccessActivity.class);
                        startActivity(i);
                    }
                });
                alertDialog.show();
            }

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}