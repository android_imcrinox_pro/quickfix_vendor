package sa.quickfix.vendor.flows.registration.activities.company_registration;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.cardview.widget.CardView;

import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.addon.PhoneValidator;
import sa.quickfix.vendor.flows.about_us.AboutUsActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.location.LocationListener;
import sa.quickfix.vendor.location.LocationService;
import sa.quickfix.vendor.map.SearchPlaceOnMapActivity;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.AddressModel;
import sa.quickfix.vendor.services.model.CompanyRegisterModel;
import sa.quickfix.vendor.services.model.LocationCheckPostModel;
import sa.quickfix.vendor.services.model.LocationCheckResponseModel;
import sa.quickfix.vendor.services.model.RegisterModel;
import sa.quickfix.vendor.services.model.RegistrationAddressListModel;
import sa.quickfix.vendor.services.model.RegistrationAddressModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.util.PermissionUtils;
import sa.quickfix.vendor.R;

import com.facebook.FacebookSdk;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CompanyRegistrationActivity extends AppCompatActivity implements LocationListener {

    public static final String TAG = "CompanyRegisterActivity";

    public static final String BUNDLE_COMPANY_REGISTER_ACTIVITY_REGISTER_MODEL = "BundleCompanyRegisterActivityRegisterModel";
    public static final String BUNDLE_COMPANY_REGISTER_ACTIVITY_COMPANY_REGISTER_MODEL = "BundleCompanyRegisterActivityCompanyRegisterModel";

    private static final int PERMISSION_REQUEST_CODE = 200;

    private static final int
            LOCATION_PERMISSION_REQUEST_CODE = 132,
            PERMISSION_ACCESS_COARSE_LOCATION = 156;

    static Intent locationintent;
    LocationService locationService;

    AddressModel headAddressModel = new AddressModel();
    AddressModel branchAddressModel = new AddressModel();

    CompanyRegisterModel companyRegisterModel = null;

    RegistrationAddressListModel registrationAddressListModel = null;
    RegistrationAddressModel headOfficeAddressModel = null;
    RegistrationAddressModel branchOfficeAddressModel = null;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    Context context;
    private static Activity activity;
    View mainView;

    RegisterModel myRegisterModel = null;
    CompanyRegisterModel myCompanyRegisterModel = null;

    List<Uri>
            uriProfileImageList,
            uriScientificDocImageList,
            uriLicenceImageList;

    private CardView continue_cardview;
    private TextView head_office_address_tv, branch_address_tv,terms_and_conditions_tv,privacy_policy_tv;
    private AppCompatCheckBox terms_checkbox;
    private EditText manager_contact_number_etv, company_name_etv, head_office_landline_no_etv, manager_name_etv, manager_email_id_etv,
            commercial_registration_etv, branch_landline_no_etv, account_number_etv, iban_etv, holder_name_etv, bank_branch_address_etv, bank_name_etv, swift_code_etv;

    public static Intent newInstance(Context context, RegisterModel model) {
        Intent intent = new Intent(context, CompanyRegistrationActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_COMPANY_REGISTER_ACTIVITY_REGISTER_MODEL, model);
        intent.putExtras(args);
        return intent;
    }

    public static Intent newInstance(Context context, CompanyRegisterModel companyRegisterModel) {
        Intent intent = new Intent(context, CompanyRegistrationActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_COMPANY_REGISTER_ACTIVITY_COMPANY_REGISTER_MODEL, companyRegisterModel);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.company_registration_activity);

        context = this;
        activity = this;

        uriProfileImageList = new ArrayList<>();
        uriScientificDocImageList = new ArrayList<>();
        uriLicenceImageList = new ArrayList<>();

        uriProfileImageList.clear();
        uriScientificDocImageList.clear();
        uriLicenceImageList.clear();

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        }

        locationintent = new Intent(LocationService.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            locationintent.setPackage(activity.getPackageName());
        }
        if (locationService != null) {
//            GlobalFunctions.showProgress(activity, activity.getString(R.string.fetching_current_location));
            startLocationService();
        }

        locationService = new LocationService();
        locationService.setListener(this);

        if (getIntent().hasExtra(BUNDLE_COMPANY_REGISTER_ACTIVITY_REGISTER_MODEL)) {
            myRegisterModel = (RegisterModel) getIntent().getSerializableExtra(BUNDLE_COMPANY_REGISTER_ACTIVITY_REGISTER_MODEL);
        } else {
            myRegisterModel = null;
        }

        if (getIntent().hasExtra(BUNDLE_COMPANY_REGISTER_ACTIVITY_COMPANY_REGISTER_MODEL)) {
            myCompanyRegisterModel = (CompanyRegisterModel) getIntent().getSerializableExtra(BUNDLE_COMPANY_REGISTER_ACTIVITY_COMPANY_REGISTER_MODEL);
        } else {
            myCompanyRegisterModel = null;
        }

        Intent i = getIntent();
        uriProfileImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE);
        uriScientificDocImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_SCIENTIFIC_DOC_LIST_CODE);
        uriLicenceImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_LICENSE_PHOTO_LIST_CODE);

        continue_cardview = (CardView) findViewById(R.id.continue_cardview);
        terms_checkbox = (AppCompatCheckBox) findViewById(R.id.terms_checkbox);

        company_name_etv = (EditText) findViewById(R.id.company_name_etv);
        head_office_landline_no_etv = (EditText) findViewById(R.id.head_office_landline_no_etv);
        manager_name_etv = (EditText) findViewById(R.id.manager_name_etv);
        manager_email_id_etv = (EditText) findViewById(R.id.manager_email_id_etv);
        manager_contact_number_etv = (EditText) findViewById(R.id.manager_contact_number_etv);
        commercial_registration_etv = (EditText) findViewById(R.id.commercial_registration_etv);
        branch_landline_no_etv = (EditText) findViewById(R.id.branch_landline_no_etv);
        account_number_etv = (EditText) findViewById(R.id.account_number_etv);
        iban_etv = (EditText) findViewById(R.id.iban_etv);
        holder_name_etv = (EditText) findViewById(R.id.holder_name_etv);
        bank_branch_address_etv = (EditText) findViewById(R.id.bank_branch_address_etv);
        bank_name_etv = (EditText) findViewById(R.id.bank_name_etv);
        swift_code_etv = (EditText) findViewById(R.id.swift_code_etv);

        head_office_address_tv = (TextView) findViewById(R.id.head_office_address_tv);
        branch_address_tv = (TextView) findViewById(R.id.branch_address_tv);

        terms_and_conditions_tv = (TextView) findViewById(R.id.terms_and_conditions_tv);
        privacy_policy_tv = (TextView) findViewById(R.id.privacy_policy_tv);

        mainView = continue_cardview;

        if (myRegisterModel != null) {
            if (myRegisterModel.getMobileNumber() != null) {
                manager_contact_number_etv.setText(myRegisterModel.getMobileNumber());
            }
        }

        if (myCompanyRegisterModel != null) {
            setEnteredData(myCompanyRegisterModel);
        }

        head_office_address_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    return;
                } else if (!sLocationEnabled(activity)) {
                    showSettingsAlert();
                } else {
                    globalFunctions.showProgress(activity, getString(R.string.fetching_location));
                    Intent mIntent = SearchPlaceOnMapActivity.newInstance(context, headAddressModel, GlobalVariables.LOCATION_TYPE.NONE);
                    startActivityForResult(mIntent, GlobalVariables.REQUEST_HEAD_OFFICE_LOCATION_CODE);
                }
            }
        });

        branch_address_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    return;
                } else if (!sLocationEnabled(activity)) {
                    showSettingsAlert();
                } else {
                    globalFunctions.showProgress(activity, getString(R.string.fetching_location));
                    Intent mIntent = SearchPlaceOnMapActivity.newInstance(context, branchAddressModel, GlobalVariables.LOCATION_TYPE.NONE);
                    startActivityForResult(mIntent, GlobalVariables.REQUEST_BRANCH_ADDRESS_LOCATION_CODE);
                }
            }
        });

        continue_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

        terms_and_conditions_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = AboutUsActivity.newInstance(activity, GlobalVariables.PAGE_LOGIN_TNC);
                startActivity(intent);
//                GlobalFunctions.openBrowser(context,GlobalVariables.HTTP_URL_TERMS_AND_CONDITION);
            }
        });
        privacy_policy_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = AboutUsActivity.newInstance(activity, GlobalVariables.PAGE_PP);
                startActivity(intent);
//                GlobalFunctions.openBrowser(context,GlobalVariables.HTTP_URL_TERMS_AND_CONDITION);
            }
        });

    }

    public boolean sLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);

// Setting Dialog Title
        alertDialog.setTitle("GPS settings");

// Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

// Setting Icon to Dialog
//alertDialog.setIcon(R.drawable.delete);

// On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivity(intent);
            }
        });

// on pressing cancel button
        alertDialog.setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

// Showing Alert Message
        alertDialog.show();
    }

    private void setEnteredData(CompanyRegisterModel mCompanyRegisterModel) {

        if (mCompanyRegisterModel != null) {

            if (mCompanyRegisterModel.getManagerMobile() != null) {
                manager_contact_number_etv.setText(mCompanyRegisterModel.getManagerMobile());
            }

            if (mCompanyRegisterModel.getCompanyName() != null) {
                company_name_etv.setText(mCompanyRegisterModel.getCompanyName());
            }

            if (mCompanyRegisterModel.getManagerName() != null) {
                manager_name_etv.setText(mCompanyRegisterModel.getManagerName());
            }

            if (mCompanyRegisterModel.getManagerEmail() != null) {
                manager_email_id_etv.setText(mCompanyRegisterModel.getManagerEmail());
            }

            if (mCompanyRegisterModel.getRegistrationNumber() != null) {
                commercial_registration_etv.setText(mCompanyRegisterModel.getRegistrationNumber());
            }

            if (mCompanyRegisterModel.getAccountNumber() != null) {
                account_number_etv.setText(mCompanyRegisterModel.getAccountNumber());
            }

            if (mCompanyRegisterModel.getIban() != null) {
                iban_etv.setText(mCompanyRegisterModel.getIban());
            }

           /* if (mCompanyRegisterModel.getAccountHolderName() != null) {
                holder_name_etv.setText(mCompanyRegisterModel.getAccountHolderName());
            }

            if (mCompanyRegisterModel.getBranch() != null) {
                bank_branch_address_etv.setText(mCompanyRegisterModel.getBranch());
            }

            if (mCompanyRegisterModel.getBankName() != null) {
                bank_name_etv.setText(mCompanyRegisterModel.getBankName());
            }

            if (mCompanyRegisterModel.getSwiftCode() != null) {
                swift_code_etv.setText(mCompanyRegisterModel.getSwiftCode());
            }*/

            terms_checkbox.setChecked(true);

            if (mCompanyRegisterModel.getAddressList() != null) {
                if (mCompanyRegisterModel.getAddressList().getAddressList() != null) {

                    if (mCompanyRegisterModel.getAddressList().getAddressList().size() > 0) {

                        if (mCompanyRegisterModel.getAddressList().getAddressList().get(0) != null) {

                            if (headOfficeAddressModel == null) {
                                headOfficeAddressModel = new RegistrationAddressModel();
                            }
                            head_office_address_tv.setText(mCompanyRegisterModel.getAddressList().getAddressList().get(0).getAddress());
                            headOfficeAddressModel.setAddress(mCompanyRegisterModel.getAddressList().getAddressList().get(0).getAddress());
                            headOfficeAddressModel.setContactNumber("");
                            headOfficeAddressModel.setLatitude(mCompanyRegisterModel.getAddressList().getAddressList().get(0).getLatitude());
                            headOfficeAddressModel.setLongitude(mCompanyRegisterModel.getAddressList().getAddressList().get(0).getLongitude());
                            headOfficeAddressModel.setType(globalVariables.TYPE_HEAD_OFFICE_ADDRESS);
                            if (mCompanyRegisterModel.getAddressList().getAddressList().get(0).getCity() != null) {
                                headOfficeAddressModel.setCity(mCompanyRegisterModel.getAddressList().getAddressList().get(0).getCity());
                            }
                            if (mCompanyRegisterModel.getAddressList().getAddressList().get(0).getLandline() != null) {
                                head_office_landline_no_etv.setText(mCompanyRegisterModel.getAddressList().getAddressList().get(0).getLandline());
                            }

                            if (mCompanyRegisterModel.getAddressList().getAddressList().get(0).getAddress() != null) {
                                if (headAddressModel == null) {
                                    headAddressModel = new AddressModel();
                                }
                                headAddressModel.setLatitude(mCompanyRegisterModel.getAddressList().getAddressList().get(0).getLatitude());
                                headAddressModel.setLongitude(mCompanyRegisterModel.getAddressList().getAddressList().get(0).getLongitude());
                            }
                        }

                        if (mCompanyRegisterModel.getAddressList().getAddressList().size() == 2) {

                            if (mCompanyRegisterModel.getAddressList().getAddressList().get(1) != null) {

                                if (branchOfficeAddressModel == null) {
                                    branchOfficeAddressModel = new RegistrationAddressModel();
                                }
                                branch_address_tv.setText(mCompanyRegisterModel.getAddressList().getAddressList().get(1).getAddress());
                                branchOfficeAddressModel.setAddress(mCompanyRegisterModel.getAddressList().getAddressList().get(1).getAddress());
                                branchOfficeAddressModel.setContactNumber("");
                                branchOfficeAddressModel.setLatitude(mCompanyRegisterModel.getAddressList().getAddressList().get(1).getLatitude());
                                branchOfficeAddressModel.setLongitude(mCompanyRegisterModel.getAddressList().getAddressList().get(1).getLongitude());
                                branchOfficeAddressModel.setType(globalVariables.TYPE_BRANCH_OFFICE_ADDRESS);
                                if (mCompanyRegisterModel.getAddressList().getAddressList().get(1).getCity() != null) {
                                    branchOfficeAddressModel.setCity(mCompanyRegisterModel.getAddressList().getAddressList().get(1).getCity());
                                }

                                if (mCompanyRegisterModel.getAddressList().getAddressList().get(1).getLandline() != null) {
                                    branch_landline_no_etv.setText(mCompanyRegisterModel.getAddressList().getAddressList().get(1).getLandline());
                                }

                                if (mCompanyRegisterModel.getAddressList().getAddressList().get(1).getAddress() != null) {
                                    if (branchAddressModel == null) {
                                        branchAddressModel = new AddressModel();
                                    }
                                    branchAddressModel.setLatitude(mCompanyRegisterModel.getAddressList().getAddressList().get(1).getLatitude());
                                    branchAddressModel.setLongitude(mCompanyRegisterModel.getAddressList().getAddressList().get(1).getLongitude());
                                }
                            }
                        }
                    }

                }
            }
        }
    }

    private void validateInput() {
        if (context != null) {
            String
                    companyName = company_name_etv.getText().toString().trim(),
                    headOfficeAddress = head_office_address_tv.getText().toString().trim(),
                    headOfficeLandlineNo = head_office_landline_no_etv.getText().toString().trim(),
                    managerName = manager_name_etv.getText().toString().trim(),
                    managerEmailId = manager_email_id_etv.getText().toString().trim(),
                    managerContactNumber = manager_contact_number_etv.getText().toString().trim(),
                    registrationNumber = commercial_registration_etv.getText().toString().trim(),
                    branchLandlineNo = branch_landline_no_etv.getText().toString().trim(),
                    branchAddress = branch_address_tv.getText().toString().trim(),
                    accountNumber = account_number_etv.getText().toString().trim(),
                    iban = iban_etv.getText().toString().trim();
//                    holderName = holder_name_etv.getText().toString().trim(),
//                    bankBranchAddress = bank_branch_address_etv.getText().toString().trim(),
//                    bankName = bank_name_etv.getText().toString().trim(),
//                    swiftCode = swift_code_etv.getText().toString().trim();

            if (companyName.isEmpty()) {
                company_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                company_name_etv.setFocusableInTouchMode(true);
                company_name_etv.requestFocus();
            } else if (headOfficeAddress.isEmpty()) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_add_head_office_address));
            } else if (headOfficeLandlineNo.isEmpty()) {
                head_office_landline_no_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                head_office_landline_no_etv.setFocusableInTouchMode(true);
                head_office_landline_no_etv.requestFocus();
            }else if (!headOfficeLandlineNo.isEmpty() && !new PhoneValidator().validate(headOfficeLandlineNo)) {
                globalFunctions.displayMessaage(activity,mainView,getString(R.string.pleaseEnterValidMobileNumber));
//                phone_number_etv.setError(getString(R.string.pleaseEnterValidMobileNumber));
                head_office_landline_no_etv.setFocusableInTouchMode(true);
                head_office_landline_no_etv.requestFocus();
            } else if (managerName.isEmpty()) {
                manager_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                manager_name_etv.setFocusableInTouchMode(true);
                manager_name_etv.requestFocus();
            } else if (managerEmailId.isEmpty()) {
                manager_email_id_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                manager_email_id_etv.setFocusableInTouchMode(true);
                manager_email_id_etv.requestFocus();
            }else if (!GlobalFunctions.isEmailValid(managerEmailId)) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.emailNotValid));
                manager_email_id_etv.setFocusableInTouchMode(true);
                manager_email_id_etv.requestFocus();
            } else if (managerContactNumber.isEmpty()) {
                manager_contact_number_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                manager_contact_number_etv.setFocusableInTouchMode(true);
                manager_contact_number_etv.requestFocus();
            } else if (registrationNumber.isEmpty()) {
                commercial_registration_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                commercial_registration_etv.setFocusableInTouchMode(true);
                commercial_registration_etv.requestFocus();
            } else if (!branchLandlineNo.isEmpty() && branchAddress.isEmpty()) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_add_branch_address));
            } else if (branchLandlineNo.isEmpty() && !branchAddress.isEmpty()) {
                branch_landline_no_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                branch_landline_no_etv.setFocusableInTouchMode(true);
                branch_landline_no_etv.requestFocus();
            }else if (!branchLandlineNo.isEmpty() && !new PhoneValidator().validate(branchLandlineNo)) {
                globalFunctions.displayMessaage(activity,mainView,getString(R.string.pleaseEnterValidMobileNumber));
//                phone_number_etv.setError(getString(R.string.pleaseEnterValidMobileNumber));
                branch_landline_no_etv.setFocusableInTouchMode(true);
                branch_landline_no_etv.requestFocus();
            } else if (accountNumber.isEmpty()) {
                account_number_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                account_number_etv.setFocusableInTouchMode(true);
                account_number_etv.requestFocus();
            } else if (iban.isEmpty()) {
                iban_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                iban_etv.setFocusableInTouchMode(true);
                iban_etv.requestFocus();
            } /*else if (holderName.isEmpty()) {
                holder_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                holder_name_etv.setFocusableInTouchMode(true);
                holder_name_etv.requestFocus();
            } else if (bankBranchAddress.isEmpty()) {
                bank_branch_address_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                bank_branch_address_etv.setFocusableInTouchMode(true);
                bank_branch_address_etv.requestFocus();
            } else if (bankName.isEmpty()) {
                bank_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                bank_name_etv.setFocusableInTouchMode(true);
                bank_name_etv.requestFocus();
            } else if (swiftCode.isEmpty()) {
                swift_code_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                swift_code_etv.setFocusableInTouchMode(true);
                swift_code_etv.requestFocus();
            }*/ else if (!terms_checkbox.isChecked()) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_select_terms_and_conditions));
            } else {

                if (companyRegisterModel == null) {
                    companyRegisterModel = new CompanyRegisterModel();
                }

                companyRegisterModel.setRegistrationNumber(registrationNumber);
                companyRegisterModel.setCompanyName(companyName);
                companyRegisterModel.setManagerName(managerName);
                companyRegisterModel.setManagerMobile(managerContactNumber);
                companyRegisterModel.setManagerEmail(managerEmailId);
//                companyRegisterModel.setBankName(bankName);
//                companyRegisterModel.setAccountHolderName(holderName);
//                companyRegisterModel.setBranch(bankBranchAddress);
                companyRegisterModel.setIban(iban);
                companyRegisterModel.setAccountNumber(accountNumber);
//                companyRegisterModel.setSwiftCode(swiftCode);

                if (headOfficeAddressModel != null) {
                    headOfficeAddressModel.setLandline(headOfficeLandlineNo);
                }

                List<RegistrationAddressModel> addressList = new ArrayList<RegistrationAddressModel>();
                addressList.add(headOfficeAddressModel);
                if (branchOfficeAddressModel != null) {
                    branchOfficeAddressModel.setLandline(branchLandlineNo);
                    addressList.add(branchOfficeAddressModel);
                }
                if (registrationAddressListModel == null) {
                    registrationAddressListModel = new RegistrationAddressListModel();
                }
                registrationAddressListModel.setAddressList(addressList);
                companyRegisterModel.setAddressList(registrationAddressListModel);

                if (myCompanyRegisterModel != null) {
                    //from edit....snd uris also....
                    if(myCompanyRegisterModel.getAboutCompany() != null){
                        companyRegisterModel.setAboutCompany(myCompanyRegisterModel.getAboutCompany());
                    }
                    Intent intent = CompanyRegistrationUploadDocumentsActivity.newInstance(activity, companyRegisterModel, globalVariables.FROM_PAGE_REVIEW_COMPANY_REGISTRATION);
                    intent.putExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE, (Serializable) uriProfileImageList);
                    intent.putExtra(GlobalVariables.UPLOAD_SCIENTIFIC_DOC_LIST_CODE, (Serializable) uriScientificDocImageList);
                    intent.putExtra(GlobalVariables.UPLOAD_LICENSE_PHOTO_LIST_CODE, (Serializable) uriLicenceImageList);
                    startActivity(intent);
                } else {
                    //normal...
                    Intent intent = CompanyRegistrationUploadDocumentsActivity.newInstance(activity, companyRegisterModel);
                    startActivity(intent);
                }
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        GlobalFunctions.hideProgress();
        if (resultCode == activity.RESULT_OK && requestCode == globalVariables.REQUEST_HEAD_OFFICE_LOCATION_CODE) {
            if (headAddressModel == null) {
                headAddressModel = new AddressModel();
            }
            headAddressModel = (AddressModel) data.getExtras().getSerializable(SearchPlaceOnMapActivity.BUNDLE_SEARCH_PLACE_ON_MAP_ACTIVITY_ADDRESS_MODEL);
            setAddress(headAddressModel, globalVariables.FROM_HEAD_OFFICE_ADDRESS);
        } else if (resultCode == activity.RESULT_OK && requestCode == globalVariables.REQUEST_BRANCH_ADDRESS_LOCATION_CODE) {
            if (branchAddressModel == null) {
                branchAddressModel = new AddressModel();
            }
            branchAddressModel = (AddressModel) data.getExtras().getSerializable(SearchPlaceOnMapActivity.BUNDLE_SEARCH_PLACE_ON_MAP_ACTIVITY_ADDRESS_MODEL);
            setAddress(branchAddressModel, globalVariables.FROM_BRANCH_OFFICE_ADDRESS);
        }

    }

    private void setAddress(AddressModel pickUpAddressModel, String from) {

        if (pickUpAddressModel != null) {
            checkLocation(pickUpAddressModel.getLatitude(), pickUpAddressModel.getLongitude(), from, pickUpAddressModel);
        }
    }

    private void checkLocation(double latitude, double longitude, String from, AddressModel pickUpAddressModel) {
        LocationCheckPostModel locationCheckPostModel = new LocationCheckPostModel();
        locationCheckPostModel.setLatitude(latitude + "");
        locationCheckPostModel.setLongitude(longitude + "");
        checkSelectedLocation(context, locationCheckPostModel, from, pickUpAddressModel);
    }

    private void checkSelectedLocation(final Context context, final LocationCheckPostModel model, final String from, final AddressModel pickUpAddressModel) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.checkLocation(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateLocation(arg0, pickUpAddressModel, from);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "Login_User");
    }

    private void validateLocation(Object model, AddressModel addressModel, String from) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            if (!statusModel.isStatus()) {
                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            }
        } else {
            LocationCheckResponseModel locationCheckResponseModel = (LocationCheckResponseModel) model;
            if (locationCheckResponseModel != null) {

                if (from != null) {
                    if (from.equalsIgnoreCase(globalVariables.FROM_HEAD_OFFICE_ADDRESS)) {
                        if (headOfficeAddressModel == null) {
                            headOfficeAddressModel = new RegistrationAddressModel();
                        }
                        if (addressModel != null) {
                            head_office_address_tv.setText(addressModel.getAddress());
                            headOfficeAddressModel.setAddress(addressModel.getAddress());
                            headOfficeAddressModel.setContactNumber("");
                            headOfficeAddressModel.setLatitude(addressModel.getLatitude());
                            headOfficeAddressModel.setLongitude(addressModel.getLongitude());
                            headOfficeAddressModel.setType(globalVariables.TYPE_HEAD_OFFICE_ADDRESS);
                            if (locationCheckResponseModel.getId() != null) {
                                headOfficeAddressModel.setCity(locationCheckResponseModel.getId());
                            }
                        }
                    } else if (from.equalsIgnoreCase(globalVariables.FROM_BRANCH_OFFICE_ADDRESS)) {
                        if (branchOfficeAddressModel == null) {
                            branchOfficeAddressModel = new RegistrationAddressModel();
                        }
                        if (addressModel != null) {
                            branch_address_tv.setText(addressModel.getAddress());
                            branchOfficeAddressModel.setAddress(addressModel.getAddress());
                            branchOfficeAddressModel.setContactNumber("");
                            branchOfficeAddressModel.setLatitude(addressModel.getLatitude());
                            branchOfficeAddressModel.setLongitude(addressModel.getLongitude());
                            branchOfficeAddressModel.setType(globalVariables.TYPE_BRANCH_OFFICE_ADDRESS);
                            if (locationCheckResponseModel.getId() != null) {
                                branchOfficeAddressModel.setCity(locationCheckResponseModel.getId());
                            }
                        }
                    }
                }
            }

        }
    }


    @Override
    public void onBackPressed() {
        closeThisActivity();
        super.onBackPressed();
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void OnLocationFetch(Location location) {
        globalFunctions.hideProgress();
        setLocation();
    }

    private void setLocation() {
        Address address = globalFunctions.getAddressfromLatLng(activity, locationService.getLatitude(), locationService.getLongitude());

        //  pickUpAddressModel = globalFunctions.getAddressModelFromAddress(address);

      /*  String completeAddress = null;
        try {
            if (pickUpAddressModel.getAddress() != null) {
                completeAddress = pickUpAddressModel.getAddress();
            } else {
                completeAddress = globalFunctions.getAddressTextFromModelExcludingNameAndAddresswithComma(context, pickUpAddressModel);
            }
            // head_office_address_tv.setText(completeAddress);
        } catch (Exception e) {
            globalFunctions.displayMessaage(activity, mainView, getString(R.string.location_error));
        }*/
        stopLocationService();
    }

    public static void startLocationService() {
        activity.startService(locationintent);
    }

    public static void stopLocationService() {
        activity.stopService(locationintent);
    }

    @Override
    public void OnProviderDisabled(String provider) {
        globalFunctions.hideProgress();
    }

    @Override
    public void OnProviderEnabled(String provider) {

    }

    @Override
    public void OnLocationFailure(String msg) {
        globalFunctions.hideProgress();
    }
}