package sa.quickfix.vendor.flows.registration.activities.company_registration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.flows.registration.activities.RegistrationSuccessActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.LicenceImagesListAdapter;
import sa.quickfix.vendor.adapters.ScientificDocImagesListAdapter;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.CompanyRegisterModel;
import sa.quickfix.vendor.services.model.LicenceImageModel;
import sa.quickfix.vendor.services.model.LicenceImagesListModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.ScientificDocumentModel;
import sa.quickfix.vendor.services.model.ScientificDocumentsListModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.upload.UploadImage;
import sa.quickfix.vendor.upload.UploadListener;
import sa.quickfix.vendor.R;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CompanyReviewApplicationActivity extends AppCompatActivity implements UploadListener {

    public static final String TAG = "CompanyReviewActivity";

    public static final String BUNDLE_COMPANY_REGISTER_MODEL = "BundleCompanyRegisterModel";

    Context context;
    private static Activity activity;
    View mainView;

    ProfileModel mProfileModel = null;

    private CardView submit_cardview;
    private ImageView back_iv;
    private LinearLayout edit_details_ll, branch_address_ll;
    private CircleImageView vendor_iv;
    private TextView vendor_name_tv, company_name_tv, head_office_address_tv, manager_name_tv, manager_email_id_tv, head_office_landline_no_tv, manager_number_tv,
            commercial_registration_tv, account_number_tv, iban_tv, holder_name_tv, branch_tv, bank_name_tv, swift_code_tv, about_header_tv,about_tv, branch_landline_number_tv, branch_address_tv;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    FirebaseStorage storage;
    StorageReference storageReference;

    List<Uri>
            uriProfileImageList,
            uriScientificDocImageList,
            uriLicenceImageList;

    LinearLayoutManager licenceLayoutManager;
    RecyclerView licence_recycler_view;
    LicenceImagesListAdapter licenceListAdapter;

    LinearLayoutManager scientificDocLayoutManager;
    RecyclerView scientific_documents_recycler_view;
    ScientificDocImagesListAdapter scientificDocListAdapter;

    ScientificDocumentsListModel scientificDocListModel = new ScientificDocumentsListModel();
    List<ScientificDocumentModel> scientificDocList = new ArrayList<>();

    LicenceImagesListModel licenceListModel = new LicenceImagesListModel();
    List<LicenceImageModel> licenceList = new ArrayList<>();

    CompanyRegisterModel companyRegisterModel = null;

    public static Intent newInstance(Context context, CompanyRegisterModel model) {
        Intent intent = new Intent(context, CompanyReviewApplicationActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_COMPANY_REGISTER_MODEL, model);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.company_review_application_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        uriProfileImageList = new ArrayList<>();
        uriScientificDocImageList = new ArrayList<>();
        uriLicenceImageList = new ArrayList<>();

        uriProfileImageList.clear();
        uriScientificDocImageList.clear();
        uriLicenceImageList.clear();

        scientificDocList.clear();
        licenceList.clear();

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        licenceLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        scientificDocLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        back_iv = (ImageView) findViewById(R.id.back_iv);
        vendor_iv = (CircleImageView) findViewById(R.id.vendor_iv);

        licence_recycler_view = (RecyclerView) findViewById(R.id.licence_recycler_view);
        scientific_documents_recycler_view = (RecyclerView) findViewById(R.id.scientific_documents_recycler_view);

        vendor_name_tv = (TextView) findViewById(R.id.vendor_name_tv);
        company_name_tv = (TextView) findViewById(R.id.company_name_tv);
        head_office_address_tv = (TextView) findViewById(R.id.head_office_address_tv);
        manager_name_tv = (TextView) findViewById(R.id.manager_name_tv);
        manager_email_id_tv = (TextView) findViewById(R.id.manager_email_id_tv);
        head_office_landline_no_tv = (TextView) findViewById(R.id.head_office_landline_no_tv);
        manager_number_tv = (TextView) findViewById(R.id.manager_number_tv);
        commercial_registration_tv = (TextView) findViewById(R.id.commercial_registration_tv);
        account_number_tv = (TextView) findViewById(R.id.account_number_tv);
        iban_tv = (TextView) findViewById(R.id.iban_tv);
        holder_name_tv = (TextView) findViewById(R.id.holder_name_tv);
        branch_tv = (TextView) findViewById(R.id.branch_tv);
        bank_name_tv = (TextView) findViewById(R.id.bank_name_tv);
        swift_code_tv = (TextView) findViewById(R.id.swift_code_tv);
        about_header_tv = (TextView) findViewById(R.id.about_header_tv);
        about_tv = (TextView) findViewById(R.id.about_tv);
        branch_landline_number_tv = (TextView) findViewById(R.id.branch_landline_number_tv);
        branch_address_tv = (TextView) findViewById(R.id.branch_address_tv);

        edit_details_ll = (LinearLayout) findViewById(R.id.edit_details_ll);
        branch_address_ll = (LinearLayout) findViewById(R.id.branch_address_ll);

        submit_cardview = (CardView) findViewById(R.id.submit_cardview);

        mainView = submit_cardview;

        if (getIntent().hasExtra(BUNDLE_COMPANY_REGISTER_MODEL)) {
            companyRegisterModel = (CompanyRegisterModel) getIntent().getSerializableExtra(BUNDLE_COMPANY_REGISTER_MODEL);
        }

        Intent i = getIntent();
        uriProfileImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE);
        uriScientificDocImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_SCIENTIFIC_DOC_LIST_CODE);
        uriLicenceImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_LICENSE_PHOTO_LIST_CODE);

        initLicenceImageList();

        initScientificDocImageList();

        if (companyRegisterModel != null) {
            //set this page...
            setThisPage(companyRegisterModel);
        }

        submit_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage(GlobalVariables.UPLOAD_PROFILE_PHOTO_PATH_CODE);
            }
        });

        edit_details_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = CompanyRegistrationActivity.newInstance(activity, companyRegisterModel);
                intent.putExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE, (Serializable) uriProfileImageList);
                intent.putExtra(GlobalVariables.UPLOAD_SCIENTIFIC_DOC_LIST_CODE, (Serializable) uriScientificDocImageList);
                intent.putExtra(GlobalVariables.UPLOAD_LICENSE_PHOTO_LIST_CODE, (Serializable) uriLicenceImageList);
                activity.startActivity(intent);
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void uploadImage(String type) {
        if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_PROFILE_PHOTO_PATH_CODE)) {
            if (uriProfileImageList != null) {
                if (uriProfileImageList.size() != 0) {
                    globalFunctions.showProgress(activity, context.getString(R.string.uploading_images));
                    for (int j = 0; j < uriProfileImageList.size(); j++) {
                        Uri uri = (uriProfileImageList.get(j));
                        UploadImage uploadImage = new UploadImage(context);
                        uploadImage.startUploading(uri, type, "image", this);
                    }
                } else {
                    uploadImage(GlobalVariables.UPLOAD_LICENSE_PHOTO_PATH_CODE);
                }
            } else {
                uploadImage(GlobalVariables.UPLOAD_LICENSE_PHOTO_PATH_CODE);
            }
        } else if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_LICENSE_PHOTO_PATH_CODE)) {
            if (uriLicenceImageList != null) {
                if (uriLicenceImageList.size() != 0) {
                    // globalFunctions.showProgress(activity, context.getString(R.string.uploading_images));
                    for (int j = 0; j < uriLicenceImageList.size(); j++) {
                        Uri uri = (uriLicenceImageList.get(j));
                        UploadImage uploadImage = new UploadImage(context);
                        uploadImage.startUploading(uri, type, "image", this);
                    }
                } else {
                    uploadImage(GlobalVariables.UPLOAD_SCIENTIFIC_DOC_PHOTO_PATH_CODE);
                }
            } else {
                uploadImage(GlobalVariables.UPLOAD_SCIENTIFIC_DOC_PHOTO_PATH_CODE);
            }
        } else if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_SCIENTIFIC_DOC_PHOTO_PATH_CODE)) {
            if (uriScientificDocImageList != null) {

                if (uriScientificDocImageList.size() != 0) {
//                GlobalFunctions.showProgress(activity, context.getString(R.string.uploading_images));
                    for (int j = 0; j < uriScientificDocImageList.size(); j++) {
                        Uri uri = (uriScientificDocImageList.get(j));
                        UploadImage uploadImage = new UploadImage(context);
                        uploadImage.startUploading(uri, type, "image", this);
                    }
                } else {
                    //update the profile...
                /*if (mProfileModel != null) {
                    //from edit profile....update profile...
                    GalleryListModel mGalleryListModel1 = new GalleryListModel();
                    mProfileModel.setGalleryList(mGalleryListModel1);
                    updateProfile(context, mProfileModel);
                }*/
                }
            } else {
                //update the profile...
                /*if (mProfileModel != null) {
                    //from edit profile....update profile...
                    GalleryListModel mGalleryListModel1 = new GalleryListModel();
                    mProfileModel.setGalleryList(mGalleryListModel1);
                    updateProfile(context, mProfileModel);
                }*/
            }
        }
    }

    private void initLicenceImageList() {
        licenceListAdapter = new LicenceImagesListAdapter(activity, uriLicenceImageList);
        licence_recycler_view.setLayoutManager(licenceLayoutManager);
        licence_recycler_view.setAdapter(licenceListAdapter);
        synchronized (licenceListAdapter) {
            licenceListAdapter.notifyDataSetChanged();
        }
    }

    private void initScientificDocImageList() {
        scientificDocListAdapter = new ScientificDocImagesListAdapter(activity, uriScientificDocImageList);
        scientific_documents_recycler_view.setLayoutManager(scientificDocLayoutManager);
        scientific_documents_recycler_view.setAdapter(scientificDocListAdapter);
        synchronized (scientificDocListAdapter) {
            scientificDocListAdapter.notifyDataSetChanged();
        }
    }

    private void setThisPage(CompanyRegisterModel companyRegisterModel) {

        if (companyRegisterModel != null) {

            branch_address_ll.setVisibility(View.GONE);

            if (companyRegisterModel.getCompanyName() != null) {
                vendor_name_tv.setText(companyRegisterModel.getCompanyName());
                company_name_tv.setText(companyRegisterModel.getCompanyName());
            }

            if (uriProfileImageList != null) {
                if (uriProfileImageList.size() > 0) {
                    try {
                        Glide.with(this).load(uriProfileImageList.get(0)).into(vendor_iv);
                    } catch (Exception exccc) {

                    }

                }
            }

            if (companyRegisterModel.getManagerName() != null) {
                manager_name_tv.setText(companyRegisterModel.getManagerName());
            }

            if (companyRegisterModel.getManagerEmail() != null) {
                manager_email_id_tv.setText(companyRegisterModel.getManagerEmail());
            }

            if (companyRegisterModel.getManagerMobile() != null) {
                manager_number_tv.setText(companyRegisterModel.getManagerMobile());
            }

            if (companyRegisterModel.getRegistrationNumber() != null) {
                commercial_registration_tv.setText(companyRegisterModel.getRegistrationNumber());
            }

            if (companyRegisterModel.getAccountNumber() != null) {
                account_number_tv.setText(companyRegisterModel.getAccountNumber());
            }

            if (companyRegisterModel.getIban() != null) {
                iban_tv.setText(companyRegisterModel.getIban());
            }

           /* if (companyRegisterModel.getAccountHolderName() != null) {
                holder_name_tv.setText(companyRegisterModel.getAccountHolderName());
            }

            if (companyRegisterModel.getBranch() != null) {
                branch_tv.setText(companyRegisterModel.getBranch());
            }

            if (companyRegisterModel.getBankName() != null) {
                bank_name_tv.setText(companyRegisterModel.getBankName());
            }

            if (companyRegisterModel.getSwiftCode() != null) {
                swift_code_tv.setText(companyRegisterModel.getSwiftCode());
            }*/

            if (companyRegisterModel.getCompanyName() != null) {
                about_header_tv.setText(activity.getString(R.string.about)+" "+companyRegisterModel.getCompanyName());
            }
            if (companyRegisterModel.getAboutCompany() != null) {
                about_tv.setText(companyRegisterModel.getAboutCompany());
            }

            if (companyRegisterModel.getAddressList() != null) {
                if (companyRegisterModel.getAddressList().getAddressList() != null) {

                    if (companyRegisterModel.getAddressList().getAddressList().size() > 0) {

                        if (companyRegisterModel.getAddressList().getAddressList().get(0) != null) {

                            if (companyRegisterModel.getAddressList().getAddressList().get(0).getAddress() != null) {
                                head_office_address_tv.setText(companyRegisterModel.getAddressList().getAddressList().get(0).getAddress());
                            }

                            if (companyRegisterModel.getAddressList().getAddressList().get(0).getLandline() != null) {
                                head_office_landline_no_tv.setText(companyRegisterModel.getAddressList().getAddressList().get(0).getLandline());
                            }
                        }

                        if (companyRegisterModel.getAddressList().getAddressList().size() == 2) {

                            if (companyRegisterModel.getAddressList().getAddressList().get(1) != null) {

                                if (companyRegisterModel.getAddressList().getAddressList().get(1).getAddress() != null) {
                                    branch_address_ll.setVisibility(View.VISIBLE);
                                    branch_address_tv.setText(companyRegisterModel.getAddressList().getAddressList().get(1).getAddress());
                                }

                                if (companyRegisterModel.getAddressList().getAddressList().get(1).getLandline() != null) {
                                    branch_landline_number_tv.setText(companyRegisterModel.getAddressList().getAddressList().get(1).getLandline());
                                }
                            }
                        }
                    }

                }
            }


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void OnSuccess(final String imgUrl, final String type, final String uploadingFile) {
       // activity.runOnUiThread(new Runnable() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                globalFunctions.displayMessaage(activity, mainView, "Uploaded");
                // GlobalFunctions.hideProgress();
                if (uploadingFile.equalsIgnoreCase("image")) {
                    setImagesToModel(type, imgUrl);
                }
            }
        });

    }

    private void setImagesToModel(String type, String imagePath) {
        if (type != null) {
            if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_PROFILE_PHOTO_PATH_CODE)) {
                if (mProfileModel != null) {
                    //from edit profile......
                    //  mProfileModel.setProfileImg(imagePath);
                } else {
                    companyRegisterModel.setLogo(imagePath);
                }
                uploadImage(GlobalVariables.UPLOAD_LICENSE_PHOTO_PATH_CODE);
            } else if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_LICENSE_PHOTO_PATH_CODE)) {
                if (mProfileModel != null) {
                    //from edit profile......
                    // mProfileModel.setLicenseImg(imagePath);
                } else {
                    setLicenceImagesList(imagePath);
                }
                uploadImage(GlobalVariables.UPLOAD_SCIENTIFIC_DOC_PHOTO_PATH_CODE);
            } else if (type.equalsIgnoreCase(GlobalVariables.UPLOAD_SCIENTIFIC_DOC_PHOTO_PATH_CODE)) {
                if (mProfileModel != null) {
                    //from edit profile......
                    // mProfileModel.setLicenseImg(imagePath);
                } else {
                    setScientificDocList(imagePath);
                }
            }
        }

    }

    private void setLicenceImagesList(String imagePath) {
        LicenceImageModel licenceImageModel = new LicenceImageModel();
        licenceImageModel.setImage(imagePath);
        licenceList.add(licenceImageModel);
        if (uriLicenceImageList.size() == licenceList.size()) {
            licenceListModel.setLicenceImageList(licenceList);
            if (mProfileModel != null) {
                //from edit profile......
               /* LicenceImagesListModel mLicenceImagesListModel = new LicenceImagesListModel();
                mProfileModel.setGalleryList(mLicenceImagesListModel);
                mProfileModel.setGalleryList(licenceListModel);*/
            } else {
                companyRegisterModel.setLicenceImagesList(licenceListModel);
            }

        } else {

        }
    }

    private void setScientificDocList(String imagePath) {
        ScientificDocumentModel scientificDocumentModel = new ScientificDocumentModel();
        scientificDocumentModel.setImage(imagePath);
        scientificDocList.add(scientificDocumentModel);
        if (uriScientificDocImageList.size() == scientificDocList.size()) {
            scientificDocListModel.setScientificDocumentList(scientificDocList);
            if (mProfileModel != null) {
                //from edit profile......
               /* LicenceImagesListModel mLicenceImagesListModel = new LicenceImagesListModel();
                mProfileModel.setGalleryList(mLicenceImagesListModel);
                mProfileModel.setGalleryList(scientificDocListModel);*/
            } else {
                companyRegisterModel.setScientificDocumentsList(scientificDocListModel);
            }
            GlobalFunctions.hideProgress();

            if (mProfileModel != null) {
                //from edit profile....update profile...
              //  updateProfile(context, mProfileModel);
            } else {
                registerUser(context, companyRegisterModel);
            }

        } else {
            GlobalFunctions.hideProgress();
        }
    }

    private void registerUser(final Context context, final CompanyRegisterModel model) {
        GlobalFunctions.showProgress(context, context.getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.registerAsCompany(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                GlobalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateOutput(arg0, model);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                GlobalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                GlobalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Error : " + msg);
            }
        }, "Register_User");
    }

    private void validateOutput(Object model, CompanyRegisterModel registerModel) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            if (statusModel.isStatus()) {
                globalFunctions.closeAllActivities();
                Intent intent = RegistrationSuccessActivity.newInstance(activity, statusModel);
                activity.startActivity(intent);
                closeThisActivity();
            } else {
                globalFunctions.displayErrorDialog(activity, statusModel.getMessage());
            }

        }
    }

    @Override
    public void OnFailure() {
        GlobalFunctions.hideProgress();
        globalFunctions.displayErrorDialog(activity, context.getString(R.string.failed_upload_image));
    }
}