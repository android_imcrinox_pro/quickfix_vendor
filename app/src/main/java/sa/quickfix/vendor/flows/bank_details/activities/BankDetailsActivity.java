package sa.quickfix.vendor.flows.bank_details.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.R;

import com.facebook.FacebookSdk;


public class BankDetailsActivity extends AppCompatActivity {

    public static final String TAG = "BankDetailsActivity";

    Context context;
    private static Activity activity;
    View mainView;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    private LinearLayout edit_details_ll;
    private ImageView back_iv;
    private TextView holder_name_tv, account_number_tv, iban_tv, bank_name_tv, swift_code_tv, branch_tv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.bank_details_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);

        holder_name_tv = (TextView) findViewById(R.id.holder_name_tv);
        account_number_tv = (TextView) findViewById(R.id.account_number_tv);
        iban_tv = (TextView) findViewById(R.id.iban_tv);
        bank_name_tv = (TextView) findViewById(R.id.bank_name_tv);
        swift_code_tv = (TextView) findViewById(R.id.swift_code_tv);
        branch_tv = (TextView) findViewById(R.id.branch_tv);

        edit_details_ll = (LinearLayout) findViewById(R.id.edit_details_ll);

        getProfile();

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void getProfile() {
        globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getProfile(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                Log.d(TAG, "Response : " + arg0.toString());
                globalFunctions.hideProgress();
                if (arg0 instanceof ProfileModel) {
                    ProfileModel profileModel = (ProfileModel) arg0;
                    globalFunctions.setProfile(context, profileModel);
                    setThisPage();
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "Get Profile");
    }

    private void setThisPage() {

        ProfileModel profileModel = globalFunctions.getProfile(context);
        if (profileModel != null) {

            if (profileModel.getAccountNumber() != null) {
                String number = profileModel.getAccountNumber();
                String value = null;
                StringBuilder stringBuilder = new StringBuilder();
                int length = number.length();

                if (number.length() > 6) {
                    for (int i = 0; i < number.length(); i++) {
                        if (i > 3 && (i != length || i != length - 1)) {
//                            value="x";
                            stringBuilder.append("x");
                        } else if (i == length || i == length - 1) {
//                            value = number.charAt(i) + "";
                            stringBuilder.append(number.charAt(i));
                        } else {
//                            value = number.charAt(i) + "";
                            stringBuilder.append(number.charAt(i));
                        }
                    }

                    account_number_tv.setText(stringBuilder);
                } else {
                    account_number_tv.setText(number);
                }
//                String mask = number.replaceAll("\\w(?=\\w{4})", "*");
//                account_number_tv.setText(mask);
            }

            if (profileModel.getIban() != null) {
                iban_tv.setText(profileModel.getIban());
            }

            if (GlobalFunctions.isNotNullValue(profileModel.getAccountHolderName())) {
                holder_name_tv.setText(profileModel.getAccountHolderName());
            } else if (GlobalFunctions.isNotNullValue(profileModel.getFirstName())) {
                if (GlobalFunctions.isNotNullValue(profileModel.getLastName())) {
                    holder_name_tv.setText(profileModel.getFirstName() + " " + profileModel.getLastName());
                } else {
                    holder_name_tv.setText(profileModel.getFirstName());
                }
            }

           /* if (profileModel.getBranch() != null) {
                branch_tv.setText(profileModel.getBranch());
            }

            if (profileModel.getBankName() != null) {
                bank_name_tv.setText(profileModel.getBankName());
            }

            if (profileModel.getSwiftCode() != null) {
                swift_code_tv.setText(profileModel.getSwiftCode());
            }*/

            edit_details_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(BankDetailsActivity.this, EditBankDetailsActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}