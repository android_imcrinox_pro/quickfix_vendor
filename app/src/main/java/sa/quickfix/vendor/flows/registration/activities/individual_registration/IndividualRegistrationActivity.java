package sa.quickfix.vendor.flows.registration.activities.individual_registration;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import sa.quickfix.vendor.flows.about_us.AboutUsActivity;
import sa.quickfix.vendor.flows.registration.activities.company_registration.CompanyRegistrationActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnLanguageRemoveInvoke;
import sa.quickfix.vendor.adapters.SelectedLanguagesListAdapter;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.location.LocationListener;
import sa.quickfix.vendor.location.LocationService;
import sa.quickfix.vendor.map.SearchPlaceOnMapActivity;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.AddressModel;
import sa.quickfix.vendor.services.model.IndividualRegisterModel;
import sa.quickfix.vendor.services.model.LanguageListModel;
import sa.quickfix.vendor.services.model.LanguageModel;
import sa.quickfix.vendor.services.model.LocationCheckPostModel;
import sa.quickfix.vendor.services.model.LocationCheckResponseModel;
import sa.quickfix.vendor.services.model.MyCityModel;
import sa.quickfix.vendor.services.model.RegisterModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.util.PermissionUtils;
import sa.quickfix.vendor.R;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.facebook.FacebookSdk;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class IndividualRegistrationActivity extends AppCompatActivity implements LocationListener, OnLanguageRemoveInvoke {

    public static final String TAG = "IndividualRegActivity";

    public static final String BUNDLE_INDIVIDUAL_REGISTER_ACTIVITY_REGISTER_MODEL = "BundleIndividualRegisterActivityRegisterModel";
    public static final String BUNDLE_INDIVIDUAL_REGISTER_ACTIVITY_INDIVIDUAL_REGISTER_MODEL = "BundleIndividualRegisterActivityIndividualRegisterModel";
    public static final String BUNDLE_MY_CITY_MODE = "BundleMyCityModel";

    private static final int PERMISSION_REQUEST_CODE = 200;

    private static final int
            LOCATION_PERMISSION_REQUEST_CODE = 132,
            PERMISSION_ACCESS_COARSE_LOCATION = 156;

    static Intent locationintent;
    LocationService locationService;

    IndividualRegisterModel individualRegisterModel = null;
    IndividualRegisterModel myIndividualRegisterModel = null;

    Context context;
    private static Activity activity;
    View mainView;

    List<Uri>
            uriProfileImageList,
            uriIdImageList,
            uriCertificatesImageList;

    RegisterModel myRegisterModel = null;
    AddressModel addressModel = new AddressModel();

    MyCityModel myCityModel = null;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    private LayoutInflater layoutInflater;

    ArrayList<Integer> selectedListOfLanguageIdsInt = new ArrayList<>();
    ArrayList<String> selectedListOfLanguageIdsString = new ArrayList<>();
    List<String> selectedListOfLanguageName = new ArrayList<>();

    MultiSelectDialog multiSelectDialog = new MultiSelectDialog();

    LanguageListModel languageListModel = null;
    List<LanguageModel> seletedLanguagesList = new ArrayList<>();

    LinearLayoutManager languagesLayoutManager;
    RecyclerView selected_languages_recycler_view;
    SelectedLanguagesListAdapter languageListAdapter;

    String
            selectedGender = "0";

    String selectedLanguagesIds = null;
    String selectedCityId = null;

    private CardView continue_cardview;
    private AppCompatCheckBox terms_checkbox;
    private LinearLayout register_as_a_company_ll, nationality_ll;
    private RelativeLayout city_rl, select_gender_rl, select_language_rl;
    private TextView city_tv, selected_language_tv, select_gender_tv, address_tv, nationality_tv, terms_and_conditions_tv, privacy_policy_tv;
    private CountryPicker nationality_picker;
    private EditText first_name_etv, last_name_etv, email_etv, national_id_no_etv, password_etv, repeat_password_etv,
            account_number_etv, iban_etv, holder_name_etv, bank_branch_address_etv, bank_name_etv, swift_code_etv;

    String selectedCountryName = "";

    public static Intent newInstance(Context context, IndividualRegisterModel individualRegisterModel) {
        Intent intent = new Intent(context, IndividualRegistrationActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_INDIVIDUAL_REGISTER_ACTIVITY_INDIVIDUAL_REGISTER_MODEL, individualRegisterModel);
        intent.putExtras(args);
        return intent;
    }

    public static Intent newInstance(Context context, RegisterModel model) {
        Intent intent = new Intent(context, IndividualRegistrationActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_INDIVIDUAL_REGISTER_ACTIVITY_REGISTER_MODEL, model);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.individual_registration_activity);

        context = this;
        activity = this;

        this.layoutInflater = activity.getLayoutInflater();

        uriProfileImageList = new ArrayList<>();
        uriIdImageList = new ArrayList<>();
        uriCertificatesImageList = new ArrayList<>();

        uriProfileImageList.clear();
        uriIdImageList.clear();
        uriCertificatesImageList.clear();

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        }

        locationintent = new Intent(LocationService.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            locationintent.setPackage(activity.getPackageName());
        }
        if (locationService != null) {
//            GlobalFunctions.showProgress(activity, activity.getString(R.string.fetching_current_location));
            startLocationService();
        }

        locationService = new LocationService();
        locationService.setListener(this);

        if (getIntent().hasExtra(BUNDLE_INDIVIDUAL_REGISTER_ACTIVITY_REGISTER_MODEL)) {
            myRegisterModel = (RegisterModel) getIntent().getSerializableExtra(BUNDLE_INDIVIDUAL_REGISTER_ACTIVITY_REGISTER_MODEL);
        }

        if (getIntent().hasExtra(BUNDLE_INDIVIDUAL_REGISTER_ACTIVITY_INDIVIDUAL_REGISTER_MODEL)) {
            myIndividualRegisterModel = (IndividualRegisterModel) getIntent().getSerializableExtra(BUNDLE_INDIVIDUAL_REGISTER_ACTIVITY_INDIVIDUAL_REGISTER_MODEL);
        } else {
            myIndividualRegisterModel = null;
        }

        Intent i = getIntent();
        uriProfileImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE);
        uriIdImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_LIST_CODE);
        uriCertificatesImageList = (List<Uri>) i.getSerializableExtra(GlobalVariables.UPLOAD_CERTIFICATES_LIST_CODE);

        languagesLayoutManager = new LinearLayoutManager(context);

        selected_languages_recycler_view = (RecyclerView) findViewById(R.id.selected_languages_recycler_view);
        terms_checkbox = (AppCompatCheckBox) findViewById(R.id.terms_checkbox);

        continue_cardview = (CardView) findViewById(R.id.continue_cardview);
        register_as_a_company_ll = (LinearLayout) findViewById(R.id.register_as_a_company_ll);
        nationality_ll = (LinearLayout) findViewById(R.id.nationality_ll);
        city_rl = (RelativeLayout) findViewById(R.id.city_rl);
        select_gender_rl = (RelativeLayout) findViewById(R.id.select_gender_rl);
        select_language_rl = (RelativeLayout) findViewById(R.id.select_language_rl);

        city_tv = (TextView) findViewById(R.id.city_tv);
        selected_language_tv = (TextView) findViewById(R.id.selected_language_tv);
        select_gender_tv = (TextView) findViewById(R.id.select_gender_tv);
        address_tv = (TextView) findViewById(R.id.address_tv);
        nationality_tv = (TextView) findViewById(R.id.nationality_tv);

        nationality_picker = CountryPicker.newInstance(getString(R.string.select_nationality));  // dialog title

        first_name_etv = (EditText) findViewById(R.id.first_name_etv);
        last_name_etv = (EditText) findViewById(R.id.last_name_etv);
        email_etv = (EditText) findViewById(R.id.email_etv);
        national_id_no_etv = (EditText) findViewById(R.id.national_id_no_etv);
        password_etv = (EditText) findViewById(R.id.password_etv);
        repeat_password_etv = (EditText) findViewById(R.id.repeat_password_etv);
        account_number_etv = (EditText) findViewById(R.id.account_number_etv);
        iban_etv = (EditText) findViewById(R.id.iban_etv);
        holder_name_etv = (EditText) findViewById(R.id.holder_name_etv);
        bank_branch_address_etv = (EditText) findViewById(R.id.bank_branch_address_etv);
        bank_name_etv = (EditText) findViewById(R.id.bank_name_etv);
        swift_code_etv = (EditText) findViewById(R.id.swift_code_etv);

        terms_and_conditions_tv = (TextView) findViewById(R.id.terms_and_conditions_tv);
        privacy_policy_tv = (TextView) findViewById(R.id.privacy_policy_tv);

        mainView = continue_cardview;

        // initLanguageList();

        getSpokenLanguagesList(context);

        if (myIndividualRegisterModel != null) {
            setEnteredData(myIndividualRegisterModel);
        }

        national_id_no_etv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith("1")) {
                    s.clear();
                }
                String digits = national_id_no_etv.getText().toString().trim();
                if (digits.length() >= getResources().getInteger(R.integer.national_id_max_length)) {
                    globalFunctions.closeKeyboard(activity);
                }
            }
        });

        nationality_picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                // Implement your code here
                nationality_tv.setText(name);
                nationality_picker.dismiss();
            }
        });

        nationality_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nationality_picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        nationality_ll.setClickable(false);
        nationality_ll.setEnabled(false);

        address_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    return;
                } else if (!sLocationEnabled(activity)) {
                    showSettingsAlert();
                } else {
                    globalFunctions.showProgress(activity, getString(R.string.fetching_location));
                    Intent mIntent = SearchPlaceOnMapActivity.newInstance(context, addressModel, GlobalVariables.LOCATION_TYPE.NONE);
                    startActivityForResult(mIntent, GlobalVariables.REQUEST_INDIVIDUAL_ADDRESS_LOCATION_CODE);
                }
            }
        });

        city_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = CityListActivity.newInstance(activity, myCityModel);
                startActivityForResult(intent, globalVariables.REQUEST_SELECT_CITY_CODE);
            }
        });

        select_language_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
            }
        });

        select_gender_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGenderDialog(activity);
            }
        });

        register_as_a_company_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myRegisterModel != null) {
                    Intent intent = CompanyRegistrationActivity.newInstance(context, myRegisterModel);
                    startActivity(intent);
                }
            }
        });

        continue_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

        terms_and_conditions_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = AboutUsActivity.newInstance(activity, GlobalVariables.PAGE_LOGIN_TNC);
                startActivity(intent);
//                GlobalFunctions.openBrowser(context, GlobalVariables.HTTP_URL_TERMS_AND_CONDITION);
            }
        });
        privacy_policy_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = AboutUsActivity.newInstance(activity, GlobalVariables.PAGE_PP);
                startActivity(intent);
//                GlobalFunctions.openBrowser(context, GlobalVariables.HTTP_URL_TERMS_AND_CONDITION);
            }
        });

        password_etv.setTextDirection(View.TEXT_DIRECTION_RTL);
        repeat_password_etv.setTextDirection(View.TEXT_DIRECTION_RTL);

    }

    public boolean sLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);

// Setting Dialog Title
        alertDialog.setTitle("GPS settings");

// Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

// Setting Icon to Dialog
//alertDialog.setIcon(R.drawable.delete);

// On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivity(intent);
            }
        });

// on pressing cancel button
        alertDialog.setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

// Showing Alert Message
        alertDialog.show();
    }

    private void setEnteredData(IndividualRegisterModel mIndividualRegisterModel) {

        if (mIndividualRegisterModel != null) {

            if (mIndividualRegisterModel.getFirstName() != null) {
                first_name_etv.setText(mIndividualRegisterModel.getFirstName());
            }

            if (mIndividualRegisterModel.getLastName() != null) {
                last_name_etv.setText(mIndividualRegisterModel.getLastName());
            }

            if (mIndividualRegisterModel.getEmailId() != null) {
                email_etv.setText(mIndividualRegisterModel.getEmailId());
            }

            if (mIndividualRegisterModel.getIdNumber() != null) {
                national_id_no_etv.setText(mIndividualRegisterModel.getIdNumber());
            }

            if (addressModel == null) {
                addressModel = new AddressModel();
            }
            addressModel.setAddress(mIndividualRegisterModel.getAddress());
            address_tv.setText(mIndividualRegisterModel.getAddress());
            addressModel.setLatitude(Double.parseDouble(mIndividualRegisterModel.getLatitude()));
            addressModel.setLongitude(Double.parseDouble(mIndividualRegisterModel.getLongitude()));

            if (individualRegisterModel == null) {
                individualRegisterModel = new IndividualRegisterModel();
            }
            individualRegisterModel.setAddress(mIndividualRegisterModel.getAddress());
            individualRegisterModel.setLatitude(mIndividualRegisterModel.getLatitude() + "");
            individualRegisterModel.setLongitude(mIndividualRegisterModel.getLongitude() + "");

            selectedCityId = mIndividualRegisterModel.getCityId();
            city_tv.setText(mIndividualRegisterModel.getCityName());
            individualRegisterModel.setCityName(mIndividualRegisterModel.getCityName());
            individualRegisterModel.setCityId(mIndividualRegisterModel.getCityId());

            if (mIndividualRegisterModel.getNationality() != null) {
                nationality_tv.setText(mIndividualRegisterModel.getNationality());
            }

            if (mIndividualRegisterModel.getSpokenLanguages() != null) {
                selectedListOfLanguageIdsInt = globalFunctions.getListFromInteger(mIndividualRegisterModel.getSpokenLanguages());
                selectedLanguagesIds = mIndividualRegisterModel.getSpokenLanguages();
                individualRegisterModel.setSpokenLanguages(mIndividualRegisterModel.getSpokenLanguages());
            }

            if (mIndividualRegisterModel.getLanguagesNames() != null) {
                selected_language_tv.setText(mIndividualRegisterModel.getLanguagesNames());
            }

            if (mIndividualRegisterModel.getGender() != null) {
                selectedGender = mIndividualRegisterModel.getGender();
                if (mIndividualRegisterModel.getGender().equalsIgnoreCase(globalVariables.GENDER_MALE)) {
                    select_gender_tv.setText(getString(R.string.male));
                } else if (mIndividualRegisterModel.getGender().equalsIgnoreCase(globalVariables.GENDER_FEMALE)) {
                    select_gender_tv.setText(getString(R.string.female));
                }
            }

            if (mIndividualRegisterModel.getConfirmPassword() != null) {
                password_etv.setText(mIndividualRegisterModel.getConfirmPassword());
                repeat_password_etv.setText(mIndividualRegisterModel.getConfirmPassword());
            }

            if (mIndividualRegisterModel.getAccountNumber() != null) {
                account_number_etv.setText(mIndividualRegisterModel.getAccountNumber());
            }

            if (mIndividualRegisterModel.getIban() != null) {
                iban_etv.setText(mIndividualRegisterModel.getIban());
            }

          /*  if (mIndividualRegisterModel.getAccountHolderName() != null) {
                holder_name_etv.setText(mIndividualRegisterModel.getAccountHolderName());
            }

            if (mIndividualRegisterModel.getBranch() != null) {
                bank_branch_address_etv.setText(mIndividualRegisterModel.getBranch());
            }

            if (mIndividualRegisterModel.getBankName() != null) {
                bank_name_etv.setText(mIndividualRegisterModel.getBankName());
            }

            if (mIndividualRegisterModel.getSwiftCode() != null) {
                swift_code_etv.setText(mIndividualRegisterModel.getSwiftCode());
            }*/

            terms_checkbox.setChecked(true);
        }
    }

    private void validateInput() {
        if (context != null) {
            String
                    firstName = first_name_etv.getText().toString().trim(),
                    lastName = last_name_etv.getText().toString().trim(),
                    emailId = email_etv.getText().toString().trim(),
                    nationalIdNo = national_id_no_etv.getText().toString().trim(),
                    address = address_tv.getText().toString().trim(),
                    nationality = nationality_tv.getText().toString().trim(),
                    password = password_etv.getText().toString().trim(),
                    repeatPassword = repeat_password_etv.getText().toString().trim(),
                    languagesNames = selected_language_tv.getText().toString().trim(),
                    accountNumber = account_number_etv.getText().toString().trim(),
                    iban = iban_etv.getText().toString().trim();
//                    holderName = holder_name_etv.getText().toString().trim(),
//                    bankBranchAddress = bank_branch_address_etv.getText().toString().trim(),
//                    bankName = bank_name_etv.getText().toString().trim(),
//                    swiftCode = swift_code_etv.getText().toString().trim();

            if (firstName.isEmpty()) {
                first_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                first_name_etv.setFocusableInTouchMode(true);
                first_name_etv.requestFocus();
            } else if (lastName.isEmpty()) {
                last_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                last_name_etv.setFocusableInTouchMode(true);
                last_name_etv.requestFocus();
            } else if (emailId.isEmpty()) {
                email_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                email_etv.setFocusableInTouchMode(true);
                email_etv.requestFocus();
            } else if (!GlobalFunctions.isEmailValid(emailId)) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.emailNotValid));
                email_etv.setFocusableInTouchMode(true);
                email_etv.requestFocus();
            } else if (nationalIdNo.isEmpty()) {
                national_id_no_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                national_id_no_etv.setFocusableInTouchMode(true);
                national_id_no_etv.requestFocus();
            } else if (address.isEmpty()) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_add_address));
            } else if (selectedCityId == null) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_select_your_city));
            } else if (nationality.isEmpty()) {
                nationality_tv.setError(getString(R.string.pleaseFillMandatoryDetails));
                nationality_tv.setFocusableInTouchMode(true);
                nationality_tv.requestFocus();
            } else if (selectedLanguagesIds == null) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_select_your_language));
            } else if (selectedGender.equalsIgnoreCase("0")) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_select_your_gender));
            } else if (password.isEmpty()) {
                password_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                password_etv.setFocusableInTouchMode(true);
                password_etv.requestFocus();
            } else if (repeatPassword.isEmpty()) {
                repeat_password_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                repeat_password_etv.setFocusableInTouchMode(true);
                repeat_password_etv.requestFocus();
            } else if (accountNumber.isEmpty()) {
                account_number_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                account_number_etv.setFocusableInTouchMode(true);
                account_number_etv.requestFocus();
            } else if (iban.isEmpty()) {
                iban_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                iban_etv.setFocusableInTouchMode(true);
                iban_etv.requestFocus();
            } /*else if (holderName.isEmpty()) {
                holder_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                holder_name_etv.setFocusableInTouchMode(true);
                holder_name_etv.requestFocus();
            } else if (bankBranchAddress.isEmpty()) {
                bank_branch_address_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                bank_branch_address_etv.setFocusableInTouchMode(true);
                bank_branch_address_etv.requestFocus();
            } else if (bankName.isEmpty()) {
                bank_name_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                bank_name_etv.setFocusableInTouchMode(true);
                bank_name_etv.requestFocus();
            } else if (swiftCode.isEmpty()) {
                swift_code_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                swift_code_etv.setFocusableInTouchMode(true);
                swift_code_etv.requestFocus();
            }*/ else if (nationalIdNo.length() != 10) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.invalid_national_id_number));
                national_id_no_etv.setFocusableInTouchMode(true);
                national_id_no_etv.requestFocus();
            } else if (password.length() < 8) {
                password_etv.setError(getString(R.string.passwordContainsError));
                password_etv.setFocusableInTouchMode(true);
                password_etv.requestFocus();
            } else if (!password.equals(repeatPassword)) {
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.passwordNotMatched));
                password_etv.setFocusableInTouchMode(true);
                password_etv.requestFocus();
            } else if (!terms_checkbox.isChecked()) {
                GlobalFunctions.displayMessaage(activity, mainView, getString(R.string.please_select_terms_and_conditions));
            } else {

                if (individualRegisterModel == null) {
                    individualRegisterModel = new IndividualRegisterModel();
                }

                if (myRegisterModel != null) {
                    if (myRegisterModel.getMobileNumber() != null) {
                        individualRegisterModel.setMobileNumber(myRegisterModel.getMobileNumber());
                    }

                    if (myRegisterModel.getCountryCode() != null) {
                        individualRegisterModel.setCountryCode(myRegisterModel.getCountryCode());
                    }
                }

                individualRegisterModel.setLanguagesNames(languagesNames);
                individualRegisterModel.setFirstName(firstName);
                individualRegisterModel.setLastName(lastName);
                individualRegisterModel.setConfirmPassword(password);
                //  individualRegisterModel.setPassword(globalFunctions.md5(password));
                individualRegisterModel.setEmailId(emailId);
                individualRegisterModel.setGender(selectedGender);
                individualRegisterModel.setIdNumber(nationalIdNo);
                individualRegisterModel.setSpokenLanguages(selectedLanguagesIds);
                individualRegisterModel.setNationality(nationality);
//                individualRegisterModel.setBankName(bankName);
//                individualRegisterModel.setAccountHolderName(holderName);
//                individualRegisterModel.setBranch(bankBranchAddress);
                individualRegisterModel.setIban(iban);
                individualRegisterModel.setAccountNumber(accountNumber);
//                individualRegisterModel.setSwiftCode(swiftCode);

                if (myIndividualRegisterModel != null) {
                    //from edit....snd uris also....

                    if (myIndividualRegisterModel.getCategoryList() != null) {
                        individualRegisterModel.setCategoryList(myIndividualRegisterModel.getCategoryList());
                    }

                    if (myIndividualRegisterModel.getMobileNumber() != null) {
                        individualRegisterModel.setMobileNumber(myIndividualRegisterModel.getMobileNumber());
                    }

                    if (myIndividualRegisterModel.getCountryCode() != null) {
                        individualRegisterModel.setCountryCode(myIndividualRegisterModel.getCountryCode());
                    }

                    if (myIndividualRegisterModel.getExperienceM() != null) {
                        individualRegisterModel.setExperienceM(myIndividualRegisterModel.getExperienceM());
                    }

                    if (myIndividualRegisterModel.getExperience() != null) {
                        individualRegisterModel.setExperience(myIndividualRegisterModel.getExperience());
                    }

                    if (addressModel != null) {
                        LocationCheckPostModel checkLocationModel = new LocationCheckPostModel();
                        checkLocationModel.setLatitude(addressModel.getLatitude() + "");
                        checkLocationModel.setLongitude(addressModel.getLongitude() + "");
                        checkLocationData(context, checkLocationModel, true);
                    }

                  /*  Intent intent = IndividualRegistrationUploadDocumentsActivity.newInstance(activity, individualRegisterModel, globalVariables.FROM_PAGE_REVIEW_INDIVIDUAL_REGISTRATION);
                    intent.putExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE, (Serializable) uriProfileImageList);
                    intent.putExtra(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_LIST_CODE, (Serializable) uriIdImageList);
                    intent.putExtra(GlobalVariables.UPLOAD_CERTIFICATES_LIST_CODE, (Serializable) uriCertificatesImageList);
                    startActivity(intent);*/
                } else {
                    //normal...
                    if (addressModel != null) {
                        LocationCheckPostModel checkLocationModel = new LocationCheckPostModel();
                        checkLocationModel.setLatitude(addressModel.getLatitude() + "");
                        checkLocationModel.setLongitude(addressModel.getLongitude() + "");
                        checkLocationData(context, checkLocationModel, false);
                    }

                  /*  Intent intent = IndividualRegistrationUploadDocumentsActivity.newInstance(activity, individualRegisterModel);
                    startActivity(intent);*/
                }
            }

        }
    }

    private void checkLocationData(final Context context, final LocationCheckPostModel checkLocationModel, final boolean isNotNormal) {
        GlobalFunctions.showProgress(context, context.getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.checkLocation(context, checkLocationModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                GlobalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateOutput(arg0, isNotNormal);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                GlobalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                GlobalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Error : " + msg);
            }
        }, "Register_User");
    }

    private void validateOutput(Object arg0, boolean isNotNormal) {
        if (arg0 instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) arg0;
            if (!statusModel.isStatus()) {
                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            }
        } else {
            LocationCheckResponseModel locationCheckResponseModel = (LocationCheckResponseModel) arg0;
            if (isNotNormal) {
                Intent intent = IndividualRegistrationUploadDocumentsActivity.newInstance(activity, individualRegisterModel, globalVariables.FROM_PAGE_REVIEW_INDIVIDUAL_REGISTRATION);
                intent.putExtra(GlobalVariables.UPLOAD_PROFILE_PHOTO_LIST_CODE, (Serializable) uriProfileImageList);
                intent.putExtra(GlobalVariables.UPLOAD_IDENTIFICATION_PHOTO_LIST_CODE, (Serializable) uriIdImageList);
                intent.putExtra(GlobalVariables.UPLOAD_CERTIFICATES_LIST_CODE, (Serializable) uriCertificatesImageList);
                startActivity(intent);
            } else {
                Intent intent = IndividualRegistrationUploadDocumentsActivity.newInstance(activity, individualRegisterModel);
                startActivity(intent);
            }
        }
    }


    private void initLanguageList() {
        languageListAdapter = new SelectedLanguagesListAdapter(activity, seletedLanguagesList, this);
        selected_languages_recycler_view.setLayoutManager(languagesLayoutManager);
        selected_languages_recycler_view.setAdapter(languageListAdapter);
        synchronized (languageListAdapter) {
            languageListAdapter.notifyDataSetChanged();
        }
    }

    private void getSpokenLanguagesList(final Context context) {
        // globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getSpokenLanguagesList(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                //globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof LanguageListModel) {
                    LanguageListModel languageListModel1 = (LanguageListModel) arg0;
                    languageListModel = languageListModel1;
                    setLanguageList();
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                // globalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                // globalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "GetSpokenLanguagesList");
    }

    private void setLanguageList() {
        ArrayList<MultiSelectModel> listOfLanguages = new ArrayList<>();

        for (int i = 0; i < languageListModel.getLanguageList().size(); i++) {
            String languageName = languageListModel.getLanguageList().get(i).getTitle();
            String languageId = languageListModel.getLanguageList().get(i).getId();
            int id = 0;
            try {

                id = Integer.parseInt(languageId);
            } catch (Exception e) {
                id = 0;
            }
            listOfLanguages.add(new MultiSelectModel(id, languageName));

        }
        setMultiSelectLanguageList(listOfLanguages);
    }

    private void setMultiSelectLanguageList(ArrayList<MultiSelectModel> listOfLanguages) {
        multiSelectDialog
                .title(context.getString(R.string.select_languages)) //setting title for dialog
                .titleSize(25)
                .positiveText(context.getString(R.string.select))
                .negativeText(context.getString(R.string.cancel))
                .setMinSelectionLimit(1) //you can set minimum checkbox selection limit (Optional)
                .setMaxSelectionLimit(languageListModel.getLanguageList().size()) //you can set maximum checkbox selection limit (Optional)
                .preSelectIDsList(selectedListOfLanguageIdsInt) //List of ids that you need to be selected
                .multiSelectList(listOfLanguages) // the multi select model list with ids and name
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                    @Override
                    public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                        selectedListOfLanguageIdsInt.clear();
                        selectedListOfLanguageIdsString.clear();
                        selectedListOfLanguageName.clear();
                        selectedListOfLanguageName.addAll(selectedNames);

                        for (int i = 0; i < selectedNames.size(); i++) {
                            LanguageModel languageModel = null;
                            Object localObjectKeyValue = getObjectFromSelectedItemPosition(selectedNames.get(i), languageListModel);
                            if (localObjectKeyValue != null) {
                                languageModel = (LanguageModel) localObjectKeyValue;
                                if (languageModel != null && languageModel.getId() != null) {
                                    int id = 0;
                                    try {

                                        id = Integer.parseInt(languageModel.getId());
                                    } catch (Exception e) {
                                        id = 0;
                                    }
                                    selectedListOfLanguageIdsInt.add(id);
                                    selectedListOfLanguageIdsString.add(languageModel.getId());
                                }
                            }
                        }
                        selected_language_tv.setText(globalFunctions.getStringFromList(selectedListOfLanguageName));
                        // select_gender_tv.setText(globalFunctions.getIntegerFromList(selectedListOfLanguageIdsInt));
                        selectedLanguagesIds = globalFunctions.getIntegerFromList(selectedListOfLanguageIdsInt);   //.......comma separated ids...
                        // selected_language_tv.setText(globalFunctions.getIntegerFromList(selectedListOfLanguageIdsInt));.......comma separated ids...
                        //selected_language_tv.setText(globalFunctions.getStringFromList(selectedListOfLanguageIdsString)); .......comma separated ids...
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "Dialog cancelled");
                    }


                });


    }

    private Object getObjectFromSelectedItemPosition(String title, Object object) {
        if (object instanceof LanguageListModel) {
            for (LanguageModel model : ((LanguageListModel) object).getLanguageList()) {
                if (model.getTitle().equalsIgnoreCase(title))
                    return model;
            }
        }
        return null;
    }

    private void openGenderDialog(final Context context) {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.gender_custom_dialog, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        TextView back_tv, male_tv, female_tv;

        back_tv = (TextView) alertView.findViewById(R.id.back_tv);
        male_tv = (TextView) alertView.findViewById(R.id.male_tv);
        female_tv = (TextView) alertView.findViewById(R.id.female_tv);

        back_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        male_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = globalVariables.GENDER_MALE;
                select_gender_tv.setText(getString(R.string.male));
                dialog.dismiss();
            }
        });

        female_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = globalVariables.GENDER_FEMALE;
                select_gender_tv.setText(getString(R.string.female));
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public static void startLocationService() {
        activity.startService(locationintent);
    }

    public static void stopLocationService() {
        activity.stopService(locationintent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        GlobalFunctions.hideProgress();
        if (resultCode == activity.RESULT_OK && requestCode == globalVariables.REQUEST_INDIVIDUAL_ADDRESS_LOCATION_CODE) {
            if (addressModel == null) {
                addressModel = new AddressModel();
            }
            addressModel = (AddressModel) data.getExtras().getSerializable(SearchPlaceOnMapActivity.BUNDLE_SEARCH_PLACE_ON_MAP_ACTIVITY_ADDRESS_MODEL);
            setSelectedAddress(addressModel);
        } else if (resultCode == activity.RESULT_OK && requestCode == globalVariables.REQUEST_SELECT_CITY_CODE) {
            MyCityModel myCityModel1 = (MyCityModel) data.getExtras().getSerializable(BUNDLE_MY_CITY_MODE);
            if (myCityModel1 != null) {
                if (individualRegisterModel == null) {
                    individualRegisterModel = new IndividualRegisterModel();
                }
                selectedCityId = myCityModel1.getId();
                city_tv.setText(myCityModel1.getTitle());
                individualRegisterModel.setCityName(myCityModel1.getTitle());
                individualRegisterModel.setCityId(myCityModel1.getId());
            }
        }

    }

    private void setSelectedAddress(AddressModel mAddressModel) {

        if (mAddressModel != null) {

            if (individualRegisterModel == null) {
                individualRegisterModel = new IndividualRegisterModel();
            }
            address_tv.setText(mAddressModel.getAddress());
            individualRegisterModel.setAddress(mAddressModel.getAddress());
            individualRegisterModel.setLatitude(mAddressModel.getLatitude() + "");
            individualRegisterModel.setLongitude(mAddressModel.getLongitude() + "");
        }
    }

    @Override
    public void OnLocationFetch(Location location) {
        globalFunctions.hideProgress();
        setLocation();
    }

    private void setLocation() {
        Address address = globalFunctions.getAddressfromLatLng(activity, locationService.getLatitude(), locationService.getLongitude());

        //  pickUpAddressModel = globalFunctions.getAddressModelFromAddress(address);

      /*  String completeAddress = null;
        try {
            if (pickUpAddressModel.getAddress() != null) {
                completeAddress = pickUpAddressModel.getAddress();
            } else {
                completeAddress = globalFunctions.getAddressTextFromModelExcludingNameAndAddresswithComma(context, pickUpAddressModel);
            }
            // head_office_address_tv.setText(completeAddress);
        } catch (Exception e) {
            globalFunctions.displayMessaage(activity, mainView, getString(R.string.location_error));
        }*/
        stopLocationService();
    }

    @Override
    public void OnProviderDisabled(String provider) {
        globalFunctions.hideProgress();
    }

    @Override
    public void OnProviderEnabled(String provider) {

    }

    @Override
    public void OnLocationFailure(String msg) {
        globalFunctions.hideProgress();
    }

    @Override
    public void OnSelectedLanguageRemoveInvoke(int position, LanguageModel languageModel) {
/*

        if (languageModel != null) {
            seletedLanguagesList.remove(position);
           // setLanguageList();
            initLanguageList();
        }
*/

    }
}