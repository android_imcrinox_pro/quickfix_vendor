package sa.quickfix.vendor.flows.reset_password.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.flows.registration.activities.RegisterActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.LoginModel;
import sa.quickfix.vendor.services.model.RegisterModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;

import com.facebook.FacebookSdk;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mukesh.OtpView;

import java.util.Locale;
import java.util.concurrent.TimeUnit;


public class ResetOtpActivity extends AppCompatActivity {

    public static final String TAG = "ResetOtpActivity";

    public static final String BUNDLE_REGISTER_MODEL = "BundleRegisterModel";
    public static final String BUNDLE_PHN_NUMBER = "BundlePhoneNumber";

    private static int RESEND_OTP_TOTAL_TIME_OUT = 60000;
    private static int RESEND_OTP_TIME_INTERVAL = 1000;
    private static int OTP_ATTEMPT_COUNT = 0;
    private static int OTP_MAX_ATTEMPT_COUNT = 6;

    private String verificationId;

    Context context;
    private static Activity activity;
    View mainView;

    private FirebaseAuth mAuth;

    RegisterModel registerModel = null;
    String phoneNumber = "";

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    private TextView submit_tv, otp_timer_tv, resend_tv;
    private View resend_view;
    private ImageView back_iv;
    private OtpView otpView;

    CountDownTimer countDownTimer;

    public static Intent newInstance(Context context, RegisterModel registerModel, String phoneNumber) {
        Intent intent = new Intent(context, ResetOtpActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_REGISTER_MODEL, registerModel);
        args.putString(BUNDLE_PHN_NUMBER, phoneNumber);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.reset_otp_activity);

        mAuth = FirebaseAuth.getInstance();

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        otpView = (OtpView) findViewById(R.id.otp_view);

        back_iv = (ImageView) findViewById(R.id.back_iv);

        submit_tv = (TextView) findViewById(R.id.submit_tv);
        otp_timer_tv = (TextView) findViewById(R.id.otp_timer_tv);
        resend_tv = (TextView) findViewById(R.id.resend_code_tv);
        resend_view = (View) findViewById(R.id.resend_view);

        mainView = back_iv;

        if (getIntent().hasExtra(BUNDLE_REGISTER_MODEL)) {
            registerModel = (RegisterModel) getIntent().getSerializableExtra(BUNDLE_REGISTER_MODEL);
        } else {
            registerModel = null;
        }

        if (getIntent().hasExtra(BUNDLE_PHN_NUMBER)) {
            phoneNumber = (String) getIntent().getStringExtra(BUNDLE_PHN_NUMBER);
        } else {
            phoneNumber = null;
        }

        if (phoneNumber != null) {
            //once got client firebase id...add setup firebase and uncomment below 2 lines...
//            sendVerificationCode(phoneNumber);
            sendOTPFromServer();
            startCountDownTimer();
        }

        otpView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String digits = otpView.getText().toString().trim();
                if (digits.length() >= 6) {
                    globalFunctions.closeKeyboard(activity);
                }
            }
        });

        resend_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (phoneNumber != null && OTP_ATTEMPT_COUNT < OTP_MAX_ATTEMPT_COUNT) {
//                    sendVerificationCode(phoneNumber);
                    sendOTPFromServer();
                    startCountDownTimer();
                } else {
                    openMaxAttemptOtpDialog(context, true);
                }
            }
        });

        submit_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String code = otpView.getText().toString().trim();

                if (code.isEmpty() || code.length() < 6) {

                    otpView.setError(getString(R.string.please_enter_valid_otp));
                    otpView.requestFocus();
                    return;
                }

                if (OTP_ATTEMPT_COUNT < OTP_MAX_ATTEMPT_COUNT) {
                    globalFunctions.showProgress(context, activity.getString(R.string.loading));
//                    verifyCode(code);
                    verifyOTP(code);
                } else {
                    openMaxAttemptOtpDialog(context, true);
                }

                //once got client firebase id...add setup firebase and uncomment below line...
//                verifyCode(code);

               /* LoginModel model = new LoginModel();
                model.setMobile(registerModel.getMobileNumber());
                checkMobileNumber(context, model);*/

                //once got client firebase id...add setup firebase and comment below lines...
                //below lines for static otp...any otp it will take...
               /* if (registerModel != null) {
                    Intent intent = ResetPasswordActivity.newInstance(context, registerModel);
                    startActivity(intent);
                }*/
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    private void sendOTPFromServer() {
        LoginModel model = new LoginModel();
        if (registerModel != null) {
            model.setCountryCode(registerModel.getCountryCode());
            model.setMobile(registerModel.getMobileNumber());
        }
        SendOTP(context, model);
    }

    private void verifyOTP(String code) {

        LoginModel model = new LoginModel();
        if (registerModel != null) {
            model.setCountryCode(registerModel.getCountryCode());
            model.setMobile(registerModel.getMobileNumber());
        }
        model.setOtp(code);
        verifySOTP(context, model);
    }

    private void SendOTP(final Context context, final LoginModel loginModel) {
        GlobalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.sendOtp(context, loginModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                GlobalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof StatusModel) {
                    StatusModel model = new StatusModel();
                    GlobalFunctions.displayMessaage(activity, mainView, model.getMessage());
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "CheckMobileNumber");
    }


    private void verifySOTP(final Context context, final LoginModel loginModel) {
        GlobalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.verifyOTP(context, loginModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                GlobalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof StatusModel) {
                    StatusModel model = new StatusModel();
                    GlobalFunctions.hideProgress();
                    OTP_ATTEMPT_COUNT++;
                    if (model.isStatus()) {
                        Intent intent = ResetPasswordActivity.newInstance(context, registerModel);
                        startActivity(intent);
                    } else {
                        GlobalFunctions.displayMessaage(activity, mainView, model.getMessage());
                        otpView.setText("");
                    }
                } else {
                    openMaxAttemptOtpDialog(context, false);
                    otpView.setText("");
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "CheckMobileNumber");
    }

    private void openMaxAttemptOtpDialog(Context context, final boolean isMaxAttempt) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        if (isMaxAttempt) {
            alertDialog.setMessage(getString(R.string.otp_max_attempt));
        } else {
            alertDialog.setMessage(getString(R.string.invalid_otp) + "! " + getString(R.string.please_enter_valid_otp));
        }
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (isMaxAttempt) {
                    closeThisActivity();
                }
            }
        });

        alertDialog.show();
    }

    private void startCountDownTimer() {
        countDownTimer = new CountDownTimer(RESEND_OTP_TOTAL_TIME_OUT, RESEND_OTP_TIME_INTERVAL) {

            public void onTick(long millisUntilFinished) {
                resend_tv.setEnabled(false);
                resend_tv.setClickable(false);
                if (getApplicationContext() != null) {
                    String text = String.format(Locale.getDefault(), "%02d : %02d sec",
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60);
                    otp_timer_tv.setText(getString(R.string.in) + " " + text);
                    resend_tv.setTextColor(globalFunctions.getColor(context, R.color.app_fontColor_hint));
                    resend_view.setBackground(activity.getResources().getDrawable(R.color.app_fontColor_hint));
                }
            }

            @Override
            public void onFinish() {
                otp_timer_tv.setText("");
                resend_tv.setEnabled(true);
                resend_tv.setClickable(true);
                resend_tv.setTextColor(globalFunctions.getColor(context, R.color.blue_light));
                resend_view.setBackground(activity.getResources().getDrawable(R.color.blue_light));
            }
        };
        countDownTimer.start();
    }

    private void sendVerificationCode(String number) {
        GlobalFunctions.showProgress(activity, context.getString(R.string.sending_otp));
        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(mAuth)
                        .setPhoneNumber(number)       // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(this)                 // Activity (for callback binding)
                        .setCallbacks(mCallBack)          // OnVerificationStateChangedCallbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);

      /*  PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallBack
        );*/

    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            GlobalFunctions.hideProgress();
            verificationId = s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            GlobalFunctions.hideProgress();
            if (code != null) {
                otpView.setText(code);
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            GlobalFunctions.hideProgress();
            Toast.makeText(ResetOtpActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    };

    private void verifyCode(String code) {
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
            signInWithCredential(credential);
        } catch (Exception Exccc) {
            OTP_ATTEMPT_COUNT++;
            //otp incorrect...
            GlobalFunctions.hideProgress();
            openMaxAttemptOtpDialog(context, false);
            //otp incorrect...
            // GlobalFunctions.hideProgress();
//            GlobalFunctions.displayMessaage(activity, mainView, activity.getString(R.string.please_enter_valid_otp));
        }
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        OTP_ATTEMPT_COUNT++;
                        GlobalFunctions.hideProgress();
                        if (task != null) {
                            if (task.isSuccessful()) {
                                if (registerModel != null) {

                                    LoginModel model = new LoginModel();
                                    model.setMobile(registerModel.getMobileNumber());
                                    checkMobileNumber(context, model);
                                }
                            } else {
                                openMaxAttemptOtpDialog(context, false);
//                                Toast.makeText(ResetOtpActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                otpView.setText("");
                            }
                        }
                    }
                });
    }

    private void checkMobileNumber(final Context context, final LoginModel loginModel) {
        GlobalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.checkMobileNumber(context, loginModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                GlobalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof StatusModel) {
                    validateOutputAfterCheckingMobileNumber((StatusModel) arg0);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "CheckMobileNumber");
    }

    private void validateOutputAfterCheckingMobileNumber(StatusModel statusModel) {
        if (statusModel.isStatus()) {
            Intent intent = ResetPasswordActivity.newInstance(context, registerModel);
            startActivity(intent);

        } else {
            //registered user..
            globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        countDownTimer.cancel();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        countDownTimer.cancel();
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}