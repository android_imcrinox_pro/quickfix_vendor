package sa.quickfix.vendor.flows.add_parts.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.adapters.AddedPartsListAdapter;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnPartDeleteItemClickInvoke;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnSparePartAddClickInvoke;
import sa.quickfix.vendor.adapters.PartsSearchResultListAdapter;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.AddPartPostModel;
import sa.quickfix.vendor.services.model.AddedPartModel;
import sa.quickfix.vendor.services.model.AddedPartsListModel;
import sa.quickfix.vendor.services.model.CategoryListModel;
import sa.quickfix.vendor.services.model.OrderDetailMainModel;
import sa.quickfix.vendor.services.model.SparePartModel;
import sa.quickfix.vendor.services.model.SparePartSearchResultModel;
import sa.quickfix.vendor.services.model.SparePartsPostModel;
import sa.quickfix.vendor.services.model.SparePartsSearchResultListModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.services.model.SubCategoryListModel;
import sa.quickfix.vendor.services.model.SubCategoryModel;
import sa.quickfix.vendor.view.AlertDialog;

import com.bigkoo.pickerview.MyOptionsPickerView;
import com.dpizarro.uipicker.library.picker.PickerUI;
import com.dpizarro.uipicker.library.picker.PickerUISettings;
import com.facebook.FacebookSdk;

import java.util.ArrayList;
import java.util.List;


public class AddPartsActivity extends AppCompatActivity implements OnSparePartAddClickInvoke, OnPartDeleteItemClickInvoke {

    public static final String TAG = "AddPartsActivity";

    public static final String BUNDLE_ORDER_DETAIL_MAIN_MODEL = "BundleOrderDetailMainModel";
    public static final String BUNDLE_SUB_CATEGORY_MODEL = "BundleSubCategoryModel";
    public static final String BUNDLE_SELECTED_HOUR = "BundleSelectedHour";
    public static final String BUNDLE_SELECTED_MINUTE = "BundleSelectedMinute";

    Context context;
    private static Activity activity;
    View mainView;

    private LayoutInflater layoutInflater;

    String categoryId = null;
    String categoryName = "";
    String subCategoryId = null;
    String orderId = null;

    MyOptionsPickerView twoPicker;

    ArrayList<String> hourlist = new ArrayList<>();
    ArrayList<String> timelist = new ArrayList<>();

    OrderDetailMainModel orderDetailMainModel = null;

    List<SparePartSearchResultModel> partsResultList = new ArrayList<>();
    List<AddedPartModel> addedPartList = new ArrayList<>();

    //search results view....
    RecyclerView parts_search_result_recycler_view;
    PartsSearchResultListAdapter partsSearchResultListAdapter;
    LinearLayoutManager searchResultLinearLayoutManager;

    //added parts view....
    RecyclerView added_parts_recycler_view;
    AddedPartsListAdapter addedPartsListAdapter;
    LinearLayoutManager addedPartsLinearLayoutManager;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    SubCategoryListModel subCategoryListModel = new SubCategoryListModel();

    CategoryListModel categoryListModel = new CategoryListModel();

    ProgressBar searchLoader;
    private CardView send_for_approval_cardview;
    private ImageView back_iv;
    private LinearLayout added_parts_ll, search_results_parts_ll;
    private TextView select_category_tv, select_sub_category_tv, add_offline_part_tv, search_result_title_tv, added_parts_count_tv, hours_etv, minutes_etv;
    private EditText part_name_etv, part_price_etv, search_parts_etv;

    boolean
            isHourSelected = false,
            isMinuteSelected = false;

    String index = "0";
    String size = "50";

    boolean isOfflinepartsAdded = false;

    public static Intent newInstance(Activity activity, OrderDetailMainModel orderDetailMainModel) {
        Intent intent = new Intent(activity, AddPartsActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_ORDER_DETAIL_MAIN_MODEL, orderDetailMainModel);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.add_parts_activity);

        context = this;
        activity = this;

        this.layoutInflater = activity.getLayoutInflater();

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        added_parts_ll = (LinearLayout) findViewById(R.id.added_parts_ll);
        back_iv = (ImageView) findViewById(R.id.back_iv);

        select_category_tv = (TextView) findViewById(R.id.select_category_tv);
        select_sub_category_tv = (TextView) findViewById(R.id.select_sub_category_tv);
        add_offline_part_tv = (TextView) findViewById(R.id.add_offline_part_tv);
        search_result_title_tv = (TextView) findViewById(R.id.search_result_title_tv);
        added_parts_count_tv = (TextView) findViewById(R.id.added_parts_count_tv);
        hours_etv = (TextView) findViewById(R.id.hours_etv);
        minutes_etv = (TextView) findViewById(R.id.minutes_etv);

        part_name_etv = (EditText) findViewById(R.id.part_name_etv);
        part_price_etv = (EditText) findViewById(R.id.part_price_etv);
        search_parts_etv = (EditText) findViewById(R.id.search_parts_etv);

        send_for_approval_cardview = (CardView) findViewById(R.id.send_for_approval_cardview);
        search_results_parts_ll = (LinearLayout) findViewById(R.id.search_results_parts_ll);

        mainView = send_for_approval_cardview;
        twoPicker = new MyOptionsPickerView(activity);

        added_parts_ll.setVisibility(View.GONE);

        parts_search_result_recycler_view = (RecyclerView) findViewById(R.id.parts_search_result_recycler_view);
        added_parts_recycler_view = (RecyclerView) findViewById(R.id.added_parts_recycler_view);
        searchResultLinearLayoutManager = new LinearLayoutManager(activity);
        addedPartsLinearLayoutManager = new LinearLayoutManager(activity);

        if (getIntent().hasExtra(BUNDLE_ORDER_DETAIL_MAIN_MODEL)) {
            orderDetailMainModel = (OrderDetailMainModel) getIntent().getSerializableExtra(BUNDLE_ORDER_DETAIL_MAIN_MODEL);
        } else {
            orderDetailMainModel = null;
        }


        if (orderDetailMainModel != null) {
            setThisList(orderDetailMainModel);
        }
        initPartsSearchResultRecyclerView();
//        initAddedPartsRecyclerView();
        getCategoryList(context);

        if (orderDetailMainModel != null) {

            if (orderDetailMainModel.getOrderId() != null) {
                orderId = orderDetailMainModel.getOrderId();
            }
        }

        search_parts_etv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String searchText = search_parts_etv.getText().toString().trim();
                if (searchText.length() >= 1) {
                    //call parts list api...
                    SparePartsPostModel sparePartsPostModel = new SparePartsPostModel();
                    sparePartsPostModel.setIndex(index);
                    sparePartsPostModel.setSize(size);
                    sparePartsPostModel.setCategoryId(categoryId);
                    sparePartsPostModel.setSubCategoryId(subCategoryId);
                    sparePartsPostModel.setSearch(searchText);
                    getSparePartsList(context, sparePartsPostModel);
                } else {
                    partsResultList.clear();
                    initPartsSearchResultRecyclerView();
                    search_result_title_tv.setText("");
                }
            }
        });

        hours_etv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                openTimeDialog();
                openTwoPickerView();
//                Intent intent = TimeListActivity.newInstance(activity, globalVariables.FROM_HOUR);
//                startActivityForResult(intent, globalVariables.REQUEST_CODE_FOR_HOUR);
            }
        });

        minutes_etv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                openTimeDialog();
                openTwoPickerView();

//                Intent intent = TimeListActivity.newInstance(activity, globalVariables.FROM_MINUTE);
//                startActivityForResult(intent, globalVariables.REQUEST_CODE_FOR_MINUTE);
            }
        });

        add_offline_part_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput(false);
            }
        });

        send_for_approval_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInputForSparePartsApproval();
            }
        });

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        hourlist.clear();
        timelist.clear();
        setUpListForHour();
        setUpListForMinute();

        twoPicker.setPicker(hourlist, timelist, false);
        twoPicker.setTitle("");
        twoPicker.setCyclic(false, false, false);
        twoPicker.setSelectOptions(1, 1);

        twoPicker.setSubmitButtonText(activity.getString(R.string.submit));
        twoPicker.setCancelButtonText(activity.getString(R.string.cancel));

        twoPicker.setOnoptionsSelectListener(new MyOptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {

                if (hourlist.get(options1) != null) {
                    isHourSelected = true;
                    hours_etv.setText(hourlist.get(options1).replaceAll(" hours", ""));
                }

                if (timelist.get(option2) != null) {
                    isMinuteSelected = true;
                    minutes_etv.setText(timelist.get(option2).replaceAll(" min", ""));
                }

//                Toast.makeText(activity, "" + hourlist.get(options1) + " " + timelist.get(option2), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void openTwoPickerView() {
        if (GlobalFunctions.isKeyboardOpen(activity)) {
            GlobalFunctions.closeKeyboard(activity);
        }
        twoPicker.show();
    }

    private void openTimeDialog() {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.dialog_time_count, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        TextView submit;
        ImageView close_iv;
        PickerUI mPickerUI;
        CardView save_cv;

        close_iv = (ImageView) alertView.findViewById(R.id.close_iv);
        mPickerUI = (PickerUI) alertView.findViewById(R.id.picker_ui_view);
        save_cv = (CardView) alertView.findViewById(R.id.save_cardview);

        close_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        save_cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                isHourSelected = true;
//                hours_etv.setText(selectedHour);
//                minutes_etv.setText("00");

                dialog.dismiss();
            }
        });

        PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                .withItems(timelist)
//                .withBackgroundColor(getRandomColor())
                .withAutoDismiss(false)
                .withItemsClickables(true)
                .withUseBlur(true)
                .build();

        mPickerUI.slide(4);

        mPickerUI.setSettings(pickerUISettings);

        mPickerUI.setOnClickItemPickerUIListener(new PickerUI.PickerUIItemClickListener() {
            @Override
            public void onItemClickPickerUI(int which, int position, String valueResult) {

                Toast.makeText(activity, valueResult, Toast.LENGTH_SHORT).show();

//                isHourSelected = true;
//                hours_etv.setText(selectedHour);
//                minutes_etv.setText("00");

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(lp);


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void setUpListForMinute() {
        String min = " min";
        timelist.clear();
        for (int i = 0; i < 60; i++) {
            if (i == 0) {
                timelist.add("00" + min);
            } else if (i == 1) {
                timelist.add("01" + min);
            } else if (i == 2) {
                timelist.add("02" + min);
            } else if (i == 3) {
                timelist.add("03" + min);
            } else if (i == 4) {
                timelist.add("04" + min);
            } else if (i == 5) {
                timelist.add("05" + min);
            } else if (i == 6) {
                timelist.add("06" + min);
            } else if (i == 7) {
                timelist.add("07" + min);
            } else if (i == 8) {
                timelist.add("08" + min);
            } else if (i == 9) {
                timelist.add("09" + min);
            } else {
                timelist.add(i + min);
            }
        }

    }

    private void setUpListForHour() {
        String hours = " hours";
        hourlist.clear();
        for (int i = 0; i < 100; i++) {
            if (i == 0) {
                hourlist.add("00" + hours);
            } else if (i == 1) {
                hourlist.add("01" + hours);
            } else if (i == 2) {
                hourlist.add("02" + hours);
            } else if (i == 3) {
                hourlist.add("03" + hours);
            } else if (i == 4) {
                hourlist.add("04" + hours);
            } else if (i == 5) {
                hourlist.add("05" + hours);
            } else if (i == 6) {
                hourlist.add("06" + hours);
            } else if (i == 7) {
                hourlist.add("07" + hours);
            } else if (i == 8) {
                hourlist.add("08" + hours);
            } else if (i == 9) {
                hourlist.add("09" + hours);
            } else {
                hourlist.add(i + hours);
            }
        }
    }


    private void setThisList(OrderDetailMainModel orderDetailMainModel) {
        List<SparePartModel> sparePartList = orderDetailMainModel.getSparePartsList().getSparePartList();
        List<AddedPartModel> addedPartList = new ArrayList<AddedPartModel>();
        AddedPartsListModel addedPartsListModel = new AddedPartsListModel();
        if (sparePartList.size() > 0) {
            for (int i = 0; i < sparePartList.size(); i++) {
                SparePartModel sparePartModel = sparePartList.get(i);
                AddedPartModel addedPartModel = new AddedPartModel();

                if (sparePartModel.getId() != null) {
                    addedPartModel.setId(sparePartModel.getId());
                }

                if (sparePartModel.getTitle() != null) {
                    addedPartModel.setTitle(sparePartModel.getTitle());
                }

                if (sparePartModel.getImage() != null) {
                    addedPartModel.setImage(sparePartModel.getImage());
                }

                if (sparePartModel.getUnitPrice() != null) {
                    addedPartModel.setUnitPrice(sparePartModel.getUnitPrice());
                }

                if (sparePartModel.getQuantity() != null) {
                    addedPartModel.setQuantity(sparePartModel.getQuantity());
                }

                if (sparePartModel.getFixingPrice() != null) {
                    addedPartModel.setFixingPrice(sparePartModel.getFixingPrice());
                }

                if (sparePartModel.getTime() != null) {
                    addedPartModel.setTime(sparePartModel.getTime());
                }

                if (sparePartModel.getPrice() != null) {
                    addedPartModel.setPrice(sparePartModel.getPrice());
                }

                if (sparePartModel.getStatus() != null) {
                    addedPartModel.setStatus(sparePartModel.getStatus());
                }
                if (sparePartModel.getStatusTitle() != null) {
                    addedPartModel.setStatusTitle(sparePartModel.getStatusTitle());
                }
                if (sparePartModel.getHour() != null && sparePartModel.getMinute() != null) {
                    addedPartModel.setTime(sparePartModel.getHour() + ":" + sparePartModel.getMinute());
                }
                addedPartList.add(addedPartModel);
            }
        }

        addedPartsListModel.setAddedPartList(addedPartList);

        if (addedPartsListModel != null) {
            setAddedPartsList(addedPartsListModel);
        }

    }

    private void validateInputForSparePartsApproval() {
        String
                partName = part_name_etv.getText().toString().trim();

        if (partName.isEmpty() && !isOfflinepartsAdded) {
            globalFunctions.displayMessaage(context, mainView, getString(R.string.please_add_atleast_one_part));
        } else if (partName.length() > 0) {
            validateInput(true);
        } else if (isOfflinepartsAdded) {
            if (orderId != null) {
                submitSpareParts(context, orderId);
            }
        }

    }

    private void validateInput(boolean isApproval) {
        if (part_name_etv != null && part_price_etv != null) {
            String
                    partName = part_name_etv.getText().toString().trim(),
                    partPrice = part_price_etv.getText().toString().trim(),
                    hours = hours_etv.getText().toString().trim(),
                    minutes = minutes_etv.getText().toString().trim();

            if (partName.isEmpty()) {
                part_name_etv.setError(getString(R.string.add_part_name));
                part_name_etv.setFocusableInTouchMode(true);
                part_name_etv.requestFocus();
            } else if (partPrice.isEmpty()) {
                part_price_etv.setError(getString(R.string.please_add_price));
                part_price_etv.setFocusableInTouchMode(true);
                part_price_etv.requestFocus();
            } else if (!isHourSelected) {
                globalFunctions.displayMessaage(context, mainView, getString(R.string.select_time));
            } else if (hours.equalsIgnoreCase("00") && minutes.equalsIgnoreCase("00")) {
                globalFunctions.displayMessaage(context, mainView, getString(R.string.select_valid_time));
            } else {
                AddPartPostModel addPartPostModel = new AddPartPostModel();
                addPartPostModel.setOrderId(orderId);
                addPartPostModel.setCategoryId(categoryId);
                addPartPostModel.setSubCategoryId(subCategoryId);
                addPartPostModel.setTitle(partName);
                addPartPostModel.setPrice(partPrice);
                addPartPostModel.setTime(hours + ":" + minutes + ":" + "00");
                addPartPostModel.setQuantity("1");
                addSparePart(context, addPartPostModel, isApproval);
            }
        }
    }

    private void submitSpareParts(final Context context, final String orderId) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.submitSpareParts(context, orderId, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateSubmitSparePartOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "SubmitSpareParts");
    }

    private void validateSubmitSparePartOutput(Object model) {
        if (model instanceof StatusModel) {
            final StatusModel statusModel = (StatusModel) model;

            final AlertDialog alertDialog = new AlertDialog(context);
            alertDialog.setCancelable(false);
            alertDialog.setIcon(R.drawable.ic_logo_dark);
            alertDialog.setTitle(getString(R.string.app_name));
            alertDialog.setMessage(statusModel.getMessage());
            alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (statusModel.isStatus()) {
                        //go to active page..
                        Intent intent = MainActivity.newInstance(activity, globalVariables.TO_PAGE_ACTIVE_REQUESTS);
                        activity.startActivity(intent);
                        closeThisActivity();
                    }
                }
            });

            alertDialog.show();
        }
    }

    private void getSparePartsList(final Context context, final SparePartsPostModel model) {
        // globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getSparePartsList(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                // globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                SparePartsSearchResultListModel sparePartsSearchResultListModel = (SparePartsSearchResultListModel) arg0;
                setPartsResultList(sparePartsSearchResultListModel, model);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                // globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                // globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "GetSparePartsList");
    }

    private void setPartsResultList(SparePartsSearchResultListModel mSparePartsSearchResultListModel, SparePartsPostModel model) {
        partsResultList.clear();
        if (mSparePartsSearchResultListModel != null && partsResultList != null) {
            partsResultList.addAll(mSparePartsSearchResultListModel.getSparePartSearchResultList());
            if (partsSearchResultListAdapter != null) {
                synchronized (partsSearchResultListAdapter) {
                    partsSearchResultListAdapter.notifyDataSetChanged();
                }
            }
            if (partsResultList.size() <= 0) {
                search_results_parts_ll.setVisibility(View.VISIBLE);
                search_result_title_tv.setText(getString(R.string.no_results_found));
            } else {
                search_results_parts_ll.setVisibility(View.VISIBLE);
//                search_result_title_tv.setText(getString(R.string.showing_results_for) + " " + categoryName);
                search_result_title_tv.setText(getString(R.string.showing_results_for) + " " + model.getSearch());
                initPartsSearchResultRecyclerView();
            }
        }

    }

    private void getCategoryList(final Context context) {
        // globalFunctions.showProgress(context, context.getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getCategoryList(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                //  globalFunctions.hideProgress();
                CategoryListModel model = (CategoryListModel) arg0;
                setUpList(model);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                Log.d(TAG, "Failure : " + msg);
                // globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                Log.d(TAG, "Error : " + msg);
                // globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
            }
        }, "GetCategoryList");
    }

    private void setUpList(CategoryListModel categoryListModel1) {
        if (categoryListModel1 != null) {
            if (categoryListModel == null) {
                categoryListModel = new CategoryListModel();
            }
            categoryListModel.setCategoryList(categoryListModel1.getCategoryList());

            if (orderDetailMainModel != null) {

                if (orderDetailMainModel.getCategoryId() != null) {
                    categoryId = orderDetailMainModel.getCategoryId();
                }

                setSubCategoryList(categoryId);

                if (orderDetailMainModel.getCategoryTitle() != null) {
                    select_category_tv.setText(orderDetailMainModel.getCategoryTitle());
                    categoryName = orderDetailMainModel.getCategoryTitle();
                }
            }
        }
    }

    private void setSubCategoryList(String categoryId) {

        if (categoryId != null) {

            if (categoryListModel != null) {
                if (categoryListModel.getCategoryList() != null) {
                    int listSizeMain = categoryListModel.getCategoryList().size();

                    for (int i = 0; i < listSizeMain; i++) {
                        if (categoryId.equalsIgnoreCase(categoryListModel.getCategoryList().get(i).getId())) {
                            subCategoryListModel = new SubCategoryListModel();
                            if (categoryListModel.getCategoryList().get(i).getSubCategoryListModel() != null) {
                                if (categoryListModel.getCategoryList().get(i).getSubCategoryListModel().getSubCategoryListModels() != null) {
                                    subCategoryListModel.setSubCategoryListModels(categoryListModel.getCategoryList().get(i).getSubCategoryListModel().getSubCategoryListModels());
                                }
                            }
                        }
                    }
                }
            }
        }

        select_sub_category_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (subCategoryListModel != null) {
                    Intent intent = SubCategoryListActivity.newInstance(activity, subCategoryListModel);
                    startActivityForResult(intent, globalVariables.REQUEST_CODE_FOR_SUB_CATEGORY);
                }
            }
        });
    }

    private void initPartsSearchResultRecyclerView() {
        parts_search_result_recycler_view.setLayoutManager(searchResultLinearLayoutManager);
        parts_search_result_recycler_view.setHasFixedSize(true);
        partsSearchResultListAdapter = new PartsSearchResultListAdapter(activity, partsResultList, this);
        parts_search_result_recycler_view.setAdapter(partsSearchResultListAdapter);
        if (partsSearchResultListAdapter != null) {
            synchronized (partsSearchResultListAdapter) {
                partsSearchResultListAdapter.notifyDataSetChanged();
            }
        }
    }

    private void initAddedPartsRecyclerView() {
        added_parts_recycler_view.setLayoutManager(addedPartsLinearLayoutManager);
        added_parts_recycler_view.setHasFixedSize(true);
        addedPartsListAdapter = new AddedPartsListAdapter(activity, addedPartList, this);
        added_parts_recycler_view.setAdapter(addedPartsListAdapter);
        if (addedPartsListAdapter != null) {
            synchronized (addedPartsListAdapter) {
                addedPartsListAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        GlobalFunctions.hideProgress();
        if (resultCode == activity.RESULT_OK && requestCode == globalVariables.REQUEST_CODE_FOR_SUB_CATEGORY) {
            SubCategoryModel mySubCategoryModel = (SubCategoryModel) data.getExtras().getSerializable(BUNDLE_SUB_CATEGORY_MODEL);
            if (mySubCategoryModel != null) {
                subCategoryId = mySubCategoryModel.getId();
                select_sub_category_tv.setText(mySubCategoryModel.getTitle());
            }
        } else if (resultCode == activity.RESULT_OK && requestCode == globalVariables.REQUEST_CODE_FOR_HOUR) {

            String selectedHour = (String) data.getExtras().getSerializable(BUNDLE_SELECTED_HOUR);
            if (selectedHour != null) {
                isHourSelected = true;
                hours_etv.setText(selectedHour);
                minutes_etv.setText("00");
            }

        } else if (resultCode == activity.RESULT_OK && requestCode == globalVariables.REQUEST_CODE_FOR_MINUTE) {

            String selectedMinute = (String) data.getExtras().getSerializable(BUNDLE_SELECTED_MINUTE);
            if (selectedMinute != null) {
                isMinuteSelected = true;
                minutes_etv.setText(selectedMinute);
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void OnAddPartClickInvoke(int position, SparePartSearchResultModel sparePartSearchResultModel) {
        if (sparePartSearchResultModel != null) {
            //add part...
            AddPartPostModel addPartPostModel = new AddPartPostModel();
            addPartPostModel.setOrderId(orderId);
            addPartPostModel.setSparePartId(sparePartSearchResultModel.getId());
            addPartPostModel.setQuantity(sparePartSearchResultModel.getSelectedQuantity());
            addSparePart(context, addPartPostModel, false);
        }
    }

    private void addSparePart(final Context context, final AddPartPostModel model, final boolean isApproval) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.addSparePart(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validatAddPartOutput(arg0, true, isApproval);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "AddSparePart");
    }

    private void validatAddPartOutput(Object model, boolean isFromAddSparePart, boolean isApproval) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            if (!statusModel.isStatus()) {
                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
                isOfflinepartsAdded = false;
            }
        } else {
            if (isFromAddSparePart) {
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.part_added_successfully));
            } else {
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.part_removed_successfully));
            }
            AddedPartsListModel addedPartsListModel = (AddedPartsListModel) model;
            if (addedPartsListModel != null) {
                setAddedPartsList(addedPartsListModel);
            }
            isOfflinepartsAdded = true;
            clearEnteredPartDetails();

            if (isApproval) {
                if (orderId != null) {
                    submitSpareParts(context, orderId);
                }
            }
        }
    }

    private void clearEnteredPartDetails() {
        part_name_etv.setText("");
        part_price_etv.setText("");
        hours_etv.setText(getString(R.string.hh));
        minutes_etv.setText(getString(R.string.mm));
        isHourSelected = false;
        isMinuteSelected = false;
    }

    private void setAddedPartsList(AddedPartsListModel addedPartsListModel) {
        if (addedPartsListModel != null && addedPartList != null) {
            addedPartList.clear();
            addedPartList.addAll(addedPartsListModel.getAddedPartList());
            if (addedPartList.size() > 0) {
                added_parts_ll.setVisibility(View.VISIBLE);
                added_parts_count_tv.setText(getString(R.string.added) + " " + addedPartList.size() + " " + getString(R.string.parts));
                initAddedPartsRecyclerView();
            } else {
                added_parts_ll.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void OnPartDeleteClickInvoke(int position, AddedPartModel addedPartModel) {

        if (addedPartModel != null) {
            //delete part..
            if (addedPartModel.getId() != null) {
                removeSparePart(context, addedPartModel.getId());
            }
        }

    }

    private void removeSparePart(final Context context, final String id) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.removeSparePart(context, id, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validatAddPartOutput(arg0, false, false);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "RemoveSparePart");
    }
}