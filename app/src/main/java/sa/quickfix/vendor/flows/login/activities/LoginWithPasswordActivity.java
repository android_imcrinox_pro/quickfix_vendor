package sa.quickfix.vendor.flows.login.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import sa.quickfix.vendor.flows.about_us.AboutUsActivity;
import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.flows.reset_password.activities.ForgotPasswordActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.LoginModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.RegisterModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;

import com.facebook.FacebookSdk;


public class LoginWithPasswordActivity extends AppCompatActivity {

    public static final String TAG = "LoginWithPassword";

    public static final String BUNDLE_REGISTER_MODEL = "BundleRegisterModel";
    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";

    Context context;
    private static Activity activity;
    View mainView;

    private TextView login_tv, forgot_tv;
    private LinearLayout reister_ll;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    String pageType = null;
    RegisterModel myRegisterModel = null;

    LoginModel loginModel = null;
    RegisterModel registerModel = null;

    String selected_country_code = "";

    private EditText phone_number_etv, password_etv;
    private TextView terms_and_conditions_tv,privacy_policy_tv;

    public static Intent newInstance(Activity activity, String pageType, RegisterModel registerModel) {
        Intent intent = new Intent(activity, LoginWithPasswordActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_REGISTER_MODEL, registerModel);
        args.putString(BUNDLE_KEY_FROM_PAGE, pageType);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login_with_password_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        login_tv = (TextView) findViewById(R.id.login_tv);
        forgot_tv = (TextView) findViewById(R.id.forgot_tv);
        reister_ll = (LinearLayout) findViewById(R.id.reister_ll);
        terms_and_conditions_tv = (TextView) findViewById(R.id.terms_and_conditions_tv);
        privacy_policy_tv = (TextView) findViewById(R.id.privacy_policy_tv);

        phone_number_etv = (EditText) findViewById(R.id.phone_number_etv);
        password_etv = (EditText) findViewById(R.id.password_etv);

        mainView = login_tv;

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_PAGE)) {
            pageType = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_PAGE);
        } else {
            pageType = null;
        }

        if (getIntent().hasExtra(BUNDLE_REGISTER_MODEL)) {
            myRegisterModel = (RegisterModel) getIntent().getSerializableExtra(BUNDLE_REGISTER_MODEL);
        } else {
            myRegisterModel = null;
        }

        if (pageType != null) {
            if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_OTP) || pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_LOGIN_WITH_PASSWORD)) {
                //set phone number and country code....
                if (myRegisterModel != null) {
                    selected_country_code = myRegisterModel.getCountryCode();
                   // country_code_picker.setCountryForPhoneCode(Integer.parseInt(myRegisterModel.getCountryCode()));
                    phone_number_etv.setText(myRegisterModel.getMobileNumber());
                }
            }
        }

        login_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
            }
        });

        reister_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInputForRegistration();
            }
        });

        forgot_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInputForForgotPassword();
            }
        });

        terms_and_conditions_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = AboutUsActivity.newInstance(activity, GlobalVariables.PAGE_LOGIN_TNC);
                startActivity(intent);
//                GlobalFunctions.openBrowser(context,GlobalVariables.HTTP_URL_TERMS_AND_CONDITION);
            }
        });
        privacy_policy_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = AboutUsActivity.newInstance(activity, GlobalVariables.PAGE_PP);
                startActivity(intent);
//                GlobalFunctions.openBrowser(context,GlobalVariables.HTTP_URL_TERMS_AND_CONDITION);
            }
        });
    }

    private void validateInputForForgotPassword() {
        if (myRegisterModel != null) {
            if (registerModel == null) {
                registerModel = new RegisterModel();
            }
            registerModel.setCountryCode(selected_country_code);
            registerModel.setMobileNumber(myRegisterModel.getMobileNumber());
            Intent intent = ForgotPasswordActivity.newInstance(context, registerModel);
            startActivity(intent);
        }
    }

    private void validateInputForRegistration() {

        if (loginModel == null) {
            loginModel = new LoginModel();
        }
        if (myRegisterModel != null) {
            loginModel.setMobile(myRegisterModel.getMobileNumber());
            loginModel.setCountryCode(selected_country_code);
        }
        validateInputForOtp(globalVariables.PAGE_FROM_REGISTRATION, loginModel);
    }

    private void validateInput() {
        if (phone_number_etv != null && password_etv != null) {
            String
                    mobileNumber = phone_number_etv.getText().toString().trim(),
                    password = password_etv.getText().toString().trim();

            if (selected_country_code.isEmpty()) {
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.countryCodeNONotValid));
            } else if (mobileNumber.isEmpty()) {
                phone_number_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                phone_number_etv.setFocusableInTouchMode(true);
                phone_number_etv.requestFocus();
            } else if (password.isEmpty()) {
                globalFunctions.displayMessaage(activity, mainView, getString(R.string.pleaseFillMandatoryDetails));
            /*    password_etv.setError(getString(R.string.pleaseFillMandatoryDetails));
                password_etv.setFocusableInTouchMode(true);
                password_etv.requestFocus();*/
            } else {
                if (loginModel == null) {
                    loginModel = new LoginModel();
                }
                loginModel.setPassword(globalFunctions.md5(password));
                loginModel.setMobile(mobileNumber);
                loginModel.setCountryCode(selected_country_code);
                loginUser(context, loginModel);
            }
        }
    }

    private void loginUser(final Context context, final LoginModel model) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.loginUser(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validateOutput(arg0, model);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "Login_User");
    }

    private void validateOutput(Object model, LoginModel loginModel) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            if (!statusModel.isStatus()) {
                if (statusModel.getExtra().equalsIgnoreCase("1")) {
                    //not registered...
                    // globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
                    showAlertMessage(statusModel.getMessage(), loginModel);
                } else if (statusModel.getExtra().equalsIgnoreCase("4")) {
                    //Your Account has not been Verified Yet,Please Contact QuickFix Support
                    showAlertMessage(statusModel.getMessage());
                } else if (statusModel.getExtra().equalsIgnoreCase("3")) {
                    //Your Account has been Blocked by QuickFix,Please Contact QuickFix Support
                    showAlertMessage(statusModel.getMessage());
                } else if (statusModel.getExtra().equalsIgnoreCase("5")) {
                    //  Please Use Website for Company Login
                    showAlertMessage(statusModel.getMessage());
                } else {
                    globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
                }
            }
        } else {
            ProfileModel profileModel = (ProfileModel) model;
            globalFunctions.setProfile(context, profileModel);
            if(profileModel != null){
                if(profileModel.getToken() != null){
                    GlobalFunctions.setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_TOKEN, profileModel.getToken());
                }
            }
            closeThisActivity();
            Intent intent = new Intent(activity, MainActivity.class);
            startActivity(intent);
        }
    }

    private void showAlertMessage(String message) {

        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finishAffinity();
            }
        });

        alertDialog.show();
    }

    private void showAlertMessage(String message, final LoginModel loginModel) {

        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                //go to registration page...
                if (loginModel != null) {
                    validateInputForOtp(globalVariables.PAGE_FROM_REGISTRATION, loginModel);
                }
             /*   Intent intent = RegisterActivity.newInstance(context, loginModel);
                startActivity(intent);*/
            }
        });

        alertDialog.setNegativeButton(getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void validateInputForOtp(String pageType, LoginModel myLoginModel) {
        if (myLoginModel != null) {
            if (registerModel == null) {
                registerModel = new RegisterModel();
            }
            registerModel.setCountryCode(myLoginModel.getCountryCode());
            registerModel.setMobileNumber(myLoginModel.getMobile());

            LoginModel model = new LoginModel();
            model.setMobile(registerModel.getMobileNumber());
            checkMobileNumber(context, model, registerModel,pageType);
        }
    }

    private void checkMobileNumber(final Context context, final LoginModel loginModel, final RegisterModel registerModel,final String pageType) {
        GlobalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.checkMobileNumber(context, loginModel, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                GlobalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof StatusModel) {
                    validateOutputAfterCheckingMobileNumber((StatusModel) arg0, registerModel,pageType);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "CheckMobileNumber");
    }

    private void validateOutputAfterCheckingMobileNumber(StatusModel statusModel, RegisterModel registerModel,String pageType) {

        if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_REGISTRATION)) {
            if (!statusModel.isStatus()) {
                //new user....go for registration..
                sendOtpToMyMobileNumber(registerModel, pageType);
//                Intent intent = RegisterActivity.newInstance(context, registerModel);
//                startActivity(intent);
            } else {
                //registered user..
//                showAlertMessage(statusModel.getMessage());
                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            }
        }else if (pageType.equalsIgnoreCase(globalVariables.PAGE_FROM_LOGIN)) {
            if (statusModel.isStatus()) {
                sendOtpToMyMobileNumber(registerModel, pageType);
            } else {
//                showAlertMessage(statusModel.getMessage());
                //registered user..
                globalFunctions.displayMessaage(activity, mainView, statusModel.getMessage());
            }
        }

    }

    private void sendOtpToMyMobileNumber(RegisterModel registerModel1, String pageType) {
        String phoneNumber = null;
        if (registerModel1 != null) {
            phoneNumber = registerModel1.getCountryCode() + registerModel1.getMobileNumber();
        }
        Intent intent = OtpActivity.newInstance(context, registerModel1, phoneNumber, pageType);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

}