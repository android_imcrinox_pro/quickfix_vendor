package sa.quickfix.vendor.flows.request_detail.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.adapters.ReqSubListAdapter;
import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.OrderImagesListAdapter;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnTheWayStatusItemClickInvoke;
import sa.quickfix.vendor.adapters.StatusListAdapter;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.AddressModel;
import sa.quickfix.vendor.services.model.OrderDetailListModel;
import sa.quickfix.vendor.services.model.OrderDetailMainModel;
import sa.quickfix.vendor.services.model.OrderDetailModel;
import sa.quickfix.vendor.services.model.OrderModel;
import sa.quickfix.vendor.services.model.OrderStatusModel;
import sa.quickfix.vendor.services.model.OrderStatusUpdateModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.StatusListModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;


public class RequestDetailsActivity extends AppCompatActivity implements OnTheWayStatusItemClickInvoke {

    public static final String TAG = "RequestDetailsActivity";

    public static final String BUNDLE_ORDER_MODEL = "BundleOrderModel";
    public static final String BUNDLE_KEY_FROM_PAGE = "BundleKeyFromPage";

    Context context;
    private static Activity activity;
    View mainView;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    OrderModel orderModel = null;
    String fromPage = null;
    String orderId = null;
    String workActive = "";
    private LayoutInflater layoutInflater;

    AddressModel pickUpAddressModel = new AddressModel();

    StatusListModel statusListModel = new StatusListModel();
    List<OrderStatusModel> list = new ArrayList<>();

    LinearLayoutManager layoutManager,linearLayoutManager, orderImagelayoutManager;
    RecyclerView status_recycler_view,services_rv, orderImage_recycler_view;
    StatusListAdapter statusListAdapter;
    OrderImagesListAdapter orderImagesListAdapter;

    private View client_description_and_images_view, images_view, req_to_cancel_view;

    private CircleImageView user_iv;
    private ImageView back_iv, category_iv, call_iv;
    private LinearLayout find_on_map_ll, client_message_ll, client_picture_ll;
    private CardView cancel_cardview, status_cardview;
    private TextView user_name_tv, date_tv, address_tv, category_title_tv, services_tv, no_of_tv, items_count_tv, order_number_tv, client_message_tv, status_header_tv,
            req_to_cancel_tv, add_comments_tv, sub_total_tv, tax_tv, discount_fee_price_tv, total_tv, job_status_tv;

    ReqSubListAdapter reqSubListAdapter;
    List<OrderDetailModel> orderDetailList = new ArrayList<>();

    private boolean
            isCommentsGiven = false,
            isImagesGiven = false;

    OrderStatusModel orderStatusModel = null;

    public static Intent newInstance(Context context, OrderModel model, String fromPage) {
        Intent intent = new Intent(context, RequestDetailsActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_ORDER_MODEL, model);
        args.putString(BUNDLE_KEY_FROM_PAGE, fromPage);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.request_details_activity);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        this.layoutInflater = activity.getLayoutInflater();
        linearLayoutManager = new LinearLayoutManager(activity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);
        category_iv = (ImageView) findViewById(R.id.category_iv);
        call_iv = (ImageView) findViewById(R.id.call_iv);
        user_iv = (CircleImageView) findViewById(R.id.user_iv);

        user_name_tv = (TextView) findViewById(R.id.user_name_tv);
        date_tv = (TextView) findViewById(R.id.date_tv);
        address_tv = (TextView) findViewById(R.id.address_tv);
        category_title_tv = (TextView) findViewById(R.id.category_title_tv);
        services_tv = (TextView) findViewById(R.id.services_tv);
        no_of_tv = (TextView) findViewById(R.id.no_of_tv);
        items_count_tv = (TextView) findViewById(R.id.items_count_tv);
        order_number_tv = (TextView) findViewById(R.id.order_number_tv);
        client_message_tv = (TextView) findViewById(R.id.client_message_tv);
        req_to_cancel_tv = (TextView) findViewById(R.id.req_to_cancel_tv);
        add_comments_tv = (TextView) findViewById(R.id.add_comments_tv);
        sub_total_tv = (TextView) findViewById(R.id.sub_total_tv);
        tax_tv = (TextView) findViewById(R.id.tax_tv);
        discount_fee_price_tv = (TextView) findViewById(R.id.discount_fee_price_tv);
        total_tv = (TextView) findViewById(R.id.total_tv);
        job_status_tv = (TextView) findViewById(R.id.job_status_tv);
        status_header_tv = (TextView) findViewById(R.id.status_header_tv);

        client_picture_ll = (LinearLayout) findViewById(R.id.client_picture_ll);
        client_message_ll = (LinearLayout) findViewById(R.id.client_message_ll);
        find_on_map_ll = (LinearLayout) findViewById(R.id.find_on_map_ll);
        cancel_cardview = (CardView) findViewById(R.id.cancel_cardview);
        status_cardview = (CardView) findViewById(R.id.status_cardview);
        status_recycler_view = (RecyclerView) findViewById(R.id.status_recycler_view);
        orderImage_recycler_view = (RecyclerView) findViewById(R.id.pictures_recycler_view);
        services_rv = (RecyclerView) findViewById(R.id.services_rv);

        client_description_and_images_view = (View) findViewById(R.id.client_description_and_images_view);
        images_view = (View) findViewById(R.id.images_view);
        req_to_cancel_view = (View) findViewById(R.id.req_to_cancel_view);

        layoutManager = new LinearLayoutManager(context);
        orderImagelayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        if (getIntent().hasExtra(BUNDLE_ORDER_MODEL)) {
            orderModel = (OrderModel) getIntent().getSerializableExtra(BUNDLE_ORDER_MODEL);
        } else {
            orderModel = null;
        }

        if (getIntent().hasExtra(BUNDLE_KEY_FROM_PAGE)) {
            fromPage = (String) getIntent().getStringExtra(BUNDLE_KEY_FROM_PAGE);
        } else {
            fromPage = null;
        }

        initStatusList();
        initOrderList();

        if (orderModel != null) {
            if (orderModel.getOrderId() != null) {
                orderId = orderModel.getOrderId();
                getOrderDetails(context, orderModel.getOrderId());
            }
        }

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        add_comments_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        job_status_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderStatusModel != null) {
                    if (orderStatusModel.getId() != null && orderId != null) {
                        //update status...
                        OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                        orderStatusUpdateModel.setOrderId(orderId);
                        orderStatusUpdateModel.setStatus(orderStatusModel.getId());
                        updateOrderStatus(context, orderStatusUpdateModel);
                    }
                }
            }
        });

    }

    private void getOrderDetails(final Context context, String orderId) {
        globalFunctions.showProgress(context, context.getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getOrderDetails(context, orderId, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                validateOutput(arg0);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                Log.d(TAG, "Failure : " + msg);
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                Log.d(TAG, "Error : " + msg);
                globalFunctions.hideProgress();
                globalFunctions.displayMessaage(context, mainView, msg);
            }
        }, "GetOrderDetails");
    }

    private void validateOutput(Object model) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            if (!statusModel.isStatus()) {
                showAlertMessage(statusModel.getMessage());
            }
        } else {
            OrderDetailMainModel orderDetailMainModel = (OrderDetailMainModel) model;
            setThisPage(orderDetailMainModel);
        }
    }

    private void showAlertMessage(String message) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });

        alertDialog.show();
    }

    private void showAlertMessage(final StatusModel statusModel, final OrderStatusUpdateModel orderStatusUpdateModel) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(statusModel.getMessage());
        alertDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (statusModel.isStatus()) {

                    if (orderStatusUpdateModel != null) {

                        if (orderStatusUpdateModel.getStatus() != null) {
                            if (orderStatusUpdateModel.getStatus().equalsIgnoreCase(globalVariables.ON_THE_WAY)) {
                                //go to active page..
                                Intent intent = MainActivity.newInstance(activity, globalVariables.TO_PAGE_ACTIVE_REQUESTS);
                                activity.startActivity(intent);
                                closeThisActivity();
                            } else if (orderStatusUpdateModel.getStatus().equalsIgnoreCase(globalVariables.CANCELLED)) {
                                //go to main page..
                                Intent intent = new Intent(RequestDetailsActivity.this, MainActivity.class);
                                activity.startActivity(intent);
                                closeThisActivity();
                            }
                        }

                    }
                }
            }
        });

        alertDialog.show();
    }

    private void initStatusList() {
        statusListAdapter = new StatusListAdapter(activity, list, this, fromPage, workActive);
        status_recycler_view.setLayoutManager(layoutManager);
        status_recycler_view.setAdapter(statusListAdapter);

        synchronized (statusListAdapter) {
            statusListAdapter.notifyDataSetChanged();
        }
    }

    private void initOrderList() {
        reqSubListAdapter = new ReqSubListAdapter(activity, orderDetailList);
        services_rv.setLayoutManager(linearLayoutManager);
        services_rv.setAdapter(reqSubListAdapter);

        synchronized (reqSubListAdapter) {
            reqSubListAdapter.notifyDataSetChanged();
        }
    }

    private void setThisPage(final OrderDetailMainModel model) {

        if (model != null) {

            if (model.getOrderId() != null) {
                orderId = model.getOrderId();
            }

            workActive = model.getWorkActive();

            if (GlobalFunctions.isNotNullValue(model.getOrderNumber())) {
                order_number_tv.setText(activity.getString(R.string.order_number) + " " + model.getOrderNumber());
            }

            if (fromPage != null) {
                if (fromPage.equalsIgnoreCase(globalVariables.FROM_PAGE_COMPLETED_REQUESTS)) {
                    cancel_cardview.setVisibility(View.GONE);
                    req_to_cancel_view.setVisibility(View.GONE);
                    req_to_cancel_tv.setVisibility(View.GONE);
                    add_comments_tv.setVisibility(View.GONE);
                    status_header_tv.setVisibility(View.VISIBLE);
                    status_recycler_view.setVisibility(View.VISIBLE);
                } else if (fromPage.equalsIgnoreCase(globalVariables.FROM_PAGE_NEW_REQUESTS)) {
                    cancel_cardview.setVisibility(View.GONE);
                    req_to_cancel_view.setVisibility(View.GONE);
                    add_comments_tv.setVisibility(View.GONE);
                    req_to_cancel_tv.setVisibility(View.GONE);
                    status_header_tv.setVisibility(View.GONE);
                    status_recycler_view.setVisibility(View.GONE);
                } else {
                    req_to_cancel_tv.setVisibility(View.VISIBLE);
                    req_to_cancel_view.setVisibility(View.VISIBLE);
                    add_comments_tv.setVisibility(View.VISIBLE);
                    cancel_cardview.setVisibility(View.GONE);
                    status_header_tv.setVisibility(View.VISIBLE);
                    status_recycler_view.setVisibility(View.VISIBLE);
                }
            }

            String
                    date = "",
                    time = "";

            if (model.getStatusList() != null) {
                setStatusList(model.getStatusList());
            }
            if (GlobalFunctions.isNotNullValue(model.getUserImage())) {
                SeparateProgress.loadImage(user_iv, model.getUserImage(), SeparateProgress.setProgress(activity));
            }

            if (model.getUserName() != null) {
                user_name_tv.setText(model.getUserName());
            }

            if (model.getDate() != null) {
                date = GlobalFunctions.getDayMonthYearFromDate(model.getDate());
            }

            if (model.getTime() != null) {
                time = GlobalFunctions.getTimeFromDate(model.getTime());
            }

            date_tv.setText(date + " " + "|" + " " + time);

            if (model.getAddress() != null) {
                address_tv.setText(model.getAddress());
            }

            find_on_map_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (model.getLatitude() != null && model.getLongitude() != null) {
                        //GlobalFunctions.showOnMap(activity, "", model.getLatitude(), model.getLongitude());
                        GlobalFunctions.openMapView(activity, model.getLatitude(), model.getLongitude());
//                        setAdsLocation(model.getLatitude(), model.getLongitude());
//                        Intent mIntent = SearchPlaceOnMapActivity.newInstance(context, pickUpAddressModel, GlobalVariables.LOCATION_TYPE.NONE);
//                        startActivity(mIntent);
                    }
                /*    if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                        return;
                    } else if (!sLocationEnabled(activity)) {
                        showSettingsAlert();
                    } else {
                        if (model.getLatitude() != null && model.getLongitude() != null) {
                            setAdsLocation(model.getLatitude(), model.getLongitude());
                            Intent mIntent = SearchPlaceOnMapActivity.newInstance(context, pickUpAddressModel, GlobalVariables.LOCATION_TYPE.NONE);
                            startActivity(mIntent);
                        }
                    }*/
                }
            });

            call_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (globalFunctions.isNotNullValue(model.getCommunicationNumber())) {
                        globalFunctions.callPhone(activity, model.getCommunicationNumber());
                    } else if (globalFunctions.isNotNullValue(model.getUserMobileNumber())) {
                        globalFunctions.callPhone(activity, model.getUserMobileNumber());
                    }
                }
            });

            if (model.getCategoryTitle() != null) {
                category_title_tv.setText(model.getCategoryTitle());
//                no_of_tv.setText(getString(R.string.number_of) + " " + model.getCategoryTitle());
            }

            if (GlobalFunctions.isNotNullValue(model.getCategoryIcon())) {
                SeparateProgress.loadImage(category_iv, model.getCategoryIcon(), SeparateProgress.setProgress(activity));
            }


           /* if (model.getOrderDetailList() != null) {
                if (model.getOrderDetailList().getOrderDetailList() != null) {
                    String number = String.format("%02d", model.getOrderDetailList().getOrderDetailList().size());
                    items_count_tv.setText(number);
                }
            }*/

//            setServicesNames(model);

            if (GlobalFunctions.isNotNullValue(model.getComments())) {
                client_message_ll.setVisibility(View.VISIBLE);
                client_message_tv.setText(model.getComments());
                isCommentsGiven = true;
            } else {
                client_message_ll.setVisibility(View.GONE);
                isCommentsGiven = false;
            }

            if (GlobalFunctions.isNotNullValue(model.getStatusTitle())) {
                job_status_tv.setText(model.getStatusTitle());
            }

            if (GlobalFunctions.isNotNullValue(model.getSubTotal())) {
                sub_total_tv.setText(model.getSubTotal() + " " + activity.getString(R.string.sar));
            }

            if (GlobalFunctions.isNotNullValue(model.getVat())) {
                tax_tv.setText(model.getVat() + " " + activity.getString(R.string.sar));
            }

            if (GlobalFunctions.isNotNullValue(model.getDiscountTotal())) {
                discount_fee_price_tv.setText(model.getDiscountTotal() + " " + activity.getString(R.string.sar));
            }

            if (GlobalFunctions.isNotNullValue(model.getGrandTotal())) {
                total_tv.setText(model.getGrandTotal() + " " + activity.getString(R.string.sar));
            }

            if (model.getOrderImageList() != null) {
                setImageList(model.getOrderImageList().getImages());
            }

            if (model.getStatusList() != null) {
                setStatusList(model.getStatusList());
            }

            if (model.getOrderDetailList() != null) {
                setOrderList(model.getOrderDetailList());
            }

            cancel_cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openCancelJobAlertDialog(context);
                }
            });

            req_to_cancel_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openCancelJobAlertDialog(context);
                }
            });

            if (isCommentsGiven || isImagesGiven) {
                client_description_and_images_view.setVisibility(View.VISIBLE);
                if (isImagesGiven){
                    images_view.setVisibility(View.VISIBLE);
                }else {
                    images_view.setVisibility(View.GONE);
                }
            } else {
                client_description_and_images_view.setVisibility(View.GONE);
                images_view.setVisibility(View.GONE);
            }

            if (fromPage.equalsIgnoreCase(globalVariables.FROM_PAGE_COMPLETED_REQUESTS) || fromPage.equalsIgnoreCase(globalVariables.FROM_PAGE_NEW_REQUESTS)) {
                status_cardview.setVisibility(View.GONE);
            } else {
                if (workActive != null) {
                    if (workActive.equalsIgnoreCase("1")) {
                        status_cardview.setVisibility(View.GONE);
                    } else {
                        status_cardview.setVisibility(View.VISIBLE);
                    }
                } else {
                    status_cardview.setVisibility(View.VISIBLE);
                }
            }

        }
    }

    private void setImageList(ArrayList<String> imagelist) {
        if (imagelist.size() > 0) {
            client_picture_ll.setVisibility(View.VISIBLE);
            // orderImage_recycler_view.setVisibility(View.VISIBLE);
            initImageList(imagelist);
            isImagesGiven = true;
        } else {
            client_picture_ll.setVisibility(View.GONE);
            isImagesGiven = false;
            //  orderImage_recycler_view.setVisibility(View.GONE);
        }
    }

    private void initImageList(ArrayList<String> imagelist) {
        orderImagesListAdapter = new OrderImagesListAdapter(activity, imagelist);
        orderImage_recycler_view.setLayoutManager(orderImagelayoutManager);
        orderImage_recycler_view.setAdapter(orderImagesListAdapter);

        synchronized (orderImagesListAdapter) {
            orderImagesListAdapter.notifyDataSetChanged();
        }
    }

    public boolean sLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);

// Setting Dialog Title
        alertDialog.setTitle("GPS settings");

// Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

// Setting Icon to Dialog
//alertDialog.setIcon(R.drawable.delete);

// On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivity(intent);
            }
        });

// on pressing cancel button
        alertDialog.setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

// Showing Alert Message
        alertDialog.show();
    }

    private void openCancelJobAlertDialog(final Context context) {

        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(activity);
        final View alertView = layoutInflater.inflate(R.layout.alert_custom_dialog_for_cancel_request, null, false);
        alertDialog.setView(alertView);
        alertDialog.setCancelable(true);
        // alertDialog.setIcon(R.drawable.app_icon);
        final android.app.AlertDialog dialog = alertDialog.create();

        TextView submit_tv, cancel_tv;
        final EditText reason_etv;

        reason_etv = alertView.findViewById(R.id.reason_etv);
        submit_tv = alertView.findViewById(R.id.submit_tv);
        cancel_tv = alertView.findViewById(R.id.cancel_tv);

        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        submit_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = false;
                String reason = reason_etv.getText().toString().trim();
                if (reason.isEmpty()) {
                    reason_etv.setError(activity.getString(R.string.please_enter_reason));
                    reason_etv.setFocusableInTouchMode(true);
                    reason_etv.requestFocus();
                } else {
                    isValid = true;
                    if (orderId != null) {
                        //update status...
                        OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                        orderStatusUpdateModel.setOrderId(orderId);
                        orderStatusUpdateModel.setStatus(globalVariables.CANCELLED);
                        orderStatusUpdateModel.setReason(reason);
                        updateOrderStatus(context, orderStatusUpdateModel);
                    }
                }
                if (isValid) {
                    dialog.dismiss();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void setServicesNames(OrderDetailMainModel model) {
        if (model != null) {
            List<String> servicesNames = new ArrayList<>();
            servicesNames.clear();
            if (model.getOrderDetailList() != null) {
                if (model.getOrderDetailList().getServiceTitle() != null) {
                    servicesNames.addAll(model.getOrderDetailList().getServiceTitle());
                    services_tv.setText(globalFunctions.getStringFromList(servicesNames));
                }
            }
        }
    }

    private void setAdsLocation(String myLatitude, String myLongitude) {
        if (pickUpAddressModel == null) {
            pickUpAddressModel = new AddressModel();
        }
        double latitude = 0.0;
        double longitude = 0.0;
        try {
            latitude = Double.parseDouble(myLatitude);
            longitude = Double.parseDouble(myLongitude);
        } catch (Exception e) {
            latitude = 0.0;
            longitude = 0.0;
        }
        pickUpAddressModel.setLatitude(latitude);
        pickUpAddressModel.setLongitude(longitude);
    }

    private void setStatusList(StatusListModel statusList) {
        if (statusList != null && list != null) {
            list.clear();
            list.addAll(statusList.getStatusList());
            if (list.size() > 0) {
                initStatusList();
                this.orderStatusModel = GlobalFunctions.getNameFromList(list);
                if (orderStatusModel != null) {
                    job_status_tv.setText(orderStatusModel.getTitle());
                }
            }
        }
    }

    private void setOrderList(OrderDetailListModel orderDetailListModel) {
        if (orderDetailListModel != null && orderDetailList != null) {
            orderDetailList.clear();
            orderDetailList.addAll(orderDetailListModel.getOrderDetailList());
            if (orderDetailList.size() > 0) {
                initOrderList();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeThisActivity();
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    public void OnSelectClickInvoke(int position, OrderStatusModel orderStatusModel) {

        if (orderStatusModel != null) {
            if (orderStatusModel.getId() != null && orderId != null) {
                //update status...
                OrderStatusUpdateModel orderStatusUpdateModel = new OrderStatusUpdateModel();
                orderStatusUpdateModel.setOrderId(orderId);
                orderStatusUpdateModel.setStatus(orderStatusModel.getId());
                updateOrderStatus(context, orderStatusUpdateModel);
            }
        }

    }

    @Override
    public void OnTroubleClickInvoke(int position, OrderStatusModel orderStatusModel) {
        ProfileModel profileModel = GlobalFunctions.getProfile(context);
        if (GlobalFunctions.isNotNullValue(profileModel.getSupportNumber())) {
            GlobalFunctions.callPhone(activity, profileModel.getSupportNumber());
        }
    }

    private void updateOrderStatus(final Context context, final OrderStatusUpdateModel model) {
        globalFunctions.showProgress(context, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.updateOrderStatus(context, model, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                validatOrderStatusUpdateOutput(arg0, model);
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                //GlobalFunctions.displayMessaage(context, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "UpdateOrderStatus");
    }

    private void validatOrderStatusUpdateOutput(Object model, OrderStatusUpdateModel orderStatusUpdateModel) {
        if (model instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) model;
            showAlertMessage(statusModel, orderStatusUpdateModel);
        }
    }
}