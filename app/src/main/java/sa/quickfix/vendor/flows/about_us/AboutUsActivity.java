package sa.quickfix.vendor.flows.about_us;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.AboutUsMainModel;
import sa.quickfix.vendor.services.model.LoginModel;
import sa.quickfix.vendor.services.model.RegisterModel;
import sa.quickfix.vendor.services.model.StatusModel;
import sa.quickfix.vendor.view.ProgressDialogs;


public class AboutUsActivity extends AppCompatActivity {

    public static final String TAG = "AboutUsActivity";
    public static final String BUNDLE_PAGE_TYPE = "AboutUsActivityPageType";

    Context context;
    private static Activity activity;
    View mainView;

    TextView about_tv, toolbarTitle_tv;

    ImageView back_iv, menu_user_iv;


    GlobalFunctions globalFunctions;
    GlobalVariables globalVariables;

    String pageType = null;

    public static Intent newInstance(Activity activity, String pageType) {
        Intent intent = new Intent(activity, AboutUsActivity.class);
        Bundle args = new Bundle();
        args.putString(BUNDLE_PAGE_TYPE, pageType);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }


        if (getIntent().hasExtra(BUNDLE_PAGE_TYPE)) {
            pageType = (String) getIntent().getStringExtra(BUNDLE_PAGE_TYPE);
        } else {
            pageType = null;
        }

        back_iv = (ImageView) findViewById(R.id.back_iv);
        menu_user_iv = (ImageView) findViewById(R.id.menu_user_iv);

        toolbarTitle_tv = (TextView) findViewById(R.id.toolbarTitle_tv);
        about_tv = (TextView) findViewById(R.id.about_tv);

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (GlobalFunctions.isNotNullValue(pageType)) {
            getTerms(activity,GlobalVariables.PAGE_LOGIN_TNC);
        }

        if (GlobalFunctions.isNotNullValue(pageType) && pageType.equalsIgnoreCase(GlobalVariables.PAGE_ABOUT)) {
            toolbarTitle_tv.setText(activity.getString(R.string.about_us));
        } else if (GlobalFunctions.isNotNullValue(pageType) && pageType.equalsIgnoreCase(GlobalVariables.PAGE_PP)) {
            toolbarTitle_tv.setText(activity.getString(R.string.privacy_policy));
        } else if (GlobalFunctions.isNotNullValue(pageType) && pageType.equalsIgnoreCase(GlobalVariables.PAGE_LOGIN_TNC)) {
            toolbarTitle_tv.setText(activity.getString(R.string.terms_and_conditions));
        }

    }


    private void getTerms(final Context context,String page) {
        GlobalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getTerms(context,page, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                GlobalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof AboutUsMainModel) {
                    AboutUsMainModel aboutUsMainModel= (AboutUsMainModel) arg0;
                    setThisPage(aboutUsMainModel);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                GlobalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "CheckMobileNumber");
    }

    private void setThisPage(AboutUsMainModel model) {
        if (model != null && model.getResponse() != null) {
            if (GlobalFunctions.isNotNullValue(model.getResponse().getDescription())) {
                about_tv.setText(globalFunctions.getHTMLString(model.getResponse().getDescription()));
//                about_tv.setText(globalFunctions.html2text(about_tv.getText().toString().trim()));
            }
        }
    }

    @Override
    public void onBackPressed() {
        closeThisActivity();

    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (activity != null) activity = null;
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}