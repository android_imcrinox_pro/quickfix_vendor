package sa.quickfix.vendor.flows.registration.activities.individual_registration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.vlonjatg.progressactivity.ProgressRelativeLayout;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.CityListAdapter;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnItemClickInvoke;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.MyCityListModel;
import sa.quickfix.vendor.services.model.MyCityModel;
import sa.quickfix.vendor.R;

public class CityListActivity extends AppCompatActivity implements OnItemClickInvoke {
    public static final String TAG = "CityListActivity";
    public static final String BUNDLE_MY_CITY_MODE = "BundleMyCityModel";
    public static final String BUNDLE_REQUEST_ID = "BundleRequestId";

    Context context = null;
    static Activity activity = null;

    int index = 0;
    int preLast = 0;
    boolean dataEnd = false;
    boolean loadingList = false;

    int visibleItemCount = 0, totalItemCount = 0, pastVisiblesItems = 0;

    RecyclerView recyclerView;
    public View mainView;

    LinearLayoutManager layoutManager;

    CityListAdapter adapter;

    ProgressRelativeLayout progressActivity;
    DilatingDotsProgressBar progressBar;

    List<MyCityModel> list = new ArrayList<>();

    MyCityModel myCityModel = null;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;
    Window window = null;

    Toolbar toolbar;
    ActionBar actionBar;
    String mTitle;
    int mResourceID, titleResourseID;
    TextView toolbar_title;
    ImageView toolbar_icon;
    AppBarLayout appBarLayout;
    Menu menu;

    LinearLayoutManager linearLayoutManager;

    EditText search_etv;

    public static Intent newInstance(Activity activity, MyCityModel myCityModel) {
        Intent intent = new Intent(activity, CityListActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_MY_CITY_MODE, myCityModel);
        intent.putExtras(args);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.city_list_activity);

        context = this;
        activity = this;
        window = getWindow();

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        index = 0;
        preLast = 0;
        dataEnd = false;

        progressActivity = findViewById(R.id.details_progressActivity);
        progressBar = (DilatingDotsProgressBar) findViewById(R.id.extraProgressBar);
        recyclerView = (RecyclerView) findViewById(R.id.city_list_recyclerView);

        search_etv = (EditText) findViewById(R.id.search_etv);

        linearLayoutManager = new LinearLayoutManager(activity);

        mainView = recyclerView;

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        //toolbar.setPadding(0, GlobalFunctions.getStatusBarHeight(context), 0, 0);
        //toolbar.setNavigationIcon(R.drawable.ic_back_draw);
        //toolbar.setContentInsetsAbsolute(0,0);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_icon = (ImageView) toolbar.findViewById(R.id.toolbar_icon);
        toolbar_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.ColorStatusBar));
        }

        if (getIntent().hasExtra(BUNDLE_MY_CITY_MODE)) {
            myCityModel = (MyCityModel) getIntent().getSerializableExtra(BUNDLE_MY_CITY_MODE);
        } else {
            myCityModel = null;
        }

        initRecyclerView();

        loadList(context);

        search_etv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String searchText = search_etv.getText().toString().trim();
                if (adapter != null) {
                    adapter.getFilter().filter(searchText);
                }
            }
        });
    }

    private void loadList(final Context context) {
        globalFunctions.showProgress(activity, getString(R.string.loading));
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.getCityList(context, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                globalFunctions.hideProgress();
                Log.d(TAG, "Response : " + arg0.toString());
                if (arg0 instanceof MyCityListModel) {
                    MyCityListModel myCityListModel1 = (MyCityListModel) arg0;
                    setUpList(myCityListModel1);
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
                globalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Failure : " + msg);
            }

            @Override
            public void OnError(String msg) {
                globalFunctions.hideProgress();
                GlobalFunctions.displayMessaage(activity, mainView, msg);
                Log.d(TAG, "Error : " + msg);
            }
        }, "GetCityList");
    }

    private void setUpList(MyCityListModel mCityListModel) {
        list.clear();
        if (mCityListModel != null && list != null) {
            list.addAll(mCityListModel.getCityList());
            if (adapter != null) {
                synchronized (adapter) {
                    adapter.notifyDataSetChanged();
                }
            }
            if (list.size() <= 0) {
                showEmptyPage();
            } else {
//                showContent();
                initRecyclerView();
            }
        }

    }

    public void initRecyclerView() {
        adapter = new CityListAdapter(activity, list, this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        list.clear();
        super.onStart();
    }

    @Override
    protected void onResume() {
        setTitle(getString(R.string.select_city), 0, 0);
        super.onResume();
    }

    public void setTitle(String title, int titleImageID, int backgroundResourceID) {
        mTitle = title;
        if (backgroundResourceID != 0) {
            mResourceID = backgroundResourceID;
        } else {
            mResourceID = 0;
        }
        if (titleImageID != 0) {
            titleResourseID = titleImageID;
        } else {
            titleResourseID = 0;
        }
        restoreToolbar();
    }

    private void restoreToolbar() {
        //toolbar = (Toolbar) findViewById(R.id.tool_bar);
        Log.d(TAG, "Restore Tool Bar");
        if (actionBar != null) {
            Log.d(TAG, "Restore Action Bar not Null");
            Log.d(TAG, "Title : " + mTitle);
            if (titleResourseID != 0) {
                toolbar_title.setVisibility(View.GONE);
            } else {
                toolbar_title.setVisibility(View.VISIBLE);
                toolbar_title.setText(mTitle);
            }


            if (mResourceID != 0) toolbar.setBackgroundResource(mResourceID);
            //actionBar.setTitle("");
            //actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (appBarLayout != null) {
            appBarLayout.bringToFront();
        }

    }

    private void showContent() {
        if (progressActivity != null) {
            progressActivity.showContent();
        }
    }

    private void showEmptyPage() {
        if (progressActivity != null) {
            progressActivity.showEmpty(getResources().getDrawable(R.drawable.ic_logo), "",
                    getString(R.string.empty_city));
        }
    }

    private void showErrorPage() {
        if (progressActivity != null) {
            progressActivity.showError(getResources().getDrawable(R.drawable.app_icon), getString(R.string.noConnection),
                    getString(R.string.noConnectionErrorMessage),
                    getString(R.string.tryAgain), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            index = 1;
                            preLast = 0;
                            dataEnd = false;
                            // getBlogList(context);
                        }
                    });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_nothing, menu);
        this.menu = menu;

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_nothing, menu);
        this.menu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                break;
        }
        return false;
    }

    public static void closeThisActivity() {
        if (activity != null) {
            activity.finish();
            //activity.overridePendingTransition(R.anim.zoom_in,R.anim.zoom_out);
        }
    }

    @Override
    public void onBackPressed() {
        closeThisActivity();
    }

    @Override
    public void OnClickInvoke(int position, MyCityModel myCityModel) {
        if (myCityModel != null) {
            setResult(true, myCityModel);
        }
    }

    private void setResult(boolean isSuccess, MyCityModel cityModel) {
        Intent intent = new Intent();
        intent.putExtra(BUNDLE_MY_CITY_MODE, cityModel);
        if (isSuccess) setResult(RESULT_OK, intent);
        else setResult(RESULT_CANCELED, intent);
        closeThisActivity();
    }

}

