package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnPartTimeEditClickInvoke;
import sa.quickfix.vendor.services.model.SparePartModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

public class AddedPartsStatusListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "AddedPartsStatusListAdapter";

    private final Activity activity;
    private final List<SparePartModel> list;
    OnPartTimeEditClickInvoke listener;
    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private TextView part_name_tv, price_tv, status_tv, edit_time_tv, fixing_time_tv;
        private ImageView item_iv, status_iv;
        private RelativeLayout fixing_time_rl;

        public CityPercentageListViewholder(View view) {
            super(view);

            part_name_tv = itemView.findViewById(R.id.part_name_tv);
            price_tv = itemView.findViewById(R.id.price_tv);
            status_tv = itemView.findViewById(R.id.status_tv);
            item_iv = itemView.findViewById(R.id.item_iv);
            status_iv = itemView.findViewById(R.id.status_iv);
            fixing_time_rl = itemView.findViewById(R.id.fixing_time_rl);
            edit_time_tv = itemView.findViewById(R.id.edit_time_tv);
            fixing_time_tv = itemView.findViewById(R.id.fixing_time_tv);
        }
    }

    public AddedPartsStatusListAdapter(Activity activity, List<SparePartModel> list, OnPartTimeEditClickInvoke listener) {
        this.activity = activity;
        this.list = list;
        this.listener = listener;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.added_parts_status_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final SparePartModel item = list.get(position);

            holder.status_iv.setVisibility(View.GONE);
            holder.fixing_time_rl.setVisibility(View.GONE);
            holder.edit_time_tv.setVisibility(View.GONE);

            if (item.getTime() != null) {
                if (item.getTime().equalsIgnoreCase("00:00:00")) {
                    //hide time view....
                    holder.fixing_time_rl.setVisibility(View.GONE);
                } else {
                    //show time view....
                    holder.fixing_time_rl.setVisibility(View.VISIBLE);
                    // holder.fixing_time_tv.setText(GlobalFunctions.getOnlyTimeFromDate(item.getTime()));

                    try {
                        String string = item.getTime();
                        String[] timeParts = string.split(":");
                        if (timeParts.length == 3) {
                            String hour = timeParts[0];
                            String minute = timeParts[1];
                            item.setHour(hour);
                            item.setMinute(minute);
                            holder.fixing_time_tv.setText(hour + " : " + minute);
                        }
                    } catch (Exception exccc) {
                    }
                }
            }

            if (GlobalFunctions.isNotNullValue(item.getImage())) {
                SeparateProgress.loadImage(holder.item_iv,item.getImage(), SeparateProgress.setProgress(activity));
            }

            if (item.getTitle() != null) {
                holder.part_name_tv.setText(item.getTitle());
            }

            if (item.getStatusTitle() != null) {
                holder.status_tv.setText(item.getStatusTitle());
            }

            if (item.getStatus() != null) {
                if (item.getStatus().equalsIgnoreCase(globalVariables.PART_STATUS_ADDED)) {
                    holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.ColorAccent));
                    holder.edit_time_tv.setVisibility(View.GONE);
                    // holder.status_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_tic));
                    holder.status_iv.setVisibility(View.GONE);
                } else if (item.getStatus().equalsIgnoreCase(globalVariables.PART_STATUS_PENDING)) {
                    holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.ColorPrimaryLight));
                    holder.status_iv.setVisibility(View.GONE);
                    holder.edit_time_tv.setVisibility(View.GONE);
                    //  holder.status_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_tic));
                } else if (item.getStatus().equalsIgnoreCase(globalVariables.PART_STATUS_ACCEPTED)) {
                    holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.green));
                    holder.status_iv.setVisibility(View.VISIBLE);
                    holder.edit_time_tv.setVisibility(View.GONE);
                    holder.status_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_tic));
                } else if (item.getStatus().equalsIgnoreCase(globalVariables.PART_STATUS_REJECTED)) {
                    holder.status_iv.setVisibility(View.VISIBLE);
                    holder.edit_time_tv.setVisibility(View.GONE);
                    holder.status_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_cancelled));
                    holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.red));
                }
            }

            if (item.getPrice() != null) {
                holder.price_tv.setText(activity.getString(R.string.sar) + " " + item.getPrice());
            }

            holder.edit_time_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnEditClickInvoke(position, item);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

