package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.services.model.CategoryModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

public class SelectedServicesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "SelectedServicesListAdapter";

    private final Activity activity;
    private final List<CategoryModel> list;
    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;

    private View mainView;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView category_iv;
        private TextView category_name_tv, services_name_tv;

        ViewHolder(View view) {
            super(view);
            category_iv = itemView.findViewById(R.id.category_iv);
            category_name_tv = itemView.findViewById(R.id.category_name_tv);
            services_name_tv = itemView.findViewById(R.id.services_name_tv);
        }
    }

    public SelectedServicesListAdapter(Activity activity, List<CategoryModel> list) {
        this.activity = activity;
        this.list = list;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_services_list_adapter, parent, false);
        mainView = itemView;
        return new SelectedServicesListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        final CategoryModel model = list.get(position);
        if (viewHolder instanceof SelectedServicesListAdapter.ViewHolder) {
            final SelectedServicesListAdapter.ViewHolder holder = (SelectedServicesListAdapter.ViewHolder) viewHolder;
            final CategoryModel item = model;

            if (GlobalFunctions.isNotNullValue(item.getImage())) {
                SeparateProgress.loadImage(holder.category_iv,item.getImage(), SeparateProgress.setProgress(activity));
            }

            if (item.getTitle() != null) {
                holder.category_name_tv.setText(item.getTitle());
            }

            List<String> selectedServiceNamesList = new ArrayList<>();
            selectedServiceNamesList.clear();

            if (item.getSubCategoryListModel() != null) {
                int listSizeSub = item.getSubCategoryListModel().getSubCategoryListModels().size();
                if (listSizeSub > 0) {
                    for (int j = 0; j < listSizeSub; j++) {
                        if (item.getSubCategoryListModel().getSubCategoryListModels().get(j).getSelected().equalsIgnoreCase("1")) {
                            selectedServiceNamesList.add(item.getSubCategoryListModel().getSubCategoryListModels().get(j).getTitle());
                        }
                    }
                    if (selectedServiceNamesList.size() == listSizeSub) {
                        // all selected..
                        holder.services_name_tv.setText(activity.getString(R.string.all_services));
                    } else {
                        try {
                            holder.services_name_tv.setText(globalFunctions.getStringFromList(selectedServiceNamesList));
                        } catch (Exception excc) {
                            holder.services_name_tv.setText("");
                        }
                    }
                } else if (item.getSelected().equalsIgnoreCase("1")) {
                    //selectedCategoryList.add(item);
                    holder.services_name_tv.setText("");
                }
            } else if (item.getSelected().equalsIgnoreCase("1")) {
                //selectedCategoryList.add(item);
                holder.services_name_tv.setText("");
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

