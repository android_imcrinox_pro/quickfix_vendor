package sa.quickfix.vendor.adapters;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import sa.quickfix.vendor.R;
import sa.quickfix.vendor.adapters.adapter_interfaces.ShiftItemClickInvoke;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.model.ShiftModel;
import sa.quickfix.vendor.view.AlertDialog;

public class ScheduleDateListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "ScheduleDateListAdapter";

    private final Activity activity;
    private final List<ShiftModel> list;
    ShiftItemClickInvoke listener;
    Calendar
            start_dateCalender,
            end_dateCalender;

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private TextView title_tv, add_shift_tv, shift_sunday_tv, shift_monday_tv, shift_tuesday_tv, shift_wednesday_tv,
                shift_thursday_tv, shift_friday_tv, shift_saturday_tv, select_time_from_tv, select_time_to_tv, delete_shift_tv;

        public CityPercentageListViewholder(View view) {
            super(view);

            title_tv = itemView.findViewById(R.id.title_tv);
            add_shift_tv = itemView.findViewById(R.id.add_shift_tv);
            shift_sunday_tv = itemView.findViewById(R.id.shift_sunday_tv);
            shift_monday_tv = itemView.findViewById(R.id.shift_monday_tv);
            shift_tuesday_tv = itemView.findViewById(R.id.shift_tuesday_tv);
            shift_wednesday_tv = itemView.findViewById(R.id.shift_wednesday_tv);
            shift_thursday_tv = itemView.findViewById(R.id.shift_thursday_tv);
            shift_friday_tv = itemView.findViewById(R.id.shift_friday_tv);
            shift_saturday_tv = itemView.findViewById(R.id.shift_saturday_tv);
            select_time_from_tv = itemView.findViewById(R.id.select_time_from_tv);
            select_time_to_tv = itemView.findViewById(R.id.select_time_to_tv);
            delete_shift_tv = itemView.findViewById(R.id.delete_shift_tv);
        }
    }

    public ScheduleDateListAdapter(Activity activity, List<ShiftModel> list, ShiftItemClickInvoke listener) {
        this.activity = activity;
        this.list = list;
        this.listener = listener;

        start_dateCalender = Calendar.getInstance();
        end_dateCalender = Calendar.getInstance();
        start_dateCalender.setTimeZone(TimeZone.getDefault());
        end_dateCalender.setTimeZone(TimeZone.getDefault());

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schedule_date_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final ShiftModel item = list.get(position);
            int val = position + 1;

            /*if (GlobalFunctions.isNotNullValue(item.getId()) && !item.getId().equalsIgnoreCase("0")) {
                holder.delete_shift_tv.setVisibility(View.VISIBLE);

            } else {
                holder.delete_shift_tv.setVisibility(View.VISIBLE);
            }*/

            holder.title_tv.setText(activity.getString(R.string.shift) + " (" + val + ")");

            if (GlobalFunctions.isNotNullValue(item.getStartTime())) {
                holder.select_time_from_tv.setText(GlobalFunctions.getTimeFromDate(item.getStartTime()));
                holder.delete_shift_tv.setVisibility(View.VISIBLE);
            } else {
                holder.select_time_from_tv.setHint(activity.getString(R.string.select_time_from));
                holder.delete_shift_tv.setVisibility(View.GONE);
            }

            if (GlobalFunctions.isNotNullValue(item.getEndTime())) {
                holder.select_time_to_tv.setText(GlobalFunctions.getTimeFromDate(item.getEndTime()));
            } else {
                holder.select_time_to_tv.setHint(activity.getString(R.string.select_time_to));
            }

            if (GlobalFunctions.isNotNullValue(item.getMonday()) && item.getMonday().equalsIgnoreCase(GlobalVariables.MONDAY)) {
                holder.shift_monday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_monday_tv.setTextColor(activity.getResources().getColor(R.color.white));
            } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.MONDAY)) {
                holder.shift_monday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_monday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                item.setMonday(GlobalVariables.MONDAY);
            } else {
                holder.shift_monday_tv.setBackground(null);
                holder.shift_monday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));
            }

            if (GlobalFunctions.isNotNullValue(item.getTuesDay()) && item.getTuesDay().equalsIgnoreCase(GlobalVariables.TUESDAY)) {
                holder.shift_tuesday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_tuesday_tv.setTextColor(activity.getResources().getColor(R.color.white));
            } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.TUESDAY)) {
                holder.shift_tuesday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_tuesday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                item.setTuesDay(GlobalVariables.TUESDAY);
            } else {
                holder.shift_tuesday_tv.setBackground(null);
                holder.shift_tuesday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));
            }

            if (GlobalFunctions.isNotNullValue(item.getWedDay()) && item.getWedDay().equalsIgnoreCase(GlobalVariables.WEDDAY)) {
                holder.shift_wednesday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_wednesday_tv.setTextColor(activity.getResources().getColor(R.color.white));
            } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.WEDDAY)) {
                holder.shift_wednesday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_wednesday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                item.setWedDay(GlobalVariables.WEDDAY);
            } else {
                holder.shift_wednesday_tv.setBackground(null);
                holder.shift_wednesday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));
            }

            if (GlobalFunctions.isNotNullValue(item.getThuDay()) && item.getThuDay().equalsIgnoreCase(GlobalVariables.THRDAY)) {
                holder.shift_thursday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_thursday_tv.setTextColor(activity.getResources().getColor(R.color.white));
            } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.THRDAY)) {
                holder.shift_thursday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_thursday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                item.setThuDay(GlobalVariables.THRDAY);
            } else {
                holder.shift_thursday_tv.setBackground(null);
                holder.shift_thursday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));
            }

            if (GlobalFunctions.isNotNullValue(item.getFriDay()) && item.getFriDay().equalsIgnoreCase(GlobalVariables.FRIDAY)) {
                holder.shift_friday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_friday_tv.setTextColor(activity.getResources().getColor(R.color.white));
            } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.FRIDAY)) {
                holder.shift_friday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_friday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                item.setFriDay(GlobalVariables.FRIDAY);
            } else {
                holder.shift_friday_tv.setBackground(null);
                holder.shift_friday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));
            }

            if (GlobalFunctions.isNotNullValue(item.getSatDay()) && item.getSatDay().equalsIgnoreCase(GlobalVariables.SATDAY)) {
                holder.shift_saturday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_saturday_tv.setTextColor(activity.getResources().getColor(R.color.white));
            } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.SATDAY)) {
                holder.shift_saturday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_saturday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                item.setSatDay(GlobalVariables.SATDAY);
            } else {
                holder.shift_saturday_tv.setBackground(null);
                holder.shift_saturday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));
            }

            if (GlobalFunctions.isNotNullValue(item.getSunday()) && item.getSunday().equalsIgnoreCase(GlobalVariables.SUNDAY)) {
                holder.shift_sunday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_sunday_tv.setTextColor(activity.getResources().getColor(R.color.white));
            } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.SUNDAY)) {
                holder.shift_sunday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                holder.shift_sunday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                item.setSunday(GlobalVariables.SUNDAY);
            } else {
                holder.shift_sunday_tv.setBackground(null);
                holder.shift_sunday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));
            }

            holder.shift_monday_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (GlobalFunctions.isNotNullValue(item.getMonday()) && item.getMonday().equalsIgnoreCase(GlobalVariables.MONDAY)) {
                        item.setMonday("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_monday_tv.setBackground(null);
                        holder.shift_monday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.MONDAY)) {
                        item.setMonday("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_monday_tv.setBackground(null);
                        holder.shift_monday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else {
                        item.setMonday(GlobalVariables.MONDAY);
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_monday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                        holder.shift_monday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                    }
                    notifyDataSetChanged();

                }
            });

            holder.shift_tuesday_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (GlobalFunctions.isNotNullValue(item.getTuesDay()) && item.getTuesDay().equalsIgnoreCase(GlobalVariables.TUESDAY)) {
                        item.setTuesDay("");
                        holder.shift_tuesday_tv.setBackground(null);
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_tuesday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.TUESDAY)) {
                        item.setTuesDay("");
                        holder.shift_tuesday_tv.setBackground(null);
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_tuesday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else {
                        item.setTuesDay(GlobalVariables.TUESDAY);
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_tuesday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                        holder.shift_tuesday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                    }
                    notifyDataSetChanged();

                }
            });

            holder.shift_wednesday_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (GlobalFunctions.isNotNullValue(item.getWedDay()) && item.getWedDay().equalsIgnoreCase(GlobalVariables.WEDDAY)) {
                        item.setWedDay("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_wednesday_tv.setBackground(null);
                        holder.shift_wednesday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.WEDDAY)) {
                        item.setWedDay("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_wednesday_tv.setBackground(null);
                        holder.shift_wednesday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else {
                        item.setWedDay(GlobalVariables.WEDDAY);
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_wednesday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                        holder.shift_wednesday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                    }
                    notifyDataSetChanged();

                }
            });

            holder.shift_thursday_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (GlobalFunctions.isNotNullValue(item.getThuDay()) && item.getThuDay().equalsIgnoreCase(GlobalVariables.THRDAY)) {
                        item.setThuDay("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_thursday_tv.setBackground(null);
                        holder.shift_thursday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.THRDAY)) {
                        item.setThuDay("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_thursday_tv.setBackground(null);
                        holder.shift_thursday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else {
                        item.setThuDay(GlobalVariables.THRDAY);
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_thursday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                        holder.shift_thursday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                    }
                    notifyDataSetChanged();

                }
            });

            holder.shift_friday_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (GlobalFunctions.isNotNullValue(item.getFriDay()) && item.getFriDay().equalsIgnoreCase(GlobalVariables.FRIDAY)) {
                        item.setFriDay("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_friday_tv.setBackground(null);
                        holder.shift_friday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.FRIDAY)) {
                        item.setFriDay("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_friday_tv.setBackground(null);
                        holder.shift_friday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else {
                        item.setFriDay(GlobalVariables.FRIDAY);
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_friday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                        holder.shift_friday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                    }
                    notifyDataSetChanged();

                }
            });

            holder.shift_saturday_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (GlobalFunctions.isNotNullValue(item.getSatDay()) && item.getSatDay().equalsIgnoreCase(GlobalVariables.SATDAY)) {
                        item.setSatDay("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_saturday_tv.setBackground(null);
                        holder.shift_saturday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.SATDAY)) {
                        item.setSatDay("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_saturday_tv.setBackground(null);
                        holder.shift_saturday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else {
                        item.setSatDay(GlobalVariables.SATDAY);
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_saturday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                        holder.shift_saturday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                    }
                    notifyDataSetChanged();

                }
            });

            holder.shift_sunday_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (GlobalFunctions.isNotNullValue(item.getSunday()) && item.getSunday().equalsIgnoreCase(GlobalVariables.SUNDAY)) {
                        item.setSunday("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_sunday_tv.setBackground(null);
                        holder.shift_sunday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else if (GlobalFunctions.isNotNullValue(item.getDays()) && item.getDays().contains(GlobalVariables.SUNDAY)) {
                        item.setSunday("");
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_sunday_tv.setBackground(null);
                        holder.shift_sunday_tv.setTextColor(activity.getResources().getColor(R.color.ColorAccent));

                    } else {
                        item.setSunday(GlobalVariables.SUNDAY);
                        item.setDays(GlobalFunctions.getUpdatedList(item));
                        holder.shift_sunday_tv.setBackground(activity.getResources().getDrawable(R.drawable.bg_circle_color_accent));
                        holder.shift_sunday_tv.setTextColor(activity.getResources().getColor(R.color.white));
                    }
                    notifyDataSetChanged();

                }
            });

            holder.select_time_from_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectTime(true, item, holder.select_time_from_tv);
                }
            });

            holder.select_time_to_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectTime(false, item, holder.select_time_to_tv);
                }
            });

            holder.delete_shift_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!item.getId().equalsIgnoreCase("0")) {
                        OpenCancelDialog(activity, item);
                    } else {
                        listener.OnClickInvoke(0, item);
                    }

                }
            });

            holder.add_shift_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!GlobalFunctions.isNotNullValue(item.getDays())) {
                        GlobalFunctions.displayMessaage(activity, holder.add_shift_tv, activity.getString(R.string.add_one_shift));
                    }else if (!GlobalFunctions.isNotNullValue(item.getStartTime())) {
                        GlobalFunctions.displayMessaage(activity, holder.add_shift_tv, activity.getString(R.string.select_time_from));
                    } else if (!GlobalFunctions.isNotNullValue(item.getEndTime())) {
                        GlobalFunctions.displayMessaage(activity, holder.add_shift_tv, activity.getString(R.string.select_time_to));
                    } else {
                        ShiftModel shiftModel = new ShiftModel();
                        shiftModel.setId("0");
                        listener.OnAddInvoke(position, shiftModel);
                    }

                }
            });

        }
    }

    public void OpenCancelDialog(Context context, ShiftModel item) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(context.getString(R.string.app_name));
        alertDialog.setMessage(context.getString(R.string.confirm_delete));
        alertDialog.setPositiveButton(context.getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                listener.OnClickInvoke(0, item);
            }
        });

        alertDialog.setNegativeButton(context.getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void selectTime(boolean isFrom, ShiftModel shiftModel, TextView view) {
        Calendar mcurrentTime = Calendar.getInstance();
        if (isFrom) {
            if (start_dateCalender != null) {
                mcurrentTime.setTimeInMillis(start_dateCalender.getTimeInMillis());
            } else {
                mcurrentTime = Calendar.getInstance();
            }
        } else {
            if (end_dateCalender != null) {
                mcurrentTime.setTimeInMillis(end_dateCalender.getTimeInMillis());
            } else {
                mcurrentTime = Calendar.getInstance();
            }
        }

        final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        final int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if (isFrom) {
                    if (start_dateCalender != null) {
//                    setHour(selectedHour);
//                    setMinute(selectedMinute);
                        start_dateCalender.set(Calendar.HOUR_OF_DAY, selectedHour);
                        start_dateCalender.set(Calendar.MINUTE, selectedMinute);
                        setTime(isFrom, shiftModel, view, start_dateCalender);
                    }
                } else {
                    if (end_dateCalender != null) {
//                    setHour(selectedHour);
//                    setMinute(selectedMinute);
                        end_dateCalender.set(Calendar.HOUR_OF_DAY, selectedHour);
                        end_dateCalender.set(Calendar.MINUTE, selectedMinute);
                        setTime(isFrom, shiftModel, view, end_dateCalender);
                    }
                }

            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }


    private void setTime(boolean isfrom, ShiftModel shiftModel, TextView view, Calendar time) {
        String selectedEndTime = (GlobalFunctions.getTimefromTimestamp((time.getTimeInMillis() / 1000), GlobalVariables.TIME_FORMAT, Locale.ENGLISH));
        String timeFormated = (GlobalFunctions.getTimefromTimestamp((time.getTimeInMillis() / 1000), GlobalVariables.DATE_FORMAT_TIME_NEW, Locale.ENGLISH));
        view.setText(selectedEndTime);
        if (isfrom) {
            shiftModel.setStartTime(timeFormated);
        } else {
            shiftModel.setEndTime(timeFormated);
        }
    }

}

