package sa.quickfix.vendor.adapters;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnTheWayStatusItemClickInvoke;
import sa.quickfix.vendor.services.model.OrderStatusModel;
import sa.quickfix.vendor.R;

public class StatusListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "StatusListAdapter";

    private final Activity activity;
    private final List<OrderStatusModel> list;
    private final String fromPage;
    private final String workActive;
    OnTheWayStatusItemClickInvoke listener;

    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private TextView status_tv, select_tv;
        private LinearLayout select_ll;
        private ImageView selected_iv;
        private View select_line_view, selected_iv_line_view;

        public CityPercentageListViewholder(View view) {
            super(view);

            status_tv = itemView.findViewById(R.id.status_tv);
            select_tv = itemView.findViewById(R.id.select_tv);
            select_ll = itemView.findViewById(R.id.select_ll);
            selected_iv = itemView.findViewById(R.id.selected_iv);
            // select_line_view = itemView.findViewById(R.id.select_line_view);
            selected_iv_line_view = itemView.findViewById(R.id.selected_iv_line_view);
        }
    }

    public StatusListAdapter(Activity activity, List<OrderStatusModel> list, OnTheWayStatusItemClickInvoke listener, String fromPage, String workActive) {
        this.activity = activity;
        this.list = list;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
        this.listener = listener;
        this.fromPage = fromPage;
        this.workActive = workActive;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.status_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final OrderStatusModel item = list.get(position);

            if (fromPage != null) {
                if (fromPage.equalsIgnoreCase(globalVariables.FROM_PAGE_COMPLETED_REQUESTS) || fromPage.equalsIgnoreCase(globalVariables.FROM_PAGE_NEW_REQUESTS)) {
                    //from completed request..
                    holder.select_ll.setVisibility(View.GONE);
                    holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontBlackColor));
                    holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_selected));
                    //holder.selected_iv_line_view.setBackgroundColor(globalFunctions.getColor(activity, R.color.ColorAccentLighter));

                } else {
                    //from upcoming request..
                    if (position == 0) {
                        //first item...
                        if (workActive != null) {
                            if (workActive.equalsIgnoreCase("1")) {
                                //hide select
                                holder.select_ll.setVisibility(View.GONE);
                                holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontColor));
                            } else {
                                holder.select_ll.setVisibility(View.GONE);
                                holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontBlackColor));
                            }
                        } else {
                            holder.select_ll.setVisibility(View.GONE);
                            holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontBlackColor));
                        }
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_unselected));
                        //holder.selected_iv_line_view.setBackgroundColor(globalFunctions.getColor(activity, R.color.grey_lighter));
                    } else {
                        holder.select_ll.setVisibility(View.GONE);
                        holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontColor));
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_unselected));
                        // holder.selected_iv_line_view.setBackgroundColor(globalFunctions.getColor(activity, R.color.grey_lighter));
                    }
                }

                if (position == list.size() - 1) {
                    holder.selected_iv_line_view.setVisibility(View.GONE);
                }else {
                    holder.selected_iv_line_view.setVisibility(View.VISIBLE);
                }

                if (GlobalFunctions.isLastPosition(position, list)) {
                    holder.selected_iv_line_view.setBackgroundColor(globalFunctions.getColor(activity, R.color.ColorAccentLighter));
                } else {
                    holder.selected_iv_line_view.setBackgroundColor(globalFunctions.getColor(activity, R.color.grey_lighter));
                }
            }

            if (item.getTitle() != null) {
                holder.status_tv.setText(item.getTitle());
            }

            holder.select_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    listener.OnSelectClickInvoke(position, item);
                    listener.OnTroubleClickInvoke(position, item);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

