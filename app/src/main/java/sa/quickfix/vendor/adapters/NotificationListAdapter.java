package sa.quickfix.vendor.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnNotificationItemClickInvoke;
import sa.quickfix.vendor.services.model.MyNotificationModel;

public class NotificationListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "NotificationListAdapter";

    private final Activity activity;
    List<MyNotificationModel> userAddressList;
    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;
    OnNotificationItemClickInvoke listener;

    public NotificationListAdapter(Activity activity, List<MyNotificationModel> userAddressList, OnNotificationItemClickInvoke listener) {
        this.activity = activity;
        this.userAddressList = userAddressList;
        this.listener = listener;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_list_adapter, parent, false);

        return new MyAddressesListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof MyAddressesListViewholder) {
            final MyAddressesListViewholder holder = (MyAddressesListViewholder) viewHolder;

            final MyNotificationModel model = userAddressList.get(position);

            String
                    mTempDate = null,
                    mTempTime = null,
                    date = null,
                    time = null;

            if (GlobalFunctions.isNotNullValue(model.getTitle())) {
                holder.title_tv.setVisibility(View.VISIBLE);
                holder.title_tv.setText(model.getTitle());
            } else {
                holder.title_tv.setVisibility(View.GONE);
            }

            if (GlobalFunctions.isNotNullValue(model.getMessage())) {
                holder.message_tv.setVisibility(View.VISIBLE);
                holder.message_tv.setText(model.getMessage());
            } else {
                holder.message_tv.setVisibility(View.GONE);
            }

            if (model.getCreatedOn() != null) {

                try {
                    String string = model.getCreatedOn();
                    String[] dateParts = string.split(" ");
                    if (dateParts.length == 2) {
                        //mTempDate = dateParts[0];
                        mTempTime = dateParts[1];
                    }
                    mTempDate = model.getCreatedOn();

                    if (mTempDate != null) {
                        date = GlobalFunctions.getDayMonthYearFromDate(mTempDate);
                    }

                    if (mTempTime != null) {
                        time = GlobalFunctions.getTimeFromDate(mTempTime);
                    }

                    if (date != null && time != null) {
                        holder.date_tv.setVisibility(View.VISIBLE);
                        holder.date_tv.setText(date + " " + "|" + " " + time);
                    } else {
                        holder.date_tv.setVisibility(View.GONE);
                    }

                } catch (Exception exccc) {
                    holder.date_tv.setVisibility(View.GONE);
                }
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnClickInvoke(position, model);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return userAddressList.size();
    }

    public class MyAddressesListViewholder extends RecyclerView.ViewHolder {

        private TextView title_tv, message_tv, date_tv;

        public MyAddressesListViewholder(View view) {
            super(view);

            title_tv = itemView.findViewById(R.id.title_tv);
            message_tv = itemView.findViewById(R.id.message_tv);
            date_tv = itemView.findViewById(R.id.date_tv);
        }
    }

}

