package sa.quickfix.vendor.adapters.adapter_interfaces;

public interface OnYearItemClickInvoke {
    void OnClickInvoke(int position, String year);
}
