package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.List;

import sa.quickfix.vendor.adapters.adapter_interfaces.OnServiceItemClickInvoke;
import sa.quickfix.vendor.services.model.CategoryModel;
import sa.quickfix.vendor.services.model.SubCategoryModel;
import sa.quickfix.vendor.R;

public class ServicesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "ServicesListAdapter";

    private final Activity activity;
    private final List<SubCategoryModel> list;
    OnServiceItemClickInvoke listener;
    SelectServicesListAdapter.ViewHolder mViewHolder;
    CategoryModel categoryModel;
    boolean isSelectAllClicked;
    boolean isUnselectAllClicked;
    int categoryItemPosition;

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private AppCompatCheckBox service_name_checkbox;

        public CityPercentageListViewholder(View view) {
            super(view);

            service_name_checkbox = itemView.findViewById(R.id.service_name_checkbox);
        }
    }

    public ServicesListAdapter(Activity activity, List<SubCategoryModel> list, OnServiceItemClickInvoke listener, SelectServicesListAdapter.ViewHolder holder, CategoryModel categoryModel, boolean isSelectAllClicked, boolean isUnselectAllClicked, int position) {
        this.activity = activity;
        this.list = list;
        this.listener = listener;
        this.mViewHolder = holder;
        this.categoryModel = categoryModel;
        this.isSelectAllClicked = isSelectAllClicked;
        this.isUnselectAllClicked = isUnselectAllClicked;
        this.categoryItemPosition = position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.services_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final SubCategoryModel model = list.get(position);
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final SubCategoryModel item = model;

            if (item.getTitle() != null) {
                holder.service_name_checkbox.setText(item.getTitle());
            }

            if(isSelectAllClicked || isUnselectAllClicked){
                if (categoryModel != null) {
                    if(categoryModel.getSelectedAll() != null){
                        if (categoryModel.getSelectedAll().equalsIgnoreCase("0")) {
                            //not selected...
                            holder.service_name_checkbox.setChecked(false);
                            item.setSelected("0");
                            listener.OnClickInvoke(categoryItemPosition, item, mViewHolder);
                        } else if (categoryModel.getSelectedAll().equalsIgnoreCase("1")) {
                            // selected...
                            item.setSelected("1");
                            holder.service_name_checkbox.setChecked(true);
                            listener.OnClickInvoke(categoryItemPosition, item, mViewHolder);
                        }
                    }
                }
            }else{
                if (item.getSelected() != null) {
                    if (item.getSelected().equalsIgnoreCase("0")) {
                        //not selected...
                        item.setSelected("0");
                        holder.service_name_checkbox.setChecked(false);
                        listener.OnClickInvoke(categoryItemPosition, item, mViewHolder);
                    } else if (item.getSelected().equalsIgnoreCase("1")) {
                        // selected...
                        item.setSelected("1");
                        holder.service_name_checkbox.setChecked(true);
                        listener.OnClickInvoke(categoryItemPosition, item, mViewHolder);
                    }
                }
            }

            holder.service_name_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        item.setSelected("1");
                    } else {
                        item.setSelected("0");
                    }
                    listener.OnClickInvoke(categoryItemPosition, item, mViewHolder);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

}

