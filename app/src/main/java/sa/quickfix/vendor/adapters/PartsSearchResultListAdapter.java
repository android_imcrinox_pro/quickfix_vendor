package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnSparePartAddClickInvoke;
import sa.quickfix.vendor.services.model.SparePartSearchResultModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

public class PartsSearchResultListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "PartsSearchResultListAdapter";

    private final Activity activity;
    private final List<SparePartSearchResultModel> list;
    OnSparePartAddClickInvoke listener;
    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;

    String minimumQuantity = "1";

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private TextView part_name_tv, add_tv, price_tv, quantity_tv;
        private ImageView item_iv, minus_iv, add_iv;

        public CityPercentageListViewholder(View view) {
            super(view);

            part_name_tv = itemView.findViewById(R.id.part_name_tv);
            price_tv = itemView.findViewById(R.id.price_tv);
            add_tv = itemView.findViewById(R.id.add_tv);
            item_iv = itemView.findViewById(R.id.item_iv);
            minus_iv = itemView.findViewById(R.id.minus_iv);
            add_iv = itemView.findViewById(R.id.add_iv);
            quantity_tv = itemView.findViewById(R.id.quantity_tv);

        }
    }

    public PartsSearchResultListAdapter(Activity activity, List<SparePartSearchResultModel> list, OnSparePartAddClickInvoke listener) {
        this.activity = activity;
        this.list = list;
        this.listener = listener;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.parts_search_results_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final SparePartSearchResultModel item = list.get(position);

            if (GlobalFunctions.isNotNullValue(item.getImage())) {
                SeparateProgress.loadImage(holder.item_iv,item.getImage(), SeparateProgress.setProgress(activity));
            }

            if (item.getTitle() != null) {
                holder.part_name_tv.setText(item.getTitle());
            }

            if (item.getPrice() != null) {
                holder.price_tv.setText(activity.getString(R.string.sar) + " " + item.getPrice());
            }

            holder.add_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    quantityCounter(holder.quantity_tv, holder.minus_iv, true, item);
                }
            });

            holder.minus_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.quantity_tv != null) {
                        String val = holder.quantity_tv.getText().toString();
                        int qty = Integer.parseInt(val == null && val.equalsIgnoreCase("") ? minimumQuantity : val);
                        if (qty > Integer.parseInt(minimumQuantity)) {
                            //qty--;
                            holder.quantity_tv.setEnabled(true);
                            holder.quantity_tv.setClickable(true);
                            quantityCounter(holder.quantity_tv, holder.minus_iv, false, item);
                        } else if (qty == Integer.parseInt(minimumQuantity)) {
                            //disable sub button...
                            holder.quantity_tv.setEnabled(false);
                            holder.quantity_tv.setClickable(false);
                        }
                    }

                }
            });

            //add selected quantity and send in model(item)....
            holder.add_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String selectedQuantity = holder.quantity_tv.getText().toString().trim();
                    item.setSelectedQuantity(selectedQuantity);
                    listener.OnAddPartClickInvoke(position, item);
                }
            });

        }
    }

    private void quantityCounter(TextView qty_ev, ImageView subIv, boolean isAddition, SparePartSearchResultModel item) {
        if (qty_ev != null) {
            String val = qty_ev.getText().toString();

            if (isAddition) {
                if (val.equalsIgnoreCase(item.getQuantity())) {
                    globalFunctions.displayMessaage(activity, subIv, activity.getString(R.string.available_quantity_is) + " " + item.getQuantity() + " " + activity.getString(R.string.no_more_quantity_is_available));
                    return;
                }
            }

            int qty = Integer.parseInt(val == null && val.equalsIgnoreCase("") ? minimumQuantity : val);
            if (isAddition) {
                qty = qty + 1;
            } else {
                if (qty > Integer.parseInt(minimumQuantity)) {
                    qty--;
                }
            }
            qty_ev.setText(qty + "");
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

