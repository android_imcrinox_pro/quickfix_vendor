package sa.quickfix.vendor.adapters;

import android.app.Activity;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import sa.quickfix.vendor.R;

public class ScientificDocImagesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "ScientificDocImageListAdapter";

    private final List<Uri> list;
    private final Activity activity;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_view;

        public ViewHolder(View view) {
            super(view);
            image_view = itemView.findViewById(R.id.image_view);
        }
    }

    public ScientificDocImagesListAdapter(Activity activity, List<Uri> list) {
        this.activity = activity;
        this.list = list;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.licence_image_item, parent, false);
        return new ScientificDocImagesListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof ScientificDocImagesListAdapter.ViewHolder) {
            final ScientificDocImagesListAdapter.ViewHolder holder = (ScientificDocImagesListAdapter.ViewHolder) viewHolder;
            final Uri item = list.get(position);
            String filesting= String.valueOf(item);

            if (!filesting.contains(".jpg") && !filesting.contains(".jpeg") && !filesting.contains(".png")){
                holder.image_view.setImageDrawable(activity.getResources().getDrawable(R.drawable.pdf_icon));
            }else {
                try {
                    Glide.with(activity).load(item).into(holder.image_view);
                } catch (Exception exccc) {

                }
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }
}
