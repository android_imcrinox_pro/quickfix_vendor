package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.services.model.LanguageModel;

public interface OnLanguageRemoveInvoke {
    void OnSelectedLanguageRemoveInvoke(int position, LanguageModel languageModel);
}
