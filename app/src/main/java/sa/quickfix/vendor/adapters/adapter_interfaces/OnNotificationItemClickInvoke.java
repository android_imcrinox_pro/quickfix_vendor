package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.services.model.MyNotificationModel;

public interface OnNotificationItemClickInvoke {
    void OnClickInvoke(int position, MyNotificationModel notificationModel);
}
