package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sa.quickfix.vendor.adapters.adapter_interfaces.OnLanguageRemoveInvoke;
import sa.quickfix.vendor.services.model.LanguageModel;
import sa.quickfix.vendor.R;

public class SelectedLanguagesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "SelectedLanguagesList";

    private final List<LanguageModel> list;
    private final Activity activity;
    OnLanguageRemoveInvoke listener;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView selected_language_tv, language_remove_tv;

        public ViewHolder(View view) {
            super(view);
            selected_language_tv = itemView.findViewById(R.id.selected_language_tv);
            language_remove_tv = itemView.findViewById(R.id.language_remove_tv);
        }
    }

    public SelectedLanguagesListAdapter(Activity activity, List<LanguageModel> list, OnLanguageRemoveInvoke listener) {
        this.activity = activity;
        this.list = list;
        this.listener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_language_item, parent, false);
        return new SelectedLanguagesListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof SelectedLanguagesListAdapter.ViewHolder) {
            final SelectedLanguagesListAdapter.ViewHolder holder = (SelectedLanguagesListAdapter.ViewHolder) viewHolder;
            final LanguageModel item = list.get(position);

            if (item != null) {
                if (item.getTitle() != null) {
                    holder.selected_language_tv.setText(item.getTitle());
                }
            }

            holder.language_remove_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //call interface....
                    listener.OnSelectedLanguageRemoveInvoke(position, item);
                }
            });

        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }
}

