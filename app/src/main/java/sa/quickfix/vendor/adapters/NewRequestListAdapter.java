package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.flows.request_detail.activities.RequestDetailsActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnNewRequestItemClickInvoke;
import sa.quickfix.vendor.services.model.OrderDetailModel;
import sa.quickfix.vendor.services.model.OrderModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

public class NewRequestListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "NewRequestListAdapter";

    private final Activity activity;
    private final List<OrderModel> list;
    OnNewRequestItemClickInvoke listener;

    ReqSubListAdapter adapter;
    LinearLayoutManager linearLayoutManager;


    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private TextView title_tv, price_tv,details_tv, address_tv, reject_tv, accept_tv, date_tv,order_number_tv;
        private ImageView item_iv;
//        private CircleImageView item_iv;
        private LinearLayout view_more_ll;
        private View item_bottom_view;
        private RecyclerView item_rv;

        public CityPercentageListViewholder(View view) {
            super(view);

            item_iv = itemView.findViewById(R.id.item_iv);
            title_tv = itemView.findViewById(R.id.title_tv);
            price_tv = itemView.findViewById(R.id.price_tv);
            details_tv = itemView.findViewById(R.id.details_tv);
            address_tv = itemView.findViewById(R.id.address_tv);
            reject_tv = itemView.findViewById(R.id.reject_tv);
            accept_tv = itemView.findViewById(R.id.accept_tv);
            date_tv = itemView.findViewById(R.id.date_tv);
            order_number_tv = itemView.findViewById(R.id.order_number_tv);
            view_more_ll = itemView.findViewById(R.id.view_more_ll);
            item_bottom_view = itemView.findViewById(R.id.item_bottom_view);
            item_rv = itemView.findViewById(R.id.item_rv);
        }
    }

    public NewRequestListAdapter(Activity activity, List<OrderModel> list, OnNewRequestItemClickInvoke listener) {
        this.activity = activity;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_request_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final OrderModel item = list.get(position);
            String
                    date = "",
                    time = "";

            if (GlobalFunctions.isNotNullValue(item.getCategoryIcon())) {
                SeparateProgress.loadImage(holder.item_iv,item.getCategoryIcon(), SeparateProgress.setProgress(activity));
            }else if (GlobalFunctions.isNotNullValue(item.getUserImage())) {
                SeparateProgress.loadImage(holder.item_iv,item.getUserImage(), SeparateProgress.setProgress(activity));
            }

            if (GlobalFunctions.isNotNullValue(item.getCategoryTitle())) {
                holder.title_tv.setText(item.getCategoryTitle());
            }else if (GlobalFunctions.isNotNullValue(item.getUserName())) {
                holder.title_tv.setText(item.getUserName());
            }

            if (GlobalFunctions.isNotNullValue(item.getGrandTotal())) {
                holder.price_tv.setText(item.getGrandTotal()+" "+activity.getString(R.string.sar));
            }

            if (GlobalFunctions.isNotNullValue(item.getOrderNumber())) {
                holder.order_number_tv.setText(item.getOrderNumber());
            }

           /* if (item.getServices() != null) {
                holder.details_tv.setText(item.getServices());
            }*/

            if (item.getAddress() != null) {
                holder.address_tv.setText(item.getAddress());
            }

            //date and time

            if (item.getDate() != null) {
                date = GlobalFunctions.getDayMonthYearFromDate(item.getDate());
            }

            if (item.getTime() != null) {
                time = GlobalFunctions.getTimeFromDate(item.getTime());
            }

            holder.date_tv.setText(date +" "+ "|"+" "+time);

            List<OrderDetailModel> orderDetailModelList = new ArrayList<>();
            orderDetailModelList.clear();

            if (item.getOrderDetailListModel() != null) {
                int listSizeSub = item.getOrderDetailListModel().getOrderDetailList().size();
                if (listSizeSub > 0) {

                    holder.item_bottom_view.setVisibility(View.VISIBLE);
                    holder.item_rv.setVisibility(View.VISIBLE);

                    orderDetailModelList.addAll(item.getOrderDetailListModel().getOrderDetailList());
                    linearLayoutManager = new LinearLayoutManager(activity);
                    adapter = new ReqSubListAdapter(activity,orderDetailModelList);
                    holder.item_rv.setLayoutManager(linearLayoutManager);
                    holder.item_rv.setHasFixedSize(true);
                    holder.item_rv.setAdapter(adapter);

                    synchronized (adapter) {
                        adapter.notifyDataSetChanged();
                    }
                } else  {
                    holder.item_bottom_view.setVisibility(View.GONE);
                    holder.item_rv.setVisibility(View.GONE);
                }
            } else  {
                holder.item_bottom_view.setVisibility(View.GONE);
                holder.item_rv.setVisibility(View.GONE);
            }


            holder.accept_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnAccpetClickInvoke(position, item);
                }
            });

            holder.reject_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnRejectClickInvoke(position, item);
                }
            });

            holder.view_more_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = RequestDetailsActivity.newInstance(activity, item, GlobalVariables.FROM_PAGE_NEW_REQUESTS);
                    activity.startActivity(intent);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

