package sa.quickfix.vendor.adapters;

import android.app.Activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.services.model.CategoryModel;
import sa.quickfix.vendor.services.model.SubCategoryModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

public class MyServicesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "MyServicesListAdapter";

    private final Activity activity;
    private final List<CategoryModel> list;

    MySelectedServicesListAdapter servicesAdapter;
    LinearLayoutManager linearLayoutManager;

    private View mainView;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView category_iv;
        private TextView category_name_tv, no_of_services_tv;
        private RelativeLayout item_rl;
        private RecyclerView services_recycler_view;

        ViewHolder(View view) {
            super(view);
            category_iv = itemView.findViewById(R.id.category_iv);
            category_name_tv = itemView.findViewById(R.id.category_name_tv);
            no_of_services_tv = itemView.findViewById(R.id.no_of_services_tv);
            item_rl = itemView.findViewById(R.id.item_rl);
            services_recycler_view = itemView.findViewById(R.id.services_recycler_view);

        }
    }

    public MyServicesListAdapter(Activity activity, List<CategoryModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_services_list_adapter, parent, false);
        mainView = itemView;
        return new MyServicesListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        final CategoryModel model = list.get(position);
        if (viewHolder instanceof MyServicesListAdapter.ViewHolder) {
            final MyServicesListAdapter.ViewHolder holder = (MyServicesListAdapter.ViewHolder) viewHolder;
            final CategoryModel item = model;

            initRecyclerView(position, item, holder);

            if (GlobalFunctions.isNotNullValue(item.getImage())) {
                SeparateProgress.loadImage(holder.category_iv, item.getImage(), SeparateProgress.setProgress(activity));
            }

            if (item.getTitle() != null) {
                holder.category_name_tv.setText(item.getTitle());
            }

            holder.item_rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.services_recycler_view.getVisibility() == View.VISIBLE) {
                        holder.services_recycler_view.setVisibility(View.GONE);
                    } else {
                        holder.services_recycler_view.setVisibility(View.VISIBLE);
                    }
                }
            });

        }
    }

    private void initRecyclerView(int position, CategoryModel item, ViewHolder mHolder) {

        List<SubCategoryModel> selectedSubCategoryList = new ArrayList<>();

        if (list.get(position) != null) {
            if (list.get(position).getSubCategoryListModel() != null) {
                if (list.get(position).getSubCategoryListModel().getSubCategoryListModels() != null) {
                    int listSizeSub = list.get(position).getSubCategoryListModel().getSubCategoryListModels().size();
                    if (listSizeSub > 0) {
                        for (int j = 0; j < listSizeSub; j++) {

                            if (list.get(position).getSubCategoryListModel().getSubCategoryListModels().get(j).getSelected().equalsIgnoreCase("1")) {
                                selectedSubCategoryList.add(list.get(position).getSubCategoryListModel().getSubCategoryListModels().get(j));
                            }
                        }
                    }
                }
            }
        }

        if (selectedSubCategoryList != null) {
            if (selectedSubCategoryList.size() > 0) {
                if (GlobalFunctions.isNotNullValue(item.getStatus()) && item.getStatus().equalsIgnoreCase("0")) {
                    mHolder.no_of_services_tv.setText(selectedSubCategoryList.size() + " " + activity.getString(R.string.services) + " "+ activity.getString(R.string.not_verified));
                } else if (GlobalFunctions.isNotNullValue(item.getStatus()) && item.getStatus().equalsIgnoreCase("2")) {
                    mHolder.no_of_services_tv.setText(selectedSubCategoryList.size() + " " + activity.getString(R.string.services) + " " + activity.getString(R.string.rejected));
                } else {
                    mHolder.no_of_services_tv.setText(selectedSubCategoryList.size() + " " + activity.getString(R.string.services));
                }

//                mHolder.no_of_services_tv.setText(selectedSubCategoryList.size() + " " + activity.getString(R.string.services));

                linearLayoutManager = new LinearLayoutManager(activity);
                servicesAdapter = new MySelectedServicesListAdapter(activity, selectedSubCategoryList);
                mHolder.services_recycler_view.setLayoutManager(linearLayoutManager);
                mHolder.services_recycler_view.setHasFixedSize(true);
                mHolder.services_recycler_view.setAdapter(servicesAdapter);

                synchronized (servicesAdapter) {
                    servicesAdapter.notifyDataSetChanged();
                }
            } else {
                if (GlobalFunctions.isNotNullValue(item.getStatus()) && item.getStatus().equalsIgnoreCase("0")) {
                    mHolder.no_of_services_tv.setText( activity.getString(R.string.not_verified));
                } else if (GlobalFunctions.isNotNullValue(item.getStatus()) && item.getStatus().equalsIgnoreCase("2")) {
                    mHolder.no_of_services_tv.setText(activity.getString(R.string.rejected));
                } else {
                    mHolder.no_of_services_tv.setText(activity.getString(R.string.services));
                }

//                mHolder.no_of_services_tv.setText("0" + " " + activity.getString(R.string.services));
            }
        } else {
            if (GlobalFunctions.isNotNullValue(item.getStatus()) && item.getStatus().equalsIgnoreCase("0")) {
                mHolder.no_of_services_tv.setText( activity.getString(R.string.not_verified));
            } else if (GlobalFunctions.isNotNullValue(item.getStatus()) && item.getStatus().equalsIgnoreCase("2")) {
                mHolder.no_of_services_tv.setText(activity.getString(R.string.rejected));
            } else {
                mHolder.no_of_services_tv.setText(activity.getString(R.string.services));
            }
//            mHolder.no_of_services_tv.setText("0" + " " + activity.getString(R.string.services));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

