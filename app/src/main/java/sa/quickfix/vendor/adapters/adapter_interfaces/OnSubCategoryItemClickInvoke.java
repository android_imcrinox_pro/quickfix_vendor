package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.services.model.SubCategoryModel;

public interface OnSubCategoryItemClickInvoke {
    void OnClickInvoke(int position, SubCategoryModel subCategoryModel);
}
