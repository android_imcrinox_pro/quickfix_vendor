package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import sa.quickfix.vendor.adapters.adapter_interfaces.OnPartDeleteItemClickInvoke;
import sa.quickfix.vendor.services.model.AddedPartModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.R;

public class AddedPartsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "AddedPartsListAdapter";

    private final Activity activity;
    private final List<AddedPartModel> list;
    OnPartDeleteItemClickInvoke listener;

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private TextView part_name_tv, price_tv;
        private ImageView remove_part_iv;

        public CityPercentageListViewholder(View view) {
            super(view);

            part_name_tv = itemView.findViewById(R.id.part_name_tv);
            price_tv = itemView.findViewById(R.id.price_tv);
            remove_part_iv = itemView.findViewById(R.id.remove_part_iv);

        }
    }

    public AddedPartsListAdapter(Activity activity, List<AddedPartModel> list, OnPartDeleteItemClickInvoke listener) {
        this.activity = activity;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.added_parts_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final AddedPartModel item = list.get(position);

            if (item.getTitle() != null) {
                holder.part_name_tv.setText(item.getTitle());
            }

            if (item.getPrice() != null) {
                holder.price_tv.setText(activity.getString(R.string.sar) + " " + item.getPrice());
            }

            //add selected quantity and send in model(item)....
            holder.remove_part_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final AlertDialog alertDialog = new AlertDialog(activity);
                    alertDialog.setCancelable(false);
                    alertDialog.setIcon(R.drawable.ic_logo_dark);
                    alertDialog.setTitle(activity.getString(R.string.app_name));
                    alertDialog.setMessage(activity.getResources().getString(R.string.really_want_to_remove_this_part));
                    alertDialog.setPositiveButton(activity.getString(R.string.yes), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            listener.OnPartDeleteClickInvoke(position, item);
                        }
                    });

                    alertDialog.setNegativeButton(activity.getString(R.string.no), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });

                    alertDialog.show();
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

