package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.services.model.MyCityModel;

public interface OnItemClickInvoke {
    void OnClickInvoke(int position, MyCityModel myCityModel);
}
