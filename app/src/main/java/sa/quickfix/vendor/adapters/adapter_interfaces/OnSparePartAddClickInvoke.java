package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.services.model.SparePartSearchResultModel;

public interface OnSparePartAddClickInvoke {
    void OnAddPartClickInvoke(int position, SparePartSearchResultModel sparePartSearchResultModel);
}
