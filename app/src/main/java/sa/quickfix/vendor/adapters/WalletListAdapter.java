package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.services.model.WalletModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

public class WalletListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "WalletListAdapter";

    private final Activity activity;
    private final List<WalletModel> list;
    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private TextView title_tv, amount_tv, date_tv, reference_id_tv;
        private ImageView credit_or_debit_iv, item_iv;

        public CityPercentageListViewholder(View view) {
            super(view);

            title_tv = itemView.findViewById(R.id.title_tv);
            amount_tv = itemView.findViewById(R.id.amount_tv);
            credit_or_debit_iv = itemView.findViewById(R.id.credit_or_debit_iv);
            item_iv = itemView.findViewById(R.id.item_iv);
            date_tv = itemView.findViewById(R.id.date_tv);
            reference_id_tv = itemView.findViewById(R.id.reference_id_tv);

        }
    }

    public WalletListAdapter(Activity activity, List<WalletModel> list) {
        this.activity = activity;
        this.list = list;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.wallet_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final WalletModel item = list.get(position);

            if (item.getType() != null) {
                if (item.getType().equalsIgnoreCase(globalVariables.TYPE_ORDER)) {

                    if (item.getCategoryTitle() != null) {
                        holder.title_tv.setText(item.getCategoryTitle());
                    }

                    if (GlobalFunctions.isNotNullValue(item.getCategoryIcon())) {
                        SeparateProgress.loadImage(holder.item_iv,item.getCategoryIcon(), SeparateProgress.setProgress(activity));
                    }


                } else if (item.getType().equalsIgnoreCase(globalVariables.TYPE_PAID)) {
                    holder.title_tv.setText(activity.getString(R.string.money_added_to_wallet));
                    holder.item_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_wallet_action));
                }
            }

            if (item.getUsaged() != null) {
                holder.amount_tv.setText(item.getUsaged() + " " + activity.getString(R.string.sar));
            }

            if (item.getFlag() != null) {
                if (item.getFlag().equalsIgnoreCase(globalVariables.TYPE_CREDIT)) {
                    holder.amount_tv.setTextColor(globalFunctions.getColor(activity, R.color.green));
                    holder.credit_or_debit_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_plus_greened));
                } else if (item.getFlag().equalsIgnoreCase(globalVariables.TYPE_DEBIT)) {
                    holder.amount_tv.setTextColor(globalFunctions.getColor(activity, R.color.red));
                    holder.credit_or_debit_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_minus_red));
                }
            }

            if (item.getReferenceId() != null) {
                holder.reference_id_tv.setText(activity.getString(R.string.reference_id) + " " + item.getReferenceId());
            }

            if (item.getDate() != null) {
                holder.date_tv.setText(GlobalFunctions.getMonthDayYearFromDate(item.getDate()));
            }

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

