package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import sa.quickfix.vendor.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class UpcomingChildRequestListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "UpcomingChildRequestAdapter";

    private final Activity activity;
    String[] mNameList;
    int[] mImageList;

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private TextView name_tv;
        private CircleImageView item_iv;

        public CityPercentageListViewholder(View view) {
            super(view);

            name_tv = itemView.findViewById(R.id.name_tv);
            item_iv = itemView.findViewById(R.id.item_iv);

        }
    }

    public UpcomingChildRequestListAdapter(Activity activity, String[] nameList, int[] imageList) {
        this.activity = activity;
        mNameList = nameList;
        mImageList = imageList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.upcoming_child_request_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;

            holder.name_tv.setText(mNameList[position]);

            holder.item_iv.setImageResource(mImageList[position]);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                /*    Intent intent = ReservationCompletedDetailActivity.newInstance(activity, item);
                    activity.startActivity(intent);*/
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return mNameList.length;
        // return list.size();
    }

}

