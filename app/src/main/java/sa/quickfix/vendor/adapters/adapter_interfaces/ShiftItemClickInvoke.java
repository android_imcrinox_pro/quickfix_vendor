package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.services.model.ShiftModel;

public interface ShiftItemClickInvoke {
    void OnClickInvoke(int position, ShiftModel myCityModel);
    void OnAddInvoke(int position, ShiftModel myCityModel);
}
