package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.services.model.OrderModel;

public interface OnNewRequestItemClickInvoke {
    void OnAccpetClickInvoke(int position, OrderModel orderModel);

    void OnRejectClickInvoke(int position, OrderModel orderModel);
}
