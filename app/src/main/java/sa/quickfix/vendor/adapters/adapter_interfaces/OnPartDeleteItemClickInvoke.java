package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.services.model.AddedPartModel;

public interface OnPartDeleteItemClickInvoke {
    void OnPartDeleteClickInvoke(int position, AddedPartModel addedPartModel);
}
