package sa.quickfix.vendor.adapters;

import android.app.Activity;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import sa.quickfix.vendor.R;
import sa.quickfix.vendor.full_image_view.FullImageMainActivity;

public class OrderImagesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "OrderImagesListAdapter";

    private final ArrayList<String> list;
    private final Activity activity;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_view;

        public ViewHolder(View view) {
            super(view);
            image_view = itemView.findViewById(R.id.image_view);
        }
    }

    public OrderImagesListAdapter(Activity activity, ArrayList<String> list) {
        this.activity = activity;
        this.list = list;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_image_item, parent, false);
        return new OrderImagesListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof OrderImagesListAdapter.ViewHolder) {
            final OrderImagesListAdapter.ViewHolder holder = (OrderImagesListAdapter.ViewHolder) viewHolder;
            final String item = list.get(position);

            try {
                Glide.with(activity).load(item).into(holder.image_view);
            } catch (Exception exccc) {

            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = FullImageMainActivity.newInstatnce(activity, activity.getString(R.string.images), list, position);
                    activity.startActivity(intent);
                }
            });

        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }
}
