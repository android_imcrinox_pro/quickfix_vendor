package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sa.quickfix.vendor.services.model.SubCategoryModel;
import sa.quickfix.vendor.R;

public class MySelectedServicesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "SelectedServicesListAdapter";

    private final Activity activity;
    private final List<SubCategoryModel> list;

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private AppCompatCheckBox service_name_checkbox;

        public CityPercentageListViewholder(View view) {
            super(view);

            service_name_checkbox = itemView.findViewById(R.id.service_name_checkbox);
        }
    }

    public MySelectedServicesListAdapter(Activity activity, List<SubCategoryModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_selected_services_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final SubCategoryModel model = list.get(position);
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final SubCategoryModel item = model;

            if (item.getTitle() != null) {
                holder.service_name_checkbox.setText(item.getTitle());
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

