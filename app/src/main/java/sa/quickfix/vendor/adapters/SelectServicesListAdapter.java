package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnServiceItemClickInvoke;
import sa.quickfix.vendor.services.model.CategoryModel;
import sa.quickfix.vendor.services.model.SubCategoryModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

public class SelectServicesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements OnServiceItemClickInvoke {

    public static final String TAG = "SelectServicesListAdapter";

    private final Activity activity;
    private final List<CategoryModel> list;

    ServicesListAdapter servicesAdapter;
    LinearLayoutManager linearLayoutManager;

    private View mainView;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView category_iv, selected_all_iv, selected_single_category_iv;
        private TextView category_name_tv, select_all_tv, no_of_services_tv, unselect_all_tv, select_tv, unselect_tv;
        private RelativeLayout item_rl;
        private RecyclerView services_recycler_view;

        ViewHolder(View view) {
            super(view);
            category_iv = itemView.findViewById(R.id.category_iv);
            selected_all_iv = itemView.findViewById(R.id.selected_all_iv);
            selected_single_category_iv = itemView.findViewById(R.id.selected_single_category_iv);
            category_name_tv = itemView.findViewById(R.id.category_name_tv);
            select_all_tv = itemView.findViewById(R.id.select_all_tv);
            unselect_all_tv = itemView.findViewById(R.id.unselect_all_tv);
            select_tv = itemView.findViewById(R.id.select_tv);
            unselect_tv = itemView.findViewById(R.id.unselect_tv);
            no_of_services_tv = itemView.findViewById(R.id.no_of_services_tv);
            item_rl = itemView.findViewById(R.id.item_rl);
            services_recycler_view = itemView.findViewById(R.id.services_recycler_view);

        }
    }

    public SelectServicesListAdapter(Activity activity, List<CategoryModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.select_services_list_adapter, parent, false);
        mainView = itemView;
        return new SelectServicesListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        final CategoryModel model = list.get(position);
        if (viewHolder instanceof SelectServicesListAdapter.ViewHolder) {
            final SelectServicesListAdapter.ViewHolder holder = (SelectServicesListAdapter.ViewHolder) viewHolder;
            final CategoryModel item = model;

            initRecyclerView(position, holder.services_recycler_view, holder, false, false);
            setSelectOrUnselectVisibility(position, holder);
            setNoSubCategorySelectOrUnselectVisibility(position, holder);

            if (GlobalFunctions.isNotNullValue(item.getImage())) {
                SeparateProgress.loadImage(holder.category_iv,item.getImage(), SeparateProgress.setProgress(activity));
            }

            if (item.getTitle() != null) {
                holder.category_name_tv.setText(item.getTitle());
            }

            if (item.getSubCategoryListModel() != null) {
                if (item.getSubCategoryListModel().getSubCategoryListModels() != null) {
                    holder.no_of_services_tv.setText(item.getSubCategoryListModel().getSubCategoryListModels().size() + " " + activity.getString(R.string.services));
                } else {
                    holder.no_of_services_tv.setText("0" + " " + activity.getString(R.string.services));
                }
            } else {
                holder.no_of_services_tv.setText("0" + " " + activity.getString(R.string.services));
            }

            holder.item_rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.services_recycler_view.getVisibility() == View.VISIBLE) {
                        holder.services_recycler_view.setVisibility(View.GONE);
                    } else {
                        holder.services_recycler_view.setVisibility(View.VISIBLE);
                    }
                }
            });

            holder.select_all_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    list.get(position).setSelectedAll("1");
                    selectAllSubCategoryItems(position);
                    initRecyclerView(position, holder.services_recycler_view, holder, true, false);
                    setSelectOrUnselectVisibility(position, holder);
                }
            });

            holder.unselect_all_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    list.get(position).setSelectedAll("0");
                    unselectAllSubCategoryItems(position);
                    initRecyclerView(position, holder.services_recycler_view, holder, false, true);
                    setSelectOrUnselectVisibility(position, holder);
                }
            });

            holder.select_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    list.get(position).setSelected("1");
                    setNoSubCategorySelectOrUnselectVisibility(position, holder);
                }
            });

            holder.unselect_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    list.get(position).setSelected("0");
                    setNoSubCategorySelectOrUnselectVisibility(position, holder);
                }
            });

        }
    }

    private void setNoSubCategorySelectOrUnselectVisibility(int position, ViewHolder holder) {
        if (list.get(position).getSubCategoryListModel() != null) {
            if (list.get(position).getSubCategoryListModel().getSubCategoryListModels() != null) {
                if (list.get(position).getSubCategoryListModel().getSubCategoryListModels().size() == 0) {

                    if (list.get(position).getSelected() != null) {
                        if (list.get(position).getSelected().equalsIgnoreCase("0")) {
                            //category with 0 subcategory is not selected..
                            holder.unselect_tv.setVisibility(View.GONE);
                            holder.selected_single_category_iv.setVisibility(View.GONE);
                            holder.select_tv.setVisibility(View.VISIBLE);
                        } else if (list.get(position).getSelected().equalsIgnoreCase("1")) {
                            //category with 0 subcategory is selected..
                            holder.unselect_tv.setVisibility(View.VISIBLE);
                            holder.selected_single_category_iv.setVisibility(View.VISIBLE);
                            holder.select_tv.setVisibility(View.GONE);
                        }
                    }
                } else {
                    holder.unselect_tv.setVisibility(View.GONE);
                    holder.selected_single_category_iv.setVisibility(View.GONE);
                    holder.select_tv.setVisibility(View.GONE);
                }
            } else {
                if (list.get(position).getSelected() != null) {
                    if (list.get(position).getSelected().equalsIgnoreCase("0")) {
                        //category with 0 subcategory is not selected..
                        holder.unselect_tv.setVisibility(View.GONE);
                        holder.selected_single_category_iv.setVisibility(View.GONE);
                        holder.select_tv.setVisibility(View.VISIBLE);
                    } else if (list.get(position).getSelected().equalsIgnoreCase("1")) {
                        //category with 0 subcategory is selected..
                        holder.unselect_tv.setVisibility(View.VISIBLE);
                        holder.selected_single_category_iv.setVisibility(View.VISIBLE);
                        holder.select_tv.setVisibility(View.GONE);
                    }
                }
            }
        } else {
            if (list.get(position).getSelected() != null) {
                if (list.get(position).getSelected().equalsIgnoreCase("0")) {
                    //category with 0 subcategory is not selected..
                    holder.unselect_tv.setVisibility(View.GONE);
                    holder.selected_single_category_iv.setVisibility(View.GONE);
                    holder.select_tv.setVisibility(View.VISIBLE);
                } else if (list.get(position).getSelected().equalsIgnoreCase("1")) {
                    //category with 0 subcategory is selected..
                    holder.unselect_tv.setVisibility(View.VISIBLE);
                    holder.selected_single_category_iv.setVisibility(View.VISIBLE);
                    holder.select_tv.setVisibility(View.GONE);
                }
            }
        }


    }

    private void selectAllSubCategoryItems(int position) {
        if (list.get(position) != null) {
            if (list.get(position).getSubCategoryListModel() != null) {
                if (list.get(position).getSubCategoryListModel().getSubCategoryListModels() != null) {
                    if (list.get(position).getSubCategoryListModel().getSubCategoryListModels().size() > 0) {
                        for (int i = 0; i < list.get(position).getSubCategoryListModel().getSubCategoryListModels().size(); i++) {
                            list.get(position).getSubCategoryListModel().getSubCategoryListModels().get(i).setSelected("1");
                        }
                    }

                }
            }
        }
    }

    private void unselectAllSubCategoryItems(int position) {
        if (list.get(position) != null) {
            if (list.get(position).getSubCategoryListModel() != null) {
                if (list.get(position).getSubCategoryListModel().getSubCategoryListModels() != null) {
                    if (list.get(position).getSubCategoryListModel().getSubCategoryListModels().size() > 0) {
                        for (int i = 0; i < list.get(position).getSubCategoryListModel().getSubCategoryListModels().size(); i++) {
                            list.get(position).getSubCategoryListModel().getSubCategoryListModels().get(i).setSelected("0");
                        }
                    }

                }
            }
        }
    }

    private void initRecyclerView(int position, RecyclerView services_recycler_view, ViewHolder holder, boolean isSelectAllClicked, boolean isUnselectAllClicked) {

        if (list.get(position).getSubCategoryListModel() != null) {
            linearLayoutManager = new LinearLayoutManager(activity);
            servicesAdapter = new ServicesListAdapter(activity, list.get(position).getSubCategoryListModel().getSubCategoryListModels(), this, holder, list.get(position), isSelectAllClicked, isUnselectAllClicked, position);
            services_recycler_view.setLayoutManager(linearLayoutManager);
            services_recycler_view.setHasFixedSize(true);
            services_recycler_view.setAdapter(servicesAdapter);

            synchronized (servicesAdapter) {
                servicesAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void OnClickInvoke(int position, SubCategoryModel subCategoryModel, SelectServicesListAdapter.ViewHolder holder) {
        //check sub category list items...if all are selected ...make text unselectAll...else select All...
        setSelectOrUnselectVisibility(position, holder);
    }

    @Override
    public void OnItemClickInvoke(int position, SubCategoryModel subCategoryModel, EditServicesListAdapter.ViewHolder viewHolder) {
        //nothing..
    }

    private void setSelectOrUnselectVisibility(int position, ViewHolder holder) {

        if (list.get(position).getSubCategoryListModel() != null) {
            if (list.get(position).getSubCategoryListModel().getSubCategoryListModels() != null) {
                if (list.get(position).getSubCategoryListModel().getSubCategoryListModels().size() > 0) {
                    ArrayList<Integer> subCategoryItemSelectedCount = new ArrayList<>();
                    for (int i = 0; i < list.get(position).getSubCategoryListModel().getSubCategoryListModels().size(); i++) {
                        if (list.get(position).getSubCategoryListModel().getSubCategoryListModels().get(i).getSelected().equalsIgnoreCase("1")) {
                            //selected...
                            subCategoryItemSelectedCount.add(1);
                        }
                    }
                    if (subCategoryItemSelectedCount.size() == list.get(position).getSubCategoryListModel().getSubCategoryListModels().size()) {
                        //all items are selected...
                        holder.unselect_all_tv.setVisibility(View.VISIBLE);
                        holder.selected_all_iv.setVisibility(View.VISIBLE);
                        holder.select_all_tv.setVisibility(View.GONE);
                    } else {
                        holder.unselect_all_tv.setVisibility(View.GONE);
                        holder.selected_all_iv.setVisibility(View.GONE);
                        holder.select_all_tv.setVisibility(View.VISIBLE);
                    }
                } else {
                    holder.unselect_all_tv.setVisibility(View.GONE);
                    holder.selected_all_iv.setVisibility(View.GONE);
                    holder.select_all_tv.setVisibility(View.GONE);
                }
            } else {
                holder.unselect_all_tv.setVisibility(View.GONE);
                holder.selected_all_iv.setVisibility(View.GONE);
                holder.select_all_tv.setVisibility(View.GONE);
            }
        } else {
            holder.unselect_all_tv.setVisibility(View.GONE);
            holder.selected_all_iv.setVisibility(View.GONE);
            holder.select_all_tv.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

