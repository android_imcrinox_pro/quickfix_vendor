package sa.quickfix.vendor.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.full_image_view.FullImageMainActivity;
import sa.quickfix.vendor.services.model.CertificatesModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

import java.util.ArrayList;
import java.util.List;

public class ProfileCertificateImageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "ProfileCertificateImage";

    private final List<CertificatesModel> list;
    private final Activity activity;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_view;

        public ViewHolder(View view) {
            super(view);
            image_view = itemView.findViewById(R.id.image_view);
        }
    }

    public ProfileCertificateImageListAdapter(Activity activity, List<CertificatesModel> list) {
        this.activity = activity;
        this.list = list;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_images_item, parent, false);
        return new ProfileCertificateImageListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof ProfileCertificateImageListAdapter.ViewHolder) {
            final ProfileCertificateImageListAdapter.ViewHolder holder = (ProfileCertificateImageListAdapter.ViewHolder) viewHolder;
            final CertificatesModel item = list.get(position);

            if (GlobalFunctions.isNotNullValue(item.getImage())) {
                SeparateProgress.loadImage(holder.image_view,item.getImage(), SeparateProgress.setProgress(activity));
            }
            
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<String> myImageList = new ArrayList<>();
                    myImageList.clear();
                    ProfileModel profileModel = GlobalFunctions.getProfile(activity);
                    myImageList.addAll(profileModel.getCertificatesList().getImages());
                    Intent intent = FullImageMainActivity.newInstatnce(activity, activity.getString(R.string.images), myImageList, position);
                    activity.startActivity(intent);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
