package sa.quickfix.vendor.adapters.adapter_interfaces;


import android.net.Uri;

public interface OnPhotoDeleteClickInvoke {
    void OnClickInvoke(int position,boolean isImage, Uri item);
}
