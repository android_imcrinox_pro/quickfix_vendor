package sa.quickfix.vendor.adapters;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.services.model.FeedbackModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

public class MyRatingsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "MyRatingsListAdapter";

    private final Activity activity;
    private final List<FeedbackModel> list;
    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private TextView comments_tv, name_tv;
        private RatingBar ratingBar;
        private CircleImageView user_iv;

        public CityPercentageListViewholder(View view) {
            super(view);

            name_tv = itemView.findViewById(R.id.name_tv);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            comments_tv = itemView.findViewById(R.id.comments_tv);
            user_iv = itemView.findViewById(R.id.user_iv);

        }
    }

    public MyRatingsListAdapter(Activity activity, List<FeedbackModel> list) {
        this.activity = activity;
        this.list = list;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_ratings_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final FeedbackModel item = list.get(position);

            String
                    firstName = "",
                    lastName = "",
                    fullName = "";

            firstName = item.getFirstName();
            lastName = item.getLastName();
            fullName = firstName + " " + lastName;
            holder.name_tv.setText(fullName);

            if (GlobalFunctions.isNotNullValue(item.getUserImage())) {
                SeparateProgress.loadImage(holder.user_iv, item.getUserImage(), SeparateProgress.setProgress(activity));
            }


            if (item.getRating() != null) {
                float value;
                value = Float.parseFloat(item.getRating());
                holder.ratingBar.setVisibility(View.VISIBLE);
                holder.ratingBar.setRating(value);
            } else {
                holder.ratingBar.setVisibility(View.GONE);
            }

            if (globalFunctions.isNotNullValue(item.getComment())) {
                //holder.comments_tv.setText(GlobalFunctions.getHTMLString(item.getComment()));
                String finalComments = "";
                String comments = item.getComment().replace("\\n", "\n");
                finalComments = comments;
                if (comments != null) {
                    finalComments = comments.replace("\\", "");
                }
                //  holder.comments_tv.setText(globalFunctions.getHTMLString(comments));
                // holder.comments_tv.setText(globalFunctions.html2text(holder.comments_tv.getText().toString().trim()));
                holder.comments_tv.setText(finalComments);
                // holder.comments_tv.setText(globalFunctions.getHTMLString(item.getComment()));
            }

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

