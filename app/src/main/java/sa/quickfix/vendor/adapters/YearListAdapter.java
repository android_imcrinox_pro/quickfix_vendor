package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnYearItemClickInvoke;

public class YearListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "YearListAdapter";

    private final List<String> list;
    private final Activity activity;
    OnYearItemClickInvoke listener;
    
    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;

    public YearListAdapter(Activity activity, List<String> list, OnYearItemClickInvoke listener) {
        this.activity = activity;
        this.list = list;
        this.listener = listener;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.year_item_layout, parent, false);

        return new YearListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof YearListAdapter.ViewHolder) {
            final YearListAdapter.ViewHolder holder = (YearListAdapter.ViewHolder) viewHolder;
             final String model = list.get(position);

            if (list.get(position) != null) {
                holder.title_tv.setText(list.get(position));
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnClickInvoke(position, list.get(position));
                }
            });

        }
    }


    @Override
    public int getItemCount() {
         return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title_tv;

        public ViewHolder(View view) {
            super(view);
            title_tv = (TextView) itemView.findViewById(R.id.title_tv);
        }
    }
}


