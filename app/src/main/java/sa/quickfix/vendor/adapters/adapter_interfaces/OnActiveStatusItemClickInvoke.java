package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.services.model.OrderStatusModel;

public interface OnActiveStatusItemClickInvoke {
    void OnItemClickInvoke(int position, OrderStatusModel orderStatusModel);
    void OnTroubleInvoke(int position, OrderStatusModel orderStatusModel);
    void OnLocationServiceInvoke(int position, boolean isStarted);
}
