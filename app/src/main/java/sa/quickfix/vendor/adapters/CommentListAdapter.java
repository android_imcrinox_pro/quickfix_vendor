package sa.quickfix.vendor.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.services.model.CommentModel;

public class CommentListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "CommentListAdapter";

    private final ArrayList<CommentModel> list;
    private final Activity activity;
    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout my_chat_ll, end_chat_ll;
        private TextView my_comments_tv, my_chat_date_tv, end_chat_comments_tv, end_chat_date_tv;

        public ViewHolder(View view) {
            super(view);
            my_chat_ll = itemView.findViewById(R.id.my_chat_ll);
            end_chat_ll = itemView.findViewById(R.id.end_chat_ll);
            my_comments_tv = itemView.findViewById(R.id.my_comments_tv);
            my_chat_date_tv = itemView.findViewById(R.id.my_chat_date_tv);
            end_chat_comments_tv = itemView.findViewById(R.id.end_chat_comments_tv);
            end_chat_date_tv = itemView.findViewById(R.id.end_chat_date_tv);
        }
    }

    public CommentListAdapter(Activity activity, ArrayList<CommentModel> list) {
        this.activity = activity;
        this.list = list;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_item, parent, false);
        return new CommentListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CommentListAdapter.ViewHolder) {
            final CommentListAdapter.ViewHolder holder = (CommentListAdapter.ViewHolder) viewHolder;
            final CommentModel item = list.get(position);

            holder.my_chat_ll.setVisibility(View.GONE);
            holder.end_chat_ll.setVisibility(View.GONE);

            String
                    mTempDate = null,
                    mTempTime = null,
                    date = "",
                    time = "",
                    finalDateTime = "";

            if (item.getCreatedOn() != null) {

                try {
                    String string = item.getCreatedOn();
                    String[] dateParts = string.split(" ");
                    if (dateParts.length == 2) {
                        mTempDate = dateParts[0];
                        mTempTime = dateParts[1];
                    }

                    if (mTempDate != null) {
                        date = GlobalFunctions.getDayMonthYearFromDate(mTempDate);
                    }

                    if (mTempTime != null) {
                        time = GlobalFunctions.getTimeFromDate(mTempTime);
                    }

                    finalDateTime = date + " " + "|" + " " + time;

                } catch (Exception exccc) {
                }
            }

            if (item != null && item.getFrom() != null) {
                if (item.getFrom().equalsIgnoreCase(globalVariables.CHAT_FROM_USER)) {
                    holder.my_chat_ll.setVisibility(View.GONE);
                    holder.end_chat_ll.setVisibility(View.VISIBLE);
                    if (globalFunctions.isNotNullValue(item.getComments())) {
                        holder.end_chat_comments_tv.setText(item.getComments());
                    }
                    holder.end_chat_date_tv.setText(finalDateTime);
                } else if (item.getFrom().equalsIgnoreCase(globalVariables.CHAT_FROM_VENDOR)) {
                    holder.my_chat_ll.setVisibility(View.VISIBLE);
                    holder.end_chat_ll.setVisibility(View.GONE);
                    if (globalFunctions.isNotNullValue(item.getComments())) {
                        holder.my_comments_tv.setText(item.getComments());
                    }
                    holder.my_chat_date_tv.setText(finalDateTime);
                }
            }
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }
}
