package sa.quickfix.vendor.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnSubCategoryItemClickInvoke;
import sa.quickfix.vendor.services.model.SubCategoryModel;
import sa.quickfix.vendor.R;

public class SubCategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    public static final String TAG = "SubCategoryAdapter";

    private final List<SubCategoryModel> list;
    private List<SubCategoryModel> listFull;
    private final Activity activity;
    OnSubCategoryItemClickInvoke listener;

    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;

    public SubCategoryListAdapter(Activity activity, List<SubCategoryModel> list, OnSubCategoryItemClickInvoke listener) {
        this.activity = activity;
        this.list = list;
        listFull = new ArrayList<>(list);
        this.listener = listener;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sub_category_item_layout, parent, false);

        return new SubCategoryListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof SubCategoryListAdapter.ViewHolder) {
            final SubCategoryListAdapter.ViewHolder holder = (SubCategoryListAdapter.ViewHolder) viewHolder;
            final SubCategoryModel item = list.get(position);

            if (item.getTitle() != null) {
                holder.title_tv.setText(item.getTitle());
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnClickInvoke(position, item);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return cityListFilter;
    }

    private Filter cityListFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<SubCategoryModel> filteredList = new ArrayList<>();

            if (charSequence == null || charSequence.length() == 0) {
                filteredList.addAll(listFull);
            } else {
                String filterPattern = charSequence.toString().toLowerCase().trim();

                for (SubCategoryModel item : listFull) {

                    if (item.getTitle().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            list.clear();
            list.addAll((List) filterResults.values);
            notifyDataSetChanged();
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title_tv;

        public ViewHolder(View view) {
            super(view);
            title_tv = (TextView) itemView.findViewById(R.id.title_tv);
        }
    }
}

