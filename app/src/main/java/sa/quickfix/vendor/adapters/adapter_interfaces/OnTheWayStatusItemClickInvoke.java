package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.services.model.OrderStatusModel;

public interface OnTheWayStatusItemClickInvoke {
    void OnSelectClickInvoke(int position, OrderStatusModel orderStatusModel);
    void OnTroubleClickInvoke(int position, OrderStatusModel orderStatusModel);
}
