package sa.quickfix.vendor.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import sa.quickfix.vendor.R;
import sa.quickfix.vendor.flows.request_detail.activities.RequestDetailsActivity;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.model.OrderDetailModel;

public class ReqSubListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "ReqSubListAdapter";

    private final Activity activity;
    private final List<OrderDetailModel> list;


    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView item_iv;
        private TextView title_tv,qty_tv,price_tv;

        ViewHolder(View view) {
            super(view);
            item_iv = itemView.findViewById(R.id.item_iv);
            title_tv = itemView.findViewById(R.id.title_tv);
            qty_tv = itemView.findViewById(R.id.qty_tv);
            price_tv = itemView.findViewById(R.id.price_tv);

        }
    }

    public ReqSubListAdapter(Activity activity, List<OrderDetailModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.req_sub_list_adapter, parent, false);
        return new ReqSubListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final OrderDetailModel item = list.get(position);
        if (viewHolder instanceof ReqSubListAdapter.ViewHolder) {
            final ReqSubListAdapter.ViewHolder holder = (ReqSubListAdapter.ViewHolder) viewHolder;

            if (item.getServiceTitle() != null) {
                holder.title_tv.setText(item.getServiceTitle());
            }

            if (item.getQuantity() != null) {
                holder.qty_tv.setText(item.getQuantity());
            }

            if (item.getUnitPrice() != null) {
                holder.price_tv.setText(item.getUnitPrice()+" "+activity.getString(R.string.sar));
            }

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

