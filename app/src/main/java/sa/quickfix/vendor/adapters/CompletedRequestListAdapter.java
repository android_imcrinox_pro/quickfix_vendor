package sa.quickfix.vendor.adapters;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sa.quickfix.vendor.flows.request_detail.activities.RequestDetailsActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.services.model.OrderDetailModel;
import sa.quickfix.vendor.services.model.OrderModel;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.view.SeparateProgress;

public class CompletedRequestListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "CompletedRequestListAdapter";

    private final Activity activity;
    private final List<OrderModel> list;

    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;

    ReqSubListAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private TextView name_tv, status_tv, address_tv, price_tv, date_tv, common_date_tv, order_number_tv;
        private ImageView item_iv, status_iv;
        private RatingBar ratingBar;
        private RecyclerView item_rv;

        public CityPercentageListViewholder(View view) {
            super(view);

            ratingBar = itemView.findViewById(R.id.ratingBar);
            item_iv = itemView.findViewById(R.id.item_iv);
            status_iv = itemView.findViewById(R.id.status_iv);
            name_tv = itemView.findViewById(R.id.name_tv);
            status_tv = itemView.findViewById(R.id.status_tv);
            address_tv = itemView.findViewById(R.id.address_tv);
            price_tv = itemView.findViewById(R.id.price_tv);
            date_tv = itemView.findViewById(R.id.date_tv);
            common_date_tv = itemView.findViewById(R.id.common_date_tv);
            order_number_tv = itemView.findViewById(R.id.order_number_tv);
            item_rv = itemView.findViewById(R.id.item_rv);

        }
    }

    public CompletedRequestListAdapter(Activity activity, List<OrderModel> list) {
        this.activity = activity;
        this.list = list;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.completed_request_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final OrderModel item = list.get(position);

            String
                    date = "",
                    time = "";

            String total = "";

            if (globalFunctions.isNotNullValue(item.getOrderNumber())) {
//                holder.order_number_tv.setText(activity.getString(R.string.order_number) + " " + item.getOrderNumber());
                holder.order_number_tv.setText(item.getOrderNumber());
            }

            if (item.getGrandTotal() != null) {
                total = item.getGrandTotal();
            }
            if (GlobalFunctions.isNotNullValue(item.getUserImage())) {
                SeparateProgress.loadImage(holder.item_iv, item.getUserImage(), SeparateProgress.setProgress(activity));
            }

            if (item.getUserName() != null) {
                holder.name_tv.setText(item.getUserName());
            }

            if (item.getUserRating() != null) {
                float value;
                value = Float.parseFloat(item.getUserRating());
                holder.ratingBar.setVisibility(View.VISIBLE);
                holder.ratingBar.setRating(value);
            } else {
                holder.ratingBar.setVisibility(View.GONE);
            }

            if (item.getStatus() != null) {

                if (item.getStatus().equalsIgnoreCase(globalVariables.ORDER_COMPLETED)) {
                    holder.status_tv.setText(item.getStatusTitle());
                    holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.green));
                    holder.price_tv.setTextColor(globalFunctions.getColor(activity, R.color.green));
//                    holder.price_tv.setText("+" + " " + activity.getString(R.string.sar) + " " + total);
                    holder.price_tv.setText("+"+ total+" "+activity.getString(R.string.sar));
                    holder.status_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_tic));
                } else {
                    holder.status_tv.setText(item.getStatusTitle());
                    holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.red));
                    holder.price_tv.setTextColor(globalFunctions.getColor(activity, R.color.red));
//                    holder.price_tv.setText("-" + " " + activity.getString(R.string.sar) + " " + total);
                    holder.price_tv.setText("-"+ total+" "+activity.getString(R.string.sar));
                    holder.status_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_cancelled));
                }

            }

            if (item.getAddress() != null) {
                holder.address_tv.setText(item.getAddress());
            }

            //date and time

            if (GlobalFunctions.isNotNullValue(item.getDate()) && item.isAddedDate()) {
                String dateMonthYear = GlobalFunctions.getDayMonthYearFRMDate(item.getDate());
//                String today  = GlobalFunctions.getFormattedDate(item.getDate());
//                holder.common_date_tv.setText(today);
                holder.common_date_tv.setText(dateMonthYear);
                holder.common_date_tv.setVisibility(View.VISIBLE);
            } else {
                holder.common_date_tv.setVisibility(View.GONE);
            }

            if (item.getDate() != null) {
                date = GlobalFunctions.getDayMonthYearFromDate(item.getDate());
            }

            if (item.getTime() != null) {
                time = GlobalFunctions.getTimeFromDate(item.getTime());
            }

            holder.date_tv.setText(date + " " + "|" + " " + time);

            List<OrderDetailModel> orderDetailModelList = new ArrayList<>();
            orderDetailModelList.clear();

            if (item.getOrderDetailListModel() != null) {
                int listSizeSub = item.getOrderDetailListModel().getOrderDetailList().size();
                if (listSizeSub > 0) {

                    holder.item_rv.setVisibility(View.VISIBLE);

                    orderDetailModelList.addAll(item.getOrderDetailListModel().getOrderDetailList());
                    linearLayoutManager = new LinearLayoutManager(activity);
                    adapter = new ReqSubListAdapter(activity,orderDetailModelList);
                    holder.item_rv.setLayoutManager(linearLayoutManager);
                    holder.item_rv.setHasFixedSize(true);
                    holder.item_rv.setAdapter(adapter);

                    synchronized (adapter) {
                        adapter.notifyDataSetChanged();
                    }
                } else  {
                    holder.item_rv.setVisibility(View.GONE);
                }
            } else  {
                holder.item_rv.setVisibility(View.GONE);
            }


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = RequestDetailsActivity.newInstance(activity, item, globalVariables.FROM_PAGE_COMPLETED_REQUESTS);
                    activity.startActivity(intent);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

