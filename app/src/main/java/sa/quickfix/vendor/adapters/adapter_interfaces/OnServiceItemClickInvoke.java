package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.adapters.EditServicesListAdapter;
import sa.quickfix.vendor.adapters.SelectServicesListAdapter;
import sa.quickfix.vendor.services.model.SubCategoryModel;

public interface OnServiceItemClickInvoke {
    void OnClickInvoke(int position, SubCategoryModel subCategoryModel, SelectServicesListAdapter.ViewHolder viewHolder);

    void OnItemClickInvoke(int position, SubCategoryModel subCategoryModel, EditServicesListAdapter.ViewHolder viewHolder);
}
