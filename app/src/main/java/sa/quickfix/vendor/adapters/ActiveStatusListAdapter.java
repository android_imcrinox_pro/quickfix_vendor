package sa.quickfix.vendor.adapters;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.AppController;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnActiveStatusItemClickInvoke;
import sa.quickfix.vendor.services.model.OrderDetailMainModel;
import sa.quickfix.vendor.services.model.OrderStatusModel;
import sa.quickfix.vendor.R;

public class ActiveStatusListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "ActiveStatusList";

    private final Activity activity;
    private final List<OrderStatusModel> list;
    private final OrderDetailMainModel mainModel;
    OnActiveStatusItemClickInvoke listener;

    private GlobalVariables globalVariables;
    private GlobalFunctions globalFunctions;
    int selectedPosition = -1;

    public class CityPercentageListViewholder extends RecyclerView.ViewHolder {

        private TextView status_tv, select_tv;
        private LinearLayout select_ll;
        private ImageView selected_iv;
        private View selected_iv_line_view;

        public CityPercentageListViewholder(View view) {
            super(view);

            status_tv = itemView.findViewById(R.id.status_tv);
            select_tv = itemView.findViewById(R.id.select_tv);
            select_ll = itemView.findViewById(R.id.select_ll);
            selected_iv = itemView.findViewById(R.id.selected_iv);
            selected_iv_line_view = itemView.findViewById(R.id.selected_iv_line_view);
        }
    }

    public ActiveStatusListAdapter(Activity activity, List<OrderStatusModel> list, OnActiveStatusItemClickInvoke listener, OrderDetailMainModel mainModel) {
        this.activity = activity;
        this.list = list;
        globalVariables = AppController.getInstance().getGlobalVariables();
        globalFunctions = AppController.getInstance().getGlobalFunctions();
        this.listener = listener;
        this.mainModel = mainModel;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.active_status_list_adapter, parent, false);

        return new CityPercentageListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof CityPercentageListViewholder) {
            final CityPercentageListViewholder holder = (CityPercentageListViewholder) viewHolder;
            final OrderStatusModel item = list.get(position);

            holder.select_ll.setVisibility(View.GONE);
            holder.status_tv.setVisibility(View.VISIBLE);
            holder.selected_iv.setVisibility(View.VISIBLE);
            holder.selected_iv_line_view.setVisibility(View.VISIBLE);

            String status = null;
            if (mainModel != null) {
                if (mainModel.getStatus() != null) {
                    status = mainModel.getStatus();
                }
            }

            if (status != null) {
                if (status.equalsIgnoreCase(globalVariables.ON_THE_WAY)) {
                    listener.OnLocationServiceInvoke(position, true);

                    if (position == 0) {
                        holder.select_ll.setVisibility(View.VISIBLE);
                        holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontBlackColor));
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_selected));
                    } else {
                        holder.select_ll.setVisibility(View.GONE);
                        holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontColor));
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_unselected));
                    }

                } else if (status.equalsIgnoreCase(globalVariables.ARRIVED_TO_CUSTOMER)) {

                    listener.OnLocationServiceInvoke(position, false);
                    if (position == 0 || position == 1) {
                        if (position == 1) {
                            holder.select_ll.setVisibility(View.VISIBLE);
                        } else {
                            holder.selected_iv_line_view.setBackgroundColor(globalFunctions.getColor(activity, R.color.grey_lighter));
                            holder.select_ll.setVisibility(View.GONE);
                        }
                        holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontBlackColor));
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_selected));
                    } else {
                        holder.select_ll.setVisibility(View.GONE);
                        holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontColor));
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_unselected));
                    }
                } else if (status.equalsIgnoreCase(globalVariables.WORK_IN_PROGRESS)) {
                    listener.OnLocationServiceInvoke(position, false);
                    if (position == 0 || position == 1 || position == 2) {
                        if (position == 2) {
                            holder.select_ll.setVisibility(View.VISIBLE);
                        } else {
                            holder.select_ll.setVisibility(View.GONE);
                        }
                        holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontBlackColor));
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_selected));
                    } else {
                        holder.select_ll.setVisibility(View.GONE);
                        holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontColor));
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_unselected));
                    }

                } else if (status.equalsIgnoreCase(globalVariables.WAITING_FOR_SPARE_PARTS_APPROVAL)) {
                    listener.OnLocationServiceInvoke(position, false);
                    if (position == 0 || position == 1 || position == 2 || position == 3) {

                        if (position == 3) {
                            holder.select_ll.setVisibility(View.VISIBLE);
                        } else {
                            holder.select_ll.setVisibility(View.GONE);
                        }

                        holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontBlackColor));
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_selected));
                    } else {
                        holder.select_ll.setVisibility(View.GONE);
                        holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontColor));
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_unselected));
                    }

                } else if (status.equalsIgnoreCase(globalVariables.JOB_COMPLETED)) {
                    listener.OnLocationServiceInvoke(position, false);
                    if (position == 0 || position == 1 || position == 2 || position == 3) {

                        if (position == 3) {
                            holder.select_ll.setVisibility(View.VISIBLE);
                        } else {
                            holder.select_ll.setVisibility(View.GONE);
                        }

                        holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontBlackColor));
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_selected));
                    } else {
                        holder.select_ll.setVisibility(View.GONE);
                        holder.status_tv.setTextColor(globalFunctions.getColor(activity, R.color.app_fontColor));
                        holder.selected_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_track_unselected));
                    }
                }

                if (position == list.size() - 1) {
                    holder.selected_iv_line_view.setVisibility(View.GONE);
                } else {
                    holder.selected_iv_line_view.setVisibility(View.VISIBLE);
                }

                if (GlobalFunctions.isLastPosition(position, list)) {
                    holder.selected_iv_line_view.setBackgroundColor(globalFunctions.getColor(activity, R.color.ColorAccentLighter));
                } else {
                    holder.selected_iv_line_view.setBackgroundColor(globalFunctions.getColor(activity, R.color.grey_lighter));
                }

            }

            if (item.getTitle() != null) {
                holder.status_tv.setText(item.getTitle());
            }

            holder.select_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnItemClickInvoke(position, item);
                }
            });

            holder.select_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnTroubleInvoke(position, item);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

