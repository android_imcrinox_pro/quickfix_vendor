package sa.quickfix.vendor.adapters;

import android.app.Activity;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.adapters.adapter_interfaces.OnPhotoDeleteClickInvoke;

public class PhotosListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "PhotosListAdapter";

    private final Activity activity;
    private final List<Uri> list;
    OnPhotoDeleteClickInvoke listener;
    boolean isNationalId=false;

    public class PhotosListViewholder extends RecyclerView.ViewHolder {

        private ImageView item_iv,item_delete_iv;

        public PhotosListViewholder(View view) {
            super(view);
            item_iv = itemView.findViewById(R.id.item_iv);
            item_delete_iv = itemView.findViewById(R.id.item_delete_iv);
        }
    }

    public PhotosListAdapter(Activity activity, List<Uri> list,boolean isNationalId, OnPhotoDeleteClickInvoke listener) {
        this.activity = activity;
        this.list = list;
        this.listener = listener;
        this.isNationalId = isNationalId;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.upload_photo_adapter, parent, false);

        return new PhotosListViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof PhotosListViewholder) {
            final PhotosListViewholder holder = (PhotosListViewholder) viewHolder;
            final Uri item = list.get(position);
            String filesting= String.valueOf(item);

            if (!filesting.contains(".jpg") && !filesting.contains(".jpeg") && !filesting.contains(".png")){
                holder.item_iv.setImageDrawable(activity.getResources().getDrawable(R.drawable.pdf_icon));
            }else {
                if (GlobalFunctions.isNotNullValue(item.toString())) {
                    Glide.with(activity).load(item).into(holder.item_iv);
                }
            }

            holder.item_delete_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnClickInvoke(position,isNationalId, item);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

