package sa.quickfix.vendor.adapters.adapter_interfaces;

import sa.quickfix.vendor.services.model.SparePartModel;

public interface OnPartTimeEditClickInvoke {
    void OnEditClickInvoke(int position, SparePartModel sparePartModel);
}
