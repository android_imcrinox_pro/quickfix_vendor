package sa.quickfix.vendor.global;

import android.util.Log;


import sa.quickfix.vendor.R;

import java.text.SimpleDateFormat;

public class GlobalVariables {

    public static final int BID_REFRESH_TIME_MS = 1000*60;//1min
    public static final int SOCKET_TIMEOUT_MS = 120000;
    public static final int VENDOR_RADIUS = 5;
    public static final int OTP_LENGTH = 4;

    public static final int LOCATION_INTERVAL         = 10000;
    public static final int FASTEST_LOCATION_INTERVAL = 5000;

    public static final int ANIMATION_SCROLL_DURATION = 400;/* milliseconds */
    private static GlobalVariables thisInstance;

    public static synchronized GlobalVariables getInstance(){
        if(thisInstance == null){thisInstance = new GlobalVariables();}
        return thisInstance;
    }

    public enum USER_TYPE {
        NONE("none"),
        USER("user"),
        VENDOR("vendor");

        private final String text;
        private USER_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(USER.toString())){return 1;}
            else{return 2;}
        }
    }

    public enum NOTIFICATION_TYPE {
        NONE("0"),
        REQUEST("3"),
        ORDER("2");

        private final String text;
        private NOTIFICATION_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(REQUEST.toString())){return 3;}
            if(this.text.equals(ORDER.toString())){return 2;}
            else{return 0;}
        }
    }

    public enum FILE_TYPE {
        URL("1"),
        BASE64("2");

        private final String text;
        private FILE_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(BASE64.toString())){return 2;}
            else{return 1;}
        }
    }

    public enum CALENDAR_TYPE {
        NONE("0"),
        START("1"),
        END("2");

        private final String text;
        private CALENDAR_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(START.toString())){return 1;}
            if(this.text.equals(END.toString())){return 2;}
            else{return 0;}
        }
    }

    /*public enum MATCH_TYPE {
        NONE("0"),
        HISTORY("1"),
        NOW("2"),
        FIXTURE("3");

        private final String text;
        private MATCH_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(HISTORY.toString())){return 1;}
            else if(this.text.equals(NOW.toString())){return 2;}
            else if(this.text.equals(FIXTURE.toString())){return 3;}
            else{return 0;}
        }

        public int getResource(){
            Log.d("Enum", this.text);
            if(this.text.equals(HISTORY.toString())){return R.string.results;}
            else if(this.text.equals(NOW.toString())){return R.string.todays_matches;}
            else if(this.text.equals(FIXTURE.toString())){return R.string.fixtures;}
            else{return R.string.not_available;}
        }

        public MATCH_TYPE toMatchType(String val){
            if(val.equalsIgnoreCase(HISTORY.toString())){
                return HISTORY;
            }else if(val.equalsIgnoreCase(NOW.toString())){
                return NOW;
            }else if(val.equalsIgnoreCase(FIXTURE.toString())){
                return FIXTURE;
            }else{
                return NONE;
            }
        }
    }*/

    public enum LOCATION_TYPE {
        NONE("0"),
        PICKUP("1"),
        DROP("2");

        private final String text;
        private LOCATION_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(PICKUP.toString())){return 1;}
            else if(this.text.equals(DROP.toString())){return 2;}
            else{return 0;}
        }

        /*public int getResource(){
            Log.d("Enum", this.text);
            if(this.text.equals(BATTING.toString())){return R.string.debit;}
            else if(this.text.equals(CREDIT.toString())){return R.string.credit;}
            else{return R.string.debit;}
        }*/

        public LOCATION_TYPE toTYPE(String val){
            if(val.equalsIgnoreCase(PICKUP.toString())){
                return PICKUP;
            }else if(val.equalsIgnoreCase(DROP.toString())){
                return DROP;
            }else{
                return NONE;
            }
        }
    }

    public enum GENDER {
        NONE("0"),
        MALE("1"),
        FEMALE("2"),
        OTHERS("3");

        private final String text;
        private GENDER(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(MALE.toString())){return 1;}
            else if(this.text.equals(FEMALE.toString())){return 2;}
            else if(this.text.equals(OTHERS.toString())){return 3;}
            else{return 0;}
        }

        public int getResource(){
            Log.d("Enum", this.text);
            if(this.text.equals(MALE.toString())){return R.string.male;}
            else if(this.text.equals(FEMALE.toString())){return R.string.female;}
//            else if(this.text.equals(OTHERS.toString())){return R.string.others;}
            else{return R.string.not_available;}
        }

        public GENDER toGender(String val){
            if(val.equalsIgnoreCase(GENDER.MALE.toString())){
                return GENDER.MALE;
            }else if(val.equalsIgnoreCase(GENDER.FEMALE.toString())){
                return GENDER.FEMALE;
            }else if(val.equalsIgnoreCase(GENDER.OTHERS.toString())){
                return GENDER.OTHERS;
            }else{
                return GENDER.NONE;
            }
        }
    }

    public enum DOCUMENT_TYPE {
        IMAGE("101"),
        VEHICLE("102"),
        AADHAR("103"),
        LICENCE("104"),
        RC("105");

        private final String text;
        private DOCUMENT_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(RC.toString())){return 105;}
            if(this.text.equals(LICENCE.toString())){return 104;}
            if(this.text.equals(AADHAR.toString())){return 103;}
            if(this.text.equals(VEHICLE.toString())){return 102;}
            else{return 101;}
        }

        public int getResource(){
            Log.d("Enum", this.text);
            if(this.text.equals(RC.toString())){return R.string.rc_details;}
            if(this.text.equals(LICENCE.toString())){return R.string.licence_details;}
            else if(this.text.equals(AADHAR.toString())){return R.string.aadhar_details;}
            else if(this.text.equals(VEHICLE.toString())){return R.string.vehicle_detail;}
            else{return R.string.not_available;}
        }

        public int getTitleResource(){
            Log.d("Enum", this.text);
            if(this.text.equals(RC.toString())){return R.string.rc_number;}
            if(this.text.equals(LICENCE.toString())){return R.string.licence_number;}
            else if(this.text.equals(AADHAR.toString())){return R.string.aadhar_number;}
            else if(this.text.equals(VEHICLE.toString())){return R.string.rc_details;}
            else{return R.string.not_available;}
        }

    }

    public enum LIST_TYPE {
        CURRENT("1"),
        HISTORY("2");

        private final String text;
        private LIST_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(CURRENT.toString())){return 1;}
            else if(this.text.equals(HISTORY.toString())){return 2;}
            else{return 0;}
        }
        public LIST_TYPE toTYPE(String val){
            if(val.equalsIgnoreCase(CURRENT.toString())){
                return CURRENT;
            }else /*if(val.equalsIgnoreCase(DROP.toString()))*/{
                return HISTORY;
            }
        }
    }

    public enum TRIP_TYPE {
        ROUND_TRIP("1"),
        SPECIAL_TRIP("2");

        private final String text;
        private TRIP_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(ROUND_TRIP.toString())){return 1;}
            else if(this.text.equals(SPECIAL_TRIP.toString())){return 2;}
            else{return 0;}
        }
        public TRIP_TYPE toTYPE(String val){
            if(val.equalsIgnoreCase(ROUND_TRIP.toString())){
                return ROUND_TRIP;
            }else /*if(val.equalsIgnoreCase(DROP.toString()))*/{
                return SPECIAL_TRIP;
            }
        }
    }

    public enum CAR_TYPE {
        AC("1"),
        NON_AC("2");

        private final String text;
        private CAR_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(AC.toString())){return 1;}
            else if(this.text.equals(NON_AC.toString())){return 2;}
            else{return 0;}
        }
        public CAR_TYPE toTYPE(String val){
            if(val.equalsIgnoreCase(AC.toString())){
                return AC;
            }else /*if(val.equalsIgnoreCase(DROP.toString()))*/{
                return NON_AC;
            }
        }
    }

    public enum RIDE_LIST_TYPE {
        NOTHING("0"),
        DRIVER_CARS("1"),
        UPCOMING_RIDES("3"),
        COMPLETED_RIDES("4");

        private final String text;
        private RIDE_LIST_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(DRIVER_CARS.toString())){return 1;}
            else if(this.text.equals(UPCOMING_RIDES.toString())){return 3;}
            else if(this.text.equals(COMPLETED_RIDES.toString())){return 4;}
            else{return 0;}
        }
        public RIDE_LIST_TYPE toTYPE(String val){
            if(val.equalsIgnoreCase(DRIVER_CARS.toString())){
                return DRIVER_CARS;
            }else if(val.equalsIgnoreCase(UPCOMING_RIDES.toString())){
                return UPCOMING_RIDES;
            }else if(val.equalsIgnoreCase(COMPLETED_RIDES.toString())){
                return COMPLETED_RIDES;
            }else /*if(val.equalsIgnoreCase(DROP.toString()))*/{
                return NOTHING;
            }
        }
    }

    public enum CONFIRMATION_TYPE {
        DECLINE("2"),
        CONFIRM("3"),
        START("4"),
        REACHED("5");

        private final String text;
        private CONFIRMATION_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(DECLINE.toString())){return 2;}
            else if(this.text.equals(CONFIRM.toString())){return 3;}
            else if(this.text.equals(START.toString())){return 4;}
            else if(this.text.equals(REACHED.toString())){return 5;}
            else{return 0;}
        }
        public CONFIRMATION_TYPE toTYPE(String val){
            if(val.equalsIgnoreCase(DECLINE.toString())){
                return DECLINE;
            }else if(val.equalsIgnoreCase(CONFIRM.toString())){
                return CONFIRM;
            }else if(val.equalsIgnoreCase(START.toString())){
                return START;
            }else if(val.equalsIgnoreCase(REACHED.toString())){
                return REACHED;
            }else /*if(val.equalsIgnoreCase(DROP.toString()))*/{
                return DECLINE;
            }
        }
    }

    public enum IMAGE_UPLOAD_TYPE {
        NONE("none"),
        POST_ADS("post_ads"),
        PROFILE("profile");

        private final String text;
        private IMAGE_UPLOAD_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public enum CART {
        MINIMUM_QUANTITY(1),
        MAXIMUM_QUANTITY(6);

        private final int value;
        private CART(final int value) {
            this.value = value;
        }
        @Override
        public String toString() {
            return value+"";
        }

        public int toInt() {
            return value;
        }
    }

    public enum QUANTITY {
        MINIMUM_QUANTITY(0),
        MAXIMUM_QUANTITY(999);

        private final int value;
        private QUANTITY(final int value) {
            this.value = value;
        }
        @Override
        public String toString() {
            return value+"";
        }

        public int toInt() {
            return value;
        }
    }

    public enum WEBVIEW_TYPE {
        ABOUT_US("about_us"),
        TERMS("terms"),
        PRIVACY("privacy");

        private final String text;
        private WEBVIEW_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public enum EARNINGS_LISTING_TYPE {
        TODAY("1"),
        WEEK("2"),
        MONTH("3"),
        OVERALL("4");

        private final String text;
        private EARNINGS_LISTING_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt(){
            Log.d("Enum", this.text);
            if(this.text.equals(TODAY.toString())){return 1;}
            else if(this.text.equals(WEEK.toString())){return 2;}
            else if(this.text.equals(MONTH.toString())){return 3;}
            else if(this.text.equals(OVERALL.toString())){return 4;}
            else{return 0;}
        }
        public EARNINGS_LISTING_TYPE toTYPE(String val){
            if(val.equalsIgnoreCase(TODAY.toString())){
                return TODAY;
            }else if(val.equalsIgnoreCase(WEEK.toString())){
                return WEEK;
            }else if(val.equalsIgnoreCase(MONTH.toString())){
                return MONTH;
            }else /*if(val.equalsIgnoreCase(DROP.toString()))*/{
                return OVERALL;
            }
        }
    }

    public enum LANGUAGE {
        ARABIC("ar"),
        URDU("ur"),
        ENGLISH("en");

        private final String text;
        private LANGUAGE(final String text) {
            this.text = text;
        }
        @Override
        public String toString() {
            return text;
        }
    }

    public enum WALLET_TYPE {
        WALLET("wallet"),
        WALLET_PLUS("Wallet plus");

        private final String text;

        private WALLET_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int toInt() {
            Log.d("Enum", this.text);
            if (this.text.equals(WALLET.toString())) {
                return 1;
            } else if (this.text.equals(WALLET_PLUS.toString())) {
                return 2;
            }
            return 2;
        }

    }

    public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

    //HttpResponse Results
    public static String  RESPONSE_HTTP_SUCCESS             = "3";
    public static String  RESPONSE_HTTP_NO_RECORDS_FOUND    = "404";
    public static String  RESPONSE_HTTP_BAD_REQUEST         = "2";
    public static String  RESPONSE_HTTP_SESSION_EXPIRED     = "401";
    public static String  RESPONSE_HTTP_METHOD_NOT_ALLOWED  = "405";
    public static int  RESPONSE_HTTP_METHOD_FORBIDDEN       = 403;
    public static int  RESPONSE_HTTP_METHOD_SESSION_EXPIRED = 401;
    public static String  RESPONSE_HTTP_RECORD_EXIST        = "409";
    public static int  RESPONSE_HTTP_UNAVAILABLE            = 503;
    public static String  RESPONSE_HTTP_BAD_REQUEST_5       = "5";

    public static String MIN_REQURIED_QUANTITY = "0";
    public static int MIN_REQURIED_QUANTITY_INT = 0;
    public static String DEVICE_TYPE = "1";

    public static final String
            AUTHORIZATION = "Authentication",
            CONTENT_TYPE = "Content-Type",
            ACCEPT_LANGUAGE = "Accept-Language";

    public static final int
            TRANS_START_NUMBER      = 100000,
            TRANS_END_NUMBER        = 999999;

    public static String  API_DB_DATE_TIME_FORMAT       ="yyyy-MM-dd'T'HH:mm:ss";
    public static String  DATE_FORMAT_NEW               ="yyyy-MM-dd";
    public static String  DATE_FORMAT_WITH_TIME         ="dd/MM/yyyy";
    public static String  MONGO_DB_DATE_TIME_FORMAT     ="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static String  DATE_FORMAT                   ="dd/MM/yyyy";
    public static String  DATE_FORMAT_MONTH             ="MMM dd, yyyy";
    public static String  DATE_FORMAT_MONTH_NEW         ="yyyy-MM-dd";
    public static String  DATE_FORMAT_TIME_NEW          ="HH:mm:ss";
    public static String  TIME_FORMAT                   ="hh:mm a";
    public static String  ENCRYPTION_TIME_FORMAT_UTC    ="yyyyMMddHH";

    public static String  AUTHENTICATION_DATE_FORMAT    ="yyyyMMdd";
    public static String  AUTHENTICATION_TIME_FORMAT    ="HHmm";

    public final static String  AUTHENTICATION_USERNAME ="user@quickfix";
    public final static String  AUTHENTICATION_PASSWORD ="JpYXQiOjE1OTU1MDc5MT";

    public final static String CONTENT_TYPE_VALUE = "application/json; charset=UTF-8";

    public static final int LIST_REQUEST_SIZE           = 100;
    public static final int REQUEST_TYPE                = 1;
    public static final int LIST_REQUEST_SIZE_SMALL     = 4;


    public static final String LIST_REQUEST_STATUS_NEW         = "1";
    public static final String LIST_REQUEST_STATUS_CONFIRM     = "3";
    public static final String LIST_REQUEST_STATUS_CANCEL      = "2";
    public static final String LIST_REQUEST_STATUS_REJECT      = "4";

    public static final String DRIVER_AVAILABILITY_ON_STATUS   = "1";
    public static final String DRIVER_AVAILABILITY_OFF_STATUS  = "2";

    public static final String  PAGE_ABOUT                 = "about";
    public static final String  PAGE_REG_TNC               = "tnc_client_reg";
    public static final String  PAGE_PP                    = "tnc_pp";
    public static final String  PAGE_LOGIN_TNC             = "tnc_service_reg";

    public static String SHARED_PREFERENCE_KEY                  =  "QuickFix-Vendor-Android-IMCRINOX@2020";
    public static String SHARED_PREFERENCE_TIME_DIFFERENCE      =  "SharedPreferenceTimeDifference";
    public static String SHARED_PREFERENCE_USERID               =  "SharedPreferenceUserID";
    public static String SHARED_PREFERENCE_HOME                 =  "SharedPreferenceHome";
    public static String SHARED_PREFERENCE_PROFILE              =  "SharedPreferenceUserProfile";
    public  String SHARED_PREFERENCE_HOME_PAGE_DATA             =  "SharedPreferenceUserHomePageData";
    public static String SHARED_PREFERENCE_USER_TYPE            =  "SharedPreferenceUserType";
    public static String SHARED_BID_CHECKER_TIMESTAMP           =  "SharedPreferenceBidCheckerTimestamp";
    public static String SHARED_PREFERENCE_SELECTED_CITY        =  "SharedPreferenceSelectedCity";
    public static String SHARED_PREFERENCE_SELECTED_COUNTRY     =  "SharedPreferenceSelectedCountry";
    public static String SHARED_PREFERENCE_SELECTED_LANGUAGE    =  "SharedPreferenceSelectedLanguage";
    public static String SHARED_PREFERENCE_NETWORK_CONNECTION_STATUS    =  "SharedPreferenceNetworkConnectionStatus";
    public static String SHARED_PREFERENCE_ONGOING_RIDE_STATUS   =  "SharedPreferenceOngoingRideStatus";
    public static String SHARED_PREFERENCE_SELECTED_COUNTRY_CODE =  "SharedPreferenceSelectedCountryCode";
    public static String SHARED_PREFERENCE_SELECTED_MOBILE_NUMBER=  "SharedPreferenceSelectedMobileNumber";

    public static String SHARED_PREFERENCE_CITY_MODEL           =  "SharedPreferenceUserCityModel";

    public static String SHARED_PREFERENCE_BANNER               =  "SharedPreferenceBanner";
    public static String SHARED_PREFERENCE_CATEGORIES           =  "SharedPreferenceCategories";

    public static String SHARED_PREFERENCE_GCM_TOKEN            =  "SharedPreferenceGCMTOKEN";

    public static String SHARED_PREFERENCE_CART_COUNT           =  "SharedPreferenceCartCount";
    public static String SHARED_PREFERENCE_NOTIFICATION_COUNT   =  "SharedPreferenceNotificationCount";
    public static String SHARED_PREFERENCE_MY_NOTIFICATION_COUNT =  "SharedPreferenceMyNotificationCount";
    public static String SHARED_PREFERENCE_NOTIFICATION_SETTINGS=  "SharedPreferenceNotificationSettings";

    public static String SHARED_PREFERENCE_TOKEN                =  "SharedPreferenceToken";
    public static String SHARED_PREFERENCE_GUID                 =  "SharedPreferenceGUID";
    public static String SHARED_PREFERENCE_COOKIE               = "SharedPreferenceCOOKIE";


    public static final int REQUEST_FOR_LOGIN               = 101;
    public static final int REQUEST_FOR_CAMERA              = 222;
    public static final int REQUEST_FOR_SELECTFILE          = 223;
    public static final int REQUEST_FOR_GOOGLE              = 224;
    public static final int REQUEST_FOR_PAYMENT             = 225;
    public static final int REQUEST_FOR_FILTER_INNER        = 226;
    public static final int REQUEST_FOR_FILTER_MAIN         = 227;

    public static final int PERMISSIONS_REQUEST_CAMERA      = 151;
    public static final int PERMISSIONS_REQUEST_CALENDER    = 152;
    public static final int PERMISSIONS_REQUEST_PRIMARY     = 153;
    public static final int REQUEST_OVER_OTHER_APP_PERMISSION = 2084;


    public static int
            AGE_MIN                = 18,
            AGE_MAX                = 68;

    public static int
            TYPE_HOME              = 1,
            TYPE_MY_EARNINGS       = 2,
            TYPE_MY_TRIPS          = 3,
            TYPE_SETTING           = 4,
            TYPE_ABOUT_US          = 5,
            TYPE_HELP              = 6,
            TYPE_SHARE             = 7,
            TYPE_RATE              = 8,
            TYPE_LOGOUT            = 9,
            TYPE_MY_BOOKINGS       = 10,
            TYPE_PROFILE           = 11,
            TYPE_CREATE_RIDE       = 12,
            TYPE_NEW               = 13,
            TYPE_ACTIVE            = 14,
            TYPE_UPCOMING          = 15;

    public static final String LIST_REQUEST_STATUS_TODAYS_EARNINGS       = "1";
    public static final String LIST_REQUEST_STATUS_WEEKS_EARNINGS        = "2";
    public static final String LIST_REQUEST_STATUS_MONTHS_EARNINGS       = "3";
    public static final String LIST_REQUEST_STATUS_OVERALL_EARNINGS      = "4";

    public static final String GO_TO_NEW_TAB         = "100";
    public static final String GO_TO_ACTIVE_TAB      = "200";
    public static final String GO_TO_UPCOMING_TAB    = "300";
    public static final String GO_TO_CHAT            = "400";


    public static final String
                              SUNDAY= "6",
                              MONDAY= "0",
                              TUESDAY= "1",
                              WEDDAY= "2",
                              THRDAY= "3",
                              FRIDAY= "4",
                              SATDAY= "5";

    public static final String NEW_REQUEST_STATUS                      = "101";
    public static final String ORDER_ACTIVE_STATUS                     = "105";
    public static final String ORDER_ACTIVE_PART_STATUS                = "106";
    public static final String ORDER_HISTORY_CANCEL_STATUS             = "107";
    public static final String ORDER_HISTORY_STATUS                    = "108";
    public static final String ORDER_HISTORY_COMPLETED_STATUS          = "109";
    public static final String WALLET_STATUS                           = "201";
    public static final String BANK_STATUS                             = "203";
    public static final String CHAT_STATUS                             = "151";
    public static final String ACCOUNT_ACTIVATED                       = "402";


    public static final int
            PLACE_AUTOCOMPLETE_REQUEST_CODE = 222,
            PLACE_PICKER_REQUEST            = 223,
            REQUEST_PREDICTION_CODE         = 224,
            REQUEST_LOCATION_CODE           = 225,
            REQUEST_GPS_ENABLED             = 226,
            REQUEST_COLOR_CODE              = 227,
            REQUEST_MAIN_PICK_UP_LOCATION_CODE = 231,
            REQUEST_MAIN_DROP_LOCATION_CODE = 232,
            REQUEST_HEAD_OFFICE_LOCATION_CODE = 233,
            REQUEST_BRANCH_ADDRESS_LOCATION_CODE = 234,
            REQUEST_INDIVIDUAL_ADDRESS_LOCATION_CODE = 235,
            REQUEST_SELECT_CITY_CODE = 236,
            REQUEST_CODE_FOR_YEAR = 237,
            REQUEST_CODE_FOR_MONTH = 238,
            REQUEST_CODE_FOR_SUB_CATEGORY = 239,
            REQUEST_CODE_FOR_HOUR = 240,
            REQUEST_CODE_FOR_MINUTE = 241;

    public static final int
            CREATE_RIDE_STATUS_CODE         = 201,
            CANCELLED_RIDE_STATUS_CODE      = 206,
            START_RIDE_STATUS_CODE          = 216,
            STOP_RIDE_STATUS_CODE           = 226,
            CONFIRMED_USER_RIDE_STATUS_CODE = 203,
            CANCELLED_USER_RIDE_STATUS_CODE = 304,
            REJECT_RIDE_STATUS_CODE         = 305;

    public static final double
            PAYMENT_MODE_NOTHING            = 600,
            PAYMENT_MODE_COD                = 601,
            PAYMENT_MODE_PAY_NOW            = 602;

    public static final String POOR = "1";
    public static final String FAIR = "2";
    public static final String GOOD = "3";
    public static final String VERY_GOOD = "4";
    public static final String EXCELLENT = "5";

    public static final String REGISTRATION_USER_TYPE_INDIVIDUAL = "1",
                               REGISTRATION_USER_TYPE_COMPANY    = "2",
                               REGISTRATION_SPEAK_IN_ENGLISH_YES    = "1",
                               REGISTRATION_SPEAK_IN_ENGLISH_NO    = "0";

    //Profile Photo Variables
    public String  VEHICLE_PICTURE_FOLDER    = "vehicle";
    public static String  PROFILE_PICTURE_FOLDER    = "Profile";
    public static String  DOCUMENTS_FOLDER    = "documents";
    public static final String CHANNEL_ID = "goride_user_channel";
    public static final String CHANNEL_NAME = "Goride_Notification";
    public static final String CHANNEL_DESCRIPTION = "goride_user";

    public static final String UPLOAD_DL_PHOTO                  = "uploadDLImage";
    public static final String UPLOAD_RC_PHOTO                  = "uploadRCImage";
    public static final String UPLOAD_INSURANCE_PHOTO           = "uploadInsuranceImage";
    public static final String UPLOAD_CAR_PHOTO                 = "uploadCarImage";

    public static final String UPLOAD_PROFILE_PHOTO_PATH         = "QuickfixVendor/registration/user_profile";
    public static final String UPLOAD_LICENSE_PHOTO_PATH         = "QuickfixVendor/registration/professional_license";
    public static final String UPLOAD_SCIENTIFIC_DOC_PHOTO_PATH  = "QuickfixVendor/registration/scientific_documents";
    public static final String UPLOAD_IDENTIFICATION_PHOTO_PATH  = "QuickfixVendor/registration/identification";
    public static final String UPLOAD_GALLERY_PHOTO_PATH         = "QuickfixVendor/registration/gallery";
    public static final String UPLOAD_COMMERCIAL_REG_PHOTO_PATH  = "QuickfixVendor/registration/commercial_registration";
    public static final String UPLOAD_CERTIFICATE_PHOTO_PATH     = "QuickfixVendor/registration/certificates";

    public static final String UPLOAD_PROFILE_PHOTO_PATH_CODE         = "1";
    public static final String UPLOAD_LICENSE_PHOTO_PATH_CODE         = "2";
    public static final String UPLOAD_IDENTIFICATION_PHOTO_PATH_CODE  = "3";
    public static final String UPLOAD_GALLERY_PHOTO_PATH_CODE         = "4";
    public static final String UPLOAD_COVER_PHOTO_PATH_CODE           = "5";
    public static final String UPLOAD_COMMERCIAL_REG_PHOTO_PATH_CODE  = "6";
    public static final String UPLOAD_SCIENTIFIC_DOC_PHOTO_PATH_CODE  = "7";
    public static final String UPLOAD_CERTIFICATE_PHOTO_PATH_CODE     = "8";

    public static final String UPLOAD_PROFILE_PHOTO_LIST_CODE         = "PROFILE_PHOTO_LIST";
    public static final String UPLOAD_LICENSE_PHOTO_LIST_CODE         = "LICENSE_PHOTO_LIST";
    public static final String UPLOAD_IDENTIFICATION_PHOTO_LIST_CODE  = "IDENTIFICATION_PHOTO_LIST";
    public static final String UPLOAD_GALLERY_PHOTO_LIST_CODE         = "GALLERY_PHOTO_LIST";
    public static final String UPLOAD_COVER_PHOTO_LIST_CODE           = "COVER_PHOTO_LIST";
    public static final String UPLOAD_COMMERCIAL_REG_PHOTO_LIST_CODE  = "COMMERCIAL_REG_PHOTO_LIST";
    public static final String UPLOAD_SCIENTIFIC_DOC_LIST_CODE        = "SCIENTIFIC_DOC_LIST";
    public static final String UPLOAD_CERTIFICATES_LIST_CODE          = "CERTIFICATES_LIST";

    public static final String  UPDATE_APP     = "1";

    public static final String  PAYMENT_BY_COD        = "1";
    public static final String  PAYMENT_BY_ONLINE     = "2";

    public static final String  ID_IMAGE_CLICKED              = "101";
    public static final String  CERTIFICATE_IMAGE_CLICKED     = "102";

    public static final String  TYPE_LOGIN       = "1";
    public static final String  TYPE_SIGNIN      = "2";

    public static final String  DOCUMENTS_NOT_VERIFIED      = "0";
    public static final String  DOCUMENTS_VERIFIED          = "1";
    public static final String  DOCUMENTS_BLOCKED           = "2";

    public static final String  FROM_DOCUMENTS_REJECT_PAGE  = "111";

    public static final String  REGISTRATION_TYPE_INDIVIDUAL      = "1";
    public static final String  REGISTRATION_TYPE_COMPANY         = "2";

    public static final String  VENDOR_IS_ACTIVE             = "1";
    public static final String  VENDOR_IS_INACTIVE           = "0";
    public static final String  VENDOR_IS_FORCE_INACTIVE     = "2";

    public static final String  FROM_EDIT_PROFILE         = "10";

    public static final boolean IS_RIDE_COMPLETED = false;

    public static final String
            NOTIFICATION_LOCAL_BROADCAST_KEY                    = "NotificationLocalBroadcastKey",
            NOTIFICATION_LOCAL_BROADCAST_NOTIFICATION_MODEL     = "NotificationLocalBroadcastNotificationModel" ;

    public static final String  PAGE_FROM_LOGIN                   = "10";
    public static final String  PAGE_FROM_REGISTRATION            = "20";
    public static final String  PAGE_FROM_EDIT_NUMBER             = "30";
    public static final String  PAGE_FROM_OTP                     = "40";
    public static final String  PAGE_FROM_LOGIN_WITH_PASSWORD     = "50";

    public static final String  FROM_HEAD_OFFICE_ADDRESS       = "51";
    public static final String  FROM_BRANCH_OFFICE_ADDRESS     = "52";

    public static final String  TYPE_HEAD_OFFICE_ADDRESS       = "1";
    public static final String  TYPE_BRANCH_OFFICE_ADDRESS     = "2";

    public static final String  FROM_PAGE_REVIEW_COMPANY_REGISTRATION        = "11";
    public static final String  FROM_PAGE_REVIEW_INDIVIDUAL_REGISTRATION     = "12";

    public static final String  GENDER_MALE      = "1";
    public static final String  GENDER_FEMALE    = "2";

    public static final String  FROM_YEAR     = "101";
    public static final String  FROM_MONTH    = "102";

    public static final String  FROM_HOUR      = "110";
    public static final String  FROM_MINUTE    = "120";

    public static final String  NEW_REQUEST         = "1";
    public static final String  UPCOMING_REQUEST    = "2";
    public static final String  HISTORY_REQUEST     = "3";

    public static final String  REQUEST_ACCEPT     = "2";
    public static final String  REQUEST_REJECT     = "3";

    public static final String  CHAT_FROM_USER        = "1";
    public static final String  CHAT_FROM_VENDOR      = "2";

    public static final String  ORDER_PLACED                         = "100";
    public static final String  WAITING_FOR_CONFIRMATION             = "101";
    public static final String  ORDER_CONFIRMED_BY_TECHNICIAN        = "102";
    public static final String  ON_THE_WAY                           = "103";
    public static final String  WORK_IN_PROGRESS                     = "104";
    public static final String  ARRIVED_TO_CUSTOMER                  = "99";
    public static final String  WAITING_FOR_SPARE_PARTS_APPROVAL     = "105";
    public static final String  JOB_COMPLETED                        = "106";
    public static final String  USER_CANCELLED                       = "107";
    public static final String  CANCELLED                            = "108";
    public static final String  ORDER_COMPLETED                      = "109";
    public static final String  CUSTOMER_NOT_RESPONDED               = "112";

    public static final String  PAYMENT_TYPE_COD                     = "1";
    public static final String  PAYMENT_TYPE_ONLINE                  = "2";
    public static final String  PAYMENT_TYPE_NOT_SET                 = "0";

    public static final String  FROM_PAGE_COMPLETED_REQUESTS        = "20";
    public static final String  FROM_PAGE_UPCOMING_REQUESTS         = "30";
    public static final String  FROM_PAGE_NEW_REQUESTS              = "31";

    public static final String  TO_PAGE_ACTIVE_REQUESTS         = "110";

    public static final String  PART_STATUS_ADDED            = "50";
    public static final String  PART_STATUS_PENDING          = "51";
    public static final String  PART_STATUS_ACCEPTED         = "52";
    public static final String  PART_STATUS_REJECTED         = "53";

    public static final String  TYPE_CREDIT         = "1";
    public static final String  TYPE_DEBIT          = "2";

    public static final String  TYPE_ORDER         = "1";
    public static final String  TYPE_PAID          = "2";

    public static final String  PAYMENT_TYPE_MADA            = "1";
    public static final String  PAYMENT_TYPE_VISA            = "2";
    public static final String  PAYMENT_TYPE_MASTER          = "3";
    public static final String  PAYMENT_TYPE_AMERICAN        = "4";

    public static final String  SOUDI_ARABIA_COUNTRY_CODE     = "+966";
    public static final String  HTTP_URL_TERMS_AND_CONDITION  = "http://35.154.4.154/quick_fix_website/contact-us";

}
