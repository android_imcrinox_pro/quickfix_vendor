package sa.quickfix.vendor.global;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.jsoup.Jsoup;

import sa.quickfix.vendor.flows.add_parts.activities.AddPartsActivity;
import sa.quickfix.vendor.flows.add_parts.activities.SubCategoryListActivity;
import sa.quickfix.vendor.flows.add_parts.activities.TimeListActivity;
import sa.quickfix.vendor.flows.bank_details.activities.BankDetailsActivity;
import sa.quickfix.vendor.flows.bank_details.activities.EditBankDetailsActivity;
import sa.quickfix.vendor.flows.edit_services.EditServicesActivity;
import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.flows.login.activities.LoginActivity;
import sa.quickfix.vendor.flows.login.activities.LoginWithPasswordActivity;
import sa.quickfix.vendor.flows.login.activities.LoginWithPhoneNumberActivity;
import sa.quickfix.vendor.flows.login.activities.OtpActivity;
import sa.quickfix.vendor.flows.my_ratings.activities.MyRatingsActivity;
import sa.quickfix.vendor.flows.notification.activities.NotificationActivity;
import sa.quickfix.vendor.flows.profile.activities.EditProfileActivity;
import sa.quickfix.vendor.flows.profile.activities.ReUploadDocumentsActivity;
import sa.quickfix.vendor.flows.registration.activities.RegisterActivity;
import sa.quickfix.vendor.flows.registration.activities.RegistrationSuccessActivity;
import sa.quickfix.vendor.flows.registration.activities.SelectServicesActivity;
import sa.quickfix.vendor.flows.registration.activities.TermsAndConditionsActivity;
import sa.quickfix.vendor.flows.registration.activities.company_registration.CompanyRegistrationActivity;
import sa.quickfix.vendor.flows.registration.activities.company_registration.CompanyRegistrationUploadDocumentsActivity;
import sa.quickfix.vendor.flows.registration.activities.company_registration.CompanyReviewApplicationActivity;
import sa.quickfix.vendor.flows.registration.activities.individual_registration.CityListActivity;
import sa.quickfix.vendor.flows.registration.activities.individual_registration.IndividualRegistrationActivity;
import sa.quickfix.vendor.flows.registration.activities.individual_registration.IndividualRegistrationUploadDocumentsActivity;
import sa.quickfix.vendor.flows.registration.activities.individual_registration.YearListActivity;
import sa.quickfix.vendor.flows.request_detail.activities.RequestDetailsActivity;
import sa.quickfix.vendor.flows.wallet.activities.MyWalletActivity;
import sa.quickfix.vendor.SplashScreen;
import sa.quickfix.vendor.addon.PasswordValidator;
import sa.quickfix.vendor.full_image_view.FullImageMainActivity;
import sa.quickfix.vendor.map.SearchPlaceOnMapActivity;
import sa.quickfix.vendor.services.model.AddressModel;
import sa.quickfix.vendor.services.model.HomeIndexModel;
import sa.quickfix.vendor.services.model.KeyValueListModel;
import sa.quickfix.vendor.services.model.NotificationSettingsModel;
import sa.quickfix.vendor.services.model.OrderStatusModel;
import sa.quickfix.vendor.services.model.ProfileModel;
import sa.quickfix.vendor.services.model.ShiftModel;
import sa.quickfix.vendor.view.AlertDialog;
import sa.quickfix.vendor.view.ProgressDialog;
import sa.quickfix.vendor.R;
import sa.quickfix.vendor.flows.completed_requests.activities.CompletedRequestsActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Pattern;

public class GlobalFunctions {

    public static String TAG = "GlobalFunctions";

    static GlobalVariables globalVariables = new GlobalVariables();

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static ProgressDialog progressDialog = null;
    private static GlobalFunctions instance;

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    private static final String uniqueID = UUID.randomUUID().toString();

    private static int navigationSelectedPosition = -1;
    private static Context context = null;


    public static GlobalFunctions getInstance(Context globalContext) {
        if (instance == null) {
            instance = new GlobalFunctions();
        }
        return instance;
    }

    public static String getDatefromTimestamp(long timestamp, String format, Locale locale) {
        Log.d(TAG, "Date:" + new Date(timestamp * 1000).getDate());
        String date = new SimpleDateFormat(format, locale).format(new Date(timestamp * 1000));
        //Log.d(TAG,"CurrentDate : "+date);
        return date;
    }

    public static String getDateFromAPITime(Context context, String timeString, String format, Locale locale) {
        try {
            DateFormat formatLocal = new SimpleDateFormat(GlobalVariables.API_DB_DATE_TIME_FORMAT, Locale.ENGLISH);
            Date date = formatLocal.parse(timeString);
            long timestamp = date.getTime() / 1000;
            if (timestamp == 0) {
                timestamp = getCurrentTimestamp(context);
            }
            return getDatefromTimestamp(timestamp, format, locale);
        } catch (ParseException e) {
            e.printStackTrace();
            long timestamp = getCurrentTimestamp(context);
            return getDatefromTimestamp(timestamp, format, locale);
        }
    }

    public static String getDateFromDBTime(String timeString, String format, Locale locale) {
        try {
            DateFormat formatLocal = new SimpleDateFormat(GlobalVariables.MONGO_DB_DATE_TIME_FORMAT, Locale.ENGLISH);
            Date date = formatLocal.parse(timeString);
            long timestamp = date.getTime() / 1000;
            return getDatefromTimestamp(timestamp, format, locale);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTimeFromDBTime(String timeString, String format, Locale locale) {
        try {
            DateFormat formatLocal = new SimpleDateFormat(GlobalVariables.MONGO_DB_DATE_TIME_FORMAT, Locale.ENGLISH);
            Date date = formatLocal.parse(timeString);
            long timestamp = date.getTime() / 1000;
            return getTimefromTimestamp(timestamp, format, locale);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTimeFromAPITime(Context context, String timeString, String format, Locale locale) {
        try {
            DateFormat formatLocal = new SimpleDateFormat(GlobalVariables.API_DB_DATE_TIME_FORMAT, Locale.ENGLISH);
            Date date = formatLocal.parse(timeString);
            long timestamp = date.getTime() / 1000;
            if (timestamp == 0) {
                timestamp = getCurrentTimestamp(context);
            }
            return getTimefromTimestamp(timestamp, format, locale);

        } catch (ParseException e) {
            e.printStackTrace();
            long timestamp = getCurrentTimestamp(context);
            return getTimefromTimestamp(timestamp, format, locale);
        }
    }

    public static long getTimeStampFromDBTime(String timeString) {
        try {
            DateFormat formatLocal = new SimpleDateFormat(GlobalVariables.MONGO_DB_DATE_TIME_FORMAT, Locale.ENGLISH);
            Date date = formatLocal.parse(timeString);
            long timestamp = date.getTime() / 1000;
            return timestamp;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static long getTimeStampFromFBTimeStirng(String timeString) {
        try {
            DateFormat formatLocal = new SimpleDateFormat(GlobalVariables.DATE_FORMAT, Locale.ENGLISH);
            Date date = formatLocal.parse(timeString);
            long timestamp = date.getTime() / 1000;
            return timestamp;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getDayMonthYearFromServerDate(String date) {
        String formatteddate = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_MONTH_NEW);
            SimpleDateFormat df2 = new SimpleDateFormat("MMM dd, yyyy");
            formatteddate = df2.format(format.parse(date));
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formatteddate;
    }


    public static String getFromEpochTIme(long millis) {
        Date date = new Date(millis);
        //SimpleDateFormat sdf = new SimpleDateFormat("EEE,MMM d,yyyy h:mm,a", Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat("h:mm a", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String formattedDate = sdf.format(date);
        System.out.println(formattedDate);
        return formattedDate;
    }

    public static int getNavigationSelectedPosition() {
        return navigationSelectedPosition;
    }

    public static String getTimefromTimestamp(long timestamp, String format, Locale locale) {
        String time = new SimpleDateFormat(format, locale).format(new Date(timestamp * 1000));
        // Log.d(TAG,"CurrentTime : "+time);
        return time;
    }

    public static int getCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        return year;
    }

    public static long getCurrentTimestamp(Context context) {
        Long tsLong = System.currentTimeMillis() / 1000;
        long diff = getSharedPreferenceLong(context, globalVariables.SHARED_PREFERENCE_TIME_DIFFERENCE);
        tsLong = tsLong - diff;
        Log.d(TAG, "CurrentTimestamp : " + tsLong);
        Log.d(TAG, "CurrentTimestamp elapsed: " + SystemClock.elapsedRealtime());
        return tsLong;
    }

    public static String getTimeDifference(long newTime, long oldTime) {
        String Function_TAG = "getTimeDifference";

        String differenceTime = null;
        long mills = (newTime / 1000) - oldTime;
        int Mins = (int) (mills / 60);
        int Hours = (int) (Mins / 60);
        int Days = (int) (Hours / 24);

        Log.d(Function_TAG, newTime + "--" + oldTime);
        Log.d(Function_TAG, Mins + "--" + Hours + "--" + Days);

        if (Days > 0) {
            String str = Days + "";
            str += Days > 1 ? " Days" : " Day";
            differenceTime = differenceTime == null ? str : differenceTime + str;
        } else {
            if (Hours > 0) {
                String str = Hours + "";
                str += Hours > 1 ? " Hours" : " Hour";
                differenceTime = differenceTime == null ? str : differenceTime + str;
            } else if (Mins > 0) {
                String str = Mins + "";
                str += Mins > 1 ? " Minutes" : " Minute";
                differenceTime = differenceTime == null ? str : differenceTime + str;
            }
        }

        return differenceTime;
    }


    public static String getAddressTextFromModelwithComma(Context context, AddressModel addressModel) {
        String returnAddress = null;

        if (addressModel.getStreet() != null && !addressModel.getStreet().equalsIgnoreCase("null") && !addressModel.getStreet().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getStreet() : returnAddress + ", " + addressModel.getStreet();
        }
        if (addressModel.getAddress() != null && !addressModel.getAddress().equalsIgnoreCase("null") && !addressModel.getAddress().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getAddress() : returnAddress + ", " + addressModel.getAddress();
        }
        if (addressModel.getCity() != null && !addressModel.getCity().equalsIgnoreCase("null") && !addressModel.getCity().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getCity() : returnAddress + ", " + addressModel.getCity();
        }
        if (addressModel.getState() != null && !addressModel.getState().equalsIgnoreCase("null") && !addressModel.getState().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getState() : returnAddress + ", " + addressModel.getState();
        }
        if (addressModel.getCountry() != null && !addressModel.getCountry().equalsIgnoreCase("null") && !addressModel.getCountry().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getCountry() : returnAddress + ", " + addressModel.getCountry();
        }
        return returnAddress;
    }

    public static String getAddressTextFromModel(Context context, AddressModel addressModel) {
        String returnAddress = null;

        if (addressModel.getName() != null && !addressModel.getName().equalsIgnoreCase("null") && !addressModel.getName().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getName() : returnAddress + "\n" + addressModel.getName();
        }
        if (addressModel.getAddress() != null && !addressModel.getAddress().equalsIgnoreCase("null") && !addressModel.getAddress().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getAddress() : returnAddress + "\n" + addressModel.getAddress();
        }
        if (addressModel.getStreet() != null && !addressModel.getStreet().equalsIgnoreCase("null") && !addressModel.getStreet().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getStreet() : returnAddress + "\n" + addressModel.getStreet();
        }
        if (addressModel.getState() != null && !addressModel.getState().equalsIgnoreCase("null") && !addressModel.getState().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getState() : returnAddress + "\n" + addressModel.getState();
            if (addressModel.getPincode() != null && !addressModel.getPincode().equalsIgnoreCase("null") && !addressModel.getPincode().equals("")) {
                returnAddress += "-" + addressModel.getPincode();
            }
        } else if (addressModel.getPincode() != null && !addressModel.getPincode().equalsIgnoreCase("null") && !addressModel.getPincode().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getPincode() : returnAddress + "\n" + addressModel.getPincode();
        }
        if (addressModel.getPhone() != null && !addressModel.getPhone().equalsIgnoreCase("null") && !addressModel.getPhone().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getPhone() : returnAddress + "\n" + addressModel.getPhone();
        }
        return returnAddress;
    }

    public static String getAddressTextFromModelExcludingNameAndAddress(Context context, AddressModel addressModel) {
        String returnAddress = null;

        if (addressModel.getAddress() != null && !addressModel.getAddress().equalsIgnoreCase("null") && !addressModel.getAddress().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getAddress() : returnAddress + "\n" + addressModel.getAddress();
        }
        if (addressModel.getCity() != null && !addressModel.getCity().equalsIgnoreCase("null") && !addressModel.getCity().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getCity() : returnAddress + "\n" + addressModel.getCity();
            if (addressModel.getPincode() != null && !addressModel.getPincode().equalsIgnoreCase("null") && !addressModel.getPincode().equals("")) {
                returnAddress += "-" + addressModel.getPincode();
            }
        } else if (addressModel.getPincode() != null && !addressModel.getPincode().equalsIgnoreCase("null") && !addressModel.getPincode().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getPincode() : returnAddress + "\n" + addressModel.getPincode();
        }
        return returnAddress;
    }

    public static String getAddressTextFromModelExcludingNameAndAddresswithComma(Context context, AddressModel addressModel) {
        String returnAddress = null;

        if (addressModel.getAddress() != null && !addressModel.getAddress().equalsIgnoreCase("null") && !addressModel.getAddress().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getAddress() : returnAddress + ", " + addressModel.getAddress();
        }
        if (addressModel.getCity() != null && !addressModel.getCity().equalsIgnoreCase("null") && !addressModel.getCity().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getCity() : returnAddress + ", " + addressModel.getCity();
            if (addressModel.getPincode() != null && !addressModel.getPincode().equalsIgnoreCase("null") && !addressModel.getPincode().equals("")) {
                returnAddress += "-" + addressModel.getPincode();
            }
        } else if (addressModel.getPincode() != null && !addressModel.getPincode().equalsIgnoreCase("null") && !addressModel.getPincode().equals("")) {
            returnAddress = returnAddress == null ? addressModel.getPincode() : returnAddress + ", " + addressModel.getPincode();
        }
        return returnAddress;
    }


    public static String getProfilePictureName(Context context) {
        ProfileModel profile = getProfile(context);
        return (profile.getId() + "_" + "profile_picture.jpg");
    }

    public static void saveProfilePicture(Context context, String folder, String fileName, Bitmap bmp) {
        File file = getProfilePicturePath(context, folder, fileName);
        if (file.exists()) {
            file.delete();
        }
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void setCountryCode(Context context, String countryCode) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_SELECTED_COUNTRY_CODE, countryCode);
    }

    public static String getCountryCode(Context context) {
        String countryCodeString = null;
        countryCodeString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_SELECTED_COUNTRY_CODE);
        return countryCodeString;
    }

    public static void setMobileNumber(Context context, String mobileNumber) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_SELECTED_MOBILE_NUMBER, mobileNumber);
    }

    public static String getMobileNumber(Context context) {
        String mobileNumberString = null;
        mobileNumberString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_SELECTED_MOBILE_NUMBER);
        return mobileNumberString;
    }


   /* public static String getPriceStringFromDecimal(double price) {
        String priceString = null;
        try {
            Locale locale = new Locale("en", "IN");
            DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
            DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(locale);
            dfs.setCurrencySymbol("\u20B9");
            decimalFormat.setDecimalFormatSymbols(dfs);
            priceString = decimalFormat.format(price);
            //priceString = NumberFormat.getCurrencyInstance(new Locale("en", "IN")).format(price);
        } catch (Exception exx) {
            priceString = String.format("%.2f", price);
        }
        return priceString;
    }*/

    public static String getPriceStringFromDecimal(String price) {
        String priceString = null;
        try {
            double priceTmp = Double.parseDouble(price);
            priceString = String.format("%.2f", priceTmp);
        } catch (Exception exx) {
            priceString = price;
        }
        return priceString;
    }

    public static String getCalculateTotalAmount(String discountTotal, String billDiscount) {
        String result = "0.0";
        try {
            int tmpValue = 0, tmpDiscount = 0, tmpBill = 0;

            if (isNotNullValue(discountTotal)) {
                tmpDiscount = Integer.parseInt(discountTotal);
            }
            if (isNotNullValue(billDiscount)) {
                tmpBill = Integer.parseInt(billDiscount);
            }

            tmpValue = (tmpDiscount + tmpBill);
            result = String.valueOf(tmpValue);

            return result;

        } catch (Exception e) {
            result = "0.0";
            return result;
        }
    }

    public static float getDistancebetweenAddress(AddressModel pickUpAddressModel, AddressModel dropAddressModel) {
        Location l1 = new Location("");
        l1.setLatitude(pickUpAddressModel.getLatitude());
        l1.setLongitude(pickUpAddressModel.getLongitude());

        Location l2 = new Location("");
        l2.setLatitude(dropAddressModel.getLatitude());
        l2.setLongitude(dropAddressModel.getLongitude());

        return l1.distanceTo(l2) / 1000;
    }

    public static boolean isGPSEnabled(Context mContext) {
        LocationManager locationManager = (LocationManager)
                mContext.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            return true;
        } else {
            return false;
        }
    }

    public static String getLocationfromLatLng(Activity activity, double lat, double lng) {
        Geocoder geocoder;
        String fullAddress = null;
        List<Address> addresses;
        geocoder = new Geocoder(activity, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            fullAddress = state + ", " + city + ", " + address;
            //String country = addresses.get(0).getCountryName();
            //String postalCode = addresses.get(0).getPostalCode();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return fullAddress;

    }


    public static String getLocation(LatLng centerLatLng, Context context) {
        if (centerLatLng != null) {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = new ArrayList<Address>();
            try {
                addresses = geocoder.getFromLocation(centerLatLng.latitude,
                        centerLatLng.longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses != null && addresses.size() > 0) {

                String addressIndex0 = (addresses.get(0).getAddressLine(0) != null) ? addresses
                        .get(0).getAddressLine(0) : null;
                String addressIndex1 = (addresses.get(0).getAddressLine(1) != null) ? addresses
                        .get(0).getAddressLine(1) : null;
                String addressIndex2 = (addresses.get(0).getAddressLine(2) != null) ? addresses
                        .get(0).getAddressLine(2) : null;
                String addressIndex3 = (addresses.get(0).getAddressLine(3) != null) ? addresses
                        .get(0).getAddressLine(3) : null;

                String completeAddress = addressIndex0 + "," + addressIndex1;

                if (addressIndex2 != null) {
                    completeAddress += "," + addressIndex2;
                }
                if (addressIndex3 != null) {
                    completeAddress += "," + addressIndex3;
                }
                return completeAddress;

            }
        }
        return null;
    }

    public static String getDateMonthFormat(String date) {
        String formatteddate = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_MONTH);
            SimpleDateFormat df2 = new SimpleDateFormat("dd, MMM EEEE yyyy");
            formatteddate = df2.format(format.parse(date));
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formatteddate;
    }

    public static String getDayMonthFromDate(String date) {
        String formatteddate = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_MONTH_NEW);
            SimpleDateFormat df2 = new SimpleDateFormat("MMM yyyy");
            formatteddate = df2.format(format.parse(date));
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formatteddate;
    }

    public static String getMonthYearFromDate(String date) {
        String formatteddate = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_MONTH_NEW);
            SimpleDateFormat df2 = new SimpleDateFormat("dd EEEE ");
            formatteddate = df2.format(format.parse(date));
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formatteddate;
    }

    public static String getDayMonthYearFromDate(String date) {
        String formatteddate = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_MONTH_NEW);
            SimpleDateFormat df2 = new SimpleDateFormat("EEE MMM dd, yyyy");
            formatteddate = df2.format(format.parse(date));
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formatteddate;
    }

    public static String getMonthDayYearFromDate(String date) {
        String formatteddate = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_MONTH_NEW);
            SimpleDateFormat df2 = new SimpleDateFormat("MMMM dd, yyyy");
            formatteddate = df2.format(format.parse(date));
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formatteddate;
    }

    public static String getDayMonthYearFRMDate(String date) {
        String formatteddate = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_MONTH_NEW);
            SimpleDateFormat df2 = new SimpleDateFormat("dd MMM yyyy");
            formatteddate = df2.format(format.parse(date));
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formatteddate;
    }

    public static String getDayMonthYearFromDateServer(String date) {
        String formatteddate = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_MONTH_NEW);
            SimpleDateFormat df2 = new SimpleDateFormat("dd.MM.yyyy, EEEE");
            formatteddate = df2.format(format.parse(date));
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formatteddate;
    }

    public static String getTimeFromDate(String time) {
        String formattedtime = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_TIME_NEW);
            SimpleDateFormat df2 = new SimpleDateFormat("hh:mm a");
            formattedtime = df2.format(format.parse(time));
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formattedtime;
    }

    public static String getOnlyTimeFromDate(String time) {
        String formattedtime = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_TIME_NEW);
            SimpleDateFormat df2 = new SimpleDateFormat("hh:mm");
            formattedtime = df2.format(format.parse(time));
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formattedtime;
    }


    public static AddressModel getLocationModelfromLatLng(Activity activity, double lat, double lng) {
        Geocoder geocoder;
        AddressModel locationModel = null;
        List<Address> addresses;
        geocoder = new Geocoder(activity, Locale.getDefault());

        try {
            locationModel = new AddressModel();
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            locationModel.setAddress(addresses.get(0).getAddressLine(0)); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            locationModel.setStreet(addresses.get(0).getAddressLine(0)); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            locationModel.setCity(addresses.get(0).getLocality());
            locationModel.setState(addresses.get(0).getAdminArea());
            //fullAddress = state + ", "+ city + ", " +address;
            locationModel.setCountry(addresses.get(0).getCountryName());
            //String postalCode = addresses.get(0).getPostalCode();
        } catch (IOException e) {
            locationModel = null;
            e.printStackTrace();
        }

        return locationModel;

    }

    public static Address getAddressfromLatLng(Activity activity, double lat, double lng) {
        Geocoder geocoder;
        Address address = null;
        List<Address> addresses;
        geocoder = new Geocoder(activity, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses == null) {
                addresses = new ArrayList<>();
            }
            if (addresses.size() > 0) {
                address = addresses.get(0);
            } else {
                address = null;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return address;

    }

    public static AddressModel getAddressModelFromAddress(Address address) {

        AddressModel addressModel = null;
        if (address != null) {
            addressModel = new AddressModel();
            String addressIndex0 = (address.getAddressLine(0) != null) ? address
                    .getAddressLine(0) : null;
            String addressIndex1 = (address.getAddressLine(1) != null) ? address
                    .getAddressLine(1) : null;
            String addressIndex2 = (address.getAddressLine(2) != null) ? address
                    .getAddressLine(2) : null;
            String addressIndex3 = (address.getAddressLine(3) != null) ? address
                    .getAddressLine(3) : null;

            String completeAddress = addressIndex0;

            addressModel.setLongitude(address.getLongitude());
            addressModel.setLatitude(address.getLatitude());
            addressModel.setStreet(address.getSubAdminArea());
            addressModel.setCity(address.getLocality() != null ? address.getLocality() : address.getSubAdminArea());
            addressModel.setState(address.getAdminArea());
            addressModel.setCountry(address.getCountryName());
            addressModel.setPincode(address.getPostalCode());
            if (address.getFeatureName() != null) addressModel.setName(address.getFeatureName());
            if (address.getPhone() != null) addressModel.setPhone(address.getPhone());
            addressModel.setAddress(completeAddress);
        }
        return addressModel;
    }

    public static void setMargin() {
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT
        );
        /*params.setMargins(left, top, right, bottom);
        yourbutton.setLayoutParams(params);*/
    }

    /*public static KeyValueListModel getBannerDetails(Context context){
        String profileString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_BANNER);
        if(profileString!=null){
            KeyValueListModel model = new KeyValueListModel();
            model.toObject(profileString);
            return model;
        }
        return null;
    }

    public static void setCategoryDetails(Context context, KeyValueListModel model){
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_CATEGORIES, model.toString());
    }

    public static void removeCategoryDetails(Context context){
        removeSharedPreferenceKey(context, GlobalVariables.SHARED_PREFERENCE_CATEGORIES);
    }

    public static KeyValueListModel getCategoryDetails(Context context){
        String profileString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_CATEGORIES);
        if(profileString!=null){
            KeyValueListModel model = new KeyValueListModel();
            model.toObject(profileString);
            return model;
        }
        return null;
    }

    public static void setBannerDetails(Context context, KeyValueListModel model){
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_BANNER, model.toString());
    }

    public static void removeBannerDetails(Context context){
        removeSharedPreferenceKey(context, GlobalVariables.SHARED_PREFERENCE_BANNER);
    }
*/
    public static Bitmap getProfilePicture(Context context, String folder, String fileName) {
        File file = getProfilePicturePath(context, folder, fileName);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
        return bitmap;
    }

    public static void deleteProfilePicture(Context context, String folder, String fileName) {
        File file = getProfilePicturePath(context, folder, fileName);
        if (file.exists()) {
            file.delete();
        }
    }

    public static File getProfilePicturePath(Context context, String folder, String fileName) {
        File file = new File(context.getFilesDir(), folder);
        if (!file.exists()) {
            file.mkdir();
        }
        file = new File(file, fileName);
        return file;
    }

    public static File getDocumentPath(Context context, String folder, String fileName) {
        File file = new File(context.getFilesDir(), folder);
        if (!file.exists()) {
            file.mkdir();
        }
        file = new File(file, fileName);
        return file;
    }

    public static File getDocumentPath(Context context, String folder) {
        File file = new File(context.getFilesDir(), folder);
        if (!file.exists()) {
            file.mkdir();
        }
        //file = new File(file,fileName);
        return file;
    }

    public static int convertDPtoPixels(Context context, int sizeInDp) {
        float scale = context.getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (sizeInDp * scale + 0.5f);
        return dpAsPixels;
    }

    public static File getExternalStorageDirectory(Context context) {
        File returnFile = null;
        if (Environment.isExternalStorageEmulated()) {
            File CardPathFolder = new File(Environment.getExternalStorageDirectory().toString());//(System.getenv("SECONDARY_STORAGE"));
            if (CardPathFolder.getFreeSpace() > 0) {
                returnFile = CardPathFolder;
            }
        } else {
            returnFile = new File(Environment.getExternalStorageDirectory().toString());
        }
        return returnFile;
    }

    public static File getInternalStorageDirectory() {
        File returnFile = null;
        if (!Environment.isExternalStorageEmulated()) {
            File internalFolder = new File(Environment.getDataDirectory().toString());
            returnFile = internalFolder;
        } else {
            returnFile = new File(Environment.getExternalStorageDirectory().toString());
        }
        return returnFile;
    }

    public Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            return contentUri.getPath();
        }
    }

    public static File getImageFilePath(Context context, String fileName) {
        File imageDirectory;

        String fileDirectory = null;
        imageDirectory = getExternalStorageDirectory(context);
        if (imageDirectory == null) {
            fileDirectory = getInternalStorageDirectory().toString();
        } else {
            fileDirectory = getExternalStorageDirectory(context).toString();
        }
        fileDirectory += "/" + context.getResources().getString(R.string.app_name) + "/Images";

        imageDirectory = new File(fileDirectory);

        if (!imageDirectory.exists()) {
            imageDirectory.mkdirs();
        }
        File outputFile = new File(imageDirectory, fileName);
        Log.d(TAG, "SDCARD : " + outputFile.toString());

        return outputFile;
    }

    public static String getDevice() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        int version = Build.VERSION.SDK_INT;
        String versionRelease = Build.VERSION.RELEASE;

        return versionRelease + "(" + version + ")," + manufacturer + "(" + model + ")";
    }

    public static void deleteTempImages(Context context) {
        String path = getExternalStorageDirectory(context).toString() + "/" + context.getResources().getString(R.string.app_name) + "/Images";
        String path1 = getInternalStorageDirectory().toString() + "/" + context.getResources().getString(R.string.app_name) + "/Images";

        File dir = new File(path);

        if (dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (int i = 0; i < children.length; i++) {
                    new File(dir, children[i]).delete();
                }
            }
        }

        dir = new File(path1);
        if (dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (int i = 0; i < children.length; i++) {
                    new File(dir, children[i]).delete();
                }
            }
        }
    }

    public static String getUniqueID(Context context) {
        String uuid = getSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_GUID);
        if (uuid == null) {
            setSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_GUID, uniqueID);
            uuid = uniqueID;
        }
        Log.d(TAG, "UUID : " + uuid);
        return uuid;
    }


    public static void removeSharedPreferenceAll(Context context) {
        sharedPreferences = context.getSharedPreferences(globalVariables.SHARED_PREFERENCE_KEY, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    /* public static void logoutSocilNetworks(){
         if(LoginManager.getInstance()!=null){
             LoginManager.getInstance().logOut();
         }
         if(LoginMainActivity.mGoogleApiClient!=null){
             if(LoginMainActivity.mGoogleApiClient.isConnected()) {
                 Auth.GoogleSignInApi.signOut(LoginMainActivity.mGoogleApiClient).setResultCallback(
                         new ResultCallback<Status>() {
                             @Override
                             public void onResult(Status status) {
                                 LoginMainActivity.mGoogleApiClient.disconnect();
                             }
                         });
             }
         }
     }*/
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getToolBarHeight(Context context) {
        int result = 0;
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            result = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        return result;
    }

    public static void setTranslucentStatusFlag(Window window, boolean on) {
        if (Build.VERSION.SDK_INT >= 19) {

            WindowManager.LayoutParams winParams = window.getAttributes();
            final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
            if (on) {
                winParams.flags |= bits;
            } else {
                winParams.flags &= ~bits;
            }
            window.setAttributes(winParams);
        }
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

/*
    public static boolean isEmailValid(String email){
        String emailPattern = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        String emailPattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
        if (email.matches(emailPattern)) { return true;}
        else if (email.matches(emailPattern2)) { return true;}
        else { return false;}
    }
*/


    public static boolean isPasswordValid(String password) {
        PasswordValidator validator = new PasswordValidator();
        if (validator.validate(password)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isAllAlpha(String name) {
        return ((name != null)
                && (!name.equals(""))
                && (name.matches("^[a-zA-Z]*$")));
    }

    public final static boolean isPhoneNumberValid(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            if (target.length() < 10 || target.length() > 10) {
                return false;
            } else {
                return Patterns.PHONE.matcher(target).matches();
            }
        }
    }

    public static boolean isShowingProgress() {
        boolean showProgress = false;
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                showProgress = true;
            }
        }
        return showProgress;
    }

    public static void hideProgress() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }

    public static void showProgress(Context context, @Nullable String msg) {
        if (context != null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(msg);
            progressDialog.setIsCancelable(false);
            progressDialog.show();
        }
    }

    public RotateAnimation rotateAnimation(float degree, int duration) {
        final RotateAnimation rotateAnim = new RotateAnimation(0.0f, degree,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotateAnim.setDuration(duration);
        rotateAnim.setFillAfter(true);
        return rotateAnim;
    }

    public static void setSharedPreferenceString(Context context, String key, String value) {
        sharedPreferences = context.getSharedPreferences(globalVariables.SHARED_PREFERENCE_KEY, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void setSharedPreferenceLong(Context context, String key, long value) {
        sharedPreferences = context.getSharedPreferences(globalVariables.SHARED_PREFERENCE_KEY, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public static void removeSharedPreferenceKey(Context context, String key) {
        sharedPreferences = context.getSharedPreferences(globalVariables.SHARED_PREFERENCE_KEY, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    public static String getSharedPreferenceString(Context context, String key) {
        String returnString = null;
        if (context == null) {
            context = MainActivity.mainContext;
        }
        sharedPreferences = context.getSharedPreferences(globalVariables.SHARED_PREFERENCE_KEY, Activity.MODE_PRIVATE);
        if (sharedPreferences.contains(key)) {
            returnString = sharedPreferences.getString(key, "");
        }
        return returnString;
    }

    public static long getSharedPreferenceLong(Context context, String key) {
        long returnLong = 0;
        if (context == null) {
            context = MainActivity.mainContext;
        }
        sharedPreferences = context.getSharedPreferences(globalVariables.SHARED_PREFERENCE_KEY, Activity.MODE_PRIVATE);
        if (sharedPreferences.contains(key)) {
            returnLong = sharedPreferences.getLong(key, 0);
        }
        return returnLong;
    }

    public static void setHomeData(Context context, HomeIndexModel homeData) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_HOME, homeData.toString());
    }

    public static void removeHomeData(Context context) {
        removeSharedPreferenceKey(context, GlobalVariables.SHARED_PREFERENCE_HOME);
    }

    public static HomeIndexModel getHomeData(Context context) {
        HomeIndexModel homeDataModel = new HomeIndexModel();
        String homeDataString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_HOME);
        if (homeDataString != null) {
            homeDataModel.toObject(homeDataString);
        }
        return homeDataModel;
    }

    private static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }


    public static boolean isTodayDate(Context context, String date) {
        boolean result = false;
        SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_MONTH_NEW);
        Date tmpDate = new Date();
        try {
            tmpDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(tmpDate.getTime());
        Calendar now = Calendar.getInstance();

        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            result = true;
            return result;
        } else {
            result = false;
        }

        return result;
    }

    public static String getFormattedDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_MONTH_NEW);
        Date tmpDate = new Date();
        try {
            tmpDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(tmpDate.getTime());
        /*try {
            smsTime.setTime(format.parse(date));

        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        Calendar now = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("h:mm aa");
        DateFormat dateTimeFormat = new SimpleDateFormat("dd MMM yyyy");

        final long HOURS = 60 * 60 * 60;
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
//            return "Today " + dateFormat.format(smsTime);
            return "Today ";
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
//            return "Yesterday " + dateFormat.format(smsTime);
            return "Yesterday ";
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            return dateTimeFormat.format(smsTime) + "";
        } else {
//            return DateFormat.format("MMMM dd yyyy, h:mm aa", smsTime).toString();
            return dateTimeFormat.format(smsTime) + "";
        }
    }

    public static String getTimeAgo(Date date) {

        long time = date.getTime();
        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = currentDate().getTime();
        if (time > now || time <= 0) {
            return "in the future";
        }

        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "moments ago";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 60 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 2 * HOUR_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

    public static String getlongtoago(long createdAt) {
        DateFormat userDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        DateFormat dateFormatNeeded = new SimpleDateFormat("MM/dd/yyyy HH:MM:SS");
        Date date = null;
        date = new Date(createdAt);
        String crdate1 = dateFormatNeeded.format(date);

        // Date Calculation
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        crdate1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(date);

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        String currenttime = dateFormat.format(cal.getTime());

        Date CreatedAt = null;
        Date current = null;
        try {
            CreatedAt = dateFormat.parse(crdate1);
            current = dateFormat.parse(currenttime);
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        long diff = current.getTime() - CreatedAt.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        String time = null;
        if (diffDays > 0) {
            if (diffDays == 1) {
                time = diffDays + "day ago ";
            } else {
                time = diffDays + "days ago ";
            }
        } else {
            if (diffHours > 0) {
                if (diffHours == 1) {
                    time = diffHours + "hr ago";
                } else {
                    time = diffHours + "hrs ago";
                }
            } else {
                if (diffMinutes > 0) {
                    if (diffMinutes == 1) {
                        time = diffMinutes + "min ago";
                    } else {
                        time = diffMinutes + "mins ago";
                    }
                } else {
                    if (diffSeconds > 0) {
                        time = diffSeconds + "secs ago";
                    }
                }

            }

        }
        return time;
    }

/*
    public static void setSelectedCity(Context context, KeyValueModel keyValueModel){
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_SELECTED_CITY, keyValueModel.toString());
    }

    public static void removeSelectedCity(Context context){
        removeSharedPreferenceKey(context, GlobalVariables.SHARED_PREFERENCE_SELECTED_CITY);
    }

    public static KeyValueModel getSelectedCity(Context context){
        KeyValueModel keyValueModel = new KeyValueModel();
        String cityString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_SELECTED_CITY);
        if(cityString!=null){keyValueModel.toObject(cityString);}
        return keyValueModel;
    }

    public static int getSelectedCityID(Context context){
        KeyValueModel keyValueModel = getSelectedCity(context);
        if(keyValueModel.getId()!=null){return Integer.parseInt(keyValueModel.getId());}
        return -1;
    }

    public static String getSelectedCityName(Context context){
        KeyValueModel keyValueModel = getSelectedCity(context);
        return keyValueModel.getName();
    }
*/


    public static KeyValueListModel getBannerDetails(Context context) {
        String profileString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_BANNER);
        if (profileString != null) {
            KeyValueListModel model = new KeyValueListModel();
            model.toObject(profileString);
            return model;
        }
        return null;
    }

    public static void setCategoryDetails(Context context, KeyValueListModel model) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_CATEGORIES, model.toString());
    }

    public static void removeCategoryDetails(Context context) {
        removeSharedPreferenceKey(context, GlobalVariables.SHARED_PREFERENCE_CATEGORIES);
    }

    public static KeyValueListModel getCategoryDetails(Context context) {
        String profileString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_CATEGORIES);
        if (profileString != null) {
            KeyValueListModel model = new KeyValueListModel();
            model.toObject(profileString);
            return model;
        }
        return null;
    }

    public static void setBannerDetails(Context context, KeyValueListModel model) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_BANNER, model.toString());
    }

    public static void removeBannerDetails(Context context) {
        removeSharedPreferenceKey(context, GlobalVariables.SHARED_PREFERENCE_BANNER);
    }

    public static void setToken(Context context, String token) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_TOKEN, token);
    }

    public static String getToken(Context context) {
        return getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_TOKEN);
    }

    public static Map<String, String> getTokenHeader(Context context) {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("X-CSRF-Token", getSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_TOKEN));
        headers.put("Content-Type", "application/json; charset=utf-8");
        headers.put("Cache-Control", "no-cache");
        headers.put("Cookie", getSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_COOKIE));
        return headers;
    }

    public static int getOfferPercentage(Double oldRate, Double newRate) {
        int offerPercentage = 0;
        if (oldRate > newRate) {
            Double percentageValue = ((oldRate - newRate) / oldRate) * 100;
            offerPercentage = percentageValue.intValue();
        }
        return offerPercentage;
    }

    public static String getUserID(Context context) {
        String userID = getSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_USERID);
        return userID;
    }

    public static String getUserToken(Context context) {
        String userToken = getSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_GCM_TOKEN);
        return userToken;
    }


    public static String getWishlistORCartID(Context context) {
        String id = getToken(context);
        if (id == null || id.equalsIgnoreCase("null")) {
            id = getUniqueID(context);
        }
        return id;
    }

    public static String getAuthenticationDateString(Context context, boolean isEncryption) {
        String dateTimeString;
        if (!isEncryption) {
            dateTimeString = getDatefromTimestamp(getCurrentTimestamp(context), globalVariables.AUTHENTICATION_DATE_FORMAT, Locale.ENGLISH) + getTimefromTimestamp(getCurrentTimestamp(context), globalVariables.AUTHENTICATION_TIME_FORMAT, Locale.ENGLISH);
        } else {
            dateTimeString = getUTCTime(context, Locale.ENGLISH);
        }
        return dateTimeString;
    }

    public static String getAuthenticationUsername() {
        return globalVariables.AUTHENTICATION_USERNAME;
    }

    public static boolean isGCM_Regestered(Context context) {
        if (getUserToken(context) == null) {
            return false;
        } else {
            return true;
        }
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }


    public static void setNetworkConnectionStatus(Context context, boolean status) {

        setSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_NETWORK_CONNECTION_STATUS, status ? "1" : "0");

    }

    public static Boolean getNetworkConnectionStatus(Context context) {
        String networkConnectionStatusValue = null;
        try {
            networkConnectionStatusValue = getSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_NETWORK_CONNECTION_STATUS);
            if (networkConnectionStatusValue != null) {
                return networkConnectionStatusValue.equalsIgnoreCase("1") ? true : false;
            }
        } catch (Exception ex) {
        }
        return false;
    }

    public static void setTimeDifference(Context context, long timestamp) {
        long systemTime = System.currentTimeMillis() / 1000;
        long diff = systemTime - timestamp;
        Log.d("Time Difference", "SystemTime : " + systemTime + "");
        Log.d("Time Difference", "TimeStamp : " + timestamp + "");
        Log.d("Time Difference", "DiffTime : " + diff + "");
        setSharedPreferenceLong(context, globalVariables.SHARED_PREFERENCE_TIME_DIFFERENCE, diff);
    }

    public static String getUTCTime(Context context, Locale locale) {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat(globalVariables.ENCRYPTION_TIME_FORMAT_UTC, locale);
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormatGmt.format(getUTCCalender(context));
    }

    /*public static Intent getNotificationIntent(Context context, JSONObject jsonObject){
        Intent intent = new Intent(context, NotificationListFragment.class);

        QuotationMainModel quotationMainModel = new QuotationMainModel();
        OrderModel orderModel = new OrderModel();

        if(quotationMainModel.toObject(jsonObject.toString())){
            if(getUserType(context)== GlobalVariables.USER_TYPE.VENDOR){
                intent = VendorQuotationsTabsActivity.newInstance(context, quotationMainModel);
            }else{
                intent = QuotationsTabsActivity.newInstance(context, quotationMainModel);
            }
        }else if(orderModel.toObject(jsonObject.toString())){
            intent = OrderDetailActivity.newInstance(context, orderModel);
        }

        return intent;
    }*/

    public static Date getDateObject(String dateStr, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.ENGLISH);
        // DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static long getUTCTimeStamp(Context context) {
        return getUTCCalender(context).getTime();
    }

    public static boolean CompareDate(String startDateStr, String endDatestr, String pattern) {
        boolean isStartDateLesser = false;
        if (startDateStr != null && endDatestr != null) {
            Date date1 = GlobalFunctions.getDateObject(startDateStr, pattern);
            Date date2 = GlobalFunctions.getDateObject(endDatestr, pattern);

            if (date1.before(date2)) {
                isStartDateLesser = true;
                System.out.println("Date1" + date1 + " is before Date2" + date2);
            }

        }
        return isStartDateLesser;
    }

    public static long getUTCTimeStampInSec(Context context) {
        long timeStamp = (getUTCCalender(context).getTime()) / 1000;
        return timeStamp;
    }

    public static int[] getRGBColors(String rawData) {
        String subStr = null;
        int[] rgbInts = new int[1];
        try {
            subStr = rawData.substring(rawData.indexOf("(") + 1, rawData.indexOf(")"));
            final String[] strings = subStr.split(",");
            rgbInts = new int[strings.length];
            for (int i = 0; i < strings.length; i++) {
                rgbInts[i] = Integer.parseInt(strings[i]);
            }

        } catch (Exception ex) {
            rgbInts = new int[1];
        }

        return rgbInts;
    }

    public static Date getUTCCalender(Context context) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(getCurrentTimestamp(context) * 1000);
        Log.d(TAG, "CurrentTimestamp elapsed: " + SystemClock.elapsedRealtime());
        return calendar.getTime();
    }

    public static String getAuthenticationPassword(Context context, String serviceURL) {
        //  String textToConvert = serviceURL + "|" + GlobalVariables.AUTHENTICATION_USERNAME + "|" + GlobalVariables.AUTHENTICATION_PASSWORD + "|" + getAuthenticationDateString(context, true);
        String textToConvert = serviceURL + "|" + GlobalVariables.AUTHENTICATION_USERNAME + "|" + GlobalVariables.AUTHENTICATION_PASSWORD;
        Log.d(TAG, "Text to Convert : " + textToConvert);
        byte[] sha1hash = new byte[40];
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(textToConvert.getBytes("UTF-8"), 0, textToConvert.length());
            sha1hash = md.digest();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return convertToHex(sha1hash);
    }

    public static String md5(String s) {
        try {
            // Create MD5 Hash
            Log.d(TAG, s);
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            return convertToHex(messageDigest);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getAutherization(Context context, String serviceURL) {
        String token = getToken(context);
        Log.d(TAG, "Token 1 :" + token);
        if (token == null) {
            token = getUniqueID(context);
        }
        Log.d(TAG, "Token 2 :" + token);
        String authorization = getAuthenticationUsername() + " " + getAuthenticationPassword(context, serviceURL) + " " + getAuthenticationDateString(context, true) + " " + token;
        return authorization;
    }

    public static void setCartCount(Context context, int count) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_CART_COUNT, count + "");
        //MainActivity.setCartCount(count);
        //ProductDetail.setCartCount(count);
    }

    public static void setMyNotificationCount(Context context, int count) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_MY_NOTIFICATION_COUNT, count + "");
        MainActivity.setNotificationCount(count);
    }

    public static int getMyNotificationCount(Context context) {
        String countString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_MY_NOTIFICATION_COUNT);
        if (countString == null) {
            return 0;
        }
        return Integer.parseInt(countString);
    }

    public void setNotificationSettings(Context context, NotificationSettingsModel notificationSettingsModel) {
        setSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_NOTIFICATION_SETTINGS, notificationSettingsModel.toString());
    }

    public NotificationSettingsModel getNotificationSettings(Context context) {
        String notificationSettingsString = getSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_NOTIFICATION_SETTINGS);
        NotificationSettingsModel notificationSettingsModel = new NotificationSettingsModel();
        if (notificationSettingsString != null) {
            notificationSettingsModel.toObject(notificationSettingsString);
        }
        return notificationSettingsModel;
    }


    public static void setNotification(Context context, boolean isNotify) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_NOTIFICATION_COUNT, isNotify ? "1" : "0");
        // MainActivity.setNotification(isNotify);
        //ProductDetail.setNotification(isNotify);
    }

    public static boolean getNotificationCount(Context context) {
        String countString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_NOTIFICATION_COUNT);
        if (countString == null) {
            return false;
        }
        return countString.equals("0") ? false : true;
    }

    public static int getCartCount(Context context) {
        String countString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_CART_COUNT);
        if (countString == null) {
            return 0;
        }
        return Integer.parseInt(countString);
    }

    public static void setProfile(Context context, ProfileModel profileModel) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_PROFILE, profileModel.toString());
    }

    public static ProfileModel getProfile(Context context) {
        String profileString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_PROFILE);
        if (profileString != null) {
            ProfileModel model = new ProfileModel();
            model.toObject(profileString);
            return model;
        }
        return null;
    }

    public static void setUserType(Context context, GlobalVariables.USER_TYPE user_type) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_USER_TYPE, user_type.toString());
    }

    public static GlobalVariables.USER_TYPE getUserType(Context context) {
        String typeString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_USER_TYPE);
        if (typeString != null) {
            if (typeString.equals(GlobalVariables.USER_TYPE.USER.toString())) {
                return GlobalVariables.USER_TYPE.USER;
            }
            return GlobalVariables.USER_TYPE.VENDOR;
        }
        return GlobalVariables.USER_TYPE.NONE;
    }

    public static void setBidTiming(Context context) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_BID_CHECKER_TIMESTAMP, getCurrentTimestamp(context) + "");
    }

    public static long getBidTiming(Context context) {
        String typeString = getSharedPreferenceString(context, GlobalVariables.SHARED_BID_CHECKER_TIMESTAMP);
        try {
            return Long.parseLong(typeString);
        } catch (Exception ex) {
            return 0;
        }
    }

    public static void setSelectedCity(Context context, String selectedCity) {

        selectedCity = selectedCity != null ? selectedCity : null;
        setSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_SELECTED_CITY, selectedCity);
    }

    public static void setSelectedCountry(Context context, String selectedCountry) {

        selectedCountry = selectedCountry != null ? selectedCountry : null;
        setSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_SELECTED_COUNTRY, selectedCountry);
    }

    public static String getSelectedCity(Context context) {
        String selectedCity = getSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_SELECTED_CITY);
        if (selectedCity == null) {
            return null;
        }
        String mySelectedCity = selectedCity;

        return mySelectedCity == null ? null : mySelectedCity;
    }

    public static String getSelectedCountry(Context context) {
        String selectedCountry = getSharedPreferenceString(context, globalVariables.SHARED_PREFERENCE_SELECTED_COUNTRY);
        if (selectedCountry == null) {
            return null;
        }
        String mySelectedCountry = selectedCountry;

        return mySelectedCountry == null ? null : mySelectedCountry;
    }

    public static boolean isNotNullValue(String value) {
        boolean result = false;
        if (value != null && !value.isEmpty() && !value.equals("null") && !value.isEmpty() && value.trim()
                .length() > 0) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public static boolean isNotZeroValue(String value) {
        boolean result = false;
        try {
            double temp = Double.parseDouble(value);
            if (temp > 0) {
                result = true;
            } else {
                result = false;
            }
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public static boolean isNotNullValueNew(String value) {
        boolean result = false;
        if (value == null || value.isEmpty() || value.equals("null") || value.equals("") || value.isEmpty() || value.trim()
                .length() == 0) {
            result = false;
        } else {
            result = true;
        }
        return result;
    }

    public static boolean isLastPosition(int position, List<OrderStatusModel> list) {
        boolean result = false;
        try {

            if (position == list.size() - 1) {
                return  true;
            }else {
                OrderStatusModel orderDetailStatusModel=list.get(position + 1);
                if (GlobalFunctions.isNotNullValue(orderDetailStatusModel.getCreatedOn())){
                    return true;
                }else {
                    return  false;
                }
            }
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public static void setLanguage(Context context, GlobalVariables.LANGUAGE language) {
        setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE, language.toString());
    }

    public static GlobalVariables.LANGUAGE getLanguage(Context context) {
        String langString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE);
        GlobalVariables.LANGUAGE returnLanguage = GlobalVariables.LANGUAGE.ARABIC;
        if (langString != null) {
            if (langString.equalsIgnoreCase(GlobalVariables.LANGUAGE.ARABIC.toString())) {
                returnLanguage = GlobalVariables.LANGUAGE.ARABIC;
            } else if (langString.equalsIgnoreCase(GlobalVariables.LANGUAGE.URDU.toString())) {
                returnLanguage = GlobalVariables.LANGUAGE.URDU;
            } else {
                returnLanguage = GlobalVariables.LANGUAGE.ENGLISH;
            }
//            returnLanguage = langString.equalsIgnoreCase(GlobalVariables.LANGUAGE.ARABIC.toString()) ? GlobalVariables.LANGUAGE.ARABIC : GlobalVariables.LANGUAGE.ENGLISH;
        }
        return returnLanguage;
    }

    public static int getRandomNumber(int StartNumber, int EndNumber) {
        int random = 0;
        Random randomClass = new Random();
        random = StartNumber + randomClass.nextInt(EndNumber);
        return random;
    }

    public static Map<String, String> getHeader(Context context, String serviceURL) {
        Map<String, String> headers = new HashMap<String, String>();
        String authorization = getAuthenticationPassword(context, serviceURL);
        Log.d(TAG, "Authorization : " + authorization);
        headers.put(GlobalVariables.CONTENT_TYPE, GlobalVariables.CONTENT_TYPE_VALUE);
        headers.put(GlobalVariables.AUTHORIZATION, authorization);
        headers.put(GlobalVariables.ACCEPT_LANGUAGE, getLanguage(context).toString());
        headers.put("Cache-Control", "no-cache");
        headers.put("app", "1");
        // headers.put("authentication_token", getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_TOKEN));
        headers.put("Token", getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_TOKEN));
        //headers.put("Cookie", getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_COOKIE));
        Log.d(TAG, "Header : " + headers);
        return headers;
    }

    public static Map<String, String> postHeader(Context context, String serviceURL) {
        Map<String, String> headers = new HashMap<String, String>();
        String authorization = getAuthenticationPassword(context, serviceURL);
        Log.d(TAG, "Authorization : " + authorization);
        headers.put(GlobalVariables.CONTENT_TYPE, GlobalVariables.CONTENT_TYPE_VALUE);
        headers.put(GlobalVariables.AUTHORIZATION, authorization);
        headers.put(GlobalVariables.ACCEPT_LANGUAGE, getLanguage(context).toString());
        headers.put("Cache-Control", "no-cache");
        headers.put("app", "1");
        //headers.put("authentication_token", getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_TOKEN));
        headers.put("Token", getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_TOKEN));
        // headers.put("Cookie", getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_COOKIE));
//        headers.put("Header", headers.toString());
        Log.d(TAG, "Header : " + headers);
        return headers;
    }

    public static boolean isLoggedIn(Context context) {
        ProfileModel model = getProfile(context);
        return model != null ? true : false;
    }

    public static int[] convertTexttoDate(String dateString) {
        int[] dateArray = {0, 0, 0};
        if (!dateString.isEmpty() && dateString != null) {
            String[] splitString = dateString.split("-");
            if (splitString.length == 3) {
                dateArray[0] = Integer.parseInt(splitString[0]);
                dateArray[1] = Integer.parseInt(splitString[1]);
                dateArray[2] = Integer.parseInt(splitString[2]);
            }
        }
        return dateArray;
    }

    /*public static void refreshAfterLogout(Context context){
        closeAllActivities();
        NavigationDrawerFragment.navigationHeaderRefresh(getProfile(context));
        Fragment homeFragment = new HomeFragment();
        MainActivity.replaceFragment(homeFragment, HomeFragment.TAG, context.getString(R.string.home), R.drawable.ic_title_home, 0);
    }*/

    public static void closeAllActivities() {
        LoginActivity.closeThisActivity();
        LoginWithPasswordActivity.closeThisActivity();
        LoginWithPhoneNumberActivity.closeThisActivity();
        OtpActivity.closeThisActivity();
        FullImageMainActivity.closeThisActivity();
        SearchPlaceOnMapActivity.closeThisActivity();
        CompanyRegistrationActivity.closeThisActivity();
        CompanyRegistrationUploadDocumentsActivity.closeThisActivity();
        CompanyReviewApplicationActivity.closeThisActivity();
        CityListActivity.closeThisActivity();
        IndividualRegistrationActivity.closeThisActivity();
        IndividualRegistrationUploadDocumentsActivity.closeThisActivity();
        IndividualRegistrationUploadDocumentsActivity.closeThisActivity();
        YearListActivity.closeThisActivity();
        RegisterActivity.closeThisActivity();
        RegistrationSuccessActivity.closeThisActivity();
        SelectServicesActivity.closeThisActivity();
        EditServicesActivity.closeThisActivity();
        TermsAndConditionsActivity.closeThisActivity();
        EditProfileActivity.closeThisActivity();
        RequestDetailsActivity.closeThisActivity();
        CompletedRequestsActivity.closeThisActivity();
        BankDetailsActivity.closeThisActivity();
        EditBankDetailsActivity.closeThisActivity();
        RequestDetailsActivity.closeThisActivity();
        MyWalletActivity.closeThisActivity();
        MyRatingsActivity.closeThisActivity();
        AddPartsActivity.closeThisActivity();
        SubCategoryListActivity.closeThisActivity();
        TimeListActivity.closeThisActivity();
        ReUploadDocumentsActivity.closeThisActivity();
        NotificationActivity.closeThisActivity();
    }

    public static void restartEntireApp(Context mainContext, boolean isLanguageChange) {
        if (isLanguageChange) {
            SharedPreferences shared_preference = PreferenceManager.getDefaultSharedPreferences(mainContext
                    .getApplicationContext());

            String mCustomerLanguage = shared_preference.getString(
                    globalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE, "null");
            String mCurrentlanguage;
            if ((mCustomerLanguage.equalsIgnoreCase("en"))) {
                setLanguage(mainContext, GlobalVariables.LANGUAGE.ARABIC);

                mCurrentlanguage = "ar";
            } else {
                mCurrentlanguage = "en";
                setLanguage(mainContext, GlobalVariables.LANGUAGE.ENGLISH);

            }
            SharedPreferences.Editor editor = shared_preference.edit();
            editor.putString(globalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE, mCurrentlanguage);
            editor.commit();
        }
        closeAllActivities();

        Intent i = new Intent(mainContext, SplashScreen.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        mainContext.startActivity(i);
        System.exit(0);
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static int getColor(Context context, int resID) {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? context.getColor(resID) : context.getResources().getColor(resID);
    }

    public static boolean isValidWalletAmount(String minAmount, String enterAmount) {
        boolean result = false;
        int tmpminAmount = 0;
        int tmpamount = 0;
        try {
            tmpminAmount = Integer.parseInt(minAmount);
            tmpamount = Integer.parseInt(enterAmount);
            if (tmpamount <= 0) {
                result = false;
            } else if (tmpamount >= tmpminAmount) {
                result = true;
            } else {
                result = false;
            }
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public static boolean isValidAmount(String bidAmount) {
        boolean result = false;
        int amount = 0;
        try {
            amount = Integer.parseInt(bidAmount);
            if (amount <= 0) {
                result = false;
            } else {
                result = true;
            }
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public static void logoutApplication(Context context, boolean isSessionExpired) {
        //remove saved details.
        GlobalVariables.LANGUAGE lang = getLanguage(context);
        removeSharedPreferenceKey(context, GlobalVariables.SHARED_PREFERENCE_PROFILE);
        removeSharedPreferenceAll(context);
        setLanguage(context, lang);

        if (isSessionExpired) {
//            closeAllActivities();
            restartEntireApp(context, false);
        }
    }

    public static String getDateForamt(String date1) {
        String date = null;
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = spf.parse(date1);
            spf = new SimpleDateFormat("dd MMM, EEE yyyy");
            date = spf.format(newDate);
            System.out.println(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getSelectedFormatDate(String date1) {
        String date = null;
        try {
            SimpleDateFormat spf = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_WITH_TIME);
            Date newDate = spf.parse(date1);
            spf = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_NEW);
            date = spf.format(newDate);
            System.out.println(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getSelectedFormatDateNew(String date1) {
        String date = null;
        try {
            SimpleDateFormat spf = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_NEW);
            Date newDate = spf.parse(date1);
            spf = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_WITH_TIME);
            date = spf.format(newDate);
            System.out.println(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getSelectedFormatTime(String date1) {
        String date = null;
        try {
            SimpleDateFormat spf = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_NEW);
            Date newDate = spf.parse(date1);
            spf = new SimpleDateFormat(GlobalVariables.DATE_FORMAT_WITH_TIME);
            date = spf.format(newDate);
            System.out.println(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static void playStoreAction(Context mContext) {
        mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mContext.getString(R.string.playstore_ID))));
    }

    public static void openBrowser(Context mContext, String link) {
        mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
    }

    public static int getSoftButtonsBarHeight(Window window) {
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            window.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            window.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }

    public static void shareLinkwithFriends(Context context, String title, String description, String link) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, title);
            String sAux = "\n" + description + "\n\n";
            if (link != null) {
                sAux = sAux + link + " \n\n";
            }
            ProfileModel profile = getProfile(context);
            if (profile != null) {
//                sAux = sAux + context.getString(R.string.reference_code)+" = "+profile.getReferenceCode();
            }
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            context.startActivity(Intent.createChooser(i, "choose one"));
        } catch (Exception e) {
            Log.e(TAG, "Exception on Sharing : " + e);
        }
    }

   /* public static void reloadCartAndWishCount(String productID, @Nullable String colorCode, int type, int quantity, boolean isSuccess){
        if(type == globalVariables.TYPE_WISHLIST){
            ItemsListMain.changeWishlistIcon(productID, isSuccess);
            WishListActivity.changeDeleteIcon(productID, isSuccess);
        }

        if(type==globalVariables.TYPE_CART&&quantity>0){CartActivity.refreshPrice(productID, colorCode, quantity, isSuccess);}
        else{CartActivity.refreshIcons(productID, colorCode, type, isSuccess);}

        MainActivity.updateCartAndWishlist(type, isSuccess);
        ProductDetail.updateCartAndWishlist(type, isSuccess);
    }*/

    /*Animation Functions*/
    public static Animation inFromRightAnimation() {

        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    public static Animation outToLeftAnimation() {
        Animation outtoLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoLeft.setDuration(500);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;
    }

    public static Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(500);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }


    public static String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static Animation outToRightAnimation() {
        Animation outtoRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoRight.setDuration(500);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;
    }

   /* public static KeyValueListModel getCityDetails(Context context){
        String cityString = getSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_CITY_MODEL);
        if(cityString!=null){
            KeyValueListModel cityModel = new KeyValueListModel();
            cityModel.toObject(cityString);
            return cityModel;
        }
        return null;
    }*/

    public static void callPhone(Activity activity, String number) {
        if (number != null && !number.equalsIgnoreCase("")) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + number));
            activity.startActivity(intent);
        }
    }

    public static void sendMail(Activity activity, String mailId, String subject, String description) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", mailId, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, description);
        try {
            activity.startActivity(Intent.createChooser(emailIntent, activity.getString(R.string.send_mail)));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }

    }

    public static void showOnMap(Context context, String label, String latitude, String longitude) {
        try {
            String uriBegin = "geo:" + latitude + "," + longitude;
            String query = latitude + "," + longitude + "(" + label + ")";
            String encodedQuery = Uri.encode(query);
            String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
            Uri uri = Uri.parse(uriString);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            context.startActivity(intent);
        } catch (Exception e) {

        }

    }

    public static void openMapView(Activity activity, String latitude, String longitude) {
        try {
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + latitude + "," + longitude));
            activity.startActivity(intent);
        } catch (Exception excc) {

        }
    }

    public void openPlayStore(Activity activity) {
        Intent i = new Intent(android.content.Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://play.google.com/store/apps/details?id=quickfix.sa.vendor"));
        activity.startActivity(i);
    }

    public static void startGoogleMapsIntent(Activity activity, String startLoatLng, String destinationLatLng) {
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", startLoatLng, destinationLatLng);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        context.startActivity(intent);

    }

    /*  public static void setCityDetails(Context context, KeyValueListModel cityModel){
          setSharedPreferenceString(context, GlobalVariables.SHARED_PREFERENCE_CITY_MODEL, cityModel.toString());
      }

      public static void removeCityDetails(Context context){
          removeSharedPreferenceKey(context, GlobalVariables.SHARED_PREFERENCE_CITY_MODEL);
      }
  */
    public static void displayMessaage(Context context, View view, String msg) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
                Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                Activity activity = (Activity) context;
                int height = getToolBarHeight(context) + getStatusBarHeight(context);

                final FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) layout.getLayoutParams();
                //params.setMargins(0, height, 0, 0);
                params.gravity = Gravity.TOP;
                layout.setLayoutParams(params);

                TextView textView = (TextView) layout.findViewById(R.id.snackbar_text);
                textView.setMaxLines(5);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(context.getResources().getColor(R.color.ColorAccentLight));
                snackbar.show();
            } catch (Exception ex) {
                Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }
        } else {
            Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }

    }

    public static void displayMessageTop(Context context, View view, String msg) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
                Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                Activity activity = (Activity) context;
                int height = getToolBarHeight(context) + getStatusBarHeight(context);

                final FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) layout.getLayoutParams();
                //params.setMargins(0, height, 0, 0);
                params.gravity = Gravity.TOP;
                layout.setLayoutParams(params);

                TextView textView = (TextView) layout.findViewById(R.id.snackbar_text);
                textView.setMaxLines(5);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(context.getResources().getColor(R.color.ColorAccentLight));
                snackbar.show();
            } catch (Exception ex) {
                Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }
        } else {
            Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }

    }

    public static void displayMessageBottom(Context context, View view, String msg) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
                Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                Activity activity = (Activity) context;
                int height = getToolBarHeight(context) + getStatusBarHeight(context);

                final FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) layout.getLayoutParams();
                //params.setMargins(0, height, 0, 0);
                params.gravity = Gravity.BOTTOM;
                layout.setLayoutParams(params);

                TextView textView = (TextView) layout.findViewById(R.id.snackbar_text);
                textView.setMaxLines(5);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(context.getResources().getColor(R.color.ColorAccentLight));
                snackbar.show();
            } catch (Exception ex) {
                Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }
        } else {
            Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }

    }

    public static void displayErrorDialog(Context context, String message) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_logo_dark);
        alertDialog.setTitle(context.getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static void displayDialog(Context context, int icon, String title, String message) {
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(icon);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(context.getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static void closeKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static boolean isKeyboardOpen(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            if (imm.isAcceptingText()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static boolean isValidShiftList(List<ShiftModel> list) {
        boolean isValid = false;
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ShiftModel shiftModel = list.get(i);
                if (isNotNullValue(shiftModel.getDays())) {
                    isValid=true;
                } else {
                    return false;

                }
            }
        }
        return isValid;
    }

    public static OrderStatusModel getNameFromList(List<OrderStatusModel> list) {
        OrderStatusModel result = null;
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                OrderStatusModel orderStatusModel = list.get(i);
                if (!isNotNullValue(orderStatusModel.getCreatedOn())) {
                    return orderStatusModel;
                }
            }
        }
        return result;
    }

    public static String getUpdatedList(ShiftModel item) {
        String idList = "";
        List<String> updatedList=new ArrayList<>();
        updatedList.clear();

        if (isNotNullValue(item.getMonday())){
            updatedList.add(item.getMonday());
        }
        if (isNotNullValue(item.getTuesDay())){
            updatedList.add(item.getTuesDay());
        }
        if (isNotNullValue(item.getWedDay())){
            updatedList.add(item.getWedDay());
        }
        if (isNotNullValue(item.getThuDay())){
            updatedList.add(item.getThuDay());
        }
        if (isNotNullValue(item.getFriDay())){
            updatedList.add(item.getFriDay());
        }
        if (isNotNullValue(item.getSatDay())){
            updatedList.add(item.getSatDay());
        }
        if (isNotNullValue(item.getSunday())){
            updatedList.add(item.getSunday());
        }

        if (updatedList.size() > 0) {
            for (int i = 0; i < updatedList.size(); i++) {
                if (i == updatedList.size() - 1) {
                    idList += updatedList.get(i);
                } else {
                    idList += updatedList.get(i) + "," + "";
                }
            }
        }

        return idList;
    }

    public static String getStringFromList(List<String> list) {
        String idList = "";
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1) {
                    idList += list.get(i);
                } else {
                    idList += list.get(i) + "," + "";
                }
            }
        }
        return idList;
    }

    public static String getIntegerFromList(List<Integer> list) {
        String idList = "";
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1) {
                    idList += list.get(i);
                } else {
                    idList += list.get(i) + ",";
                }
            }
        }
        return idList;
    }

    public static ArrayList<Integer> getListFromInteger(String list) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        try {
            if (list != null) {
                String str[] = list.split(",");
                for (int i = 0; i < str.length; i++) {
                    String value = str[i];
                    int tmpValue = 0;
                    tmpValue = Integer.parseInt(value);
                    arrayList.add(tmpValue);
                }
            }
        } catch (Exception e) {
            arrayList = new ArrayList<>();
        }

        return arrayList;
    }

    public static ArrayList<String> getListFromString(String list) {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            if (list != null) {
                String str[] = list.split(",");
                for (int i = 0; i < str.length; i++) {
                    String value = str[i];
                    arrayList.add(value);
                }
            }
        } catch (Exception e) {
            arrayList = new ArrayList<>();
        }

        return arrayList;
    }

   /* public Spanned getHTMLString(String unFormatedString) {
        if (Build.VERSION.SDK_INT >= 24) {
            return Html.fromHtml(unFormatedString, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(unFormatedString);
        }
    }*/

    public static Spanned getHTMLString(String unFormatedString) {
        if (Build.VERSION.SDK_INT >= 24) {
            return Html.fromHtml(unFormatedString, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(unFormatedString);
        }
    }

    public static String html2text(String html) {
        return Jsoup.parse(html).text();
    }

    public static Spanned getHtmlString(String html) {
        if (!TextUtils.isEmpty(html)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT);
            } else {
                return Html.fromHtml(html);
            }
        }
        return null;
    }
}
