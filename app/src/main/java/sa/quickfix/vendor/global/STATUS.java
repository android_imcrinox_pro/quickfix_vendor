package sa.quickfix.vendor.global;


import java.util.HashMap;
import java.util.Map;

import sa.quickfix.vendor.R;

public class STATUS {

    public static final String TAG = "STATUS";

    public enum VALIDATION {
        NEW(""),
        NOTHING("NOTHING"),
        LOGIN_FAILED("LOGIN_FAILED"),
        LOGIN_SUCCESS("LOGIN_SUCCESS"),
        REGISTER_FAILED("REGISTER_FAILED"),
        REGISTER_SUCCESS("REGISTER_SUCCESS"),
        OTP_FAILED("OTP_FAILED"),
        OTP_SUCCESS("OTP_SUCCESS"),
        NEED_VERIFICATION("NEED_VERIFICATION");

        private enum CODE {
            VALIDATION_NOTHING("600"),
            CODE_LOGIN_FAILED("601"),
            CODE_LOGIN_SUCCESS("602"),
            CODE_REGISTER_FAILED("603"),
            CODE_REGISTER_SUCCESS("604"),
            CODE_OTP_FAILED("605"),
            CODE_OTP_SUCCESS("606"),
            CODE_NEED_VERIFICATION("607");

            private final String code;

            private CODE(final String code) {
                this.code = code;
            }

            @Override
            public String toString() {
                return code;
            }

            public String toCode(){return this.code;}

            private static final Map<String, CODE> map = new HashMap<>();
            static {
                for (CODE en : values()) {
                    map.put(en.code, en);
                }
            }

            public static CODE valueFor(String name) {
                return map.get(name);
            }
        }

        private final String text;
        private VALIDATION(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public String getCode(){
            if(this.text.equals(LOGIN_SUCCESS.toString())){return CODE.CODE_LOGIN_SUCCESS.toCode();}
            else if(this.text.equals(LOGIN_FAILED.toString())){return CODE.CODE_LOGIN_FAILED.toCode();}
            else if(this.text.equals(REGISTER_FAILED.toString())){return CODE.CODE_REGISTER_FAILED.toCode();}
            else if(this.text.equals(REGISTER_SUCCESS.toString())){return CODE.CODE_REGISTER_SUCCESS.toCode();}
            else if(this.text.equals(OTP_FAILED.toString())){return CODE.CODE_OTP_FAILED.toCode();}
            else if(this.text.equals(OTP_SUCCESS.toString())){return CODE.CODE_OTP_SUCCESS.toCode();}
            else if(this.text.equals(NEED_VERIFICATION.toString())){return CODE.CODE_NEED_VERIFICATION.toCode();}
            else {return CODE.VALIDATION_NOTHING.toCode();}
        }

        public VALIDATION getValidation(String code){
            CODE codeEnum = CODE.VALIDATION_NOTHING;
            try{
                codeEnum = CODE.valueFor(code);
            }catch (Exception ex){
                codeEnum = CODE.VALIDATION_NOTHING;
            }
            if(codeEnum!=null){
                switch (codeEnum){
                    case CODE_LOGIN_FAILED:
                        return LOGIN_FAILED;
                    case CODE_LOGIN_SUCCESS:
                        return LOGIN_SUCCESS;
                    case CODE_REGISTER_FAILED:
                        return REGISTER_FAILED;
                    case CODE_REGISTER_SUCCESS:
                        return REGISTER_SUCCESS;
                    case CODE_OTP_FAILED:
                        return OTP_FAILED;
                    case CODE_OTP_SUCCESS:
                        return OTP_SUCCESS;
                    case CODE_NEED_VERIFICATION:MPLETED:
                        return NEED_VERIFICATION;
                    default :
                        return NOTHING;
                }
            }else{
                return NOTHING;
            }
        }
    }

    public enum QUESTION_TYPE {
        NEW(""),
        TEAM_SELECTION("TEAM_SELECTION"),
        PLAYER_SELECTION("PLAYER_SELECTION"),
        CUSTOMIZED("CUSTOMIZED"),
        SCORE_VALUE("SCORE_VALUE");

        private enum CODE {
            CODE_NEW("0"),
            CODE_TEAM_SELECTION("1"),
            CODE_PLAYER_SELECTION("4"),
            CODE_CUSTOMIZED("3"),
            CODE_SCORE_VALUE("2");

            private final String code;

            private CODE(final String code) {
                this.code = code;
            }

            @Override
            public String toString() {
                return code;
            }

            public String toCode(){return this.code;}

            private static final Map<String, CODE> map = new HashMap<>();
            static {
                for (CODE en : values()) {
                    map.put(en.code, en);
                }
            }

            public static CODE valueFor(String name) {
                return map.get(name);
            }
        }

        private final String text;
        private QUESTION_TYPE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public String getCode(){
            if(this.text.equals(TEAM_SELECTION.toString())){return CODE.CODE_TEAM_SELECTION.toCode();}
            else if(this.text.equals(PLAYER_SELECTION.toString())){return CODE.CODE_PLAYER_SELECTION.toCode();}
            else if(this.text.equals(SCORE_VALUE.toString())){return CODE.CODE_SCORE_VALUE.toCode();}
            else if(this.text.equals(CUSTOMIZED.toString())){return CODE.CODE_CUSTOMIZED.toCode();}
            else {return CODE.CODE_NEW.toCode();}
        }

        public QUESTION_TYPE getQuestionType(String code){
            CODE codeEnum = CODE.CODE_NEW;
            try{
                codeEnum = CODE.valueFor(code);
            }catch (Exception ex){
                codeEnum = CODE.CODE_NEW;
            }
            if(codeEnum!=null){
                switch (codeEnum){
                    case CODE_TEAM_SELECTION:
                        return TEAM_SELECTION;
                    case CODE_SCORE_VALUE:
                        return SCORE_VALUE;
                    case CODE_PLAYER_SELECTION:
                        return PLAYER_SELECTION;
                    case CODE_CUSTOMIZED:
                        return CUSTOMIZED;
                    default :
                        return NEW;
                }
            }else{
                return NEW;
            }
        }
    }

    public enum PROCESSING {

        NEW(""),
        ACTIVE("ACTIVE"),
        INACTIVE("INACTIVE"),
        VERIFIED("INACTIVE"),
        PROFILE_COMPLETED("INACTIVE"),
        PENDING("PENDING"),
        ACCEPTED("ACCEPTED"),
        CANCELLED("CANCELLED"),
        DECLINED("DECLINED"),
        COMPLETED("COMPLETED"),
        ONGOING("ONGOING");

        private enum CODE {
            PROCESSING_NOTHING("0"),
            CODE_ACTIVE("1"),
            CODE_INACTIVE("2"),
            CODE_VERIFIED("3"),
            CODE_PROFILE_COMPLETED("4"),
            CODE_PENDING("201"),
            CODE_ACCEPTED("202"),
            CODE_CANCELLED("203"),
            CODE_DECLINED("204"),
            CODE_COMPLETED("205"),
            CODE_ONGOING("206");

            private final String code;

            private CODE(final String code) {
                this.code = code;
            }

            @Override
            public String toString() {
                return code;
            }

            public String toCode(){return this.code;}

            private static final Map<String, CODE> map = new HashMap<>();
            static {
                for (CODE en : values()) {
                    map.put(en.code, en);
                }
            }

            public static CODE valueFor(String name) {
                return map.get(name);
            }
        }

        private final String text;
        private PROCESSING(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public String getCode(){
            if(this.text.equals(ACTIVE.toString())){return CODE.CODE_ACTIVE.toCode();}
            else if(this.text.equals(INACTIVE.toString())){return CODE.CODE_INACTIVE.toCode();}
            else if(this.text.equals(PROFILE_COMPLETED.toString())){return CODE.CODE_PROFILE_COMPLETED.toCode();}
            else if(this.text.equals(VERIFIED.toString())){return CODE.CODE_VERIFIED.toCode();}
            else if(this.text.equals(PENDING.toString())){return CODE.CODE_PENDING.toCode();}
            else if(this.text.equals(ACCEPTED.toString())){return CODE.CODE_ACCEPTED.toCode();}
            else if(this.text.equals(CANCELLED.toString())){return CODE.CODE_CANCELLED.toCode();}
            else if(this.text.equals(DECLINED.toString())){return CODE.CODE_DECLINED.toCode();}
            else if(this.text.equals(COMPLETED.toString())){return CODE.CODE_COMPLETED.toCode();}
            else if(this.text.equals(ONGOING.toString())){return CODE.CODE_ONGOING.toCode();}
            else {return CODE.PROCESSING_NOTHING.toCode();}
        }

        public PROCESSING getProcessing(String code){
            CODE codeEnum = CODE.PROCESSING_NOTHING;
            try{
                codeEnum = CODE.valueFor(code);
            }catch (Exception ex){
                codeEnum = CODE.PROCESSING_NOTHING;
            }
            if(codeEnum!=null){
                switch (codeEnum){
                    case CODE_ACTIVE:
                        return ACTIVE;
                    case CODE_INACTIVE:
                        return INACTIVE;
                    case CODE_PROFILE_COMPLETED:
                        return PROFILE_COMPLETED;
                    case CODE_VERIFIED:
                        return VERIFIED;
                    case CODE_PENDING:
                        return PENDING;
                    case CODE_ACCEPTED:
                        return ACCEPTED;
                    case CODE_CANCELLED:
                        return CANCELLED;
                    case CODE_DECLINED:
                        return DECLINED;
                    case CODE_COMPLETED:
                    return COMPLETED;
                    case CODE_ONGOING:
                    return ONGOING;
                    default :
                        return NEW;
                }
            }else{
                return NEW;
            }
        }
    }

    public enum PROFILE_STATUS {

        INACTIVE("0"),
        ACTIVE("1"),
        NEED_VERIFICATION("2");

        private enum CODE {
            CODE_INACTIVE("0"),
            CODE_ACTIVE("1"),
            CODE_NEED_VERIFICATION("2");

            private final String code;

            private CODE(final String code) {
                this.code = code;
            }

            @Override
            public String toString() {
                return code;
            }

            public String toCode(){return this.code;}

            private static final Map<String, CODE> map = new HashMap<>();
            static {
                for (CODE en : values()) {
                    map.put(en.code, en);
                }
            }

            public static CODE valueFor(String name) {
                return map.get(name);
            }
        }

        private final String text;
        private PROFILE_STATUS(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public String getCode(){
            if(this.text.equals(ACTIVE.toString())){return CODE.CODE_ACTIVE.toCode();}
            else if(this.text.equals(NEED_VERIFICATION.toString())){return CODE.CODE_NEED_VERIFICATION.toCode();}
            else {return CODE.CODE_INACTIVE.toCode();}
        }

        public PROFILE_STATUS getProfileStatus(String code){
            CODE codeEnum = CODE.CODE_INACTIVE;
            try{
                codeEnum = CODE.valueFor(code);
            }catch (Exception ex){
                codeEnum = CODE.CODE_INACTIVE;
            }
            if(codeEnum!=null){
                switch (codeEnum){
                    case CODE_ACTIVE:
                        return ACTIVE;
                    case CODE_NEED_VERIFICATION:
                        return NEED_VERIFICATION;
                    default :
                        return INACTIVE;
                }
            }else{
                return INACTIVE;
            }
        }
    }

    public enum QUOTE {
        NEW(""),
        NOTHING("QUOTE_NOTHING"),
         INIT("QUOTE_INIT"),
        DECLINED("QUOTE_DECLINED"),
        WAITING("QUOTE_WAITING"),
        PROCESSING("QUOTE_PROCESSING"),
        COMPLETED("QUOTE_COMPLETED");

        private enum CODE {
            QUOTE_NOTHING("100"),
            QUOTE_INIT("101"),
            QUOTE_DECLINED("102"),
            QUOTE_WAITING("103"),
            QUOTE_PROCESSING("104"),
            QUOTE_COMPLETED("105");

            private final String code;

            private CODE(final String code) {
                this.code = code;
            }

            @Override
            public String toString() {
                return code;
            }

            public String toCode(){return this.code;}

            private static final Map<String, CODE> map = new HashMap<>();
            static {
                for (CODE en : values()) {
                    map.put(en.code, en);
                }
            }

            public static CODE valueFor(String name) {
                return map.get(name);
            }
        }

        private final String text;
        private QUOTE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public String getCode(){
            if(this.text.equals(INIT.toString())){return CODE.QUOTE_INIT.toCode();}
            else if(this.text.equals(DECLINED.toString())){return CODE.QUOTE_DECLINED.toCode();}
            else if(this.text.equals(WAITING.toString())){return CODE.QUOTE_WAITING.toCode();}
            else if(this.text.equals(PROCESSING.toString())){return CODE.QUOTE_PROCESSING.toCode();}
            else if(this.text.equals(COMPLETED.toString())){return CODE.QUOTE_COMPLETED.toCode();}
            else {return CODE.QUOTE_NOTHING.toCode();}
        }

        public QUOTE getQuote(String code){
            CODE codeEnum = CODE.QUOTE_NOTHING;
            try{
                codeEnum = CODE.valueFor(code);
            }catch (Exception ex){
                codeEnum = CODE.QUOTE_NOTHING;
            }
            if(codeEnum!=null){
                switch (codeEnum){
                    case QUOTE_INIT:
                        return INIT;
                    case QUOTE_DECLINED:
                        return DECLINED;
                    case QUOTE_WAITING:
                        return WAITING;
                    case QUOTE_PROCESSING:
                        return PROCESSING;
                    case QUOTE_COMPLETED:
                        return COMPLETED;
                    default :
                        return NOTHING;
                }
            }else{
                return NOTHING;
            }
        }
    }

    public enum PAYMENT_MODE {
        NEW(""),
        NOTHING("PAYMENT_MODE_NOTHING"),
        COD("PAYMENT_MODE_COD"),
        PAY_NOW("PAYMENT_MODE_PAY_NOW");

        private enum CODE {
            PAYMENT_MODE_NOTHING("600"),
            PAYMENT_MODE_COD("601"),
            PAYMENT_MODE_PAY_NOW("602");

            private final String code;

            private CODE(final String code) {
                this.code = code;
            }

            @Override
            public String toString() {
                return code;
            }

            public String toCode(){return this.code;}

            private static final Map<String, CODE> map = new HashMap<>();
            static {
                for (CODE en : values()) {
                    map.put(en.code, en);
                }
            }

            public static CODE valueFor(String name) {
                return map.get(name);
            }
        }

        private final String text;
        private PAYMENT_MODE(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public String getCode(){
            if(this.text.equals(COD.toString())){return CODE.PAYMENT_MODE_COD.toCode();}
            else if(this.text.equals(PAY_NOW.toString())){return CODE.PAYMENT_MODE_PAY_NOW.toCode();}
            else {return CODE.PAYMENT_MODE_NOTHING.toCode();}
        }

        public PAYMENT_MODE getPaymentMode(String code){
            CODE codeEnum = CODE.PAYMENT_MODE_NOTHING;
            try{
                codeEnum = CODE.valueFor(code);
            }catch (Exception ex){
                codeEnum = CODE.PAYMENT_MODE_NOTHING;
            }
            if(codeEnum!=null){
                switch (codeEnum){
                    case PAYMENT_MODE_PAY_NOW:
                        return PAY_NOW;
                    case PAYMENT_MODE_COD:
                        return COD;
                    default :
                        return NOTHING;
                }
            }else{
                return NOTHING;
            }
        }
    }

    public enum BID {
        NEW(""),
        NOTHING("BID_NOTHING"),
        INIT("BID_INIT"),
        ACCEPTED("BID_ACCEPTED"),
        DECLINED("BID_DECLINED"),
        WAITING("BID_WAITING"),
        PROCESSING("BID_PROCESSING"),
        COMPLETED("BID_COMPLETED");

        private enum CODE {
            BID_NOTHING("200"),
            BID_INIT("201"),
            BID_ACCEPTED("203"),
            BID_DECLINED("204"),
            BID_WAITING("205"),
            BID_PROCESSING("206"),
            BID_COMPLETED("202");

            private final String code;

            private CODE(final String code) {
                this.code = code;
            }

            @Override
            public String toString() {
                return code;
            }

            public String toCode(){return this.code;}

            private static final Map<String, CODE> map = new HashMap<>();
            static {
                for (BID.CODE en : values()) {
                    map.put(en.code, en);
                }
            }

            public static BID.CODE valueFor(String name) {
                return map.get(name);
            }
        }
        private final String text;

        private BID(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public String getCode(){
            if(this.text.equals(INIT.toString())){return CODE.BID_INIT.toCode();}
            else if(this.text.equals(ACCEPTED.toString())){return CODE.BID_ACCEPTED.toCode();}
            else if(this.text.equals(DECLINED.toString())){return CODE.BID_DECLINED.toCode();}
            else if(this.text.equals(WAITING.toString())){return CODE.BID_WAITING.toCode();}
            else if(this.text.equals(PROCESSING.toString())){return CODE.BID_PROCESSING.toCode();}
            else if(this.text.equals(COMPLETED.toString())){return CODE.BID_COMPLETED.toCode();}
            else {return CODE.BID_NOTHING.toCode();}
        }

        public BID getBID(String code){
            BID.CODE codeEnum = CODE.BID_NOTHING;
            try{
                codeEnum = BID.CODE.valueFor(code);
            }catch (Exception ex){
                codeEnum = CODE.BID_NOTHING;
            }
            if(codeEnum!=null){
                switch (codeEnum){
                    case BID_INIT:
                        return INIT;
                    case BID_ACCEPTED:
                        return ACCEPTED;
                    case BID_DECLINED:
                        return DECLINED;
                    case BID_WAITING:
                        return WAITING;
                    case BID_PROCESSING:
                        return PROCESSING;
                    case BID_COMPLETED:
                        return COMPLETED;
                    default :
                        return NOTHING;
                }
            }else{
                return NOTHING;
            }

        }
    }

    public enum CART {
        NOTHING("CART_NOTHING"),
        INIT("CART_INIT"),
        DECLINED("CART_DECLINED"),
        PROCESSING("CART_PROCESSING");

        private enum CODE {
            CART_NOTHING(300),
            CART_INIT(301),
            CART_DECLINED(303),
            CART_PROCESSING(302);

            private final int code;

            private CODE(final int code) {
                this.code = code;
            }

            @Override
            public String toString() {
                return code+"";
            }

            public int toCode(){return this.code;}
        }

        private final String text;

        private CART(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public int getCode(){
            if(this.text.equals(INIT.toString())){return CODE.CART_INIT.toCode();}
            else if(this.text.equals(DECLINED.toString())){return CODE.CART_DECLINED.toCode();}
            else if(this.text.equals(PROCESSING.toString())){return CODE.CART_PROCESSING.toCode();}
            else {return CODE.CART_NOTHING.toCode();}
        }

        public String getName(int code){
            CODE codeEnum = CODE.valueOf(code+"");
            switch (codeEnum){
                case CART_INIT:
                    return INIT.toString();
                case CART_DECLINED:
                    return DECLINED.toString();
                case CART_PROCESSING:
                    return PROCESSING.toString();
                default :
                    return NOTHING.toString();
            }
        }
    }

    public enum TRANS {
        NEW(""),
        NOTHING("TRANS_NOTHING"),
        INIT("TRANS_INIT"),
        ACCEPTED("TRANS_ACCEPT"),
        DECLINED("TRANS_DECLINED"),
        WAITING("TRANS_WAITING"),
        PROCESSING("TRANS_PROCESSING"),
        CANCELLED("TRANS_CANCELLED"),
        COMPLETED("TRANS_COMPLETED");

        private enum CODE {
            TRANS_NOTHING("400"),
            TRANS_INIT("401"),
            TRANS_ACCEPT("404"),
            TRANS_DECLINED("405"),
            TRANS_WAITING("403"),
            TRANS_PROCESSING("402"),
            TRANS_CANCELLED("407"),
            TRANS_COMPLETED("406");

            private final String code;

            private CODE(final String code) {
                this.code = code;
            }

            @Override
            public String toString() {
                return code+"";
            }

            public String toCode(){return this.code;}

            private static final Map<String, CODE> map = new HashMap<>();
            static {
                for (CODE en : values()) {
                    map.put(en.code, en);
                }
            }

            public static CODE valueFor(String name) {
                return map.get(name);
            }
        }

        private final String text;

        private TRANS(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public String getCode(){
            if(this.text.equals(INIT.toString())){return CODE.TRANS_INIT.toCode();}
            else if(this.text.equals(ACCEPTED.toString())){return CODE.TRANS_ACCEPT.toCode();}
            else if(this.text.equals(DECLINED.toString())){return CODE.TRANS_DECLINED.toCode();}
            else if(this.text.equals(WAITING.toString())){return CODE.TRANS_WAITING.toCode();}
            else if(this.text.equals(PROCESSING.toString())){return CODE.TRANS_PROCESSING.toCode();}
            else if(this.text.equals(COMPLETED.toString())){return CODE.TRANS_COMPLETED.toCode();}
            else if(this.text.equals(CANCELLED.toString())){return CODE.TRANS_CANCELLED.toCode();}
            else {return CODE.TRANS_NOTHING.toCode();}
        }

        public TRANS getTRANS(String code){
            CODE codeEnum = CODE.TRANS_NOTHING;
            try{
                codeEnum = CODE.valueFor(code);
            }catch (Exception ex){
                codeEnum = CODE.TRANS_NOTHING;
            }
            if(codeEnum!=null){
                switch (codeEnum){
                    case TRANS_INIT:
                        return INIT;
                    case TRANS_ACCEPT:
                        return ACCEPTED;
                    case TRANS_COMPLETED:
                        return COMPLETED;
                    case TRANS_DECLINED:
                        return DECLINED;
                    case TRANS_PROCESSING:
                        return PROCESSING;
                    case TRANS_WAITING:
                        return WAITING;
                    case TRANS_CANCELLED:
                        return CANCELLED;
                    default :
                        return NOTHING;
                }
            }else{
                return NOTHING;
            }

        }
    }

    public enum ORDER {
        NEW(""),
        NOTHING("ORDER_NOTHING"),
        INIT("ORDER_INIT"),
        COMPLETED("ORDER_COMPLETED"),
        DECLINED("ORDER_DECLINED"),
        REFUND("ORDER_REFUND"),
        PROCESSING("ORDER_PROCESSING"),
        DELIVERED("ORDER_DELIVERED"),
        SHIPPING("ORDER_SHIPPING");

        private enum CODE {
            ORDER_NOTHING("500"),
            ORDER_INIT("501"),
            ORDER_PROCESSING("502"),
            ORDER_COMPLETED("506"),
            ORDER_DECLINED("505"),
            ORDER_REFUND("507"),
            ORDER_SHIPPING("503"),
            ORDER_DELIVERED("504");

            private final String code;

            private CODE(final String code) {
                this.code = code;
            }

            @Override
            public String toString() {
                return code+"";
            }

            public String toCode(){return this.code;}

            private static final Map<String, CODE> map = new HashMap<>();
            static {
                for (CODE en : values()) {
                    map.put(en.code, en);
                }
            }

            public static CODE valueFor(String name) {
                return map.get(name);
            }
        }

        private final String text;

        private ORDER(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public String getCode(){
            if(this.text.equals(INIT.toString())){return CODE.ORDER_INIT.toCode();}
            else if(this.text.equals(DECLINED.toString())){return CODE.ORDER_DECLINED.toCode();}
            else if(this.text.equals(SHIPPING.toString())){return CODE.ORDER_SHIPPING.toCode();}
            else if(this.text.equals(DELIVERED.toString())){return CODE.ORDER_DELIVERED.toCode();}
            else if(this.text.equals(PROCESSING.toString())){return CODE.ORDER_PROCESSING.toCode();}
            else if(this.text.equals(COMPLETED.toString())){return CODE.ORDER_COMPLETED.toCode();}
            else if(this.text.equals(REFUND.toString())){return CODE.ORDER_REFUND.toCode();}
            else {return CODE.ORDER_NOTHING.toCode();}
        }

        public ORDER getORDER(String code){
            CODE codeEnum = CODE.ORDER_NOTHING;
            try{
                codeEnum = CODE.valueFor(code);
            }catch (Exception ex){
                codeEnum = CODE.ORDER_NOTHING;
            }
            if(codeEnum!=null){
                switch (codeEnum){
                    case ORDER_INIT:
                        return INIT;
                    case ORDER_SHIPPING:
                        return SHIPPING;
                    case ORDER_PROCESSING:
                        return PROCESSING;
                    case ORDER_DELIVERED:
                        return DELIVERED;
                    case ORDER_DECLINED:
                        return DECLINED;
                    case ORDER_COMPLETED:
                        return COMPLETED;
                    case ORDER_REFUND:
                        return REFUND;
                    default :
                        return NOTHING;
                }
            }else{
                return NOTHING;
            }

        }
    }

}
