package sa.quickfix.vendor.addon;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneValidator {

    private Pattern pattern;
    private Matcher matcher;

    private static final String PHONE_PATTERN = "^((?:[+?0?0?966]+)(?:\\s?\\d{2})(?:\\s?\\d{7}))$";

    public PhoneValidator(){
        pattern = Pattern.compile(PHONE_PATTERN);
    }

    public boolean validate(final String phone){

        matcher = pattern.matcher(phone);
        return matcher.matches();

    }
}