package sa.quickfix.vendor.addon;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Sivasabharish Chinnaswamy on 2/26/2016.
 */
public class FileDownloader extends AsyncTask<Void, String, File> {

    private static final String
            TAG                 = "FileDownloader";

    private static final int
            TIMEOUT_CONNECTION  = 120000,
            TIMEOUT_SOCKET      = 120000;

    String url = null;
    File downloadPath = null;
    Context context = null;
    Listener listener;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    public FileDownloader(Context context, String url, File downloadPath, Listener listener){
        this.context = context;
        this.url = url;
        this.downloadPath = downloadPath;
        this.listener = listener;
    }

    @Override
    protected File doInBackground(Void... params) {
        File file = DownloadDatabase(url, downloadPath);
        return file;
    }

    @Override
    protected void onPostExecute(File stream) {

        if(stream!=null){
            listener.onSuccess(url, stream);
        }else{
            listener.onError();
        }
        super.onPostExecute(stream);
    }

    @Override
    protected void onProgressUpdate(String... values) {
        listener.onProgress(Integer.parseInt(values[0]));
        super.onProgressUpdate(values);
    }

    public File DownloadDatabase(String DownloadUrl, File downloadPath) {
        try {
            File root = android.os.Environment.getExternalStorageDirectory();
            File dir = new File(root.getAbsolutePath() + "/myclock/databases");
            if (dir.exists() == false) {
                dir.mkdirs();
            }

            URL url = new URL("http://myexample.com/android/");

            long startTime = System.currentTimeMillis();

            URLConnection uconn = url.openConnection();
            uconn.setReadTimeout(TIMEOUT_CONNECTION);
            uconn.setConnectTimeout(TIMEOUT_SOCKET);

            InputStream is = uconn.getInputStream();
            BufferedInputStream bufferinstream = new BufferedInputStream(is);

            ByteArrayBuffer baf = new ByteArrayBuffer(5000);
            int current = 0;
            while ((current = bufferinstream.read()) != -1) {
                baf.append((byte) current);
            }

            FileOutputStream fos = new FileOutputStream(downloadPath);
            fos.write(baf.toByteArray());
            fos.flush();
            fos.close();
            Log.d("DownloadManager", "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + "sec");
            return downloadPath;
        } catch(IOException e) {
                Log.d("DownloadManager" , "Error:" + e);
            }

            return null;
        }

    public interface Listener {
        void onSuccess(String url, File file);
        void onProgress(int value);
        void onError();
    }
}
