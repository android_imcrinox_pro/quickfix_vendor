package sa.quickfix.vendor.addon;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Calendar;

public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public static final int DAY = 1, MONTH = 2, YEAR = 3;

    DatePickerInterface listener = null;
    Calendar defaultDate = null;
    Boolean disablePreviousDate = false;

    boolean
            isVisibleFields = false,
            showMonth = true,
            showDay = true,
            showYear = true ;

    public SelectDateFragment(){
    }

    public void visibleFields (int fields[]){
        isVisibleFields = true;
        showMonth = false;
        showYear = false;
        showDay = false;
        for (int field : fields){
            switch (field){
                case MONTH : showMonth = true;break;
                case DAY : showDay = true;break;
                case YEAR : showYear = true;break;
                default :
                    showMonth = true;
                    showDay = true;
                    showYear = true;
                    break;
            }
        }
    }

    public DatePickerInterface getListener() {
        return listener;
    }

    public void setListener(DatePickerInterface listener) {
        this.listener = listener;
    }

    public Calendar getDefaultDate() {
        return defaultDate;
    }

    public void setDefaultDate(Calendar defaultDate) {
        this.defaultDate = defaultDate;
    }

    public Boolean getDisablePreviousDate() {
        return disablePreviousDate;
    }

    public void setDisablePreviousDate(Boolean disablePreviousDate) {
        this.disablePreviousDate = disablePreviousDate;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        //setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(defaultDate == null){
            defaultDate = Calendar.getInstance();
        }
        int yy = defaultDate.get(Calendar.YEAR);
        int mm = defaultDate.get(Calendar.MONTH);
        int dd = defaultDate.get(Calendar.DAY_OF_MONTH);
        //getActivity().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, yy, mm, dd);
        try {
            if(isVisibleFields) {
               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    int daySpinnerId = Resources.getSystem().getIdentifier("day", "id", "android");
                    if (daySpinnerId != 0 )
                    {
                        View daySpinner = dialog.findViewById(daySpinnerId);
                        if (daySpinner != null)
                        {
                            daySpinner.setVisibility(showDay?View.VISIBLE:View.GONE);
                        }
                    }

                    int monthSpinnerId = Resources.getSystem().getIdentifier("month", "id", "android");
                    if (monthSpinnerId != 0)
                    {
                        View monthSpinner = dialog.findViewById(monthSpinnerId);
                        if (monthSpinner != null)
                        {
                            monthSpinner.setVisibility(showMonth?View.VISIBLE:View.GONE);
                        }
                    }

                    int yearSpinnerId = Resources.getSystem().getIdentifier("year", "id", "android");
                    if (yearSpinnerId != 0)
                    {
                        View yearSpinner = dialog.findViewById(yearSpinnerId);
                        if (yearSpinner != null)
                        {
                            yearSpinner.setVisibility(showYear?View.VISIBLE:View.GONE);
                        }
                    }
                } else { //Older SDK versions
                    Field innerFields[] = dialog.getClass().getDeclaredFields();
                    for (Field field : innerFields) {
                        if (field.getName().equals("mDayPicker") || field.getName().equals("mDaySpinner")) {
                            field.setAccessible(true);
                            Object dayPicker = null;
                            try {
                                dayPicker = field.get(dialog);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            ((View) dayPicker).setVisibility(showDay?View.VISIBLE:View.GONE);
                        }

                        if (field.getName().equals("mMonthPicker") || field.getName().equals("mMonthSpinner")) {
                            field.setAccessible(true);
                            Object monthPicker = null;
                            try {
                                monthPicker = field.get(dialog);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            ((View) monthPicker).setVisibility(showMonth?View.VISIBLE:View.GONE);
                        }

                        if (field.getName().equals("mYearPicker") || field.getName().equals("mYearSpinner")) {
                            field.setAccessible(true);
                            Object yearPicker = null;
                            try {
                                yearPicker = field.get(dialog);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            ((View) yearPicker).setVisibility(showYear?View.VISIBLE:View.GONE);
                        }
                    }
                }*/
                if(!showYear||!showMonth||!showDay){
                    if(showYear && !showDay && !showMonth) {
                        /*show Only Year*/
                        dialog.getDatePicker().setCalendarViewShown(false);
                        LinearLayout pickerParentLayout = (LinearLayout) dialog.getDatePicker().getChildAt(0);
                        LinearLayout pickerSpinnersHolder = (LinearLayout) pickerParentLayout.getChildAt(0);
                        pickerSpinnersHolder.getChildAt(1).setVisibility(View.GONE);
                        ArrayList<View> touchables = dialog.getDatePicker().getTouchables();//.get(0).performClick();
                        touchables.get(0).performClick();
                    }else if(showYear && !showDay && showMonth) {
                        /*Show Year and month*/
                        dialog.getDatePicker().setCalendarViewShown(false);
                        LinearLayout pickerParentLayout = (LinearLayout) dialog.getDatePicker().getChildAt(0);
                        LinearLayout topPicker = (LinearLayout) pickerParentLayout.getChildAt(0);
                        topPicker.getChildAt(1).setVisibility(View.GONE);
                        ViewGroup belowPicker = (ViewGroup) pickerParentLayout.getChildAt(1);
                        belowPicker.getChildAt(1).setVisibility(View.GONE);

                    }else if(!showYear && showDay && showMonth) {
                        /*Show Month and Date*/
                        dialog.getDatePicker().setCalendarViewShown(false);
                        LinearLayout pickerParentLayout = (LinearLayout) dialog.getDatePicker().getChildAt(0);
                        LinearLayout pickerSpinnersHolder = (LinearLayout) pickerParentLayout.getChildAt(0);
                        pickerSpinnersHolder.getChildAt(0).setVisibility(View.GONE);
                    }else if(!showYear && showDay && !showMonth) {
                        /*Show Date only*/
                        dialog.getDatePicker().setCalendarViewShown(false);
                        LinearLayout pickerParentLayout = (LinearLayout) dialog.getDatePicker().getChildAt(0);
                        LinearLayout pickerSpinnersHolder = (LinearLayout) pickerParentLayout.getChildAt(0);
                        pickerSpinnersHolder.getChildAt(0).setVisibility(View.GONE);
                        pickerSpinnersHolder.getChildAt(1).setVisibility(View.GONE);
                    }
                }
            }

            if(disablePreviousDate) {
                dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            }else{
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 1000);
            }

        } catch (Exception e) {
            Log.d("ERROR", e.getMessage());
        }
        return dialog;
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        populateSetDate(yy, mm, dd);
    }
    public void populateSetDate(int year, int month, int day) {
        defaultDate.set(Calendar.YEAR, year);
        defaultDate.set(Calendar.MONTH, month);
        defaultDate.set(Calendar.DAY_OF_MONTH, day);
        defaultDate.set(Calendar.HOUR_OF_DAY, 00);
        defaultDate.set(Calendar.MINUTE, 00);
        defaultDate.set(Calendar.SECOND,0);
        defaultDate.set(Calendar.MILLISECOND,0);
        listener.onDateSet(defaultDate);
    }

    public interface DatePickerInterface {
        void onDateSet(Calendar date);
    }

}