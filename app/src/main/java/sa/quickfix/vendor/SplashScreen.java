package sa.quickfix.vendor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import androidx.core.content.ContextCompat;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;


import sa.quickfix.vendor.flows.home.activities.MainActivity;
import sa.quickfix.vendor.flows.login.activities.LoginWithPhoneNumberActivity;
import sa.quickfix.vendor.global.GlobalFunctions;
import sa.quickfix.vendor.global.GlobalVariables;
import sa.quickfix.vendor.services.ServerResponseInterface;
import sa.quickfix.vendor.services.ServicesMethodsManager;
import sa.quickfix.vendor.services.model.NotificationModel;

import java.util.Locale;

public class SplashScreen extends Activity {

    public static String TAG = "SplashScreen";

    public static final String BUNDLE_MAIN_NOTIFICATION_MODEL = "BundleMainModelNotificationModel";
    Context context;
    Activity activity;
    private static int SPLASH_TIME_OUT = 2000;
    int count = 0;

    GlobalVariables globalVariables;
    GlobalFunctions globalFunctions;

    String
            countryCode = null,
            mobileNumber = null;

    View mainView;

    SharedPreferences shared_preference;
    private NotificationModel notificationModel = null;
    private boolean isFromAccountActivationPush = false;

    public static Intent newInstance(Context mainContext, NotificationModel notificationModel) {
        Intent intent = new Intent(mainContext, SplashScreen.class);
        intent.putExtra(BUNDLE_MAIN_NOTIFICATION_MODEL, notificationModel);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        context = this;
        activity = this;

        globalFunctions = AppController.getInstance().getGlobalFunctions();
        globalVariables = AppController.getInstance().getGlobalVariables();

        try {
            if (getIntent().hasExtra(BUNDLE_MAIN_NOTIFICATION_MODEL)) {
                notificationModel = (NotificationModel) getIntent().getSerializableExtra(BUNDLE_MAIN_NOTIFICATION_MODEL);
            } else {
                notificationModel = null;

                Intent i = getIntent();
                Bundle extras = i.getExtras();
                String
                        title = "",
                        message = "",
                        orderId = "",
                        pushId = "",
                        type = null;
                if (extras != null) {
                    title = extras.getString("title");
                    message = extras.getString("body");
                    type = extras.getString("type");
                    orderId = extras.getString("oid");
                    pushId = extras.getString("push_id");
                    if (type != null) {
                        notificationModel = new NotificationModel();
                        notificationModel.setTitle(title);
                        notificationModel.setMessage(message);
                        notificationModel.setType(type);
                        notificationModel.setoID(orderId);
                        notificationModel.setPushId(pushId);
                    }
                }
            }
        } catch (Exception exc) {
            notificationModel = null;
        }

        if (notificationModel != null && notificationModel.getType() != null) {
            if (notificationModel.getType().equalsIgnoreCase(GlobalVariables.ACCOUNT_ACTIVATED)) {
                //from account activation...
                isFromAccountActivationPush = true;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.app_fontBlackColor));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
        }

        shared_preference = PreferenceManager.getDefaultSharedPreferences(this
                .getApplicationContext());
        String mCustomerLanguage = shared_preference.getString(
                globalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE, "null");

        if (mCustomerLanguage.equalsIgnoreCase("null")) {

            SharedPreferences.Editor editor = shared_preference.edit();
            editor.putString(globalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE, "ar");
            editor.commit();
        }

        String mCurrentLocale = shared_preference.getString(globalVariables.SHARED_PREFERENCE_SELECTED_LANGUAGE,
                "null");
        Locale locale;
        if ((mCurrentLocale.equalsIgnoreCase("ar"))) {
            locale = new Locale("ar");
        }else if ((mCurrentLocale.equalsIgnoreCase("ur"))) {
            locale = new Locale("ur");
        } else {
            locale = new Locale("en");
        }
        //AppController.getInstance().myAppLanguage = mCurrentLocale;
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        new Handler().postDelayed(new Runnable() {
            /* * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company*/
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                goToMainActivity();
            }
        }, SPLASH_TIME_OUT);
    }

    private void goToMainActivity() {
        Intent intent = null;

        if (isFromAccountActivationPush) {
            //from account activation push...clear notification count and logout app locally...
            if (notificationModel != null && notificationModel.getPushId() != null) {
                //clear this notification...
               // clearNotification(context, notificationModel.getPushId());
            }
            intent = new Intent(SplashScreen.this, LoginWithPhoneNumberActivity.class);
            startActivity(intent);
            activity.finish();
        } else if (globalFunctions.isLoggedIn(context)) {
            if (notificationModel != null) {
                //clicked notification....
                intent = MainActivity.newInstance(SplashScreen.this, notificationModel);
            } else {
                intent = new Intent(SplashScreen.this, MainActivity.class);
            }
            startActivity(intent);
            activity.finish();
        } else {
            if (notificationModel != null) {
                //clicked notification....
                intent = MainActivity.newInstance(SplashScreen.this, notificationModel);
            } else {
                intent = new Intent(SplashScreen.this, LoginWithPhoneNumberActivity.class);
            }
            startActivity(intent);
            activity.finish();
        }
    }

    private void clearNotification(final Context context, String id) {
        ServicesMethodsManager servicesMethodsManager = new ServicesMethodsManager();
        servicesMethodsManager.clearNotification(context, id, new ServerResponseInterface() {
            @Override
            public void OnSuccessFromServer(Object arg0) {
                if (context != null) {
                    // Log.d(TAG, "Response: " + arg0.toString());
                }
            }

            @Override
            public void OnFailureFromServer(String msg) {
            }

            @Override
            public void OnError(String msg) {
            }
        }, "ClearNotification");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
